% quadratic_spline(x,y):
% given a cloud of points (x,y), determine the second degree
% smooth spline (that is, parabolic with same derivative at
% meeting points). The first derivative is fixed as the mean
% of the first and second slopes (from the first to the second
% point and from the second to the third).
%
% Right now the code is a bit hackish but works.
function [A, b, r] = quadratic_spline(x,y)
n=length(x)-1
b=zeros(3*n,1);
A=zeros(3*n);
% b(1) first derivative
A(1,[1 2]) = [2*x(2) 1];
b(1) = ((y(2)-y(1))/(x(2)-x(1))+(y(3)-y(2))/(x(3)-x(2)))/2;
for k=1:n-1
  A([3*(k-1)+2:3*(k-1)+4],[3*(k-1)+1:3*(k-1)+6]) = [
   x(k)^2 x(k) 1 0 0 0;
   x(k+1)^2 x(k+1) 1 0 0 0;
   2*x(k+1) 1 0 -2*x(k+1) -1 0;
   ];
  b(3*(k-1)+2) = y(k);
  b(3*(k-1)+3) = y(k+1);
  b(3*(k-1)+4) = 0;
end
b(3*n-1) = y(n);
b(3*n) = y(n+1);
A(3*n-1,[3*n-2:3*n]) = [x(n)^2 x(n) 1];
A(3*n,[3*n-2:3*n]) = [x(n+1)^2 x(n+1) 1]; 
r = A\b;
end

