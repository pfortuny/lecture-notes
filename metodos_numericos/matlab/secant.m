function [xn n] = secant(f, x0, x1, error = eps, MAX = 50)
  n = 0;
  xn1 = x0;
  xn = x1;
  % Se supone que f y fp son funciones  
  fn1 = f(xn1);
  fn = f(xn);
  while(abs(fn) >= error & n <= MAX)
    n = n + 1;  
    % siguiente punto
    t = xn;
    xn = xn - f(xn)*(xn-xn1)/(f(xn)-f(xn1)) % podria haber una excepcion
    xn1 = t;
    fn = f(xn);
  end
  if(n==MAX)
    warning('No converge en MAX iteraciones');
  end
end
