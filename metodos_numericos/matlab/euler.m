% Euler's Method for numerical integration of ODE
% The equation is of the form
% y' = f(x,y)

function [y] = euler(f, x, y0)
    h_v = diff(x);
    c   = 1;
    y   = zeros(1, length(x));
    y(c)= y0;
    for h = h_v
        y(c+1) = y(c) + h*f(x(c), y(c));
        c = c+1;
    end
end
