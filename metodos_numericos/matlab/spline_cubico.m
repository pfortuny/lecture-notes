% natural cubic spline: second derivative at both
% endpoints is 0. Input is a pair of lists describing
% the cloud of points.
function [f] = spline_cubico(x, y)
  % safety checks
  n = length(x)-1;
  if(n<=1 | length(x) ~= length(y))
    warning('Wrong data')
    f= [];
    return
  end

  % variables and coefficients for the linear system,
  % these are the ordinary names. Initialization
  a = y(1:n);
  h = diff(x);
  F = zeros(n);
  alpha = zeros(n, 1);

  % true coefficients (and independent terms) of the linear system
  for k=1:n
    if(k> 1& k < n)
      F(k,[k-1 k k+1]) = [h(k-1), 2*(h(k-1) + h(k)), h(k)] ;
      alpha(k) = 3*(y(k+1)-y(k))/h(k) - 3*(y(k) - y(k-1))/h(k-1);
    else
      % these two special cases are the 'natural' condition
      % (second derivatives at endpoints = 0)
      F(k,k) = 1;
      alpha(k) = 0;
    end
    k=k+1;
  end

  % These are the other coefficients of the polynomials
  % (a + bx + cx^2 + dx^3)... Initialization
  c = (F\alpha)';
  b = zeros(1,n);
  d = zeros(1,n);
  % unroll all the coefficients as in the theory
  k = 1;
  while(k<n)
    b(k) = (y(k+1)-y(k))/h(k) - h(k) *(c(k+1)+2*c(k))/3;
    k=k+1;
  end
  d(1:n-1) = diff(c)./(3*h(1:n-1));

  % the last b and d have explicit expressions:
  b(n) = b(n-1) + h(n-1)*(c(n)+c(n-1));
  d(n) = (y(n+1)-y(n)-b(n)*h(n)-c(n)*h(n)^2)/h(n)^3;

  % finally, build the piecewise polynomial (a Matlab function)
  % we might implement it by hand, though
  f = mkpp(x,[d; c; b ;a ]');
end
