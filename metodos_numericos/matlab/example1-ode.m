% example 1) ode: heun, modified euler & lsode...
% f(x,y) = y - 2*cos(y)*x;
% x0 = 0
% y0 = .1
% surprisingly, lsode & euler are similar, while
% the other two are quite different from 6.4 on
x=[0:.1:3*pi];
Y2=modified_euler(f, x, 0.1); Y3=heun(f, x, .1); 
clf;
plot(x, Y2); hold on; plot(x, Y3, "r-");
x=[0:.3:3*pi];
Y2=modified_euler(f, x, 0.1); Y3=heun(f, x, .1); 
clf;
plot(x, Y2); hold on; plot(x, Y3, "r-");
x=[0:.2:3*pi];
Y2=modified_euler(f, x, 0.1); Y3=heun(f, x, .1); 
clf;
plot(x, Y2); hold on; plot(x, Y3, "r-");
help ode
error: help: `ode' not found
ft=@(x,t) x -2*cos(x)*t;
Y4=lsode(ft, .1, x);
plot(x, Y4, "g-")
