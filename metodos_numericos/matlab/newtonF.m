% Implementacion del metodo de Newton-Raphson
function [xn n] = NewtonF(f, fp, x0, error = eps, MAX = 50)
  n = 0;
  xn = x0;
  % Se supone que f y fp son funciones  
  fn = f(xn);
  while(abs(fn) >= error & n <= MAX)
    n = n + 1;  
    % siguiente punto
    xn = xn - f(xn)/fp(xn) % podria haber una excepcion
  end
  if(n==MAX)
    warning('No converje en MAX iteraciones');
  end
end
