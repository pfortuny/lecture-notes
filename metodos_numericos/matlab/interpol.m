% interpol.m
% Linear interpolation.
% Given a cloud (x,y), and a Cell Array of functions F,
% return the coefficients of the least squares linear
% interpolation of (x,y) with the base F.
#
% Input:
% x: vector of scalars
% y: vector of scalars
% F: Cell array of anonymous functions
#
% Outuput:
% c: coefficients such that
% c(1)*F{1,1} + c(2)*F{1,2} + ... + c(n)*F{1,n}
% is the LSI function in the linear space <F>.

function [c] = interpol(x, y, F)
  n = length(F);
  m = length(x);
  X = zeros(n, m);
  for k=1:n
    X(k,:) = F{1,k}(x);
  end
  A = X*X.';
  b = X*y.';
  c = (A\b)';
end
