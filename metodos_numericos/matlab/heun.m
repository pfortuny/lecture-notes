% Heun's Method[ for numerical integration of ODE
% The equation is of the form
% y' = f(x,y)

function [y] = heun(f, x, y0)
    h_v = diff(x);
    c   = 1;
    y   = zeros(1, length(x));
    y(c)= y0;
    for h = h_v
        v1 = f(x(c), y(c));
        z  = y(c) + h*f(x(c), y(c));
        v2 = f(x(c+1), z);
        v  = (v1 + v2)/2;
        y(c+1) = y(c) + h*v;
        c = c+1;
    end
end
