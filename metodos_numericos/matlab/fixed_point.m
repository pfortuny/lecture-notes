% Fixed Point
function [xn n] = fixed_point(f, x0, error = eps, MAX = 50)
  n = 1;
  xn = x0;
  fn = f(xn);
  while(abs(fn-xn) >= error & n <= MAX)
    n = n + 1;  
    % siguiente punto
    xn = f(xn)
    fn = f(xn); % podria haber una excepcion
  end
  if(n==MAX)
    warning('No converje en MAX iteraciones');
  end
end
