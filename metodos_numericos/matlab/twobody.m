% two-body problem with Heun
% x is the initial value vector:
% (x1, y1, x2, y2, v1x, v1y, v2x, v2y)
function s = twobody (m1, m2, x, h, steps)
% use next line for plotting to pngs (prevents plotting on screen)
% set(0, 'defaultfigurevisible', 'on');
% hold on
s = [x(1) x(2) x(3) x(4)];
for k = 1:steps
d = ((x(3) - x(1))^2 + (x(4) - x(2))^2)^(1/2);
F1  = m2/d^3*[x(3)-x(1) x(4)-x(2)];
F2  = -m1/(d^3*m2)*F1;
w1  = [x(5:end) F1 F2];
z   = x + h*[x(5:end) F1 F2];
dd  = ((z(3) - z(1))^2 + (z(4) - z(2))^2)^(1/2);
FF1 = m2/dd^3*[z(3)-z(1) z(4)-z(2)];
FF2 = -m1/(dd^3*m2)*FF1;
w2  = [z(5:end) FF1 FF2];
v   = (w1 + w2)/2;
x   = x + h*v;
s   = [s; x(1:4)];
% next lines for filming, if desired
%plot(x1,y1,'r', x2,y2,'b');
%axis([-2 2 -2 2]);
%filename=sprintf('pngs/%05d.png',k);
%print(filename);
%clf;
end
endfunction
% interesting values:
% r=twobody(4,400,[-1,0,1,0,0,14,0,-0.1],.01,40000); (pretty singular)
% r=twobody(1,1,[-1,0,1,0,0,0.3,0,-0.5],.01,10000); (strange->loss of precision!)
% r=twobody(1,900,[-30,0,1,0,0,2.25,0,0],.01,10000); (like sun-earth)
% r=twobody(1, 333000, [149600,0,0,0,0,1,0,0], 100, 3650); (earth-sun)...
