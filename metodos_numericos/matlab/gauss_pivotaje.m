function [L, At, P, bt] = gauss_pivotaje(A,b)
  n = size(A);
  m = size(b);
  if(n(2) ~= m(1))
    warning('Dimensions of A and b do not match');
    return;
  end
  At=A;
  bt=b;
  L=eye(n);
  P=eye(n);
  i=1;
  while (i<n)
    j=i+1;
    % beware nomenclature:
    % L(j,i) is ROW j, COLUMN i
    % the pivot with greatest absolute value is sought
    p = abs(At(i,i));
    pos = i;
    for c=j:n
      u = abs(At(c,i));
      if(u>p)
        pos = c;
        p = u;
      end
    end
    if(u == 0)
      warning('Singular system');
      return;
    end
    % Swap rows i and p if i != p
    % in A and swap left part of  L
    % This is quite easy in Matlab, there is no need
    % for temporal storage
    P([i pos],:) = P([pos i], :);
    if(i ~= pos)
      At([i pos], :) = At([pos i], :);
      L([i pos], 1:i-1) = L([pos i], 1:i-1);
      b([i pos], :) = b([pos i], :);
    end
    while(j<=n)
      L(j,i)=At(j,i)/At(i,i);
      % Combining these rows is easy
      % They are 0 up to column i
      % And combining rows is easy as above
      At(j,i:n) = [0 At(j,i+1:n) - L(j,i)*At(i,i+1:n)];
      bt(j)=bt(j)-bt(i)*L(j,i);
      j=j+1;
    end 
    i=i+1;
  end 
end
