% Modfied Euler Method for numerical integration of ODE
% The equation is of the form
% y' = f(x,y)

function [y] = modified_euler(f, x, y0)
    h_v = diff(x);
    c   = 1;
    y   = zeros(1, length(x));
    y(c)= y0;
    for h = h_v
        z = y(c) + h/2*f(x(c), y(c));
        k = f(x(c)+h/2, z);
        y(c+1) = y(c) + h*k;
        c = c+1;
    end
end
