% Lagrange interpolation polynomial
% A single base polynomial is computed at
% each step and then added (multiplied by
% its coefficient) to the final result.
% input is a vector of x coordinates and
% a vector (of equal length) of y coordinates
% output is a polynomial in vector form (help poly).
function [l] = lagrange(x,y)
  n = length(x);
  l = 0;
  for m=1:n
    b = poly(x([1:m-1 m+1:n]));
    c = prod(x(m)-x([1:m-1 m+1:n]));
    l = l + y(m) * b/c;
  end
end
