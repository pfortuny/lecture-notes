clf
%x=linspace(-1,1,11);
%y=@(x) x.^6; % @(x) 1./(12*x.^2+1);   % 1./(1+12*x.^2);
u=linspace(x(1),x(length(x)),1000);
[A, b, r] = quadratic_spline(x,y(x));
n=length(x)
xn=[x x(n)]
%hold on;
a=[];
for k=1:length(r)/3
  uk = u(u>=xn(k)-0.01 & u<=xn(k+1)+0.01);
  a=[a;uk' (r(3*(k-1)+1)*uk.^2+r(3*(k-1)+2)*uk+r(3*(k-1)+3))']
  plot(uk, r(3*(k-1)+1)*uk.^2+r(3*(k-1)+2)*uk+r(3*(k-1)+3));
  hold on
end
%plot(u(u>=x(1)), spline(x,y(x),u(u>=x(1))), '*-g', "markersize", .2)
plot(x,y(x),'+r',"markersize",2);
%plot(u,y(u),'.y');
