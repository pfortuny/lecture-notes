% Two functions in a single .m file is not allowed
% in MatLab. However, we are mostly using Octave, which
% has no problem with this.

% Newton-Raphson step
function [x1] = stp(f, fp, x)
x1 = x - f(x)/fp(x)
end

% Newton-Raphson Algorithm
function [c, i] = newtonR(f, fp, x0, e=eps, N=50)
i = 0;
c = x0;
fi = f(x0);
while(abs(fi)>=eps & i<=N)
  c = c - f(c)/fp(c);
  i = i + 1;
  fi=f(c);
endwhile
if(i>N)
  warning('Not found');
  return
endif
end