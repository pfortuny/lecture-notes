% Second example of ODE. This has an exact solution. Beautiful
octave> f=@(x,y) 2*y-5*x.^2;
octave> Y=[euler(f,x,1); modified_euler(f, x, 1); heun(f, x, 1)];
octave> clf; plot(x, Y(1,:), "r", x, Y(2,:), "k", x, Y(3,:), "b")
octave> plot(x,.25*(10*x.^2+10*x-exp(2*x)+5), "y")
octave> clf; plot(x, Y(1,:), "r", x, Y(2,:), "k", x, Y(3,:), "b")
octave> hold on;plot(x,.25*(10*x.^2+10*x-exp(2*x)+5), "y")
