% Third example of ODE, simplest one.
x=[-2:.4:2];
xs=[-2:.01:2];
f=@(x,y) y;
Y=[euler(f,x,.1); modified_euler(f, x, .1); heun(f, x, .1)];
clf; plot(x, Y(1,:), "r", x, Y(2,:), "g", x, Y(3,:), "b")
hold on;
plot(xs,exp(xs)*.1/exp(-2), "k")

