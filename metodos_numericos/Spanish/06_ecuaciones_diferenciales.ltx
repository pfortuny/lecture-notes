% -*- TeX-master: "notas.ltx" -*-
\chapter{Ecuaciones diferenciales}

Sin duda, la actividad primordial para la que se utilizan los
m\'etodos num\'ericos es \emph{integrar ecuaciones diferenciales} (es
la denominaci\'on cl\'asica del proceso de resoluci\'on de una
ecuaci\'on diferencial).

\section{Introducci\'on}
Una ecuaci\'on diferencial es un tipo especial de ecuaci\'on
funcional: una ecuaci\'on (es decir, una igualdad en la que alguno de
los elementos es desconocido) en la que una de las inc\'ognitas es una
funci\'on. Ya se han resuelto ecuaciones diferenciales antes:
cualquier integral es la soluci\'on de una ecuaci\'on diferencial. Por
ejemplo,
\begin{equation*}
y^{\prime}=x
\end{equation*}
es una ecuaci\'on en la que se busca conocer una funci\'on $y(x)$ cuya
derivada es $x$. Como se sabe, la soluci\'on \emph{no es \'unica}: hay
una constante de integraci\'on, de manera que la soluci\'on general se
escribe
\begin{equation*}
y = \frac{x^2}{2} + C.
\end{equation*}

En realidad, la constante de integraci\'on tiene una manera m\'as
natural de entenderse. Cuando se calcula una primitiva, lo que se
est\'a buscando es una funci\'on cuya derivada es conocida. Una
funci\'on, al fin y al cabo, no es m\'as que una gr\'afica en el plano
$X,Y$. Esta gr\'afica puede estar situada m\'as arriba o m\'as abajo
(la derivada no cambia por  dibujar una funci\'on paralelamente arriba
o abajo). Sin embargo, si lo que se busca es resolver el problema
\begin{equation*}
y^{\prime}=x, \,\,\, y(3) = 28,
\end{equation*}
entonces se est\'a buscando una funci\'on cuya derivada es $x$
\emph{con una condici\'on puntual}: que su valor en $3$ sea $28$. Una
vez que este valor est\'a fijado, \emph{solo hay una gr\'afica que
  tenga esa forma y pase por ese punto} (el $(3,28)$): a esto se le
llama una \emph{condici\'on inicial}: se busca una funci\'on cuya
derivada cumpla algo, pero \emph{imponiendo que pase por determinado
  punto}: de este modo la constante ya no aparece, se puede determinar
sin m\'as sustituyendo:
\begin{equation*}
28 = 3^2+C \,\,\,\Rightarrow\,\,\, C = 19.
\end{equation*}
Esta es la idea de \emph{condici\'on inicial} para una ecuaci\'on
diferencial. 

Consid\'erese la ecuaci\'on
\begin{equation*}
y^{\prime}=y
\end{equation*}
(cuya soluci\'on general deber\'ia ser obvia). Esta ecuaci\'on
significa: la funci\'on $y(x)$ cuya derivada es igual a ella
misma. Uno tiende siempre a pensar en $y(x)=e^x$, pero ?`es esta la
\'unica soluci\'on? Si el problema se piensa de otra manera,
geom\'etrica, la ecuaci\'on significa: la funci\'on $y(x)$ cuya
derivada es igual a la altura ($y$) en cada punto de la
gr\'afica. Pensada as\'i, est\'a claro que tiene que haber m\'as de
una funci\'on soluci\'on. De hecho, uno supone que por cada punto del
plano $(x,y)$ pasar\'a al menos una soluci\'on (pues geom\'etricamente
no hay ninguna raz\'on por la que los puntos $(x,e^x)$ sean
preferibles a otros para que pase por ellos una soluci\'on.) En efecto,
la soluci\'on general es
\begin{equation*}
y(x) = Ce^x,
\end{equation*}
donde $C$ es una \emph{constante de integraci\'on}. Ahora, si
especificamos una condici\'on inicial, que $y(x_0)=y_0$ para
$x_0,y_0\in {\mathbb R}$, entonces est\'a claro que
\begin{equation*}
y_0=Ce^{x_0}
\end{equation*}
y por tanto $C=y_0/e^{x_0}$, que est\'a siempre definido (pues el
denominador no es $0$.)

De este modo, dada una ecuaci\'on diferencial, hace falta especificar
al menos una \emph{condici\'on inicial} para que la soluci\'on est\'e
completamente determinada. Si se considera
\begin{equation*}
y^{\prime\prime} = -y,
\end{equation*}
no basta con especificar una sola condici\'on inicial. ?`Qu\'e
soluciones tiene esta ecuaci\'on? En breve, son las de la forma
$y(x)=a\sen(x) + b\cos(x)$, para dos constantes $a,b\in {\mathbb R}$. 
Para que la soluci\'on est\'e totalmente
determinada hay que fijar esas dos constantes. Esto, por ejemplo,
puede hacerse imponiendo (es lo com\'un) el valor en un punto,
$y(0)=1$ y el valor de la derivada en ese punto,
$y^{\prime}(0)=-1$. En cuyo caso, la soluci\'on es
$y(x)=-\sen(x)+\cos(x)$.

As\'i que este tema trata de la soluci\'on \emph{aproximada} de
ecuaciones diferenciales. De momento se estudiar\'an solo funciones de
una variable y ecuaciones en las que solo aparece la primera derivada,
pero esto puede variar en el futuro.

\section{Lo m\'as b\'asico}
Comencemos por el principio:
\begin{definition}
Una \emph{ecuaci\'on diferencial ordinaria} es una igualdad $a=b$ en
la que  aparece la derivada (o una derivada de orden
superior, pero no derivadas parciales) de una variable libre cuyo
rango es el conjunto de funciones de una variable real.
\end{definition}
Obviamente, as\'i no se entiende nada. Se puede decir que una
ecuaci\'on diferencial ordinaria es una ecuaci\'on en la que una de las
inc\'ognitas es una funci\'on de una variable $y(x)$, 
cuya derivada aparece expl\'icitamente
en dicha ecuaci\'on. Las \emph{no ordinarias} son las ecuaciones
\emph{en derivadas parciales}, que no se tratan en este texto, de momento.

\begin{example}
Arriba se han dado dos ejemplos, las ecuaciones diferenciales pueden
ser de muchos tipos:
\begin{equation*}
\begin{split}
y^{\prime}=\sin(x)\\
xy=y^{\prime}-1\\
(y^{\prime})^{2}-2y^{\prime\prime}+x^{2}y=0\\
\frac{y^{\prime}}{y}-xy=0
\end{split}
\end{equation*}
etc.
\end{example}

De ahora en adelante se supondr\'a que la ecuaci\'on diferencial de
partida solo tiene una variable libre, que es la de la funci\'on que
se busca, que se asume que es $y$. 

\begin{definition}
Se dice que una ecuaci\'on diferencial tiene orden $n$ si
$n$ es la mayor derivada de $y$ que aparece. Si $y$ y sus derivadas
solo aparen como sumas y productos, se llama \emph{grado} de la
ecuaci\'on a la mayor potencia de una derivada de $y$ que aparece.
\end{definition}

En este cap\'itulo estamos interesados exclusivamente en ecuaciones
diferenciales \emph{resueltas} (que no significa que ya est\'en
resueltas, sino que tienen una estructura concreta):
\begin{equation*}
y^{\prime}=f(x,y).
\end{equation*}

\begin{definition}
Un \emph{problema de condici\'on inicial} es una ecuaci\'on
diferencial junto con una condici\'on inicial de la forma
$y(x_0)=y_0$, donde $x_0,y_0\in {\mathbb R}$.
\end{definition}

\begin{definition}
La \emph{soluci\'on general} de una ecuaci\'on diferencial $E$ es una
familia de funciones $f(x,c)$ donde $c$ es una (o varias) constantes
tal que:
\begin{itemize}
\item Cualquier soluci\'on de $E$ tiene la forma $f(x,c)$ para alg\'un $c$.
\item Cualquier expresi\'on $f(x,c)$ es una soluci\'on de $E$,
\end{itemize}
salvo para quiz\'as un n\'umero finito de valores de $c$.
\end{definition}

Lo primero que hay que tener en cuenta es que, si ya el problema de
calcular una primitiva de una funci\'on es complejo, \emph{integrar
  una ecuaci\'on diferencial} es, por lo general, imposible. Es decir,
calcular la funci\'on que cumple una determinada ecuaci\'on
diferencial y su condici\'on inicial (o calcular la soluci\'on
general) es un problema \emph{que no se desea resolver en general}. Lo
que se desea es (sobre todo en ingenier\'ia), calcular una soluci\'on
aproximada y conocer una buena cota del error cometido al utilizar
dicha soluci\'on en lugar de la ``real''.

\section{Discretizaci\'on}
En todo este cap\'itulo, se supondr\'a que se tiene una funci\'on
$f(x,y)$ de dos variables, definida en una regi\'on $x\in[x_0, x_n]$,
$y\in [a,b]$, que cumple la siguiente condici\'on:
\begin{definition}
Se dice que $f(x,y)$ definida en un conjunto $X\in {\mathbb R}^2$
\emph{cumple la condici\'on de Lipschitz} si existe una constante
$K>0$ tal que
\begin{equation*}
\abs{f(x_{1})-f(x_2)}\leq K \abs{x_1-x_2}
\end{equation*}
para cualesquiera $x_1, x_2\in X$, donde $\abs{\,\,}$ denota el m\'odulo
de un vector.
\end{definition}
La condici\'on de Lipschitz es una forma de continuidad un poco
fuerte (es decir, es m\'as f\'acil ser continua que Lipschitz). Lo
importante de esta condici\'on es que asegura la existencia y unicidad
de soluci\'on de las ecuaciones diferenciales ``normales''. Sea $X$ un
conjunto de la forma $[x_0,x_n]\times [a,b]$ (una banda
vertical) y $f(x,y):X\rightarrow {\mathbb R}$ una funci\'on de
Lipschitz en $X$:
\begin{theorem}[de Cauchy-Kovalevsky]
Si $f$ cumple las condiciones anteriores, cualquier ecuaci\'on
diferencial $y^{\prime}=f(x,y)$ con una condici\'on inicial
$y(x_0)=y_0$ con $y_0\in(a,b)$ tiene \emph{una \'unica soluci\'on}
$y=y(x)$ definida
en $[x_0,x_0+t]$ para cierto $t\in {\mathbb R}$ mayor que $0$.
\end{theorem}

No es dif\'icil que una funci\'on cumpla la condici\'on de Lipschitz:
basta con que sea, por ejemplo, diferenciable con continuidad en todos
los puntos y tenga derivada acotada. De hecho, todos los polinomios y
todas las funciones derivables que se han utilizado en este curso
son de Lipschitz en
conjuntos de la forma $[x_0,x_n]\times [a,b]$, salvo que tengan una
discontinuidad o alg\'un punto en el que no sean derivables. Por
ejemplo, la funci\'on $f(x)=\sqrt{x}$ \emph{no es de Lipschitz} si el
intervalo $[x_0,x_n]$ contiene al $0$ (porque el crecimiento de $f$
cerca de $0$ es ``vertical'').

\begin{example}[Bourbaki ``Functions of a Real Variable'',
  Ch. 4, \S 1]
La ecuaci\'on diferencial $y^{\prime}=2\sqrt{\abs{y}}$ con la
condici\'on inicial $y(0)=0$ tiene infinitas soluciones. Por ejemplo,
cualquier funci\'on definida as\'i:
\begin{enumerate}
\item $y(t)=0$ para cualquier intervalo $(-b,a)$,
\item $y(t)=-(t+\beta)^2$ para $t\leq -b$,
\item $y(t)=(t-a)^2$ para $t\geq a$
\end{enumerate}
es una soluci\'on de dicha ecuaci\'on diferencial. Esto se debe a que
la funci\'on $\sqrt{\abs{y}}$ \emph{no es de Lipschitz} cerca de
$y=0$. (Compru\'ebense ambas afirmaciones: que dichas funciones
resuelven la ecuaci\'on diferencial y que la funci\'on
$\sqrt{\abs{y}}$ no es de Lipschitz).
\end{example}


Es decir, cualquier problema de condici\'on inicial ``normal'' tiene
una \'unica soluci\'on. Lo dif\'icil es calcularla con exactitud.

?`Y c\'omo puede calcularse de manera aproximada?

\subsection{La derivada como flecha}
Es costumbre pensar en la derivada de una funci\'on como \emph{la
  pendiente} de la gr\'afica de dicha funci\'on en el punto
correspondiente. Es m\'as \'util pensar que es la \emph{coordenada $Y$
del vector velocidad de la gr\'afica}.

Cuando se representa gr\'aficamente una funci\'on, lo que se debe
pensar es que se est\'a trazando una curva con velocidad horizontal
constante (el eje $OX$ tiene la misma escala en todas partes, ``se va
de izquierda a derecha con velocidad uniforme''). De esta manera, una
gr\'afica de una funci\'on $f(x)$ es una curva $(x,f(x))$. El vector
tangente a esta curva es (derivando respecto del par\'ametro, que es
$x$) el vector $(1,f^{\prime}(x))$: la derivada de $f$ indica la
direcci\'on vertical del vector tangente a la gr\'afica si la
velocidad horizontal es constante.

De esta manera, se puede entender una ecuaci\'on diferencial de la
forma $y^{\prime}=f(x,y)$ como una ecuaci\'on que dice ``se busca la
curva $(x,y(x))$ tal que el vector velocidad en cada punto es
$(1,f(x,y))$.'' As\'i que puede dibujarse el conjunto de vectores
``velocidad'' en el plano $(x,y)$, que est\'an dados por $(1,f(x,y))$
(la funci\'on $f$ es conocida) y se trata de dibujar una curva que
tenga como vector velocidad ese en cada punto y (la condici\'on
inicial) que pase por un punto concreto ($y(x_0)=y_0$). Como se ve en
la Figura \ref{fig:ecuadiff-example-1}, si se visualiza as\'i, es
sencillo hacerse una idea de c\'omo ser\'a la curva que se busca.

Si se tuvieran las flechas (los vectores $(1,f(x,y))$) dibujados en un
plano, trazar una curva tangente a ellos no es necesariamente muy
dif\'icil. De hecho, si lo que se quiere es una aproximaci\'on, la
idea m\'as intuitiva es ``sustituir la curva por segmentos que van en
la direcci\'on de las flechas''. Si el segmento se toma con longitud
muy peque\~na, uno piensa que se aproximar\'a a la soluci\'on.

\spanishdeactivate{<>}

\begin{figure}[h]
  \centering
  \begin{tikzpicture}
\begin{axis}[
enlargelimits=false,
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={},
ytick={},
xticklabels={},
yticklabels={},
restrict x to domain=-0.1:0.1,
restrict y to domain=-0.105:0.105
]
\input{../datafiles/vector.dat}
%\addplot[only marks, mark=*] coordinates {(0,0) (1,1)};
\end{axis}
\end{tikzpicture}
  \caption{Flechas que representan vectores de una ecuaci\'on
    diferencial del tipo $y^{\prime}=f(x,y)$. Obs\'ervese que todos
    los vectores tienen misma magnitud en horizontal.}
  \label{fig:ecuadiff-example-1}
\end{figure}

%\spanishactivate{<>}

Esta es precisamente la idea de Euler.

Ante un plano ``lleno de flechas'' que indican los vectores velocidad
en cada punto, la idea m\'as simple para trazar una curva que
\begin{itemize}
\item Pase por un punto especificado (condici\'on inicial $y(x_0)=y_0$)
\item Tenga el vector velocidad que indica $f(x,y)$ en cada punto
\end{itemize}

La idea m\'as sencilla es \emph{discretizar las coordenadas
  $x$}. Partiendo de $x_0$, se supone que el eje $OX$ est\'a
cuantizado en intervalos de anchura $h$ ---de momento una constante
fija--- ``lo suficientemente peque\~na''. Con esta hip\'otesis, en
lugar de trazar una \emph{curva suave}, se aproxima la soluci\'on
\emph{dando saltos de anchura $h$} en la variable $x$.

Como se exige que la soluci\'on pase por un punto concreto
$y(x_0)=y_0$, no hay m\'as que:
\begin{itemize}
\item Trazar el punto $(x_0, y_0)$ (la condici\'on inicial).
\item Computar $f(x_0,y_0)$, que es el valor \emph{vertical} del
  vector velocidad de la curva en cuesti\'on en el punto inicial.
\item Puesto que las coordenadas $x$ est\'an cuantizadas, el
  \emph{siguiente} punto de la curva tendr\'a coordenada $x$ igual a
  $x_0+h$.
\item Puesto que la velocidad en $(x_0,y_0)$ es $(1, f(x_0,y_0))$, la
  aproximaci\'on m\'as simple a \emph{cu\'anto se desplaza la curva en
  un intervalo de anchura $h$ en la $x$} es $(h, hf(x_0,y_0))$ (tomar
la $x$ como el tiempo y desplazarse justamente ese tiempo en l\'inea
recta). Ll\'amense $x_1=x_0+h$ e $y_1=y_0+hf(x_0,y_0)$.
\item Tr\'acese el segmento que une $(x_0,y_0)$ con $(x_1,y_1)$: este
  es el primer ``tramo aproximado'' de la curva buscada.
\item En este momento se tiene la misma situaci\'on que al principio
  pero con $(x_1, y_1)$ en lugar de $(x_0, y_0)$. Rep\'itase el
  proceso con $(x_1, y_1)$. 
\end{itemize}
Y as\'i hasta que se desee. Lo que se acaba de describir se conoce
como el \emph{algoritmo de Euler} para integrar num\'ericamente una
ecuaci\'on diferencial.



\section{Fuentes del error: truncamiento y redondeo}
Obviamente, la soluci\'on de una ecuaci\'on diferencial
$y^{\prime}=f(x,y)$ no ser\'a siempre (ni casi nunca) una curva
formada por segmentos rectil\'ineos: al aproximar la soluci\'on $y(x)$
por los segmentos dados por el m\'etodo de Euler, se est\'a cometiendo
un error que puede analizarse con la f\'ormula de Taylor. Si se supone
que $f(x,y)$ es suficientemente diferenciable, entonces $y(x)$
tambi\'en lo ser\'a y se tendr\'a que
\begin{equation*}
y(x_0+h)=y(x_0)+hy^{\prime}(x_0)+\frac{h^2}{2}y^{\prime\prime}(\theta)
\end{equation*}
para cierto $\theta\in[x_0,x_0+h]$. Como lo que se est\'a haciendo es
\emph{olvidarse del \'ultimo sumando}, el error que se produce es
exactamente ese sumando: un t\'ermino del tipo $O(h^2)$, de segundo
orden \emph{al hacer la primera iteraci\'on}. 
Este error, que aparece al aproximar una funci\'on por su
desarrollo limitado de Taylor de un grado finito, se denomina
\emph{error de truncamiento} (pues se est\'a truncando la expresi\'on
exacta de la funci\'on soluci\'on).

Por lo general, se supone siempre que el intervalo en que se integra
la ecuaci\'on diferencial es del orden de magnitud de $h^{-1}$. De hecho,
si el intervalo de c\'alculo es $[a,b]$, se fija un n\'umero de
intervalos $n$ y se toma $x_0=a$,
$x_n=b$ y $h=(b-a)/n$. Por tanto, realizar el c\'alculo desde $x_0=a$
hasta $x_n=b$ requiere $n=1/h$ iteraciones, de manera que el error de
truncamiento \emph{global} es del orden de $h^{-1}O(h^2)\simeq O(h)$:
tiene magnitud similar a la anchura intervalo. Aunque, como se ver\'a,
esto hay que entenderlo bien, pues por ejemplo $(b-a)^{10}h$ es del
orden de $h$, pero si $b\gg a$, entonces es mucho mayor que $h$. En
estos problemas en que se acumulan errores, es importante tener en
cuenta que la acumulaci\'on puede hacer aparecer constantes muy grandes.

Pero el truncamiento no es la \'unica parte de la que surgen errores. Como ya se
ha insistido, calcular en coma flotante (que es lo que se hace en este
curso y en todos los ordenadores, salvo raras excepciones) lleva
impl\'icito el \emph{redondeo} de todos los datos a una precisi\'on
espec\'ifica. En concreto, si se utiliza coma flotante de doble
precisi\'on como especifica el est\'andar IEEE-754, se considera que
el n\'umero m\'as peque\~no \emph{significativo} (el \emph{\'epsilon}
de dicho formato) es $\epsilon=2^{-52}\simeq 2.2204\times
10^{-16}$. Es decir, lo m\'as que se puede suponer es que el error de
redondeo es \emph{menor que $\epsilon$} en cada
operaci\'on. Si se supone que ``operaci\'on'' significa ``iteraci\'on
del algoritmo'', se tiene que cada paso de $x_i$ a $x_{i+1}$ genera un
error de magnitud $\epsilon$. Como hay que realizar $n=1/h$ pasos
desde $x_0$ hasta $x_n$, el \emph{error de redondeo global} es del
orden de $\epsilon/h$. Por tanto, como $\epsilon$ es un dato
\emph{fijo} (no se puede mejorar en las circunstancias de trabajo),
\emph{cuanto m\'as peque\~no sea el intervalo entre $x_i$ y $x_{i+1}$,
mayor ser\'a el error de redondeo}.

As\'i pues, la suma del error de truncamiento y el de redondeo es de
la forma (tomando infinit\'esimos equivalentes)
\begin{equation*}
E(h) \simeq \frac{\epsilon}{h} + h,
\end{equation*}
que crece cuando $h$ se hace grande y cuando $h$ se hace peque\~no. El
m\'inimo de $E(h)$ se alcanza (esta es una f\'acil cuenta) cuando
$h\simeq \sqrt{\epsilon}$: no tiene sentido utilizar intervalos de
anchura menor que $\sqrt{\epsilon}$ porque el error de redondeo se
magnificar\'a. Por tanto: \emph{tomar intervalos lo m\'as peque\~nos
  posibles puede conducir a errores de gran magnitud.}

En el caso de doble precisi\'on, la anchura m\'as peque\~na sensata es
del orden de $10^{-8}$: anchuras menores son in\'utiles, adem\'as de
la sobrecarga de operaciones que exigen al ordenador. Si en alg\'un
momento parece necesario elegir una anchura menor, lo m\'as probable
es que lo que se requiera es elegir un algoritmo mejor.

Esta consideraci\'on sobre el error de redondeo y de truncamiento es
independiente del m\'etodo: cualquier algoritmo discreto incurrir\'a
en un error de truncamiento inherente al algoritmo y en un error de
redondeo. Lo que se ha de hacer es conocer c\'omo se comporta cada
algoritmo en ambos casos.

\subsection{Anchura variable}
Hasta ahora se ha supuesto que el intervalo $[a,b]$ (o, lo que es lo
mismo, $[x_0,x_n]$) se divide en subintervalos de la misma anchura
$h$. Esto no es necesario en general. Pero simplifica la exposici\'on
lo suficiente como para que siempre supongamos que los intervalos son
de anchura constante, salvo que se indique expl\'icitamente lo
contrario (y esto es posible que no ocurra).

\section{Integraci\'on e integral}
Sup\'ongase que la ecuaci\'on diferencial $y^{\prime}=f(x,y)$ es tal
que la funci\'on $f$ depende solo de la variable $x$. En este caso, la
ecuaci\'on se escribir\'a
\begin{equation*}
y^{\prime}=f(x)
\end{equation*}
y significa \emph{$y$ es la funci\'on cuya derivada es $f(x)$,} es
decir, es el problema de calcular una primitiva. Este problema ya se
ha estudiado en su cap\'itulo, pero est\'a intr\'insecamente ligado a
casi todos los algoritmos de integraci\'on num\'erica y por ello es
relevante. 

Cuando la funci\'on $f(x,y)$ depende tambi\'en de $y$, la relaci\'on
es m\'as dif\'icil, pero tambi\'en se sabe (por el Teorema Fundamental
del C\'alculo) que, si $y(x)$ es la soluci\'on de la ecuaci\'on
$y^{\prime}=f(x,y)$ con condici\'on inicial $y(x_0)=y_0$, entonces, 
integrando ambos miembros, queda
\begin{equation*}
y(x) = \int_{x_{0}}^x f(t, y(t))\,dt + y_0,
\end{equation*}
que no es el c\'alculo de una primitiva \emph{pero se le parece
  bastante.} Lo que ocurre en este caso es que los puntos en los que
hay que evaluar $f(x,y)$ no se conocen, pues justamente dependen de la
gr\'afica de la socluci\'on (mientras que al calcular una primitiva,
el valor de $f(x,y)$ \emph{no depende de $y$}). Lo que se hace es
\emph{intentar acertar lo m\'as posible} con la informaci\'on local
que se tiene.

\section[El m\'etodo de Euler]{El m\'etodo de Euler: integral usando el extremo izquierdo}
Si se explicita el algoritmo para integrar una ecuaci\'on diferencial
utilizando el m\'etodo de Euler como en el Algoritmo
\ref{alg:euler-numerical-integration}, se observa que se aproxima cada
valor siguiente $y_{i+1}$ como el valor anterior al que se le suma el
valor de $f$ \emph{en el punto anterior} multiplicado por la anchura
del intervalo. Es decir, se est\'a aproximando
\begin{equation*}
\int_{x_i}^{x_{i+1}}f(t,y(t))\,dt \simeq (x_{i+1}-x_i)f(x_i,y_i) = h f(x_i,y_i).
\end{equation*}
Si la funci\'on $f(x,y)$ \emph{no dependiera de $y$}, se estar\'ia
haciendo la aproximaci\'on
\begin{equation*}
\int_a^bf(t)\,dt = (b-a)f(a),
\end{equation*}
que se puede denominar (por falta de otro nombre mejor) la \emph{regla
del extremo izquierdo}: se toma como \'area bajo $f(x)$ la anchura del
intervalo por el valor en el extremo izquierdo de este.

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} Una funci\'on $f(x,y)$, una condici\'on
inicial $(x_0,y_0)$, un intervalo $[x_0,x_n]$ y un paso $h=(x_n-x_0)/n$
\STATE \textbf{Output: } una colecci\'on de puntos $y_0,y_1, \dots,
y_n$ (que aproximan los valores de la soluci\'on de
$y^{\prime}=f(x,y)$ en la malla $x_0, \dots, x_n$)
\PART{Inicio}
\STATE $i\leftarrow 0$
\WHILE{$i\leq n$}
\STATE $y_{i+1}\leftarrow y_i+hf(x_i,y_i)$
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $(y_0,\dots, y_n)$
\end{algorithmic}
\caption{(Algortimo de Euler con aritm\'etica exacta)}
\label{alg:euler-numerical-integration}
\end{algorithm}

Podr\'ia intentarse (se deja de momento como ejercicio) plantearse el
problema ``y si se quisiera utilizar el extremo derecho del intervalo,
?`qu\'e habr\'ia que hacer?'': esta pregunta da lugar al m\'etodo
impl\'icito m\'as simple, que todav\'ia no estamos en condiciones de
explicar.

%Como se ha visto arriba, el m\'etodo de Euler tiene un error de
%$O(h)$, que, cuanto m\'as grande sea el intervalo, mayor se hace y
%puede f\'acilmente llegar a ser \emph{macrosc\'opico}.

\section{Euler modificado: regla del ``punto medio''}
En lugar de utilizar el extremo izquierdo del intervalo
$[x_i,x_{i+1}]$ para ``integrar'' y calcular el siguiente $y_{i+1}$,
ser\'ia preferible intentar utilizar la regla del punto medio como
primera idea. El problema es que con los datos que se tienen, no se
conoce cu\'al sea dicho punto medio (por la dependencia de $y$ que
tiene $f$). Aun as\'i, se puede tratar de aproximar de la siguiente
manera: 

\begin{itemize}
\item Se utilizar\'a un punto cercano a $P_i = (x_i,y_i)$ y cuya
  coordenada $x$ esté
  mitad de la distancia entre $x_i$ y $x_{i+1}$.
\item Como no se conoce nada mejor, se hace la primera aproximaci\'on
  como en el algoritmo de Euler y se toma como extremo derecho
  $Q_i = (x_{i+1}, y_i+hf(x_i,y_i))$.
\item Se calcula el punto medio del segmento $\overline{P_iQ_i}$, que
  es $(x_i+h/2, y_i+h/2f(x_i,y_i))$. Ll\'amese $k$ a su coordenada $y$.
\item Se utiliza el valor de $f$ en este  punto para calcular el
  siguiente $y_{i+1}$: $y_{i+1}=y_i+hf(x_i+h/2,k)$.
\end{itemize}

% Como se explic\'o en el Cap\'itulo \ref{cha:derivacion-e-integracion},
% es m\'as preciso aproximar la derivada en un punto por la diferencia
% sim\'etrica que por la diferencia incremental directa. Lo que hace el
% paso descrito arriba es tomar en $(x_i,y_i)$ como valor de la derivada
% el valor en el punto medio aproximado por la diferencia sim\'etrica
% (aunque no lo parezca). 
Si $f(x,y)$ no depende de $y$, es sencillo
comprobar que lo que se est\'a haciendo con los pasos descritos es la
aproximaci\'on
\begin{equation*}
\int_a^bf(x)\,dx \simeq (b-a)f(\frac{a+b}{2}),
\end{equation*}
que es la \emph{regla del punto medio} de integraci\'on
num\'erica. 

Este m\'etodo se denomina \emph{m\'etodo de Euler
  modificado} y tiene un \emph{orden} de precisi\'on superior al de
Euler; es decir, es de orden $2$, lo que significa que el error
acumulado en $x_n$ es $O(h^2)$. Se describe en detalle en el Algoritmo
\ref{alg:modified-euler}.


\begin{figure}[h!]
  \centering
  \begin{tikzpicture}
\begin{axis}[
%enlargelimits=false,
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={0, 0.5, 1},
ytick={0, 0.25, 0.3, 0.55, 0.6},
xticklabels={$x_i$, $x_i+\frac{h}{2}$, $x_{i+1}$},
yticklabels={$y_i$, \small{$y_{i+1}$}, \small{$z^2$},\small{$z_2+k^2$},\small{$y_i+k^1$}},
restrict x to domain=-0.1:1.6,
restrict y to domain=-0.1:0.7
]
%\input{../datafiles/vector.dat}
\addplot[only marks, mark=*] coordinates {(0,0) (0.5, 0.3)};
\addplot[blue, ->] coordinates {(0,0) (1,0.6)};
\addplot[red, ->] coordinates {(0.5, 0.3) (1.5, 0.55)};
\addplot[gray, loosely dashed] coordinates {(1.5, 0.55) (1, 0.25)}; 
\addplot[green, ->] coordinates {(0,0) (1, 0.25)};
\addplot[green, only marks, mark=o] coordinates {(1,0.25)};
\addplot[gray, dotted] coordinates {(0.5, -0.1) (0.5, 0.3) (-0.1, 0.3)};
\addplot[gray, dotted] coordinates {(1, -0.1) (1, 0.25) (-0.1, 0.25)};
%\addplot[only marks, black, mark=.] coordinates {(1.7, 0.7) };
\end{axis}
\end{tikzpicture}
  \caption{Ilustraci\'on de Algoritmo de Euler modificado. En lugar de
  sumar el vector $(h,hf(x_i,y_i))$ (azul), se suma en el punto
  $(x_i,y_i)$ el vector verde, que corresponde al vector tangente dado
  en el punto medio (en rojo).}
  \label{fig:modified-euler}
\end{figure}

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} Una funci\'on $f(x,y)$, una condici\'on
inicial $(x_0,y_0)$, un intervalo $[x_0,x_n]$ y un paso $h=(x_n-x_0)/n$
\STATE \textbf{Output: } una colecci\'on de puntos $y_0,y_1, \dots,
y_n$ (que aproximan los valores de la soluci\'on de
$y^{\prime}=f(x,y)$ en la malla $x_0, \dots, x_n$)
\PART{Inicio}
\STATE $i\leftarrow 0$
\WHILE{$i\leq n$}
\STATE $k^1 \leftarrow f(x_i,y_i)$
\STATE $z^2\leftarrow y_i+\frac{h}{2}k^1$
\STATE $k^2\leftarrow f(x_i+\frac{h}{2}, z^2)$
\STATE $y_{i+1}\leftarrow y_i+h k^2$
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $(y_0,\dots, y_n)$
\end{algorithmic}
\caption{(Algortimo de Euler Modificado con aritm\'etica exacta)}
\label{alg:modified-euler}
\end{algorithm}

%\spanishactivate{<>}

Como muestra la Figura \ref{fig:modified-euler}, el algoritmo de Euler
modificado consiste en utilizar, en lugar del vector tangente a la
curva descrito por $(1,f(x_i,y_i))$, ``llevar a $(x_i,y_i)$'' el
vector tangente en el punto medio del segmento que une $(x_i, y_i)$
con el punto que corresponder\'ia a utilizar el algoritmo de Euler.
Desde luego, se sigue cometiendo un error, pero al tomar la
informaci\'on \emph{un poco m\'as lejos}, se aproxima algo mejor el
comportamiento de la soluci\'on real.

La siguiente idea es utilizar una ``media'' de vectores. En concreto,
la media entre los dos vectores, el del ``inicio del paso'' y el del
``final del paso de Euler.''

\section{M\'etodo de Heun (regla del trapecio)}
En lugar de utilizar el punto medio del segmento que une el inicio y
el final del m\'etodo de Euler puede hacerse una operaci\'on parecida
con los vectores: utilizar la media entre el vector al inicio del
paso y el vector al final del paso de Euler. Este es el algoritmo de
Euler \emph{mejorado} \'o algoritmo de Heun. Se describe en detalle en
el Algoritmo \ref{alg:heun}.

Es decir, cada paso consiste en las siguientes operaciones:
\begin{itemize}
\item Se calcula el valor $k^1=f(x_i,y_i)$.
\item Se calcular el valor $z^2=y_j+hk^1$. Esta es la coordenada
  $y_{i+1}$ en el algoritmo de Euler.
\item Se calcula el valor $k^2=f(x_{i+1}, z^2)$. Esta ser\'ia la
  pendiente en el punto $(x_{i+1},y_{i+1})$
  en el algoritmo de Euler.
\item Se calcula la media de $k^1$ y $k^2$: $\frac{k^1+k^2}{2}$ y se
  usa este valor como ``paso'', es decir: $y_{i+1}=y_i+\frac{h}{2}(k^1+k^2)$.
\end{itemize}

El m\'etodo de Heun se describe gr\'aficamente en la Figura
\ref{fig:heun}. 

\begin{figure}[h]
  \centering
  \begin{tikzpicture}
\begin{axis}[
enlargelimits=false,
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={0,1,2},
ytick={0,0.1,0.25,0.4,0.5},
xticklabels={$x_i$, $x_{i+1}$, $x_{i+1}+h$},
yticklabels={$y_i$, \small{$y_{i}+hk^2$}, \small{$y_{i+1}$},\small{$y_i+hk_1$},},
restrict x to domain=-0.1:2.1,
restrict y to domain=-0.05:0.55
]
%\input{../datafiles/vector.dat}
\addplot[only marks, mark=*] coordinates {(0,0)};
\addplot[blue, ->] coordinates {(0,0) (1,0.4)};
\addplot[red, ->] coordinates {(1, 0.4) (2, 0.5)};
\addplot[gray, loosely dashed] coordinates {(2, 0.5) (1, 0.1)};
\addplot[red, ->] coordinates {(0,0) (1, 0.1)};
\addplot[green, ->] coordinates {(0,0) (1, 0.25)};
\addplot[gray, dotted] coordinates {(1, -0.05) (1, 0.4)};
\addplot[gray, dotted] coordinates {(1,0.1) (-0.1, 0.1)};
\addplot[gray, dotted] coordinates {(1,0.25) (-0.1, 0.25)};
\addplot[gray, dotted] coordinates {(1, 0.4) (-0.1, 0.4)}; 
\addplot[only marks, black, mark=.] coordinates {(2.1, 0.55) };
\addplot[only marks, mark=o, green] coordinates {(1, 0.25)};
\end{axis}
\end{tikzpicture}
  \caption{Ilustraci\'on de Algoritmo de Heun. En el punto $(x_i,y_i)$
  se a\~nade la media (en verde) de los vectores correspondientes al punto
  $(x_i,y_i)$ (azul) y al siguiente de Euler (en rojo).}
  \label{fig:heun}
\end{figure}



\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} Una funci\'on $f(x,y)$, una condici\'on
inicial $(x_0,y_0)$, un intervalo $[x_0,x_n]$ y un paso $h=(x_n-x_0)/n$
\STATE \textbf{Output: } una colecci\'on de puntos $y_0,y_1, \dots,
y_n$ (que aproximan los valores de la soluci\'on de
$y^{\prime}=f(x,y)$ en la malla $x_0, \dots, x_n$)
\PART{Inicio}
\STATE $i\leftarrow 0$
\WHILE{$i\leq n$}
\STATE $k^1 \leftarrow f(x_i,y_i)$
\STATE $z^2\leftarrow y_i+hk^1$
\STATE $k^2\leftarrow f(x_{i}+h, z^2)$
\STATE $y_{i+1}\leftarrow y_i+\frac{h}{2} (k^1+k^2)$
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $(y_0,\dots, y_n)$
\end{algorithmic}
\caption{(Algortimo de Heun con aritm\'etica exacta)}
\label{alg:heun}
\end{algorithm}

%\section{Runge-Kutta: regla de Simpson}




