% -*- TeX-master: "notas.ltx" -*-
\chapter{Soluci\'on aproximada de sistemas lineales}
Se estudia en este cap\'itulo una colecci\'on de algoritmos cl\'asicos
para resolver de manera aproximada sistemas de ecuaciones lineales. Se
comienza con el algoritmo de Gauss (y su interpretaci\'on como
factorizaci\'on $LU$) y se discute brevemente su complejidad. Se
aprovecha esta secci\'on para introducir la noci\'on de \emph{n\'umero
de condici\'on de una matriz} y su relaci\'on con el error relativo
(de manera sencilla, sin contemplar posibles errores en la matriz). A
continuaci\'on se introducen los algoritmos de tipo \emph{punto fijo}
(se estudian espec\'ificamente dos de ellos, el de Jacobi y el de
Gauss-Seidel), y se compara su complejidad con la del algoritmo de
Gauss.

Aunque los algoritmos son interesantes de por s\'i (y su enunciado
preciso tambi\'en), es igual de importante conocer las condiciones de
utilidad de cada uno, de manera al menos te\'orica (por ejemplo, la
necesidad de que la matriz de un algoritmo de punto fijo sea
\emph{convergente}, noci\'on \emph{an\'aloga} en este contexto a la
contractividad). No se discutir\'an en profundidad aspectos
relacionados con el espectro de una aplicaci\'on lineal, aunque es la
manera m\'as simple de comprender la necesidad de la condici\'on de
convergencia. 

En todo este tema, se supone que ha de resolverse un sistema de
ecuaciones lineales de la forma
\begin{equation}
\label{eq:sistema-original-lineal}
Ax=b
\end{equation}
donde $A$ es una matriz cuadrada de orden $n$ y $b$ es un vector de
${\mathbb R}^n$. Se supone, \emph{siempre} que $A$ no es singular (es
decir, que el sistema es \emph{compatible determinado}).

\section{El algoritmo de Gauss y la factorizac\'on $LU$}
Partiendo de la Ecuaci\'on (\ref{eq:sistema-original-lineal}), el
algoritmo de Gauss consiste en transformar $A$ y $b$ mediante operaciones
``sencillas'' en una matriz $\tilde{A}$ triangular superior y un
vector $\tilde{b}$, para que
el nuevo sistema $\tilde{A}x=\tilde{b}$ sea directamente soluble mediante
\emph{sustituci\'on regresiva} (es decir, se puede calcular la
variable $x_n$ despejando directamente y, sustituyendo ``hacia
arriba'' en la ecuaci\'on $n-1$, se puede calcular $x_{n-1}$, etc.)
Es obvio que se requiere que la soluci\'on del sistema nuevo sea la
misma que la del original. Para ello, se permiten realizar la
siguiente operaci\'on:
\begin{itemize}
\item Se puede sustituir una ecuaci\'on $E_{i}$ (la fila $i-$\'esima de
  $A$) por una combinaci\'on lineal de la forma $E_i+\lambda E_k$
  donde $k<i$ y $\lambda\in {\mathbb R}$. Si se hace esto, tambi\'en
  se sustituye $b_i$ por $b_i+\lambda b_k$.
\end{itemize}
El hecho de que la combinaci\'on tenga coeficiente $1$ en $E_i$ es lo
que obliga a que las soluciones del sistema modificado sean las mismas
que las del original.

\begin{lemma}
\label{lem:combinacion-de-gauss-matriz-tonta}
Para transformar una matriz $A$ en una matriz $\tilde{A}$ seg\'un la
operaci\'on anterior, basta multiplicar $A$ por la izquierda por la
matriz $L_{ik}(\lambda)$ cuyos elementos son:
\begin{itemize}
\item Si $m=n$, entonces $(L_{ik}(\lambda))_{mn}=1$ (diagonal de $1$).
\item Si $m=i,n=k$, entonces $(L_{ik}(\lambda))_{mn}=\lambda$ (el
  elemento $(i,k)$ es $\lambda$).
\item Cualquier otro elemento es $0$.
\end{itemize}
\end{lemma}

\begin{example}
Si se parte de la matriz $A$
\begin{equation*}
A=
\begin{pmatrix}
3 & 2 & -1 & 4\\
0 & 1 & 4 & 2 \\
6 & -1 & 2 & 5\\
1 & 4 & 3 & -2
\end{pmatrix}
\end{equation*}
y se combina la fila $3$ con la fila $1$ por $-2$ (para ``hacer un
cero'' en el $6$), entonces se ha de multiplicar por $L_{31}(-2)$
\begin{equation*}
\begin{pmatrix}
1 & 0 & 0 & 0\\
0 & 1 & 0 & 0\\
-2 & 0 & 1 & 0\\
0 & 0 & 0 & 1
\end{pmatrix}
\begin{pmatrix}
3 & 2 & -1 & 4\\
0 & 1 & 4 & 2 \\
6 & -1 & 2 & 5\\
1 & 4 & 3 & -2
\end{pmatrix}
=
\begin{pmatrix}
  3 & 2 & -1 &  4\\
  0 & 1 & 4  &  2\\
  0 &-5 & 4  & -3\\
  1 & 4 & 3  & -2\\
\end{pmatrix}.
\end{equation*}
\end{example}

De manera simplificada, el algoritmo de Gauss puede enunciarse como
indica el Algoritmo \ref{alg:gauss-simplificado-sistemas-lineales}.
\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} Una matriz $A$ y un vector $b$, ambos de
orden $n$
\STATE \textbf{Output:} O bien un mensaje de error o bien una matriz
$\tilde{A}$ y un vector $\tilde{b}$ 
tales que $\tilde{A}$ es triangular superior y que el sistema
$\tilde{A}x=\tilde{b}$ tiene las mismas soluciones que el $Ax = b$
\PART{Inicio}
\STATE $\tilde{A}\leftarrow A, \tilde{b}\leftarrow b, i\leftarrow 1$
\WHILE{$i<n$}
\IF{$\tilde{A}_{ii}=0$}
\STATE \textbf{return} ERROR \comm{divisi\'on por cero}
\ENDIF
\STATE \comm{combinar cada fila bajo la $i$ con la $i$}
\STATE{$j\leftarrow i+1$}
\WHILE{$j<n$}
\STATE $m_{ji}\leftarrow \tilde{A}_{ji}/ \tilde{A}_{ii}$
\STATE \comm{La siguiente linea es un bucle, cuidado}
\STATE $\tilde{A}_j \leftarrow \tilde{A}_{j} - m_{ij} \tilde{A}_i$
\,\,\,\comm{*}
\STATE $\tilde{b}_j\leftarrow \tilde{b}_j-m_{ij} \tilde{b}_{i}$
\STATE $j\leftarrow j+1$
\ENDWHILE
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $\tilde{A}, \tilde{b}$
\end{algorithmic}
\caption{(Algoritmo de Gauss para sistemas lineales)}
\label{alg:gauss-simplificado-sistemas-lineales}
\end{algorithm}

La l\'inea marcada con el asterisco en el Algoritmo
\ref{alg:gauss-simplificado-sistemas-lineales} es exactamente la
multiplicaci\'on de $\tilde{A}$ por la izquierda por la matriz
$L_{ji}(-m_{ji})$. As\'i que al final, la matriz $\tilde{A}$,
triangular superior, es un
producto de esas matrices:
\begin{equation}
\label{eq:gauss-como-producto}
\small{\tilde{A} =
L_{n,n-1}(-m_{n,n-1})L_{n,n-2}(-m_{n,n-2})\cdots
L_{2,1}(-m_{2,1}) A} = \tilde{L}A
\end{equation}
donde $\tilde{L}$ es una matriz triangular inferior con $1$ en la
diagonal (esto es un sencillo ejercicio). Resulta tambi\'en sencillo
comprobar que (y esto realmente parece \emph{magia}) que\margin{comprobar subindices\dots}
\begin{lemma*}
  La matriz inversa del producto de todas las matrices de
  (\ref{eq:gauss-como-producto}) es la matriz triangular inferior que
  en cada componente $(j,i)$ contiene el valor $m_{ji}$.
\end{lemma*}

As\'i que, sin m\'as complicaci\'on, se ha demostrado el siguiente
resultado:
\begin{theorem}
\label{the:lu-factorization}
Si en el proceso de reducci\'on de Gauss no hay ning\'un elemento de
la diagonal igual a $0$, entonces existe una matriz $L$ triangular
inferior cuyos elementos son los sucesivos multiplicadores en su
posici\'on correspondiente y una matriz diagonal superior $U$
tal que
\begin{equation*}
A = LU
\end{equation*}
y tal que el sistema $Ux = \tilde{b} = L^{-1}b$ es equivalente al
sistema incial $Ax=b$.
\end{theorem}
Con este resultado, se obtiene una factorizaci\'on de $A$ que
simplifica la resoluci\'on del sistema original, pues se puede
reescribir $Ax=b$ como $LUx = b$; yendo por partes, se hace:
\begin{itemize}
\item Primero se resuelve el sistema $Ly=b$, por \emph{sustituci\'on
    directa} ---es decir, de arriba abajo, sin siquiera dividir.
\item Luego se resuelve el sistema $Ux = y$, por \emph{sustituci\'on
    regresiva} ---es decir, de abajo arriba.
\end{itemize}
Esta manera de resolver solo requiere conservar en memoria las
matrices $L$ y $U$ y es muy r\'apida.

\subsection{Complejidad del algoritmo de Gauss}

Cl\'asicamente, la complejidad de los algoritmos que utilizan
operaciones aritm\'eticas se med\'ia calculando el n\'umero de
multiplicaciones necesarias (pues la multiplicaci\'on era una
operaci\'on mucho m\'as compleja que la adici\'on). Hoy d\'ia esta
medida es menos representativa, pues las multiplicaciones en coma
flotante se realizan en un tiempo pr\'acticamente equivalente al de
las adiciones (con ciertos matices, pero este es uno de los avances
importantes en velocidad de procesamiento).

De esta manera, si \emph{se supone que las divisiones pueden hacerse
  con exactitud}\footnote{Lo cual es, como ya se ha dicho muchas
  veces, demasiado suponer.}, se tiene el siguiente resultado:
\begin{lemma}
Si en el proceso de simplificaci\'on de Gauss no aparece ning\'un cero
en la diagonal, tras una cantidad de a lo sumo $(n-1)+(n-2)+\dots+1$
combinaciones de filas, se obtiene un sistema $\tilde{A}x=\tilde{b}$
donde $\tilde{A}$ es triangular superior.
\end{lemma}

Cada combinaci\'on de filas en el paso $k$, tal como se hace el algoritmo requiere
una multiplicaci\'on por cada elemento de la fila (descontando el que
est\'a justo debajo del pivote, que se sabe que hay que sustituir por $0$), que en este paso
da $n-k$ productos, m\'as una divisi\'on (para calcular el
multiplicador de la fila) y otra multiplicaci\'on
para hacer la combinaci\'on del vector. Es decir, en el paso $k$ hacen
falta
\begin{equation*}
(n-k)^2 + (n-k ) + (n-k)
\end{equation*}
operaciones ``complejas'' (multiplicaciones, esencialmente) 
y hay que sumar desde $k=1$ hasta $k=n-1$, es decir, hay que sumar
\begin{equation*}
\sum_{i=1}^{n-1}i^2+2\sum_{i=1}^{n-1}i.
\end{equation*}
La suma de los primeros $r$ cuadrados es (cl\'asica) $r(r+1)(2r+1)/6$,
mientras que la suma de los primeros $r$ n\'umeros es
$r(r+1)/2$. As\'i pues, en total, se tienen
\begin{equation*}
\frac{(n-1)n(2(n-1)+1)}{6} + (n-1)n = \frac{n^3}{3}+\frac{n^2}{2}-\frac{5n}{6}.
\end{equation*}
Para ahora resolver el sistema triangular superior
$\tilde{A}x\tilde{b}$, hace falta una divisi\'on por cada l\'inea y
$k-1$ multiplicaciones en la fila $n-k$ (con $k$ desde $0$ hasta $n$),
as\'i que hay que sumar $n+1+\dots+(n-1)= \frac{n(n+1)}{2}$. Por tanto,
para resolver un sistema con el m\'etodo de Gauss, hacen falta en
total
\begin{equation*}
\frac{n^3}{3} + n^2- \frac{n}{3} \mbox{ operaciones para $Ax=b$.}
\end{equation*}

Si el algoritmo se utiliza para resolver $m$ sistemas de la forma
$Ax=b_i$ (para diferentes $b_i$), todas las operaciones son iguales
para triangular $A$ y simplemente hay que recomputar $\tilde{b}_i$ y
resolver, Esto requiere $(n-1)+\dots +1=n(n-1)/2$ multiplicaciones (las del
proceso de triangulaci\'on) y $1+2+\dots + n$ para
``despejar''. Descontando las que se hicieron en la resoluci\'on de
$b$, resulta que para resolver los $m$ sistemas, hacen falta:
\begin{equation}\label{eq:complexity-of-m-systems-gauss}
\frac{n^3}{3} + mn^{2}-\frac{n}{3} \mbox{ operaciones para $m$ sistemas.}
\end{equation}

\subsubsection{Comparaci\'on con utilizar $A^{-1}$}
Es sencillo comprobar que el c\'alculo general de la inversa de una
matriz requiere, utilizando el algoritmo de Gauss-Jordan 
(o por ejemplo, resolviendo los sitemas $Ax=e_i$ para la
base est\'andar) al menos $n^3$ operaciones. Hay mejores algoritmos
(pero se est\'an intentando comparar m\'etodos an\'alogos). Una vez
calculada la inversa, resolver un sistema $Ax=b$ consiste en
multiplicar $b$ a la izquierda por $A^{-1}$, que requiere (obviamente)
$n^2$ productos. Por tanto, la resoluci\'on de $m$ sistemas requiere
\begin{equation*}
n^3 +mn^2 \mbox{ operaciones complejas.}
\end{equation*}
Que siempre es m\'as grande que
(\ref{eq:complexity-of-m-systems-gauss}). As\'i que, si se utiliza el
m\'etodo de Gauss, es mejor conservar la factorizaci\'on $LU$ y
utilizarla para ``la sustituci\'on'' que calcular la inversa y
utilizarla para multiplicar por ella.

Claro est\'a que \emph{esta comparaci\'on es entre m\'etodos
  an\'alogos}: hay maneras de computar la inversa de una matriz en
menos (bastante menos) de $n^3$ operaciones (aunque siempre m\'as que
un orden de $n^2\log(n)$).

\subsection{Estrategias de Pivotaje, el algoritmo $LUP$}
Como ya se dijo, si en el proceso de reducci\'on gaussiana aparece un
pivote (el elemento que determina el multiplicador) con valor $0$ (o
incluso con denominador peque\~no), o bien puede no continuarse de
manera normal o bien puede que aparezcan errores muy grandes debido al
redondeo. Esto puede aliviarse utilizando estrategias de pivotaje:
cambiando el orden de las filas o de las columnas. Si solo se cambia
el orden de las filas, se dice que se realiza un \emph{pivotaje
  parcial}. Si se hacen ambas operaciones, se dice que se realiza un
\emph{pivotaje total}. En estas notas solo se estudiar\'a el pivotaje
parcial.

\begin{definition}
Una \emph{matriz de permutaci\'on} es una matriz cuadrada formada por ceros
salvo que en cada fila y en cada columna hay exactamente un $1$.
\end{definition}

(Para que una matriz sea de permutaci\'on basta con que se construya a
partir de la matriz identidad, \emph{permutando} filas).

Es obvio que el determinante de una matriz de permutaci\'on es
distinto de $0$ (de hecho, es bien $1$ bien $-1$). No es tan sencillo
comprobar que la inversa de una matriz de permutaci\'on $P$ es tambi\'en
una matriz de permutaci\'on y, de hecho, es $P^T$ (su traspuesta).

\begin{lemma}
Si $A$ es una matriz $n\times m$ y $P$ es una matriz cuadrada de orden
$n$, de permutaci\'on que solo tiene $2$ elementos fuera de la
diagonal no nulos, digamos los $(i,j)$ y $(j,i)$ (pues tiene que ser
sim\'etrica), entonces $PA$ es la matriz obtenida a partir de $A$
intercambiando las \emph{filas} $i$ y $j$. Para intercambiar por
columnas se ha de hacer a la derecha (pero no se explica aqu\'i).
\end{lemma}


\begin{definition}
Se dice que en el proceso de reducci\'on de Gauss se sigue una
estrategia de \emph{pivotaje parcial} si el pivote en el paso $i$ es siempre el
elemento de la columna $i$ de mayor valor absoluto.
\end{definition}

Para realizar el algoritmo de Gauss con pivotaje parcial, basta
realizarlo paso a paso salvo que, antes de fijar el pivote,
se ha de buscar, bajo la fila $i$ el elemento de mayor valor absoluto
de la columna $i$. Si este est\'a en la fila $j$ (y ser\'a siempre
$j\geq i$, al buscar por debajo de $i$), entonces se han de
intercambiar las filas $i$ y $j$ de la matriz.

La estrategia de pivotaje da lugar a una factorizaci\'on que no es
\emph{exactamente} la $LU$, sino con una matriz de permutaci\'on
a\~nadida: 

\begin{lemma}
Dado el sistema de ecuaciones $Ax=b$ con $A$ no singular, siempre
existe una matriz $P$ de permutaci\'on y dos matrices $L$, $U$, la
primera triangular inferior, la segunda triangular superior, con
\begin{equation*}
PA = LU.
\end{equation*}
\end{lemma}

La prueba de este resultado no es directa, hay que hacer un
razonamiento por recuerrencia (sencillo pero no inmediato).

Tanto Octave como Matlab incluyen la funci\'on \texttt{lu} que, dada
una matriz $A$, devuelve tres valores: $L$, $U$ y $P$.

Por ejemplo, si
\begin{equation*}
A =
\begin{pmatrix}
  1 & 2  &3  &4\\
  -1 & -2  &5  &6\\
  -1 & -2  &-3  &7\\
  0 & 12  &7  &8\\
\end{pmatrix}
\end{equation*}
entonces
\begin{equation*}
L=  \begin{pmatrix}
    1 & 0 & 0 & 0\\
    0 & 1 & 0 & 0\\
    -1 & 0 & 1 & 0\\
    -1 & 0 & 0 & 1\\
  \end{pmatrix}\,\,\,
U=
\begin{pmatrix}
1 & 2 & 3 & 4\\
0 & 12 & 7 & 8\\
0 & 0 & 8 & 10\\
0 & 0 & 0 & 11\\
\end{pmatrix},\,\,
P=
\begin{pmatrix}
 1 & 0 & 0 & 0\\
 0 & 0 & 0 & 1\\
 0 & 1 & 0 & 0\\
 0 & 0 & 1 & 0\\
\end{pmatrix}
\end{equation*}

Para calcular $L, U$ y $P$ no hay m\'as que realizar el algoritmo
ordinario de Gauss salvo que \emph{cuando se realice un intercambio
  entre la fila $i$ y la fila $j$} se ha de realizar el mismo en la
$L$ hasta entonces calculada (cuidado: solo en las primeras $i-1$
columnas, la zona de la diagonal con ``1'' no se ha de tocar) 
y multiplicar la $P$ ya calculada
(comenzando con $P=\rm{Id}_n$) por la izquierda por $P_{ij}$ (la matriz de
permutaci\'on de las filas $i$ y $j$).

En fin, puede expresarse el algoritmo $LUP$ como en el Algoritmo 
\ref{alg:lup}.

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} Una matriz $A$ de orden $n$
\STATE \textbf{Output:} O bien un mensaje de error o bien tres
matrices: $L$ triangular inferior con $1$ en la diagonal, $U$
triangular superior y $P$ matriz de permutaci\'on tales que $LU=PA$
\PART{Inicio}
\STATE $L\leftarrow\rm{Id}_n, U\leftarrow\rm{Id}_n,
P\leftarrow\rm{Id}_n$, $i\leftarrow 1$
\WHILE{$i<n$}
\STATE $p\leftarrow $ fila tal que $\abs{U_{pi}}$ es m\'aximo, con
$p\geq i$
\IF{$U_{pi}=0$}
\STATE \textbf{return} ERROR \comm{divisi\'on por cero}
\ENDIF
\STATE \comm{intercambiar filas $i$ y $p$}
\STATE $P\leftarrow P_{ip}P$
\STATE $U\leftarrow P_{ip} U$
\STATE \comm{en $L$ solo se intercambian las filas $i$ y $p$ de la
  submatriz $n\times (i-1)$ de la izquierda, ver texto}
\STATE $L\leftarrow \tilde{L}$
\STATE \comm{combinar filas en $U$ y llevar cuenta en $L$}
\STATE{$j\leftarrow i+1$}
\WHILE{$j<=n$}
\STATE $m_{ji}\leftarrow U_{ji}/ U_{ii}$
\STATE $U_j \leftarrow U_{j} - m_{ij} U_i$
\STATE $L_{ji} \leftarrow m_{ji}$
\STATE $j\leftarrow j+1$
\ENDWHILE
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $L,U,P$
  \end{algorithmic}
\caption{(Factorizaci\'on $LUP$ para una matriz $A$)}
\label{alg:lup}
\end{algorithm}


\subsection{El n\'umero de condici\'on: comportamiento del error relativo}
?`C\'omo es de ``estable'' la resoluci\'on de un sistema de ecuaciones
lineales? Una manera \emph{muy simple} de acercarse a este problema es
comparar los ``errores relativos'' en una soluci\'on si se cambia el
vector $b$ por uno \emph{modificado un poco}. Sup\'ongase que en lugar
del sistema (\ref{eq:sistema-original-lineal}) original, cuya
soluci\'on es $x$, se tiene uno
modificado
\begin{equation*}
Ay = b+\delta b
\end{equation*}
donde $\delta b$ es un \emph{vector peque\~no}. La soluci\'on ser\'a
de la forma $x+\delta x$, para cierto $\delta x$ (que uno
\emph{espera} que sea peque\~no).

La manera de medir tama\~nos de vectores es mediante \emph{una norma}
(la m\'as com\'un es la \emph{longitud}, en el espacio ecul\'ideo,
pero no es la que se utilizar\'a aqu\'i). Como $x$ es una
soluci\'on, se tiene que
\begin{equation*}
A(x+\delta x) = b+\delta b,
\end{equation*}
as\'i que
\begin{equation*}
A \delta x = \delta b,
\end{equation*}
pero como se parte de una modificaci\'on de $b$ y estudiar
c\'omo se modifica $x$, despejando y queda
\begin{equation*}
\delta x = A^{-1} \delta b
\end{equation*}
y midiendo \emph{tama\~nos} (es decir, \emph{normas}, que se denotan
$\| \|$), quedar\'ia
\begin{equation*}
\| \delta x \| = \| A^{-1}\delta b \|.
\end{equation*}
Se est\'a estudiando el \emph{desplazamiento relativo}, que es m\'as relevante que el
absoluto.  Ahora se ha incluir $\| x \|$ en el primer miembro. La
informaci\'on que se tiene es que $Ax = b$, de donde $\| Ax \| = \| b
\|$. As\'i que queda
\begin{equation*}
\frac{\| \delta x \|}{\| A x \| } = \frac{\| A^{-1} \delta b\|}{\| b \|},
\end{equation*}
pero esto no dice mucho (pues es una identidad obvia, se querr\'ia
acotar el \emph{desplazamiento} relativo de la soluci\'on si se
desplaza un poco el t\'ermino independiente).

Sup\'ongase que existe determinado objeto llamado \emph{norma de una
  matriz}, que se denotar\'ia $\| A \|$ que cumple que $\| A x \| \leq
\| A \| \| x \|$. Entonces el miembro de la izquierda la ecuaci\'on
anterior, le\'ida de derecha a izquierda quedar\'ia
\begin{equation*}
\frac{\| A^{-1} \delta b\|}{\| b \|}= \frac{\| \delta x \|}{\| A x \|
} \geq \frac{\| \delta x \|}{\|A\| \|x\|}
\end{equation*}
y, por otro lado, aplicando el mismo razonamiento a la parte ``de las
$b$'', se tendr\'ia que
\begin{equation*}
\frac{\| A^{-1}\| \|\delta b\|}{\| b \|} \geq \frac{\| A^{-1} \delta b\|}{\| b \|},
\end{equation*}
y combinando todo, 
\begin{equation*}
\frac{\| A^{-1}\| \|\delta b\|}{\| b \|} \geq \frac{\| \delta x \|}{\|A\| \|x\|}
\end{equation*}
de donde, finalmente, se obtiene una cota superior para el \emph{desplazamiento
  relativo de $x$}:
\begin{equation*}
\frac{\| \delta x \|}{\| x \|} \leq \| A \| \| A^{-1} \| \frac{\|
  \delta b \| }{\| b \|}
\end{equation*}
El caso es que tal objeto, llamado \emph{norma de una matriz (o m\'as
  bien de una aplicaci\'on lineal)} existe. De hecho, existen
muchos. En estas notas se va a utilizar el siguiente:
\begin{definition}
\label{def:norma-infinito}
Se denomina \emph{norma infinito} de una matriz cuadrada $A=(a_{ij})$ al
n\'umero
\begin{equation*}
\| A \|_{\infty} = \max_{1\leq i \leq n} \sum_{j=1}^n \abs{a_{ij}},
\end{equation*}
es decir, el m\'aximo de las sumas de los valores absolutos \emph{por
  filas}.
\end{definition}
En realidad, se podr\'ian utilizar muchas otras definiciones, pero
esta es la que se usar\'a en estas notas.
\begin{lemma}
La \emph{norma infinito} cumple que, para cualquier vector $x$, $\| Ax
\|_{\infty} \leq \|A\|_{\infty}\|x\|_{\infty}$, donde $\|x\|_{\infty}$
es la norma dada por el m\'aximo de los valores absolutos de las
coordenadas de $x$.
\end{lemma}
Es decir, si se toma como \emph{medida del tama\~no de un vector} el
m\'aximo de sus coordenadas en valor absoluto, y lo denominamos $\| x
\|_{\infty}$ se tiene que
\begin{equation*}
\frac{\| \delta x \|_{\infty}}{\| x \|_{\infty}} \leq \| A \|_{\infty}
\|A^{-1}\|_{\infty} \frac{\|\delta b \|_{\infty}}{\| b \|_{\infty}}.
\end{equation*}
Al producto $\| A \|_{\infty} \| A^{-1}\|_{\infty}$ se le denomina
\emph{n\'umero de condici\'on de la matriz $A$ para la norma
  infinito}, se denota $\kappa(A)$
y es una medida del
m\'aximo desplazamiento posible de la soluci\'on si se desplaza un poco el
vector. 
%De hecho, se tiene el siguiente resultado:
%\begin{lemma}
%Siempre hay un desplazamiento $\delta b$ para el que 
%\begin{equation*}
%\frac{\| \delta x \|_{\infty}}{\| x \|_{\infty}} = \kappa(A)
% \frac{\|\delta b \|_{\infty}}{\| b \|_{\infty}}.
%\end{equation*}
%Es decir, el n\'umero de condici\'on es una cota que se cumple alguna
%vez. 
%\end{lemma}
As\'i que, cuanto m\'as grande sea el n\'umero de condici\'on peor se
comportar\'a en principio el sistema respecto de peque\~nos cambios en el t\'ermino
independiente.

El n\'umero de condici\'on tambi\'en sirve para acotar
\emph{inferiormente} el error relativo cometido:

\begin{lemma}
Sea $A$ una matriz no singular de orden $n$ y $x$ una soluci\'on del
sistema $Ax=b$. Sea $\delta b$ un ``desplazamiento'' de las
condiciones iniciales y $\delta x$ el ``desplazamiento''
correspondiente en la soluci\'on. Entonces:
\begin{equation*}
\frac{1}{\kappa (A)} \frac{\| \delta b\|}{\| b \|}\leq \frac{\| \delta
x\|}{\| x \|} \leq \kappa (A) \frac{\| \delta b \|}{\| b \|}.
\end{equation*}
As\'i que se puede acotar el error relativo con el \emph{residuo
  relativo} (el n\'umero $\| \delta b\| / \| b \|$).

\end{lemma}


\begin{example}
Consid\'erese el sistema
\begin{equation*}
\begin{matrix}
0{.}853 x + 0{.}667 y & = & 0{.}169\\
0{.}333 x + 0{.}266 y & = & 0{.}067
\end{matrix}
\end{equation*}
Su n\'umero de condici\'on para la norma infinito es $376{.}59$, as\'i
que un cambio de una mil\'esima relativo en las condiciones iniciales (el
vector $b$) puede dar lugar a un cambio relativo de m\'as del $37\%$ en la
soluci\'on. La de dicho sistema es $x=0{.}055+, y=0{.}182+$. Pero el n\'umero de
condici\'on tan grande avisa de que una peque\~na perturbaci\'on
originar\'a grandes modificaciones en la soluci\'on. Si se pone, en
lugar de $b=(0{.}169, 0{.}067)$ el vector $b=(0{.}167, 0{.}067)$ (un
desplazamiento relativo del $1{.}1\%$  en la primera coordenada), la soluci\'on es
$x=-0{.}0557+, y=0{.}321+$. Por un lado, la $x$ no tiene siquiera el
mismo signo, por otro, el desplazamiento relativo en la $y$ es del $76\%$,
totalmente inaceptable. Imag\'inese que el problema describe un
sistema est\'atico de fuerzas y que las medidas se han realizado con
instrumentos de precisi\'on de $\pm 10^{-3}$. Para
$b=(0{.}168,0{.}067)$, la diferencia es menor (aunque aun inaceptable
en $y$), pero el cambio de signo
persiste\dots
\end{example}


\section{Algoritmos de punto fijo}
Tanto la complejidad computacional del algoritmo de Gauss como su
inestabilidad respecto a la divisi\'on por peque\~nos n\'umeros
(debida a la introducci\'on inevitable de errores de redondeo) llevan
a plantearse buscar otro tipo de algoritmos con mejores propiedades.
En algunas situaciones ser\'ia incluso ut\'opico plantearse resolver
un sistema en un cantidad similar a $n^3$ multiplicaciones (pongamos
por caso que $n\simeq 10^6$, entonces $n^3\simeq 10^{18}$ y har\'ia
falta demasiado tiempo para realizar todas las operaciones).

Si se parte de un sistema como el original
(\ref{eq:sistema-original-lineal}), de la forma $Ax=b$,
puede tratar de convertirse en un
problema de punto fijo mediante una \emph{separaci\'on} de la matriz
$A$ en dos, $A=N-P$, donde $N$ es invertible. De este modo, el sistema
$Ax=b$ queda $(N-P)x=b$, es decir
\begin{equation*}
(N-P)x = b \Rightarrow Nx = b + Px \Rightarrow x = N^{-1}b + N^{-1}P x,
\end{equation*}
si se llama $c = N^{-1}b$ y $M=N^{-1}P$, queda el siguiente problema
de punto fijo
\begin{equation*}
x = M x + c
\end{equation*}
que, si puede resolverse, puede hacerse mediante iteraciones de la
misma forma que en el Cap\'itulo \ref{chap:ecuaciones-no-lineales}: se
comienza con una semilla $x_0$ y se itera
\begin{equation*}
x_n = M x_{n-1} + c,
\end{equation*}
hasta alcanzar una precisi\'on suficiente. Hacen falta los siguientes
resultados.

\begin{theorem}
\label{the:condiciones-necesarias-convergencia-iteraciones-lineales}
Supongamos que $M$ es una matriz de orden $n$ y que $\| M
\|_{\infty}<1$. Entonces la ecuaci\'on $x=Mx+c$ tiene soluci\'on
\'unica para todo $c$ y la iteraci\'on $x_n = Mx_{n-1}+c$ converge a
ella para cualquier vector inicial $x_0$.
\end{theorem}

\begin{theorem}
\label{the:cota-para-punto-fijo-matricial}
Dada la matriz $M$ con $\| M\|_{\infty}<1$, y dado $x_0$ una semilla
para el m\'etodo iterativo del Teorema
\ref{the:condiciones-necesarias-convergencia-iteraciones-lineales}, si
$s$ es la soluci\'on del problema $x = Mx + c$, se
tiene la siguiente cota:
\begin{equation*}
\| x_n - s \|_{\infty} \leq \frac{\| M \|_{\infty}^n}{1- \| M
  \|_{\infty}}\| x_1 - x_0\|_{\infty}.
\end{equation*}
Recu\'erdese que, para vectores, la norma infinito $\| x \|_{\infty}$
viene dada por el mayor valor absoluto de las componentes de $x$.
\end{theorem}

Con estos resultados, se explican los dos m\'etodos b\'asicos
iterativos para resolver sistemas de ecuaciones lineales: el de
Jacobi, que corresponde a tomar $N$ como la diagonal de $A$ y el de
Gauss-Seidel, que corresponde a tomar $N$ como la parte triangular
inferior de $A$ incluyendo la diagonal.

\subsection{El algortimo de Jacobi}
Si en el sistema $Ax=b$ se ``despeja'' cada coordeanada $x_i$,
\emph{en funci\'on de las otras}, queda una expresi\'on as\'i:
\begin{equation*}
x_i = \frac{1}{a_{ii}} (b_i - a_{i1}x_1 - \dots - a_{ii-1}x_{i-1} -
a_{ii+1}x_{i+1} - \dots -a_{in}x_{n}),
\end{equation*}
en forma matricial, 
\begin{equation*}
x = D^{-1}(b - (A-D)x),
\end{equation*}
donde $D$ es la matriz diagonal cuyos elementos no nulos son
exactamente los de la diagonal de $A$. En fin, puede escribirse, por
tanto, 
\begin{equation*}
x = D^{-1}b - D^{-1}(A-D)x,
\end{equation*}
que es una ecuaci\'on de tipo \emph{punto fijo}. Si la matriz
$D^{-1}(A-D)$ cumple las condiciones del Teorema
\ref{the:condiciones-necesarias-convergencia-iteraciones-lineales},
entonces la iteraci\'on de Jacobi converge para cualquier semilla
$x_0$ y se tiene la cota del Teorema
\ref{the:cota-para-punto-fijo-matricial}. Para comprobar las
condiciones, hace falta calcular $D^{-1}(A-D)$, aunque puede
comprobarse de otras maneras (v\'ease el Lema
\ref{lem:jacobi-gs-convergentes-estrictamente-diag}).

\subsection{El algoritmo de Gauss-Seidel}
Si en lugar de utilizar la diagonal de la matriz $A$ para
descomponerla, se utiliza la parte triangular inferior (incluyendo la
diagonal), se obtiene un sistema de la forma
\begin{equation*}
x = T^{-1}b - T^{-1}(A-T)x,
\end{equation*}
que tambi\'en es una ecuaci\'on de \emph{punto fijo}. Si la matriz
$T^{-1}(A-T)$ cumple las condiciones del Teorema
\ref{the:condiciones-necesarias-convergencia-iteraciones-lineales},
entonces la iteraci\'on de Gauss-Seidel converge para cualquier
semilla $x_0$, y se tiene la cota del Teorema
\ref{the:cota-para-punto-fijo-matricial}. Para comprobar las
condiciones, har\'ia falta calcular $T^{-1}(A-T)$, aunque puede
comprobarse de otras maneras. 

\begin{definition}
Se dice que una matriz $A=(a_{ij})$ es \emph{estrictamente diagonal dominante por
  filas} si
\begin{equation*}
\abs{a_{ii}}>\sum_{j\neq i}\abs{a_{ij}}
\end{equation*}
para todo $i$ entre $1$ y $n$.
\end{definition}

Para estas matrices, tanto el m\'etodo de Jacobi como el de
Gauss-Seidel son convergentes:
\begin{lemma}\label{lem:jacobi-gs-convergentes-estrictamente-diag}
Si $A$ es una matriz \emph{estrictamente diagonal dominante por
  filas}, entonces tanto el m\'etodo de Jacobi como el de Gauss-Seidel
convergen para cualquier sistema de la forma $Ax=b$.
\end{lemma}

Para el de Gauss-Seidel, adem\'as, se tiene que
\begin{lemma}
Si $A$ es una matriz sim\'etrica definida positiva, entonces el
m\'etodo de Gauss-Seidel converge para cualquier sistema de la forma
$Ax=b$. 
\end{lemma}




\section{Anexo: C\'odigo en Matlab/Octave}

Se incluye el c\'odigo de varios de los algoritmos descritos, usable
tanto en Matlab como en Octave.

\subsection{El algoritmo de Gauss}
El siguiente c\'odigo implementa el algoritmo de reducci\'on de Gauss
para un sistema $Ax=b$ y devuelve la matriz $L$, la matriz $U$ y el
vector $b$ transformado, \emph{suponiendo que los multiplicadores por
  defecto nunca son $0$.} Si alguno es cero, se abandona el
procedimiento.

La entrada ha de ser:
\begin{description}
\item[\texttt{A}] una matriz cuadrada (si no es cuadrada, la salida es
  la triangulaci\'on por la diagonal principal),
\item[\texttt{b}] un vector con tantas filas como columnas tiene \texttt{A}.
\end{description}

La salida es una terna \texttt{L, At, bt}, como sigue:
\begin{description}
\item[\texttt{L}] es la matriz triangular inferior (de los multiplicadores),
\item[\texttt{At}] es la matriz $A$ transformada (la reducida), que se
  denomina $U$ en la factorizaci\'on $LU$, y es triangular superior.
\item[\texttt{bt}] el vector transformado.
\end{description}
De manera, que el sistema que se ha de resolver, equivalente al
anterior, es $At\times x = bt$.

\renewcommand{\lstlistingname}{C\'odigo}
\begin{figure}
%\begin{lstlisting}
\lstinputlisting{../matlab/gauss.m}
%\end{lstlisting}
\caption{C\'odigo del algoritmo de reducci\'on de Gauss}
\end{figure}


\subsection{El algoritmo $LUP$}
Como se vio arriba, el algoritmo de reducci\'on de Gauss est\'a sujeto
a que no aparezca ning\'un cero como pivote y adem\'as puede dar lugar
a errores importantes de redondeo si el pivote es peque\~no. A
continuaci\'on se muestra un c\'odigo en Matalb/Octave que implementa
el algoritmo $LUP$, por el cual se obtiene una factorizaci\'on $LU=PA$,
donde $L$ y $U$ son triangular inferior y superior, respectivamente,
la diagonal de $L$ est\'a formada por $1$ y $P$ es una matriz de
permutaci\'on. La entrada es:
\begin{description}
\item[\texttt{A}] una matriz cuadrada de orden $n$.
\item[\texttt{b}] un vector de $n$ filas. 
\end{description}

Devuelve cuatro matrices, \texttt{L}, \texttt{At}, \texttt{P} y
\texttt{bt}, que corresponded a $L$, $U$, $P$ y al vector transformado
seg\'un el algoritmo.

\renewcommand{\lstlistingname}{C\'odigo}
\begin{figure}
%\begin{lstlisting}
\lstinputlisting{../matlab/gauss_pivotaje.m}
%\end{lstlisting}
\caption{C\'odigo del algoritmo $LUP$}
\end{figure}


% \subsection{Implementaci\'on del algoritmo de Newton-Raphson}
% El algoritmo de Newton-Raphson es m\'as sencillo de escribir (siempre
% sin tener en cuenta las posibles \emph{excepciones} de coma flotante),
% pero requiere un dato m\'as complejo en el input: la derivada de $f$,
% que debe ser otra funci\'on an\'onima.

% Como se ha de utilizar la derivada de $f$ y no se quiere suponer que
% el usuario tiene un programa con c\'alculo simb\'olico, se requiere
% que uno de los par\'ametros sea expl\'icitamente la funci\'on
% $f^{\prime}(x)$. En un entorno con c\'alculo simb\'olico esto no
% ser\'ia necesario.

% As\'i, la entrada es:
% \begin{description}
% \item[\texttt{f}] una funci\'on an\'onima, 
% \item[\texttt{fp}] otra funci\'on an\'onima, la derivada de \texttt{f},
% \item[\texttt{x0}] la semilla,
% \item[\texttt{epsilon}] una tolerancia (por defecto \texttt{eps}),
% \item[\texttt{N}] el n\'umero m\'aximo de iteraciones (por defecto 50).
% \end{description}

% El formato de la salida, para facilitar
% el estudio es un par \texttt{[xn, N]} donde \texttt{xn} es la ra\'iz
% aproximada (o el valor m\'as cercano calculado) y \texttt{N} es el
% n\'umero de iteraciones hasta calcularla.

% \begin{figure}
% \caption{Implementaci\'on de Newton-Raphson}
% \label{code:newton-raphson}
% \begin{lstlisting}
% % Implementacion del metodo de Newton-Raphson
% function [z n] = NewtonF(f, fp, x0, epsilon = eps, N = 50)
%   n = 0;
%   xn = x0;
%   % Se supone que f y fp son funciones  
%   fn = f(xn);
%   while(abs(fn) >= epsilon & n <= N)
%     n = n + 1;  
%     fn = f(xn); % evaluar una sola vez
%     % siguiente punto
%     xn = xn - fn/fp(xn); % podria haber una excepcion
%   end
%   z = xn;
%   if(n == N)
%     warning('No converje en MAX iteraciones');
%   end
% end
% \end{lstlisting}
% \end{figure}

