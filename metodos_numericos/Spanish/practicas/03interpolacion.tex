% -*- TeX-master: "practicas.ltx" -*-
\chapter{Interpolation}
After a brief digression on integration (which may be useful for
computing areas related to solutions of ODEs) we return to the problem
of finding an approximate ``solution'' to an ODE. All of the methods
explained in Chapter \ref{cha:ode} gave numerical approximations to
the solution of an ODE as a list of values at the points of a network
on the $x-$axis. However, in many instances, values of the solution at
points not on the network will be required (for example, to plot the
solution or to approximate the values at points not on the
network). When the values required fall \emph{inside} the network,
this is known as the \emph{interpolation} problem. If the values
required fall \emph{outside} it, one is \emph{extrapolating}. We shall
mostly deal with the first problem. The second one is hard to tackle
and requires some extra knowledge of the function..

We shall only explain \emph{one-dimensional}
interpolation. Techniques for more than one dimension obviously exist
but they are all related to the ones we shall explain: a good command
of one-dimensional tools permits an easy grasp of the higher
dimensional ones.

Assume a vector ${\bf x}=(x_1,\dots,x_n)$ of (ordered) $x-$coordinates is given and another one
${\bf y}=(y_1,\dots,y_n)$ of the same length represents the values of a function $f$
at each point of ${\bf x}$. The problem under consideration consists
in approximating the values of $f$ at any point between $x_1$
and $x_n$.

\section{Linear interpolation}
One of the simplest solutions to the interpolation problem is to draw
line segments between each $(x_i,y_i)$ and $(x_{i+1},y_{i+1})$ for
each $i$ and, if $\tilde{x}\in[x_i,x_{i+1}]$, approximate the value of
$f(\tilde{x})$ as the corresponding line in the segment. This means
using the approximation
\begin{equation*}
  f(\tilde{x}) \simeq \frac{y_{i+1}-y_i}{x_{i+1}-x_i}(\tilde{x}-x_i)+y_i,
\end{equation*}
having previously found the $i$ such that
$\tilde{x}\in[x_i,x_{i+1}]$. Notice that \emph{there may be two such
  $i$}, but the result is the same in either case (why?).

\begin{exercise}\label{exer:linear-interpolation}
  Implement the above interpolation method. Specifically, write a file
  called \texttt{linear\_int.m} implementing a function
  \texttt{linear\_int} which, given two vectors \texttt{x} and
  \texttt{y} (corresponding to the $\bf{x}$ and $\bf{y}$ above) and a
  value \texttt{x1}, returns the value of the linear interpolation at
  \texttt{x1} corresponding to \texttt{x} and \texttt{y}. A couple of examples
  of calling could be
\begin{lstlisting}
> x = [1 2 3 4 5];
> y = [0 2 4.1 6.3 8.7];
> linear_int(x, y, 2.3)
ans = 2.6300
> linear_int(x, y, [2.3 3.5])
ans =
   2.6300    5.6400
\end{lstlisting}
  Notice how the last parameter can be a vector of values on which to
  compute the interpolation.
\end{exercise}

\begin{exercise}\label{exer:plot-linear-interpolation}
  Use the function defined in Exercise~\ref{exer:linear-interpolation}
  to plot the graph of the linear interpolation corresponding to the
  following clouds of points:
  \begin{itemize}
  \item The points $(1,2), (2, 3.5), (3, 4.7), (4, 5), (5, 7.2)$.
  \item The points $(-2, 3), (-1, 3.1), (0, 2.8), (1, 3.5), (2, 4),
    (3, 5.7)$.
  \item On the $x-$axis, a list of $100$ points evenly distributed
    between $0$ and $\pi$. On the $y-$axis, the values of the function $\sin(100x)$
    at those points.
  \end{itemize}
\end{exercise}

\section{Cubic splines}
Linear interpolation is useful mainly due to its
simplicity. However, in most situations, the functions with which one is working
are differentiable (and in many cases, several times so), and
linear interpolation does not usually satisfy this condition. To solve
this problem, splines were devised.

As was explained in the theory classes, \emph{cubic splines} are
the most used and they are the ones we shall implement.

Before proceeding, we shall explain the internal commands Matlab uses
for dealing with cubic splines. Then we shall implement a spline
function in detail.

\subsection{The \texttt{spline} and \texttt{ppval} functions}
Given a cloud of points described by the vectors of $x-$ and
$y-$coordinates, say ${\bf x}=(x_1,\dots, x_n)$ and ${\bf y}=(y_1,
\dots, y_n)$, Matlab can compute different types of splines. The
command for doing so is \texttt{spline} and it takes as input, in its most 
basic form, two vectors, \texttt{x} and \texttt{y}. Then:
\begin{itemize}
\item If \texttt{x} and \texttt{y} are of the same length, then
  \texttt{spline(x,y)} returns the \emph{not-a-knot} cubic spline
  interpolating the cloud given by \texttt{x} and \texttt{y}.
\item If \texttt{y} has exactly \emph{two} more components than
  \texttt{x}, then the call \texttt{spline(x,y)} returns the interpolating
  cubic spline using the cloud given by \texttt{x} and
  \texttt{y(2:end-1)} with the condition that the derivative of the
  spline at the first point is \texttt{y(1)} and the derivative at
  the last point is \texttt{y(end)}. These splines are called, for
  obvious reasons,
  \emph{clamped}.
\end{itemize}

\begin{example}\label{ex:first-spline-example}
  Let us work with the first cloud of points in Exercise
  \ref{exer:linear-interpolation}. In this case, \texttt{x} is
  \texttt{[1 2 3 4 5]} whereas \texttt{y} is \texttt{[2 3.5 4.7 5
    7.2]}.
  \begin{itemize}
  \item The \emph{not-a-knot} spline is computed straightaway using
    \texttt{spline}:
\begin{lstlisting}[language=matlab]
> x = [1 2 3 4 5];
> y = [2 3.5 4.7 5 7.2];
> p = spline(x, y);
\end{lstlisting}
    The object returned by \texttt{spline}, which we have called
    \texttt{p}, has a special nature: it is a \emph{piecewise
      polynomial}. In order to evaluate it, one has to use the
    function \texttt{ppval}:
\begin{lstlisting}[language=matlab]
> ppval(p, 2.5)
ans =  4.2281
\end{lstlisting}
    which can be used with vectors as well:
\begin{lstlisting}
> ppval(p, [1:.33:2.33])
ans =

   2.0000   2.4389   2.9510   3.4841   3.9861
\end{lstlisting}
    and hence, can be used for plotting the actual spline:
\begin{lstlisting}[language=matlab]
> u = linspace(1, 5, 300);
> plot(u, ppval(u));
\end{lstlisting}
  \item The \emph{clamped} cubic spline imposes specific values for
    the first derivative at the endpoints. In order to compute it using
    Matlab one has to add this condition as the first and last values
    of the \texttt{y} parameter. For the same cloud of points as
    above, and setting the first derivative at the endpoints to $0$,
    one would write:
\begin{lstlisting}[language=matlab]
> x = [1 2 3 4 5];
> y = [2 3.5 4.7 5 7.2];
> q = spline(x, [0 y 0];
\end{lstlisting}
    Let us plot \texttt{q} on the same graph as \texttt{p}:
\begin{lstlisting}[language=matlab]
> hold on
> plot(u, ppval(q, u), 'r');
\end{lstlisting}
    What is the difference?
  \item Take into account that Matlab does not include in its default
    toolbox the ability to compute \emph{natural} splines (those for
    which the second derivative at the endpoints is $0$). We shall
    implement this below.
  \end{itemize}
\end{example}

\begin{exercise}\label{exer:compare-linear-cubic}
  Describe (in detail) at least two situations in which linear
  interpolation should be preferred to cubic splines. Same for the
  reverse.

  Can you come up with examples in which the natural spline is better
  suited than the ``not-a-knot''? What about the reverse?
\end{exercise}

\subsection{Implementing the natural cubic spline}
We shall write a long function implementing the natural cubic
spline. From the theory, we know that solving a linear system of
equations is required. However, this system, once the problem is
stated properly, has a very simple structure. We shall solve it using
Matlab's solver. Also, the function shall return a piecewise defined
polynomial, using the \texttt{mkpp} utility. This section should be
read more as a thorough exercise on Matlab programming than as a
useful example.

\paragraph{From polynomials to a linear system}
The problem under consideration consists in, given ${\bf x}=(x_1,\dots,
x_n)$ and ${\bf y}=(y_1,\dots, y_n)$, find polynomials $P_1(x), \dots,
P_{n-1}(x)$ (notice that there are $n-1$ polynomials, not $n$)
satisfying the following conditions:
\begin{enumerate}
\item Each $P_i$ passes through the points $(x_i,y_i)$ and
  $(x_{i+1},y_{i+1})$.
\item At each $x_i$, for $i=2,\dots,n-1$, the derivative of
  $P_{i}(x)$ equals that of $P_{i-1}(x)$.
\item At each $x_i$, for $i=2,\dots,n-1$, the second derivative of
  $P_{i}$ equals that of $P_{i-1}(x)$.
\end{enumerate}
It is easy to check that those conditions give a total of $4(n-1)-2$
linear equations for the coefficients of the polynomials $P_i(x)$. As there
are $4(n-1)$ coefficients, there are two missing equations for a system
with a unique solution. These two equations allow for the different
types of splines (natural, not-a-knot, etc.).

Let us express each polynomial $P_i(x)$ (for $i=2,\dots,
n$\footnote{This is a bit awkward but simplifies the notation.}) as
\begin{equation*}
  P_i(x) = a_i+b_i(x-x_{i-1})+c_i(x-x_{i-1})^2+d_i(x-x_{i-1})^3
\end{equation*}
and each difference
\begin{equation*}
  x_{i}-x_{i-1} = h_i
\end{equation*}
(for $i=2,\dots, n$ also).
After some algebraic manipulations (which can be found in the theory
or anywhere on the Internet), one arrives at the following set of
equations:
\begin{equation*}
  h_{i-1}c_{i-1}+(2h_{i-1}+2h_i)c_i+h_ic_{i+1}=3\left(
    \frac{y_i-y_{i-1}}{h_i} - \frac{y_{i-1}-y_{i-2}}{h_{i-1}}\right)
\end{equation*}
for $i=3,\dots, n-1$ (this gives $n-1-3+1=n-3$ equations, two less
than $n-1$, exactly as expected). There are also explicit linear expressions
for each $a_i$, $b_i$ and $d_i$ in terms of the $c_i$. One can easily check
that the equations are independent. Those $n-3$ equations can be
written as a linear system $Ac=\alpha$, where $A$ is the $(n-3)\times
(n-1)$ matrix
\begin{equation*}
\label{eq:tridiagonal-spline-matrix}
A=\begin{pmatrix}
h_2 & 2(h_2+h_3) & h_{3} & 0 & \dots & 0 & 0 & 0\\
0 & h_3 & 2(h_3+h_4) & h_4 & \dots & 0 & 0 & 0\\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots
\\
0 & 0 & 0 & 0 & \dots & h_{n-2} & 2(h_{n-2} + h_{n-1}) & h_{n-1}
\end{pmatrix}
\end{equation*}
and $c$ is the vector column of the unknowns $(c_2,\dots, c_n)^t$,
whereas $\alpha$ is
\begin{equation*}
  \begin{pmatrix}
    \alpha_3\\
    \alpha_4\\
    \vdots\\
    \alpha_{n-1}
  \end{pmatrix}
\end{equation*}
and each $\alpha_i$ (for $i=3,\dots, n-1$) is
\begin{equation}\label{eq:alpha-i-spline}
\begin{pmatrix}
  \alpha_i=3\left(
    \frac{y_i-y_{i-1}}{h_i}-\frac{y_{i-1}-y_{i-2}}{h_{i-1}}
  \right).
\end{pmatrix}
\end{equation}
There are obviously two missing equations for a square system. As we
want to implement the \emph{natural spline} condition, which reads as
$d_2=0$ and $d_n=0$, we need to ``translate'' these conditions into
new equations involving only the $c_i$ coefficients. After some
algebraic manipulations, one obtains the following:
\begin{equation*}
  \begin{split}
    c_{2} & = 0,\\
    \frac{h_{n-1}}{2}c_{n-1}+(h_n+h_{n-1})c_n & =
    \frac{3}{2}\left(
      -\frac{y_{n-1}-y_{n-2}}{h_{n-1}}+\frac{y_n-y_{n-1}}{h_n}
    \right)
  \end{split}
\end{equation*}
Letting $\alpha_n=6(y_n-y_{n-1})/h_n$ and $\alpha_2=0$, the complete system
is
\begin{equation*}
  \tilde{A} c^t = \tilde{\alpha}
\end{equation*}
where  $\tilde{A}$ is $A$ together with a first row
\begin{equation*}
  \begin{pmatrix}
    1 & 0 & 0\dots &0
  \end{pmatrix}
\end{equation*}
and a last row
\begin{equation*}
  \begin{pmatrix}
    0 & 0 & \dots & 0 & 2h_{n-1} & (4h_{n-1} + 5h_n)
  \end{pmatrix}
\end{equation*}
and $\tilde{\alpha}$ is the same $\alpha$ with  $\alpha_2=0$ on the
first row and $\alpha_n=6(y_n-y_{n-1})/h_n$.

\textbf{Writing the system as a Matlab matrix:}
so far, we have just written ``in human terms'' the rows of the linear system to be
solved, and the column corresponding to the independent terms. We now
proceed to write the appropriate Matlab code which describes it.

Before proceeding any further, we know that $a_i=y_{i-1}$ for
$i=2,\dots, n$, which we can write (recalling that indices in Matlab
start at $1$, whereas our polynomials start at $2$):
\begin{lstlisting}[language=matlab]
a = y(1:end-1);
\end{lstlisting}

For the system of equations, we need a matrix,
which we shall call \texttt{A} of size $(n-1)\times
(n-1)$ (remember that there are $n-1$ polynomials, not $n$). We know
that the first row is a $1$ followed by $n-2$ zeros, the following
$n-3$ rows are those of $A$ in
Equation~\ref{eq:tridiagonal-spline-matrix} and the last one is a list
of $n-3$ zeros followed by $2h_{n-1}, (4h_{n-1}+5h_n)$. The matrix $A$
is tridiagonal, and its structure can be described as:
\begin{itemize}
\item The line below the main diagonal is $(h_2, h_3, \dots, h_{n-1})$.
\item The line above the main diagonal is $(h_3, h_4, \dots, h_n)$.
\item The diagonal is twice the sum of the lines above.
\end{itemize}
So it turns out that the vector $(h_2, h_3,\dots, h_n)$ will be
useful. Recall that $h_i=x_i-x_{i-1}$. If \texttt{x} is the Matlab
vector corresponding to the $x-$coordinates, then setting
\begin{lstlisting}[language=matlab]
> h = diff(x);
\end{lstlisting}
makes \texttt{h} the vector of differences, the one we wish to
use. (Notice that $h_2$ is \emph{the first coordinate of} \texttt{h}
because $h_2=x_2-x_1$).

The command to construct matrices with diagonal values is
\texttt{diag}. It works as follows:
\begin{lstlisting}[language=matlab]
diag(v, k)
\end{lstlisting}
will create a \emph{square} matrix whose \texttt{k}-th diagonal contains the
vector \texttt{v}. If \texttt{k} is missing, it is set to $0$. The
\texttt{k}-th diagonal is the diagonal row which is \texttt{k}-steps
away from the main diagonal. Thus, the $0-$th diagonal is the main
one, the $1-$diagonal is the one just to the right of the main one,
the $-1-$diagonal is the one just to the left, etc. For example,
\begin{lstlisting}[language=matlab]
> diag([2 -1 3], -2)
ans =

   0   0   0   0   0
   0   0   0   0   0
   2   0   0   0   0
   0  -1   0   0   0
   0   0   3   0   0
\end{lstlisting}
This allows for a very fast specification of the matrix $\tilde{A}$ in
of the linear system to be solved. The $-1-$diagonal is
\begin{lstlisting}[language=matlab]
[h(1:end-1) h(end-1)/2]
\end{lstlisting}
the $1-$diagonal is
\begin{lstlisting}[language=matlab]
[0 h(2:end)]
\end{lstlisting}
and the proper diagonal is
\begin{lstlisting}[language=matlab]
[1 2*(h(1:end-1) + h(2:end)) h(end)+h(end-1)]
\end{lstlisting}
where \texttt{end} means, in Matlab, the last index of a vector. From
these values, it should be easy to understand that the matrix
\texttt{A} of the linear system to be solved (which is $\tilde{A}$
above) can be defined as follows (we divide the matrix into three
``diagonal'' ones for clarity):
\begin{lstlisting}[language=matlab]
A1 = diag([h(1:end-2) h(end-1)/2], -1);
A2 = diag([0 h(2:end-1)], 1);
A3 = diag([1 2*(h(1:end-2)+h(2:end-1)) h(end)+h(end-1)]);
A  = A1+A2+A3;
\end{lstlisting}

The column vector $\tilde{\alpha}$ is defined as follows: recall that the
first element is $0$, the next $n-3$ are as in
Equation~\ref{eq:alpha-i-spline} and the last one is
$6(y_n-y_{n-1})/h_n$. Thus, letting \texttt{dy = diff(y)}, we can
write (notice the dots before the slashes in the second element):
\begin{lstlisting}[language=matlab]
alpha = [0 3*(dy(2:end-1)./h(2:end-1)-dy(1:end-1)./h(1:end-2))
           3/2*(-dy(end-1)/h(end-1)+dy(end)/h(end))]';
\end{lstlisting}

Once the system of equations $\tilde{A}c'= \tilde{\alpha}'$ has been
properly set up, one solves it using Matlab's solver:
\begin{lstlisting}[language=matlab]
c = (A\alpha)';
\end{lstlisting}
The translation of the explicit expressions for $b$ and $d$ (in the
theory), into matlab goes as (where \texttt{n} is the number of
interpolation polynomials):
\begin{lstlisting}[language=matlab]
  % Initialize b and d
  b = zeros(1,n);
  d = zeros(1,n);

  % unroll all the coefficients as in the theory
  k = 1;
  while(k<n)
    b(k) = (y(k+1)-y(k))/h(k) - h(k) *(c(k+1)+2*c(k))/3;
    k=k+1;
  end
  d(1:end-1) = diff(c)./(3*h(1:end-1));

  % the last b and d have explicit expressions:
  b(n) = b(n-1) + h(n-1)*(c(n)+c(n-1));
  d(n) = (y(n+1)-y(n)-b(n)*h(n)-c(n)*h(n)^2)/h(n)^3;
\end{lstlisting}

At this point, we have computed all the coefficients of the
interpolating polynomials. The way to define a piecewise-polynomial
function in matlab is using \texttt{mkpp}, which takes as arguments
the vector of $x-$coordinates defining the intervals on which each
polynomial is used (in our case the same as the $\bf{x}$ input vector)
and a matrix containing the coefficients of each polynomial
\emph{relative to $x_{i-1}$}, that is, in our case\footnote{Important:
remember that Matlab understands a vector \texttt{[d c b a]} as a
polynomial taking the coefficients from greatest to lowest degree, in
the example, $dx^3+cx^2+bx+a$.}:
\begin{equation*}
  \begin{pmatrix}
    d_n & c_n & b_n & a_n\\
    d_{n-1} & c_{n-1} & b_{n-1} & a_{n-1}\\
    \vdots\\
    d_2 & c_2 & b_2 & a_2 
  \end{pmatrix}
\end{equation*}
which gives, in matlab:
\begin{lstlisting}[language=matlab]
f = mkpp(x, [d; c; b; a]');
\end{lstlisting}

Putting everything together in a file called
\texttt{natural\_spline.m}, we get the code of
Listing~\ref{lst:natural-spline}.
\lstinputlisting[language=matlab, caption={Function implementing the
computation of the natural spline for a cloud of points.},
label=lst:natural-spline]
{code/natural_spline.m}


\begin{example}\label{ex:spline-five-points}
  Consider the points $(0,1), (1, 3), (2, 7), (4, 3), (6,0)$. The
  natural cubic spline passing through them can be computed and
  plotting, using the
  function just defined, as follows:
\begin{lstlisting}[language=matlab]
> x=[0 1 2 4 6];
> y=[1 3 7 3 0];
> P=natural_spline(x,y);
> u=[0:.01:6];
> plot(u, ppval(P, u));
\end{lstlisting}
  Notice how, instead of \texttt{P(u)}, one needs to use the function
  \texttt{ppval} to evaluate a \emph{piecewise defined function}. In
  order to visually verify that $P$ passes through all the points, one
  can plot them on top of $P$:
\begin{lstlisting}[language=matlab]
> hold on;
> plot(x,y,'*r');
\end{lstlisting}
\end{example}

\begin{example}\label{ex:spline-sin}
  A more complicated example: let us try to estimate (visually) the
  difference between the \emph{sine} function and a natural cubic
  spline, along the period $[0,2\pi]$, using $10$ points. This could
  be done as follows:
\begin{lstlisting}[language=matlab,
label=spline-sine,caption={Comparison of the graphs of the sine function
and a cubic spline with $10$ points.}]
> x=linspace(0, 2*pi, 10);
> y=sin(x);
> Q=natural_spline(x, y);
> u=[0:.01:2*pi];
> clf;
> plot(u, sin(u));
> hold on;
> plot(u, ppval(Q, u), 'r');
\end{lstlisting}
  The graphs should be indistinguishable. This gives an idea of the
  power of cubic splines: for \emph{sufficiently well-behaved
    functions}, they give surprisingly good approximations.
\end{example}

\begin{exercise}
  Matlab has a \texttt{spline} function, as explained above. Compare
  the plots of this function with those of the natural spline for
  examples \ref{ex:spline-sin} and \ref{ex:spline-five-points}. Which
  gives a better approximation to the sine function?
\end{exercise}

\begin{exercise}
  Let $f(x)=cos(exp(x))$, for $x\in[2,5]$. Let $P$ be the natural
  spline interpolating the values of $f$ on $10$ points from $2$ to
  $5$. Compare the plots of $P$ and of $f$ on that interval. Are they
  similar? Are they different? Why do you think that happens? How do
  you think you can fix this problem?
\end{exercise}

\begin{exercise}
  Let $f(x)=exp(cos(x))$, for $x\in[0,6\pi]$. Let $Q$ be the natural
  spline interpolating the values of $f$ on $20$ points. Plot both $f$
  and $Q$ on that interval. Where are the most noticeable differences
  between those two plots? Can you get a better approximation using
  the \texttt{spline} function? Why?
\end{exercise}

\begin{exercise}\label{exer:solution-ode-spline}
  Consider the differential equation
  \begin{equation*}
    y^{\prime}=\frac{y}{1+x^2}.
  \end{equation*}
  Use any of the algorithms defined in Chapter \ref{cha:ode} to
  compute the approximate values of a solution on the interval $[0,5]$
  with initial condition $y(0)=1$ and using a step of size
  $0{.}5$. Use a natural spline to interpolate the values of an
  approximate solution passing through those points.

  The true solution to that ODE is $y(x)=ce^{\atan(x)}$, for $c$ a
  constant. Compute the constant for the initial value $y(0)=1$ and
  compare the plots of the true solution and the spline. Explain the
  difference between both plots: is it due to the nature of the spline
  or to the approximate solution of the ODE? Would the graphs be more
  similar if more points were used?

  Explain as much as possible. This is a very important
  exercise. Perform different computations, use a different number of
  intermediate points, etc\dots
\end{exercise}


\chapter{Interpolación por mínimos cuadrados}
Dada una nube $C$ de $N$ puntos y un \emph{espacio vectorial} $V$
de funciones que ``representan adecuadamente la nube de puntos'', el
problema de encontrar la \emph{mejor} aproximación a la nube de puntos
dentro de las funciones de $V$ se puede comprender de diversos
modos. El más común es la \emph{aproximación por mínimos cuadrados},
que se explicó en la teoría y que puede enunciarse así:

Sean $(x_1,y_1), \dots, (x_N, y_N)$ los puntos de la nube $C$ y sea
$\left\{f_1, \dots, f_n \right\}$ una base de $V$. El \emph{problema
  de interpolación por mínimos cuadrados} para $C$ y $V$ consiste en
encontrar los coeficientes $a_1, \dots, a_n$ tales que el valor
\begin{equation*}
  E(a_1,\dots,a_n) = \sum_{i=1}^N (a_1f_1(x_i)+\dots+a_nf_n(x_i) - y_i)^2
\end{equation*}
es mínimo (ese valor se llama \emph{error cuadrático total} y,
obviamente, depende de los coeficientes).

Hay diversas maneras de resolverlo. Quizás la más sencilla sea
utilizar cálculo diferencial en varias variables. Al final, los
coeficientes  $a_1,
\dots, a_n$ se calculan como la solución del sistema de ecuaciones
lineales: 
\begin{equation}\label{eq:system-for-least-squares}
  X X^t
  \begin{pmatrix}
    a_1\\
    \vdots\\
    a_n
  \end{pmatrix}
  = X
  \begin{pmatrix}
    y_1\\
    \vdots\\
    y_N
  \end{pmatrix}
\end{equation}
(nótese que ambos miembros de la igualdad terminan siendo vectores
columna de $n$ componentes). La matriz $X$ es
\begin{equation*}
  X=
  \begin{pmatrix}
    f_1(x_1) & f_1(x_{2}) & \dots & f_1(x_N) \\
    \vdots & \vdots & \ddots & \vdots \\
    f_n(x_1) & f_2(x_2) & \dots & f_n(x_N)
  \end{pmatrix}
\end{equation*}
y $X^t$ es su traspuesta. La ecuación
\eqref{eq:system-for-least-squares} siempre (en condiciones muy
generales) da un sistema compatible, pero habitualmente está mal
condicionado. 

Así pues, el problema de interpolación por mínimos cuadrados parte de
un conjunto de $N$ puntos (la nube) y una familia de $n$ funciones
(con $n<N$, de hecho $N$ suele ser muy grande y $n$ muy pequeño).


\begin{example}
  El ejemplo más sencillo es la interpolación de una nube de puntos
  mediante una función lineal (la llamada \emph{recta de
    regresión}). Las funciones lineales tienen la forma
  $a+bx$, así que el espacio vectorial que se estudia está generado
  por $f_1(x)=1$ y $f_2(x)=x$ (y por tanto hay dos valores que
  calcular, la $a$ y la $b$).
  Tómese por ejemplo la nube dada por $(1,2), (2, 2{.}1), (3,2{.}15),
  (4,2{.}41), 
  (5, 2{.}6)$. Es fácil darse cuenta de que la recta de regresión será
  más o menos $y=0{.}1(x-1)+2=1{.}9+0{.}1x$ (puesto que la pendiente
  es más o menos $0{.}1$ y la línea pasará más o menos poor $(1,2)$).
  Usando Matlab para resolver este problema:
  \begin{lstlisting}[language=matlab]
    > x=[1 2 3 4 5];
    > y=[2 2.1 2.15 2.41 2.6];
    > f1=@(x) 1 + x.*0;
    > f2=@(x) x;
    > X=[f1(x); f2(x)]
    X =

    1 1 1 1 1 1 2 3 4 5

    > A=X*X'
    A =

    5 15 15 55

    > Y=X*y'
    Y =

    11.260 35.290

    > coefs=A\Y
    coefs =

    1.79900 0.15100
\end{lstlisting}
  Por tanto, la función lineal (recta) que interpola la nube de puntos
  por mínimos cuadrados es $y=1{.}799 + 0{.}151x$, que está de acuerdo
  con nuestra suposición.
\end{example}

\begin{remark*}
  Hay que tener cuidado con la definición de la función constante
  $f_1(x)=1$, en Matlab: hay que hacerla ``vectorial'' añadiéndole,
  por ejemplo, un vector nulo (en este caso, $0.*x$). Esto es un apaño
  para un defecto de Matlab, no algo mágico.
\end{remark*}


  \begin{table}
    \centering
    \begin{tabular}[h]{r|r}
      $X$ & $Y$ \\
      \hline
      1.0 &  6.20 \\
      1.5 &  9.99 \\
      2.0 &  15.01 \\
      2.5 &  23.23 \\
      3.0 &  32.70 \\
      3.5 &  43.08 \\
      4.0 &  54.01 \\
      4.5 &  65.96 \\
      5.0 &  80.90
    \end{tabular}
    \caption{Datos que siguen una fórmula cuadrática.}
    \label{tab:quadratic-data}
  \end{table}

\begin{example}\label{ex:quadratic-table}
  Los datos de la Tabla \ref{tab:quadratic-data} vienen de un
  experimento. Se sabe que la variable  $Y$ depende cuadráticamente de
  la variable $X$ (es decir, hay una fórmula de grado dos que
  relaciona $Y$ con $X$). Usando mínimos cuadrados, encuéntrense los
  coeficientes que mejor ajustan esta nube.

  Como se sabe que $Y$ depende de $X$ como un polinomio de grado $2$,
  el espacio vectorial $V$ de funciones está generado por $1, x,
  x^2$. Sean 
  $f_1=1, f_2=x, f_3=x^2$. Se puede usar Matlab como sigue para
  resolver el problema:
  \begin{lstlisting}[language=matlab]
    > x=[1:.5:5];
    > y=[6.2 9.99 15.01 23.23 32.7 43.08 54.01 65.96
    80.9];
    > % definimos las funciones:
    > f1=@(x) 1+0.*x;
    > f2=@(x) x;
    > f3=@(x) x.^2;
    > % main matrix
    > X=[f1(x); f2(x); f3(x)];
    > A=X*X';
    > Y=X*y';
    > % the system is A*a'=Y, use matlab to solve it:
    > A\Y ans =

    0.55352 2.27269 2.75766
    > % this means that the least squares interpolating
    > % polynomial of degree 2 is
    > % 0.55352 + 2.27269*x + 2.75766*x.^2
    > % plot both the cloud of points and the polynomial
    > plot(x,y,'*');
    > hold on
    > u=[1:.01:5];
    > plot(u, 0.55352 + 2.27269.*u + 2.75766*u.^2, 'r')
\end{lstlisting}
  Obsérvese cómo el polinomio interpolador por mínimos cuadrados
  \emph{no pasa por todos los puntos de la nube} (de hecho, es muy
  posible que no pase por ninguno).
\end{example}

\begin{remark}\label{rem:not-only-polynomials}
  El problema de interpolación por mínimos cuadrados no tiene por qué
  ser solo de encontrar polinomios. Puede haber funciones más
  complejas que aproximen la nube de puntos, pero siempre habrá de
  tratarse de un problema \emph{lineal}: si no, la técnica expuesta no
  sirve.
\end{remark}

\begin{exercise}\label{exer:cubic-least-squares}
  Un nuevo desarrollo teórico muestra que los datos de la Tabla
  \ref{tab:quadratic-data} se describen mejor mediante una función de
  grado $3$. Úsese la interpolación por mínimos cuadrados para
  calcular el polinomio cúbico que mejor se ajusta a la nube. ¿Se
  parecen los coeficientes de grados $0, 1$ y $2$ de este polinomio a
  los del Ejemplo \ref{ex:quadratic-table}? ¿Es el ajuste mejor o peor?

  Este ejercicio es interesante porque muestra que, por lo general,
  usar un polinomio para interpolar una nube de puntos, no es una
  buena idea, \emph{salvo que haya una explicación clara} de por qué
  la nube está relacionada con un polinomio (como en los ejemplos y
  ejercicios que siguen).
\end{exercise}

\begin{exercise}\label{exer:log-lin-exp}
  La Tabla \ref{tab:log-lin-exp-data} representa datos de un
  experimento. Se sabe que siguen una función de la forma
  $y(x)=a\log(x)+bx+ce^x$, para ciertos $a, b$ y $c$. Usando mínimos
  cuadrados, calcular estos $a, b$ y $c$.
  \begin{table}
    \centering
    \begin{tabular}[h]{r|r}
      $X$ & $Y$ \\
      \hline
      2.0 & 11.39\\
      2.7 &  15.31\\
      3.4 &  18.18\\
      4.1 &  19.80\\
      4.8 &  19.76\\
      5.5 &  15.03\\
      6.2 &   1.74\\
      6.9 & -29.67\\
    \end{tabular}
    \caption{Datos que siguen una fórmula compleja.}
    \label{tab:log-lin-exp-data}
  \end{table}
\end{exercise}

\begin{exercise}\label{exer:anscombe-quartet}
  La Tabla \ref{tab:anscombe-quartet} contiene lo que se conoce como
  el \emph{Cuarteto de Anscombe}: es una lista de datos interesante
  por las propiedades que comparten las cuatro colecciones. Nosotros
  nos fijaremos solo en la recta de regresión $a+bx$ para cada nube de
  puntos. Calcúlese (hay cuatro nubes de puntos, por tanto, cuatro
  rectas de regresión).
  \begin{table}
    \centering
    \begin{tabular}[h]{r|r@{\hspace{25pt}}r|r@{\hspace{25pt}}r|r@{\hspace{25pt}}r|r}
      X  &  Y     &  X  &  Y     & X   &  Y     &  X  &  Y\\
      \hline
      10  &  8.04  & 10  &  9.14  & 10  &  7.46  &  8  &  6.58\\
      8  &  6.95  &  8  &  8.14  &  8  &  6.77  &  8  &  5.76\\
      13  &  7.58  & 13  &  8.74  & 13  & 12.74  &  8  &  7.71\\
      9  &  8.81  &  9  &  8.77  &  9  &  7.11  &  8  &  8.84\\
      11  &  8.33  & 11  &  9.26  & 11  &  7.81  &  8  &  8.47\\
      14  &  9.96  & 14  &  8.10  & 14  &  8.84  &  8  &  7.04\\
      6  &  7.24  &  6  &  6.13  &  6  &  6.08  &  8  &  5.25\\
      4  &  4.26  &  4  &  3.10  &  4  &  5.39  & 19  & 12.50\\
      12  & 10.84  & 12  &  9.13  & 12  &  8.15  &  8  &  5.56\\
      7  &  4.82  &  7  &  7.26  &  7  &  6.42  &  8  &  7.91\\
      5  &  5.68  &  5  &  4.74  &  5  &  5.73  &  8  &  6.89
    \end{tabular}
    \caption{Cuarteto de Anscombe.}
    \label{tab:anscombe-quartet}
  \end{table}
  Una vez calculadas las cuatro rectas, dibújense las cuatro nubes de
  puntos y las rectas (todo en la misma figura) y compárense. ¿Qué se
  puede deducir de esto?
\end{exercise}

\begin{exercise}\label{exer:mass-velocity-energy}
  Un experimento sirve para calcular el valor de la energía cinética a
  partir de la velocidad de un objeto en movimiento. La Tabla
  \ref{tab:kinetic-velocity} muestra los resultados de llevar a cabo dicho
  experimento $5$ veces con el mismo objeto.
  Dése un valor razonable para la masa del objeto.
  \begin{table}
    \centering
    \begin{tabular}[h]{r|r}
      $v$ & $E$\\
      \hline
      1.0 &  8.05\\
      1.5 & 16.97\\
      2.3 & 39.69\\
      2.7 & 55.58\\
      3.0 & 66.91\\
    \end{tabular}
    \caption{Energía cinética frente a velocidad para el mismo objeto.}
    \label{tab:kinetic-velocity}
  \end{table}
  La velocidad se da en m/s y la energía en julios.
\end{exercise}

\begin{exercise}\label{exer:uniformly-accelerated-motion}
  La Tabla \ref{tab:unif-accel-motion} lista los valores del cómputo
  de la distancia recorrida por un objeto en un tiempo determinado. Se
  sabe que dicho objeto sigue un movimiento uniformemente acelerado
  que comienza con la misma velocidad inicial. Calcúlense valores
  razonables para esta velocidad inicial y para la acelaración.
  \begin{table}[h]
    \centering
    \begin{tabular}[h]{r|r}
      $t$ (s) & $d$ (m)\\
      \hline
      1 & 4.89\\
      2 & 11.36\\
      3 & 21.64\\
      4 & 34.10\\
      5 & 50.05\\
      6 & 69.51
    \end{tabular}
    \caption{Distancia frente a tiempo de un m.u.a.}
    \label{tab:unif-accel-motion}
  \end{table}
\end{exercise}

% \begin{exercise}\label{exer:seasonality}
%   The following plot shows the mean price of a can of beer (in
%   pesetas) at each month from 1960 to 1980 (there are 240 values in
%   the series). The plot has two remarkable properties: on the one
%   hand, it ``seems to increase with time,'' on the other, the price
%   has a wavy behavior (with local lows and highs at intervals of the
%   same length). As a matter of fact, minima and maxima happen
%   (approximately) with a year of difference. This behavior is called
%   ``seasonality'' (the season of the year affects the price of
%   commodities: in this case, beer is drunk more frequently in the
%   Summer, as people are more thirsty, and prices tend to be higher
%   than in the Winter). How would you model this graph in order to find
%   a ``suitable'' fitting function?
  
%   \begin{figure}[h]
%     \centering
%     \begin{tikzpicture}
%       \begin{axis}[ xmin=0, xmax=241, ylabel=Price (\euro),
%         xlabel=Year, xtick={6,54, 114, 174, 234}, xticklabels={1961,
%           1965, 1970, 1975, 1980}]
%         \addplot[color=olive, line width=1.5pt] file
%         {code/prices.dat};
%       \end{axis}
%     \end{tikzpicture}
%   \end{figure}
  
%   One has to be aware that \emph{prices are always modeled with
%     products, not with additions}: prices increase by a rate, not by a
%   fixed amount (things are ``more or less expensive'' in rate, you
%   would not complain of a computer costing \EUR{5} more but you would
%   if the loaf of bread had that same increase in price). So, the
%   relevant data should not be that in the graph but its logarithm
%   (which increases and decreases in absolute values, not relative
%   ones). Hence, one should aim at a least-squares interpolation of the
%   logarithm of the true values\footnote{Despite not being completely
%     correct, the fact that the values are much larger than $0$ makes
%     using logarithms and fitting the new curve with least squares
%     useful, albeit inexact.}.

%   Once logarithms are taken, the curve should be fit (interpolated) as
%   a linear function with a seasonal (yearly) modification: how would
%   you perform this interpolation? (One needs to use the sine and
%   cosine function, but how?). What functions would you use as basis?
%   Why?

%   The data can be found at \texttt{http://pfortuny.net/prices.dat}.
% \end{exercise}
