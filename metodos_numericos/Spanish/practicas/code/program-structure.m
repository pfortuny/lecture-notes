function [s1, s2, ..., sn] = NombreF(p1, p2, ...., pn)
    s1 = valor razonable por defecto
    s2 = valor razonable por defecto
    % ...
    % y así con todos los valores de salida
    
    % si hace falta un contador, se genera aquí
    k = 1
    
    % es posible que haga falta calcular algunas cosas antes
    % del bucle principal
    
    % AHORA VENDRÁ UN BUCLE while() o for().
    % Por lo general sera un while salvo que se sepa
    % exactamente el numero de veces que se va a hacer algo
    % la última condicion siempre será (si hay contador)
    %       que k <= número maximo de iteraciones,
    %       REPITO: si hubiera contador
    while( CONDICION1 && CONDICION2 ... && CONDICIONn )
    
         % aquí se escribe el "cuerpo del algoritmo",
         % "lo que es el propio algoritmo en sí"
         % (bisección, Newton-Raphson...)
         % aquí habrá ifs, cálculos, asignaciones, todo
         % tipo de cosas
    
    end
    
    % Antes de terminar, hay que verificar que la respuesta
    % es correcta. Por ejemplo, si hay un contador, hay que
    % verificar si se ha pasado el límite. Si esto ha ocurrido,
    % es IMPRESCINDIBILE avisar de que el resultado que se
    % devuelve es incorrecto. Por ejemplo:
    if( k > N) % asumiendo el límite es N
        warning('Numero maximo de iteraciones superado')
    end
    
    % fin del programa: no olvidar el 'end' de aquí abajo

end

