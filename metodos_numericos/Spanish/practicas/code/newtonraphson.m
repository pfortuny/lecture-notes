% Newton-Raphson implementation.
% Returns both the approximate root and the number of
% iterations performed.
% Input:
% f, fp (derivative), x0, epsilon, N
function [z n] = newtonraphson(f, fp, x0, epsilon = eps, N = 50)
  n = 0;
  xn = x0;
  % initialize z to a NaN (i.e. error by default)
  z = NaN; 
  % Both f and fp are anonymous functions  
  fn = f(xn);
  while(abs(fn) >= epsilon & n <= N)
    n = n + 1;  
    fn = f(xn); % memorize to prevent recomputing
    % next iteration
    xn = xn - fn/fp(xn); % an exception might take place here
  end
  z = xn;
  if(n == N)
    warning('Tolerance not reached.');
  end
end
