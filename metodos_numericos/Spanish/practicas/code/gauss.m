function [L, At, bt] = gauss(A,b)
  n = size(A);
  m = size(b);
  if(n(2) ~= m(1))
    warning('The sizes of A and b do not match');
    return;
  end
  At=A; bt=b; L=eye(n);
  k=1;
  while (k<n(1))
    l=k+1;
    if(At(k,k) == 0)
      warning('There is a 0 on the diagonal');
      return;
    end
    % careful with rows & columns: 
    % L(l,k) means ROW l, COLUMN k
    while(l<=n)
      L(l,k)=At(l,k)/At(k,k);
      % Combining rows is easy in Matlab
      At(l,k:n) = [0 At(l,k+1:n) - L(l,k) * At(k,k+1:n)];
      bt(l)=bt(l)-bt(k)*L(l,k);
      l=l+1;
    end 
    k=k+1;
  end 
end
