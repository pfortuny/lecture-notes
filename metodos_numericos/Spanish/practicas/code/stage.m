% A 'stage' of a cycling race, using random slopes.

% Problem setup.
% Notice that there is ONE slope less than x-coordinates!
x  = linspace(0, 200, 25);

% Explanation of the next line: 
%    Take a random value (well, 249 of them) between 0 and 1
%    Scale them so that they are between 0 and .30
%    Subtract .15 so that they are between -.15 and +.15
yp = rand(1,24) * .30 - .15;

% Initial height (choose your own)
y0 = 870;

% Approximate stage profile.
% 1) List of "rises" or "descents"
h  = diff(x).*yp;

% 2) Create the vector of heights, "empty" but for the first one:
y    = zeros(1, 25);
y(1) = y0;

% 3) For each "step" do:
%        Add to the list of heights the last one computed
%               plus the correspoding difference
for s = 1:length(h)
    y(s+1) = y(s) + h(s);
end
% This loop is not 'right', there is a simpler way to do the same:
%  y(2:end) = y(1:end-1) + h;
% Which is more 'Matlab'-ish and, in fact, clearer.

% 4) Finally, plot the profile of the stage
plot(x,y);
