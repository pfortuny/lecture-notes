% Numerical quadrature using the midpoint rule.
% Input: a, b, f, where
%   a, b are real numbers (the endpoints of the integration interval)
%   f    is an anonymous function

function [v] = midpoint(a, b, f)
    v = (b-a).*f((a+b)./2);
end
