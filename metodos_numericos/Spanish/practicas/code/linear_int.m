% Linear interpolation
%
% linear_int(x, y, u):
% given a pair of lists x, y and a vector u of x-coordinates,
% compute the linear interpolation at u corresponding
% to the cloud of points defined by (x,y)
%
% Input: x, y, u
%
% x, y: same-sized vectors describing a 2D cloud of points.
%       x MUST be ordered (otherwise garbage should be expected)
% u:    vector on which to compute the interpolation

function [v] = linear_int(x, y, u)
    v=zeros(1,length(u));
    for k=1:length(u)
        for l=1:length(x)-1
            if(x(l) <= u(k) && u(k) < x(l+1))
                v(k) = (y(l+1)-y(l))./(x(l+1)-x(l))*(u(k)-x(l))+y(l);
                break
            end 
        end
    end
end
