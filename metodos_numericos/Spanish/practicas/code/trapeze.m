% Numerical quadrature using the trapeze rule.
% Input: a, b, f, where
%   a, b are real numbers (the endpoints of the integration interval)
%   f    is an anonymous function

function [v] = trapeze(a, b, f)
    v = (b-a).*(f(a) + f(b))./2;
end
