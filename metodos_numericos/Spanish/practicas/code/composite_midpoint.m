% Numerical quadrature using the composite midpoint rule.
% Input: a, b, f, n=3, where
%   a, b are real numbers (the endpoints of the integration interval)
%   f    is an anonymous function
%   n    is the number of subintervals in which to divide [a,b]

function [v] = composite_midpoint(a, b, f, n)
    l = (b-a)./n;
    % midpoints
    a1 = a + l./2;
    b1 = b - l./2;

    % Can you explain why this is correct?
    m  = linspace(a1, b1, n);
    v = l.*sum(f(m));
end
