% Numerical quadrature using the composite trapeze rule.
% Input: a, b, f, n=3, where
%   a, b are real numbers (the endpoints of the integration interval)
%   f    is an anonymous function
%   n    is the number of subintervals in which to divide [a,b]
%        (defaults to 3)

function [v] = composite_trapeze(a, b, f, n=3)
    l = (b-a)./n;

    % subinterval endpoints
    m  = linspace(a+l, b-l, n-1);

    v = l/2.*(f(a)+f(b)+2*sum(f(m)));
end
