% Numerical quadrature using composite Simpson's rule.
% Input: a, b, f, n=3, where
%   a, b are real numbers (the endpoints of the integration interval)
%   f    is an anonymous function
%   n    is the number of subintervals in which to divide [a,b]
%        (defaults to 3)

function [v] = composite_simpson(a, b, f, n=3)
    l = (b-a)./n;

    % subinterval midpoints
    a1 = a + l./2;
    b1 = b - l./2;
    m  = linspace(a1, b1, n);

    % internal endpoints:
    ep = linspace(a+l, b-l, n-1);

    v = l/6.*(f(a)+f(b)+2*sum(f(ep))+4*sum(f(m)));
end
