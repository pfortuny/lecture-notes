% -*- TeX-master: "practical.ltx" -*-
\chapter{Linear Systems of Equations}\label{cha:linear-systems}
In the Theory classes several algorithms for solving linear systems of
equations have been explained. Their interest is not only for problems
which can, by themselves, be expressed as linear equations (like in
Statics, for example) but, as the student should have already
realized, for solving problems which appear as intermediate steps of
others: mainly, spline interpolation (recall that in order to compute
the cubic spline, solving a tridiagonal system is required), linear
least-squares interpolation. Although we have not covered this in
the course, any attempt to solve a differential equation using
implicit methods or the numerical solution of partial differential
equations lead to linear systems of equations, usually of huge
size (think of thousands and millions of rows).

As the student knows, one can divide the methods of solving linear
systems of equations into two main groups: ``exact'' methods (those
who aim to produce an \emph{exact} solution) and ``iterative''
methods, which use a fixed-point iteration to approximate the solution
step by step.

\section{Exact methods}
The first exact method, and the most useful for small systems (up to
$4$ or $5$ equations) is Gauss's reduction algorithm. This algorithm
will serve us to show some programming techniques in Matlab. One
should be able to reproduce the program from the description of the
algorithm given in the Theory Lecture Notes. We include it for the
sake of completeness in Algorithm \ref{alg:gauss}

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} A square matrix $A$ and a vector
$b$, of order $n$
\STATE \textbf{Output:} Either an error message or a matrix
$\tilde{A}$ and a vector $\tilde{b}$ 
such that $\tilde{A}$ is upper triangular and the system
$\tilde{A}x=\tilde{b}$ has the same solutions as $Ax = b$
\PART{Start}
\STATE $\tilde{A}\leftarrow A, \tilde{b}\leftarrow b, i\leftarrow 1$
\WHILE{$i<n$}
\IF{$\tilde{A}_{ii}=0$}
\STATE \textbf{return} ERROR \comm{division by zero}
\ENDIF
\STATE \comm{combine rows underneath $i$ with row $i$}
\STATE{$j\leftarrow i+1$}
\WHILE{$j \leq n$} 
\STATE $m_{ji}\leftarrow \tilde{A}_{ji}/ \tilde{A}_{ii}$
\STATE \comm{Next line is usually a loop, careful here}
\STATE $\tilde{A}_j \leftarrow \tilde{A}_{j} - m_{ji} \tilde{A}_i$
\,\,\,\comm{*}
\STATE $\tilde{b}_j\leftarrow \tilde{b}_j-m_{ji} \tilde{b}_{i}$
\STATE $j\leftarrow j+1$
\ENDWHILE
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $\tilde{A}, \tilde{b}$
\end{algorithmic}
\caption{Gauss' Algorithm for linear systems}
\label{alg:gauss}
\end{algorithm}

\begin{example}[Implementation of Gauss's reduction method in
  Matlab]\label{ex:gauss-completely-described}
  The code in Listing \ref{lst:gauss} includes an implementation of
  Gauss's reduction method in Matlab.
  \lstinputlisting[language=matlab, caption={Gauss's method in
    Matlab.}, label={lst:gauss}]{code/gauss.m}
  There are some relevant remarks to be made about the code. To begin
  with,
  notice that \texttt{i} and \texttt{j} are \emph{reserved names}
  for Matlab (they both represent the complex root of unity), so our
  code, instead of \texttt{i} and \texttt{j}, uses \texttt{k} and
  \texttt{l}. 
  \begin{itemize}
  \item First of all, the size of the coefficient matrix and the
    independent terms are checked for equality. If they are not, a
    warning is shown and the program finishes. The variable \texttt{n}
    is set to the number of rows of \texttt{A}.
  \item Then the output variables are initialized (this must be done
    at the beginning to prevent strange errors): they are \texttt{At},
    which holds the final upper triangular matrix, \texttt{bt}, which
    holds the final independent terms and \texttt{L}, which is the
    transformation matrix.
  \item Then the main \texttt{while} loop starts (which iterates on
    the rows of \texttt{At}), using the variable \texttt{k}, starting
    at \texttt{k=1} until \texttt{k>=n(1)} (which means, until the
    last-but-one row). Then, for each row:
    \begin{itemize}
    \item The corresponding element \texttt{At(k,k)} is verified to be nonzero.
      If it is zero, the algorithm finishes (there is no
      pivoting done in this program) with an error message.
    \item If the diagonal element is nonzero, then for each row under
      the \texttt{k}-th one (this is the \texttt{while l<=n}),
      \begin{itemize}
      \item The corresponding multiplier is computed and stored in
        \texttt{L(l,k)}: \texttt{At(l,k)/At(k,k)}.
      \item The \texttt{l}-th row of \texttt{At} is updated. The line
\begin{lstlisting}[language=matlab]
      At(l,k:n) = [0 At(l,k+1:n) - L(l,k)*At(k,k+1:n)];
\end{lstlisting}
        does the following: the element \texttt{At(l,k)} is set to
        $0$. Then the elements \texttt{At(l, r)} are set to the
        corresponding value of the combination $\tilde{A}_j\leftarrow
        \tilde{A}_j-m_{ji}\tilde{A}_i$. Notice that the elements to
        the left of \texttt{k} are already zero. Why?
      \item Then the independent terms are updated accordingly.
      \item The counter \texttt{l} is increased by one.
      \end{itemize}
    \item The counter \texttt{k} is increased by one.
    \end{itemize}
  \item There is no ending statement because all the output variables
    have already been computed at this point.
  \end{itemize}
The function thus defined, \texttt{gauss}, returns three values: the
lower triangular matrix \texttt{L} (which contains the multipliers),
the upper triangular matrix \texttt{At}, which is the \emph{reduced}
matrix and the new vector of independent terms \texttt{bt}. In order
to solve the initial system $Ax=b$, one needs only solve the new one
$\tilde{A}x=\tilde{b}$ (where $\tilde{A}$ is \texttt{At} and
$\tilde{b}$ is \texttt{bt}).
\end{example}

\begin{exercise}\label{exer:several-gauss-examples}
  Use the function \texttt{gauss} defined in listing \ref{lst:gauss}
  (which implies your saving it in an adequate file in an adequate
  directory) to compute the reduced form of the following systems of
  equations:
  \begin{table}[h!]
    \centering
    \begin{tabular}[h]{cc}
    $
    \begin{pmatrix}
      1 & 2 & 3 \\
      4 & 5 & 6\\
      7 & 8 & 33
    \end{pmatrix}
    \begin{pmatrix}
      x\\
      y\\
      z
    \end{pmatrix}
    =
    \begin{pmatrix}
      2\\
      7\\
      8
    \end{pmatrix}
    $ &
    $
    \begin{pmatrix}
      2 & -1 & 3\\
      6 & -3 & 2 \\
      0 & 2 & 1
    \end{pmatrix}
    \begin{pmatrix}
      x_1\\
      x_2\\
      x_3
    \end{pmatrix}
    =
    \begin{pmatrix}
      -1\\
      -1\\
      -1
    \end{pmatrix}
    $\\[2em]
    $
    \begin{pmatrix}
      -1 & 2 & 1 & 3\\
      2 & -4 & 4 & 1\\
      0 & 1 & 2 & 3\\
      -1 & -2 & -3 & -4
    \end{pmatrix}
    \begin{pmatrix}
      x\\
      y\\
      z\\
      t
    \end{pmatrix}
    =
    \begin{pmatrix}
      -4\\
      3\\
      -2\\
      1
    \end{pmatrix}
    $ &
    $
    \begin{array}{r@{\;=\;}l}
      2x + 3y + z & 1\\
      z - y & 0\\
      2y + 3x - 2 & z
    \end{array}
    $
  \end{tabular}
\end{table}
\end{exercise}

\begin{exercise}\label{exer:pivoting-gauss}
  Write a function in an \texttt{m}-file of name
  \texttt{gauss\_pivot.m} which implements Gauss's reduction algorithm
  with partial pivoting (i.e. pivoting in rows). The output should
  include at least the upper triangular matrix and the transformed
  vector of independent terms. Bonus points for returning also the
  lower triangular and the permutation  matrices. Recall that this
  algorithm is also called \emph{LUP} factorization. Use this function
  to compute the reduced form of the systems in Exercise
  \ref{exer:several-gauss-examples}. 
\end{exercise}

\begin{exercise}\label{exer:gauss-linear-solution}
  Using either the code from Listing \ref{lst:gauss} or the one you
  produced for Exercise \ref{exer:pivoting-gauss}, write a new
  function which explicitly \emph{solves} a linear system of
  equations using Gauss's method. This requires computing the values
  of each variable \emph{after} the reduction step and returning them
  as a vector. Call that function \texttt{gauss\_solve}, write it in
  an \texttt{m}-file and use it for solving the systems in Exercise
  \ref{exer:several-gauss-examples}.
\end{exercise}


\section{Iterative algorithms}
Iterative algorithms are much simpler to implement, as they only need
an iteration of a simple matrix multiplication. They are also much
faster than exact methods for large systems and can produce
high-quality approximations to the solutions. As a matter of fact, the
name \emph{exact methods} for Gauss-type algorithms is a misnomer, as
rounding errors are always present and floating-point operations are
almost never exact. This makes iterative algorithms as powerful as
those methods and, in real life, much more useful.

Even though they require the computation of the inverse of a matrix,
this does not complicate the methods too much because the matrix to be
inverted is ``simple'' (either a \emph{diagonal} one, in Jacobi's or a
triangular one for
Gauss-Seidel). In what follows, we shall make use of the inversion
utility of Matlab (although, to be honest, we should implement
inversion ourselves, but this would complicate matters too much to
little avail).

\subsection{Jacobi's Method}
Given a linear system of equations
\begin{equation}\label{eq:system-for-jacobi}
  Ax=b
\end{equation}
Jacobi's method consists in transforming it into a fixed-point problem
by rewriting $A$ as the sum of a diagonal matrix and another one. Let
$D$ be the diagonal matrix whose diagonal coincides with that of
$A$. Then $A=D+N$, where $N$ has only $0$ on the diagonal. System
\eqref{eq:system-for-jacobi} is then
\begin{equation*}
  (D+N)x=b
\end{equation*}
If $D$ is invertible, we can rewrite this as
\begin{equation*}
  D^{-1}(D+N)x = D^{-1}b,
\end{equation*}
which, expanding the left hand side gives
\begin{equation*}
  x+D^{-1}Nx = D^{-1}b
\end{equation*}
and finally, moving the second term of the addition to the right hand
side, we get
\begin{equation*}
  x = D^{-1}b - D^{-1}Nx.
\end{equation*}
This means that, if we call $f$ to the transformation
\begin{equation*}
  f(x) = D^{-1}b - D^{-1}Nx,
\end{equation*}
system \eqref{eq:system-for-jacobi} is equivalent to the
fixed-point problem
\begin{equation*}
  f(x) = x.
\end{equation*}

It is known that, under certain conditions, given \emph{any} initial
seed $x_0$, the iteration $x_1=f(x_0),$ $x_i=f(x_{i-1})$ converges to
a solution of the problem. This means that the following steps are a
correct description of Jacobi's method.
\begin{enumerate}
\item Set $x_0$ to any vector.
\item Let $k=1$, $N$ be a bound for the number of steps and $\epsilon$
  a tolerance.
\item Let $x_1=D^{-1}b-D^{-1}Nx_0$
\item While $k<N$ and $\norm{x_k-x_{k-1}}>\epsilon$ do:
  \begin{itemize}
  \item $k=k+1$
  \item $x_k=D^{-1}b-D^{-1}Nx_{k-1}$.
  \end{itemize}
\item If $k==N$ finish with an error (tolerance not reached),
  otherwise return $x_k$.
\end{enumerate}

\begin{exercise}\label{exer:jacobi}
  Implement Jacobi's method using the rough description above. Call
  the file \texttt{jacobi.m} and use it to solve the systems of
  Exercise \ref{exer:several-gauss-examples}. Compare the approximate
  solutions obtained for different tolerances with those of Exercise
  \ref{exer:gauss-linear-solution}.

  \textbf{Remark:} Notice that both $N$ and $\epsilon$ are required as
  input to the function \texttt{jacobi} and that default values should
  be given for them.
\end{exercise}

\begin{exercise}\label{exer:random-jacobi}
  Use the \texttt{rand} function to create a $100\times 100$ matrix
  \texttt{A} and a $100\times 1$ vector $b$. Use the \texttt{gauss} and
  \texttt{jacobi} functions of exercises \ref{exer:gauss-linear-solution} and
  \ref{exer:jacobi} to compute two solutions, \texttt{x1} and
  \texttt{x2}. Compare them between themselves and compare them to any
  solution found using Matlab's solver. Which do you think is better?
\end{exercise}

\subsection{The Gauss-Seidel Method}
The Gauss-Seidel method is very similar to Jacobi's. Instead of
transforming System \eqref{eq:system-for-jacobi} using the diagonal of
$A$, one takes its lower triangular part and writes
\begin{equation*}
  (L+M)x=b
\end{equation*}
where $L$ is lower triangular and $M$ is upper triangular with $0$ on
its diagonal. If $L$ is invertible, this equation can be rewritten
\begin{equation*}
  L^{-1}(L+M)x = L^{-1}b,
\end{equation*}
which, after transformations similar to those made for Jacobi's
method, turns into
\begin{equation*}
  x = L^{-1}b - L^{-1}Mx,
\end{equation*}
which is a fixed-point problem.

\begin{exercise}\label{exer:implement-gauss-seidel}
  Implement the Gauss-Seidel method using a file called
  \texttt{gauss\_seidel.m}. Use it to solve the systems of Exercise
  \ref{exer:several-gauss-examples} and compare your solutions with
  those of exercises \ref{exer:gauss-linear-solution} and
  \ref{exer:jacobi}.
\end{exercise}

\begin{exercise}\label{exer:random-gauss-seidel}
  Use the \texttt{random} utility of Matlab to create a $100\times
  100$ matrix \texttt{A} and a $100\times 1$ vector \texttt{b}. Use
  the \texttt{gauss\_seidel} function of Exercise
  \ref{exer:implement-gauss-seidel} to compute a solution \texttt{x1}
  and the \texttt{jacobi} function of Exercise \ref{exer:jacobi} to
  compute another one \texttt{x2}. Compare them. Which is better?
\end{exercise}





