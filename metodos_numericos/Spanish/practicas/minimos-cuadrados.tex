% -*- TeX-master: "practicas.ltx" -*-
\chapter{Interpolación por mínimos cuadrados}
Dada una nube $C$ de $N$ puntos y un \emph{espacio vectorial} $V$
de funciones que ``representan adecuadamente la nube de puntos'', el
problema de encontrar la \emph{mejor} aproximación a la nube de puntos
dentro de las funciones de $V$ se puede comprender de diversos
modos. El más común es la \emph{aproximación por mínimos cuadrados},
que se explicó en la teoría y que puede enunciarse así:

Sean $(x_1,y_1), \dots, (x_N, y_N)$ los puntos de la nube $C$ y sea
$\left\{f_1, \dots, f_n \right\}$ una base de $V$. El \emph{problema
  de interpolación por mínimos cuadrados} para $C$ y $V$ consiste en
encontrar los coeficientes $a_1, \dots, a_n$ tales que el valor
\begin{equation*}
  E(a_1,\dots,a_n) = \sum_{i=1}^N (a_1f_1(x_i)+\dots+a_nf_n(x_i) - y_i)^2
\end{equation*}
es mínimo (ese valor se llama \emph{error cuadrático total} y,
obviamente, depende de los coeficientes).

Hay diversas maneras de resolverlo. Quizás la más sencilla sea
utilizar cálculo diferencial en varias variables. Al final, los
coeficientes  $a_1,
\dots, a_n$ se calculan como la solución del sistema de ecuaciones
lineales: 
\begin{equation}\label{eq:system-for-least-squares}
  X X^t
  \begin{pmatrix}
    a_1\\
    \vdots\\
    a_n
  \end{pmatrix}
  = X
  \begin{pmatrix}
    y_1\\
    \vdots\\
    y_N
  \end{pmatrix}
\end{equation}
(nótese que ambos miembros de la igualdad terminan siendo vectores
columna de $n$ componentes). La matriz $X$ es
\begin{equation*}
  X=
  \begin{pmatrix}
    f_1(x_1) & f_1(x_{2}) & \dots & f_1(x_N) \\
    \vdots & \vdots & \ddots & \vdots \\
    f_n(x_1) & f_2(x_2) & \dots & f_n(x_N)
  \end{pmatrix}
\end{equation*}
y $X^t$ es su traspuesta. La ecuación
\eqref{eq:system-for-least-squares} siempre (en condiciones muy
generales) da un sistema compatible, pero habitualmente está mal
condicionado. 

Así pues, el problema de interpolación por mínimos cuadrados parte de
un conjunto de $N$ puntos (la nube) y una familia de $n$ funciones
(con $n<N$, de hecho $N$ suele ser muy grande y $n$ muy pequeño).


\begin{example}
  El ejemplo más sencillo es la interpolación de una nube de puntos
  mediante una función lineal (la llamada \emph{recta de
    regresión}). Las funciones lineales tienen la forma
  $a+bx$, así que el espacio vectorial que se estudia está generado
  por $f_1(x)=1$ y $f_2(x)=x$ (y por tanto hay dos valores que
  calcular, la $a$ y la $b$).
  Tómese por ejemplo la nube dada por $(1,2), (2, 2{.}1), (3,2{.}15),
  (4,2{.}41), 
  (5, 2{.}6)$. Es fácil darse cuenta de que la recta de regresión será
  más o menos $y=0{.}1(x-1)+2=1{.}9+0{.}1x$ (puesto que la pendiente
  es más o menos $0{.}1$ y la línea pasará más o menos poor $(1,2)$).
  Usando Matlab para resolver este problema:
  \begin{lstlisting}[language=matlab]
    > x=[1 2 3 4 5];
    > y=[2 2.1 2.15 2.41 2.6];
    > f1=@(x) 1 + x.*0;
    > f2=@(x) x;
    > X=[f1(x); f2(x)]
    X =

    1 1 1 1 1 1 2 3 4 5

    > A=X*X'
    A =

    5 15 15 55

    > Y=X*y'
    Y =

    11.260 35.290

    > coefs=A\Y
    coefs =

    1.79900 0.15100
\end{lstlisting}
  Por tanto, la función lineal (recta) que interpola la nube de puntos
  por mínimos cuadrados es $y=1{.}799 + 0{.}151x$, que está de acuerdo
  con nuestra suposición.
\end{example}

\begin{remark*}
  Hay que tener cuidado con la definición de la función constante
  $f_1(x)=1$, en Matlab: hay que hacerla ``vectorial'' añadiéndole,
  por ejemplo, un vector nulo (en este caso, $0.*x$). Esto es un apaño
  para un defecto de Matlab, no algo mágico.
\end{remark*}


  \begin{table}
    \centering
    \begin{tabular}[h]{r|r}
      $X$ & $Y$ \\
      \hline
      1.0 &  6.20 \\
      1.5 &  9.99 \\
      2.0 &  15.01 \\
      2.5 &  23.23 \\
      3.0 &  32.70 \\
      3.5 &  43.08 \\
      4.0 &  54.01 \\
      4.5 &  65.96 \\
      5.0 &  80.90
    \end{tabular}
    \caption{Datos que siguen una fórmula cuadrática.}
    \label{tab:quadratic-data}
  \end{table}

\begin{example}\label{ex:quadratic-table}
  Los datos de la Tabla \ref{tab:quadratic-data} vienen de un
  experimento. Se sabe que la variable  $Y$ depende cuadráticamente de
  la variable $X$ (es decir, hay una fórmula de grado dos que
  relaciona $Y$ con $X$). Usando mínimos cuadrados, encuéntrense los
  coeficientes que mejor ajustan esta nube.

  Como se sabe que $Y$ depende de $X$ como un polinomio de grado $2$,
  el espacio vectorial $V$ de funciones está generado por $1, x,
  x^2$. Sean 
  $f_1=1, f_2=x, f_3=x^2$. Se puede usar Matlab como sigue para
  resolver el problema:
  \begin{lstlisting}[language=matlab]
    > x=[1:.5:5];
    > y=[6.2 9.99 15.01 23.23 32.7 43.08 54.01 65.96
    80.9];
    > % definimos las funciones:
    > f1=@(x) 1+0.*x;
    > f2=@(x) x;
    > f3=@(x) x.^2;
    > % main matrix
    > X=[f1(x); f2(x); f3(x)];
    > A=X*X';
    > Y=X*y';
    > % the system is A*a'=Y, use matlab to solve it:
    > A\Y ans =

    0.55352 2.27269 2.75766
    > % this means that the least squares interpolating
    > % polynomial of degree 2 is
    > % 0.55352 + 2.27269*x + 2.75766*x.^2
    > % plot both the cloud of points and the polynomial
    > plot(x,y,'*');
    > hold on
    > u=[1:.01:5];
    > plot(u, 0.55352 + 2.27269.*u + 2.75766*u.^2, 'r')
\end{lstlisting}
  Obsérvese cómo el polinomio interpolador por mínimos cuadrados
  \emph{no pasa por todos los puntos de la nube} (de hecho, es muy
  posible que no pase por ninguno).
\end{example}

\begin{remark}\label{rem:not-only-polynomials}
  El problema de interpolación por mínimos cuadrados no tiene por qué
  ser solo de encontrar polinomios. Puede haber funciones más
  complejas que aproximen la nube de puntos, pero siempre habrá de
  tratarse de un problema \emph{lineal}: si no, la técnica expuesta no
  sirve.
\end{remark}

\begin{exercise}\label{exer:cubic-least-squares}
  Un nuevo desarrollo teórico muestra que los datos de la Tabla
  \ref{tab:quadratic-data} se describen mejor mediante una función de
  grado $3$. Úsese la interpolación por mínimos cuadrados para
  calcular el polinomio cúbico que mejor se ajusta a la nube. ¿Se
  parecen los coeficientes de grados $0, 1$ y $2$ de este polinomio a
  los del Ejemplo \ref{ex:quadratic-table}? ¿Es el ajuste mejor o peor?

  Este ejercicio es interesante porque muestra que, por lo general,
  usar un polinomio para interpolar una nube de puntos, no es una
  buena idea, \emph{salvo que haya una explicación clara} de por qué
  la nube está relacionada con un polinomio (como en los ejemplos y
  ejercicios que siguen).
\end{exercise}

\begin{exercise}\label{exer:log-lin-exp}
  La Tabla \ref{tab:log-lin-exp-data} representa datos de un
  experimento. Se sabe que siguen una función de la forma
  $y(x)=a\log(x)+bx+ce^x$, para ciertos $a, b$ y $c$. Usando mínimos
  cuadrados, calcular estos $a, b$ y $c$.
  \begin{table}
    \centering
    \begin{tabular}[h]{r|r}
      $X$ & $Y$ \\
      \hline
      2.0 & 11.39\\
      2.7 &  15.31\\
      3.4 &  18.18\\
      4.1 &  19.80\\
      4.8 &  19.76\\
      5.5 &  15.03\\
      6.2 &   1.74\\
      6.9 & -29.67\\
    \end{tabular}
    \caption{Datos que siguen una fórmula compleja.}
    \label{tab:log-lin-exp-data}
  \end{table}
\end{exercise}

\begin{exercise}\label{exer:anscombe-quartet}
  La Tabla \ref{tab:anscombe-quartet} contiene lo que se conoce como
  el \emph{Cuarteto de Anscombe}: es una lista de datos interesante
  por las propiedades que comparten las cuatro colecciones. Nosotros
  nos fijaremos solo en la recta de regresión $a+bx$ para cada nube de
  puntos. Calcúlese (hay cuatro nubes de puntos, por tanto, cuatro
  rectas de regresión).
  \begin{table}
    \centering
    \begin{tabular}[h]{r|r@{\hspace{25pt}}r|r@{\hspace{25pt}}r|r@{\hspace{25pt}}r|r}
      X  &  Y     &  X  &  Y     & X   &  Y     &  X  &  Y\\
      \hline
      10  &  8.04  & 10  &  9.14  & 10  &  7.46  &  8  &  6.58\\
      8  &  6.95  &  8  &  8.14  &  8  &  6.77  &  8  &  5.76\\
      13  &  7.58  & 13  &  8.74  & 13  & 12.74  &  8  &  7.71\\
      9  &  8.81  &  9  &  8.77  &  9  &  7.11  &  8  &  8.84\\
      11  &  8.33  & 11  &  9.26  & 11  &  7.81  &  8  &  8.47\\
      14  &  9.96  & 14  &  8.10  & 14  &  8.84  &  8  &  7.04\\
      6  &  7.24  &  6  &  6.13  &  6  &  6.08  &  8  &  5.25\\
      4  &  4.26  &  4  &  3.10  &  4  &  5.39  & 19  & 12.50\\
      12  & 10.84  & 12  &  9.13  & 12  &  8.15  &  8  &  5.56\\
      7  &  4.82  &  7  &  7.26  &  7  &  6.42  &  8  &  7.91\\
      5  &  5.68  &  5  &  4.74  &  5  &  5.73  &  8  &  6.89
    \end{tabular}
    \caption{Cuarteto de Anscombe.}
    \label{tab:anscombe-quartet}
  \end{table}
  Una vez calculadas las cuatro rectas, dibújense las cuatro nubes de
  puntos y las rectas (todo en la misma figura) y compárense. ¿Qué se
  puede deducir de esto?
\end{exercise}

\begin{exercise}\label{exer:mass-velocity-energy}
  Un experimento sirve para calcular el valor de la energía cinética a
  partir de la velocidad de un objeto en movimiento. La Tabla
  \ref{tab:kinetic-velocity} muestra los resultados de llevar a cabo dicho
  experimento $5$ veces con el mismo objeto.
  Dése un valor razonable para la masa del objeto.
  \begin{table}
    \centering
    \begin{tabular}[h]{r|r}
      $v$ & $E$\\
      \hline
      1.0 &  8.05\\
      1.5 & 16.97\\
      2.3 & 39.69\\
      2.7 & 55.58\\
      3.0 & 66.91\\
    \end{tabular}
    \caption{Energía cinética frente a velocidad para el mismo objeto.}
    \label{tab:kinetic-velocity}
  \end{table}
  La velocidad se da en m/s y la energía en julios.
\end{exercise}

\begin{exercise}\label{exer:uniformly-accelerated-motion}
  La Tabla \ref{tab:unif-accel-motion} lista los valores del cómputo
  de la distancia recorrida por un objeto en un tiempo determinado. Se
  sabe que dicho objeto sigue un movimiento uniformemente acelerado
  que comienza con la misma velocidad inicial. Calcúlense valores
  razonables para esta velocidad inicial y para la acelaración.
  \begin{table}[h]
    \centering
    \begin{tabular}[h]{r|r}
      $t$ (s) & $d$ (m)\\
      \hline
      1 & 4.89\\
      2 & 11.36\\
      3 & 21.64\\
      4 & 34.10\\
      5 & 50.05\\
      6 & 69.51
    \end{tabular}
    \caption{Distancia frente a tiempo de un m.u.a.}
    \label{tab:unif-accel-motion}
  \end{table}
\end{exercise}

% \begin{exercise}\label{exer:seasonality}
%   The following plot shows the mean price of a can of beer (in
%   pesetas) at each month from 1960 to 1980 (there are 240 values in
%   the series). The plot has two remarkable properties: on the one
%   hand, it ``seems to increase with time,'' on the other, the price
%   has a wavy behavior (with local lows and highs at intervals of the
%   same length). As a matter of fact, minima and maxima happen
%   (approximately) with a year of difference. This behavior is called
%   ``seasonality'' (the season of the year affects the price of
%   commodities: in this case, beer is drunk more frequently in the
%   Summer, as people are more thirsty, and prices tend to be higher
%   than in the Winter). How would you model this graph in order to find
%   a ``suitable'' fitting function?
  
%   \begin{figure}[h]
%     \centering
%     \begin{tikzpicture}
%       \begin{axis}[ xmin=0, xmax=241, ylabel=Price (\euro),
%         xlabel=Year, xtick={6,54, 114, 174, 234}, xticklabels={1961,
%           1965, 1970, 1975, 1980}]
%         \addplot[color=olive, line width=1.5pt] file
%         {code/prices.dat};
%       \end{axis}
%     \end{tikzpicture}
%   \end{figure}
  
%   One has to be aware that \emph{prices are always modeled with
%     products, not with additions}: prices increase by a rate, not by a
%   fixed amount (things are ``more or less expensive'' in rate, you
%   would not complain of a computer costing \EUR{5} more but you would
%   if the loaf of bread had that same increase in price). So, the
%   relevant data should not be that in the graph but its logarithm
%   (which increases and decreases in absolute values, not relative
%   ones). Hence, one should aim at a least-squares interpolation of the
%   logarithm of the true values\footnote{Despite not being completely
%     correct, the fact that the values are much larger than $0$ makes
%     using logarithms and fitting the new curve with least squares
%     useful, albeit inexact.}.

%   Once logarithms are taken, the curve should be fit (interpolated) as
%   a linear function with a seasonal (yearly) modification: how would
%   you perform this interpolation? (One needs to use the sine and
%   cosine function, but how?). What functions would you use as basis?
%   Why?

%   The data can be found at \texttt{http://pfortuny.net/prices.dat}.
% \end{exercise}


