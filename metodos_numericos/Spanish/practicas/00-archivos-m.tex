% -*- TeX-master: "practicas.ltx" -*-
\chapter[Funciones y archivos \texttt{m}]{Introducción a las funciones
  y los archivos \texttt{.m}}\label{cha:primer-m-functions}
En este curso vamos a utilizar con frecuencia la manera habitual de
``programar'' en Matlab/Octave, que es mediante \emph{archivos}
\texttt{.m} y funciones definidas en ellos.

Un archivo \texttt{.m} no es más que un archivo ordinario cuyo nombre
termina en \texttt{.m} y que contiene una lista de instrucciones de
Matlab/Octave con una estructura precisa. El código del listado
\ref{lst:simple-m-file} se puede incluir en un archivo
\texttt{.m} que simplemente realizará una secuencia de instrucciones:

\begin{lstlisting}[language=matlab,label=lst:simple-m-file,
caption={Un archivo \texttt{.m} simple}]
% Un archivo elemental
e = exp(1);
b = linspace(-2,2,1000);
plot(b, e.\^b)
\end{lstlisting}

La potencia de los archivos \texttt{.m} se debe a dos propiedades:
\begin{itemize}
\item Si se graban en un directorio directamente accesible a
  Matlab/Octave (por ejemplo, \texttt{Mis Documentos/Matlab}),
  se puede ejecutar un comando cuyo nombre sea el mismo que el de un
  fichero de ese directorio (sin la extensión \texttt{.m}) y
  Matlab/Octave hará las instrucciones pertinentes. Por ejemplo, si se
  ha grabado el archivo \texttt{ejemplo1.m} del código de
  \ref{lst:simple-m-file}, entonces puede ejecutarse la línea
\begin{lstlisting}[language=matlab]
> trial1
\end{lstlisting}
  que dibujará la función $e^x$ para $x\in[-2,2]$ usando $1000$
  puntos.
\item Se pueden usar para definir funciones complicadas.
\end{itemize}

En lugar de una mera secuencia de comandos, se pueden definir
un programa (``una función'', suele decirse) dentro de un archivo
\texttt{.m}. Considérese el listado
\ref{lst:first-m-function}, que define una función que devuelve los
dos máximos valores de una lista, en orden creciente.

La estrucutra es la siguiente:
\begin{enumerate}
\item Varias líneas de comentarios (las que comienzan con
  \texttt{\%}). Se utilizan para describir el programa definido
  después. 
\item Una línea como
\begin{lstlisting}[language=matlab]
function [y1, y2,...] = nombreFun(p1, p2,...)
\end{lstlisting}
  donde la palabra \texttt{function} aparece justo al principio,
  después una lista de nombres (entre \emph{corchetes}, \texttt{[]}),
  que designan las \emph{variables de salida}, un signo igual
  \texttt{=}, después el \emph{nombre de la función}, en este caso
  \texttt{nombreFun}, y una lista de nombres, que son los
  \emph{parámetros de entrada}, entre paréntesis.
\item Luego viene una sucesión de líneas de comandos de Matlab, que
  implementan justamente lo que hace la función (esto sería el
  programa en sí).
\item La palabra clave \texttt{end} al final.
\item Por último, es esencial que el nombre del archivo sea el mismo
  que el de la función junto con la extensión \texttt{.m}. Por
  ejemplo, para el listado \ref{lst:first-m-function}, el nombre del
  archivo \emph{debe ser} \texttt{max2.m}.
\end{enumerate}

Un archivo que siga todas estas reglas definirá un comando de
Matlab/Octave nuevo que se puede utilizar como cualquier otro.

\begin{lstlisting}[language=matlab,label=lst:first-m-function,
caption={Un primer fichero \texttt{.m} que define una función. Grábese
como \texttt{max2.}}]
% max2(x)
% devuelve los 2 valores máximos de x, en orden creciente
% si solo hay un valor (length(x) == 1), se devuelve [-inf, x].

function [y] = max2(x)

  % inicializar: el primer elemento es siempre -inf
  y = [-inf, x(1)];

  % si solo hay un número, ya hemos terminado
  if(length(x) == 1)
    return;
  end

  % para cada elemento, solo hay que hacer algo si es mayor que y(1)
  for X=x(2:end)
    if(X > y(1))
      if(X > y(2))
        y(1) = y(2);
        y(2) = X;
      else
        y(1) = X;
      end
    end
  end

end
\end{lstlisting}

Por ejemplo, si se guarda el código del listado
\ref{lst:first-m-function} en un archivo de nombre \texttt{max2.m}
dentro del directorio
\texttt{Mis Documentos/MATLAB}, entonces se puede ejecutar la
siguiente cadena de comandos:

\begin{lstlisting}[language=matlab]
> x = [-1 2 3 4 -6 9 -8 11]
x =

   -1    2    3    4   -6    9   -8   11

> max2(x)
ans =

    9  11
\end{lstlisting}

que muestra cómo se ha definido una nueva función llamada
\texttt{max2} que, cuando se corre, ejecuta las instrucciones del
archivo \texttt{max2.m}.

Como se van a utilizar archivos \texttt{.m} y funciones definidas en
ellos con bastante frecuencia, se sugiere al estudiante que haga
tantos ejemplos como le sea posible. La programación en Matlab/Octave
no es para nada más compleja que la de Python (y en muchos casos, es
más sencilla, por el carácter vectorial del lenguaje).

\begin{remark}\label{rem:programming-constructs}
  Los principales elementos de programación que se requerirán están
  incluidos en la hoja de consulta rápida y son los
  siguientes:\marginpar{Where is this in Spanish?}
  \begin{itemize}
  \item El enunciado \texttt{if...else}. Tiene la sintaxis siguiente:
\begin{lstlisting}[language=matlab]
if CONDICIÓN
... % sucesión de comandos si se cumple CONDICIÓN
else
... % sucesión de comandos si NO se cumple CONDICIÓN 
end
\end{lstlisting}
    Hay más posibilidades (con \texttt{elseif}) pero no vamos a entrar
    en demasiados detalles.
  \item El bucle \texttt{while}. Sintaxis:
\begin{lstlisting}[language=matlab]
while CONDICIÓN
... % sucesión de comandos si se cumple CONDICIÓN
end
\end{lstlisting}
    que ejecutará los comandos internos mientras (while) se cumpla la
    condición. 
  \item El bucle \texttt{for}. Sintaxis:
\begin{lstlisting}[language=matlab, inputencoding=utf8]
for var=LIST
... % sucesión de comandos
end
\end{lstlisting}
    asignará en la variable \texttt{var} secuencialmente cada elemento
    de la lista \texttt{LIST} y ejecutará los comandos internos del
    bucle hasta que se termine la lista.
  \item Expresiones lógicas. Las \texttt{CONDICIÓN}es (de \texttt{if}
    y \texttt{while}) pueden ser expresiones simples (como
    \texttt{x<3}) o complejas (construidas utilizando los operadores
    \texttt{and}, \texttt{or} y \texttt{not}, que se escriben como
    \texttt{\&\&}, \texttt{||} y \texttt{~}).
  \end{itemize}
\end{remark}

\begin{exercise}
  Impleméntese una función \texttt{min3} que, dada una lista de
  números como entrada, devuelva los \emph{tres} elementos
  mínimos. Úsese la función \texttt{max2} definida arriba como guía.
\end{exercise}

\begin{exercise}\label{exer:increases}
  Impleméntese una función \texttt{es\_creciente} que, dada una lista
  como entrada, devuelve un \texttt{1} si la lista está en orden no
  decreciente y un \texttt{0} si no. ¿Cómo lo harías?
\end{exercise}

\begin{exercise}\label{exer:increases-enhanced}
  Mejórese la función del Ejercicio \ref{exer:increases} para que
  devuelva dos valores: primero, el mismo que la función
  \texttt{es\_creciente}, y como segundo valor, la longitud de la
  ``secuencia creciente'' más larga que haya al principio de los
  valores de entrada. Llámese a la función
  \texttt{es\_creciente2}. Por ejemplo, podría usarse así:
\begin{lstlisting}[language=matlab]
> [a,b] = es_creciente2(7, -1, 2, 3 4)
  a = 0
  b = 1
> [u,v] = increases2(-1, 2, 5, 8, 9)
  u = 1
  v = 5
> [a,b] = increases2(3, 4, 5, -6, 8, 10)
  a = 1
  b = 3
\end{lstlisting}
\end{exercise}

\begin{exercise}\label{exer:positive}
  Defínase una función llamada \texttt{positiva} que, dada como
  entrada una lista de números, devuelve dos valores: el número de
  elementos positivos de la lista (estrictamente mayores que $0$) y la
  lista de dichos elementos como segundo valor de salida. Ejemplos de
  uso:
\begin{lstlisting}[language=matlab]
> [a,b] = positiva(-2, 3, 4, -5, 6, 7, 0)
  a = 4
  b = [3 4 6 7]
> [a,b] = positiva(-1, -2, -3, -exp(1), -pi)
  a = 0
  b = []
> [a,b] = positiva(4, 2, 1, 3 2)
  a = 5
  b = [4 2 1 3 2]
\end{lstlisting}
  ¿Usarías un bucle \texttt{while} o un \texttt{for}? ¿Por qué?
\end{exercise}

Obsérvese que para las funciones que devuelven varios valores de
salida, si se quiere que devuelvan más de uno, hay que pedirlo
explícitamente asignando una lista de variables,c omo en el Ejercicio
\ref{exer:positive}. Si solo se quiere un valor (el primero de la
lista de salida), se llama con normalidad:
\begin{lstlisting}[language=matlab]
> positiva(1, 2, -2, 0, 4)
  3
> x = positiva(-2, 1, 3, 7, 2)
  x = 4
\end{lstlisting}
pero si se quiere que devuelva los dos, hay que hacer una asignación
con corchetes:
\begin{lstlisting}[language=matlab]
> [n x] = positiva(pi, -2, 3, -5, 0)
  n = 2
  x = [pi 3]
\end{lstlisting}

Si la función se llama sin asignar el valor de salida, solo devuelve
el primero. Este valor es habitualmente el más importante, pero hay
ocasiones en que uno requiere acceder a más parámetros de salida.