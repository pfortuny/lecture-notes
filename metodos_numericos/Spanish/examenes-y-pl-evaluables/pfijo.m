function [r] = pfijo(f, c, N, e)
         r = c;
         k = 1;
         while(abs(f(r) -r)>e && k < N)
              r = f(r);
              k = k + 1;
         end
end
