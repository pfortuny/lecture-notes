function [r] = secante(f, x0, x1, E)
  r = [x0 x1];
  while abs(f(r)) > E
    temp = x0;
    x1 = r(end);
    r = [r, x1 - f(x1) * (x0 - x1)./(f(x0) - f(x1))];
    x0 = temp;
  end
end
