function [r] = falsi(f, a, b, E)
  r = [a - f(a).*(b-a)./(f(b)-f(a))];
  while abs(f(r)) > E
    if(f(a)*f(r) < 0)
      b = r(end);
    else
      a = r(end);
    end
    r = [r, a - f(a).*(b-a)./(f(b)-f(a))];
  end
end
