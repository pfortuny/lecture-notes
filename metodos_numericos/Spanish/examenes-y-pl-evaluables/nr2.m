function [r, l] = nr2(f, fp, x0, n, E)
N = 0;
l = [x0];
r = l(end);
while N < n && abs(f(r)) > E
   N = N + 1;
   r = r - f(r)/fp(r);
   l = [l r];    
end
end
