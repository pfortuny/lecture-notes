% -*- TeX-master: "notas.ltx" -*-
\chapter{Ecuaciones no lineales: soluciones num\'ericas}
\label{chap:ecuaciones-no-lineales}

Se estudia en este cap\'{\i}tulo el problema de resolver una
ecuaci\'on no lineal de la forma $f(x)=0$, dada la funci\'on
$f$ y ciertas condiciones inciales.

Cada uno de los algoritmos que se estudia tiene sus ventajas e
inconvenientes; no hay que desechar ninguno a priori simplemente
porque ``sea muy lento'' ---esta es la tentaci\'on f\'acil al estudiar
la convergencia de la bisecci\'on. Como se ver\'a, el ``mejor''
algoritmo de los que se estudiar\'an ---el de Newton-Raphson--- tiene
dos pegas importantes: puede converger (o incluso no hacerlo) lejos de la condici\'on inicial
(y por tanto volverse in\'util si lo que se busca es una ra\'{\i}z
``cerca'' de un punto) y adem\'as, requiere computar el valor de la
derivada de $f$ en cada iteraci\'on, lo cual puede tener un coste
computacional excesivo.

Antes de proseguir, n\'otese que todos los algoritmos se describen con
condiciones de parada \emph{en funci\'on del valor de $f$}, no con
condiciones de parada tipo \emph{Cauchy}. Esto se hace para
simplificar la exposici\'on. Si se quiere garantizar un n\'umero de
decimales de precisi\'on, es necesario o bien evaluar cambios de signo
cerca de cada punto o bien hacer un estudio muy detallado de la
derivada, que est\'a m\'as all\'a del alcance de este curso a mi
entender.\margin{Por tanto, a\~nadir la precisi\'on con evaluaci\'on
  en agl\'un lugar.}


\section{Introducci\'on}
Calcular ra\'{\i}ces de funciones ---y especialmente de polinomios---
es uno de los problemas cl\'asicos de las Matem\'aticas. Hasta hace unos
doscientos a\~nos se pensaba que cualquier polinomio pod\'{\i}a
``resolverse algebraicamente'', es decir: dada una ecuaci\'on
polin\'omica $a_{n}x^{n}+\dots+a_{1}x+a_{0}=0$ habr\'{\i}a una
f\'ormula ``con radicales'' que dar\'{\i}a todos los ceros de ese
polinomio, como la f\'ormula de la ecuaci\'on de segundo grado pero
para grado cualquiera. La Teor\'{\i}a de Galois mostr\'o que esta idea
no era m\'as que un bonito sue\~no: el polinomio general de quinto
grado no admite una soluci\'on en funci\'on de radicales.

De todos modos,  buscar una \emph{f\'ormula cerrada}
para resolver una ecuai\'on no es m\'as que una manera de postponer el
problema de la aproximaci\'on, pues al fin y al cabo (y esto es
importante):
\begin{center}
  Las \'unicas operaciones que se pueden hacer con precisi\'on total siempre
  son la suma, la resta y la multiplicaci\'on.
\end{center}
Ni siquiera es posible dividir y obtener resultados
exactos\footnote{Podr\'{\i}a argumentarse que si se dividen n\'umeros
  racionales se puede escribir el resultado como decimales
  peri\'odicos, pero esto sigue siendo ``postponer el problema''.}.

En fin, es l\'ogico buscar maneras lo m\'as precisas y r\'apidas
posibles de resolver ecuaciones no lineales y que no requieran
operaciones mucho m\'as complejas que la propia ecuaci\'on (claro).

Se distinguiran en este cap\'{\i}tulo dos tipos de algoritmos: los
``geo\-m\'e\-tri\-cos'', por lo que se entienden aquellos que se basan en una
idea geom\'etrica simple y el ``de punto fijo'', que requiere
desarrollar la idea de \emph{contractividad}.

Antes de proseguir, hay dos requerimientos esenciales en cualquier
algoritmo de b\'usqueda de ra\'{\i}ces:
\begin{itemize}
\item Especificar una \emph{precisi\'on}, es decir, un $\epsilon>0$
  tal que si $\abs{f(c)}< \epsilon $ entonces se asume que $c$ es una
  ra\'{\i}z. Esto se debe a que, al utilizar un n\'umero finito de
  cifras, es posible que \emph{nunca} ocurra que $f(c)=0$, siendo $c$
  el valor que el algoritmo obtiene en cada paso\margin{Ejemplo de las
  cuentas del a\~no pasado}.
\item Incluir una \emph{condici\'on de parada}. Por la
  misma raz\'on o por fallos del algoritmo o porque la funci\'on no
  tiene ra\'{\i}ces, podr\'{\i}a ocurrir que nunca se diera que
  $\abs{f(c)}<\epsilon $. Es \emph{imprescindible} especificar un
  momento en que el algoritmo se detiene, \emph{de manera
    determinista}. En estas notas la condici\'on de parada ser\'a
  siempre un n\'umero de repeticiones del algoritmo, $N>0$: si se
  sobrepasa, se devuelve un ``error''\margin{Ditto here}.
\end{itemize}

\section{Algoritmos ``geom\'etricos''}
Si suponemos que la funci\'on $f$ cuya ra\'{\i}z se quiere calcular es
continua en un intervalo compacto $[a,b]$ (lo cual es una suposici\'on
razonable), el Teorema de Bolzano puede ser de utilidad, si es que sus
hip\'otesis se cumplen. Recordemos:

\begin{theorem*}[Bolzano]
  Sea $f:[a,b]\rightarrow {\mathbb R}$ una funci\'on continua en
  $[a,b]$ y tal que $f(a)f(b)<0$ (es decir, cambia de signo entre $a$
  y $b$). Entonces existe $c\in (a,b)$ tal que $f(c)=0$.
\end{theorem*}

El enunciado afirma que si $f$ cambia de signo en un intervalo
cerrado, entonces al menos hay una ra\'{\i}z. Se podr\'{\i}a pensar en
muestrear el intervalo (por ejemplo en anchuras $(b-a)/10^{i}$) e ir
buscando subintervalos m\'as peque\~nos en los que sigue cambiando el
signo, hasta llegar a un punto en que la precisi\'on hace que hayamos
localizado una ra\'{\i}z ``o casi''. En realidad, es m\'as simple ir
subdividiendo en mitades. Esto es el algoritmo de bisecci\'on.

\section{El algoritmo de bisecci\'on}
Se parte de una funci\'on $f$, continua en un intervalo $[a,b]$, y de
los valores $a$ y $b$. Como siempre, se ha de especificar una
precisi\'on $\epsilon > 0$ (si $\abs{f(c)}<\epsilon$ entonces $c$ es
una ``ra\'{\i}z aproximada'') y un n\'umero m\'aximo de ejecuciones
del bucle principal $N>0$. Con todos estos datos, se procede
utilizando el Teorema de Bolzano:

Si $f(a)f(b)<0$, puesto que $f$ es continua en $[a,b]$, tendr\'a
alguna ra\'{\i}z. T\'omese $c$ como el punto medio de $[a,b]$, es
decir, $c=\displaystyle{\frac{a+b}{2}}$. Hay tres posibilidades:
\begin{itemize}
\item O bien $\abs{f(c)}<\epsilon $, en cuyo caso se termina y se
  devuelve el valor $c$ como ra\'{\i}z aproximada.
\item O bien $f(a)f(c)<0$, en cuyo caso se sustituye $b$ por $c$ y se
  repite todo el proceso.
\item O bien $f(b)f(c)<0$, en cuyo caso se sustituye $a$ por $c$ y se
  repite todo el proceso.
\end{itemize}
La iteraci\'on se realiza como mucho $N$ veces (para lo cual hay que
llevar la cuenta, obviamente). Si al cabo de $N$ veces no se ha
obtenido una ra\'{\i}z, se termina con un mensaje de error.

El enunciado formal es el Algoritmo \ref{alg:biseccion}. N\'otese que
se utiliza un signo nuevo: $a \leftrightarrow b$, que indica que los
valores de $a$ y $b$ se intercambian.

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} una funci\'on $f(x)$, un par de n\'umeros
    reales, $a,b$ con $f(a)f(b)<0$, una tolerancia $\epsilon >0$ y
    un l\'imite de iteraciones $N>0$ 
    \STATE \textbf{Output} o bien un mensaje
    de error o bien un n\'umero real $c$ entre $a$ y $b$ tal que $\abs{f(c)} <
    \epsilon$ (una ra\'{\i}z aproximada)
    \PART{Precondici\'on}
    \IF{$a+b\not\in {\mathbb R}$ \textbf{or} $f(b)\not\in {\mathbb R}$ \comm{overflow}}
    \STATE \textbf{return} ERROR
    \ENDIF
    \PART{Inicio}
    \STATE $i\leftarrow 0, x_{-1} \leftarrow a, x_{0} \leftarrow b$
    \STATE \comm{en cada iteraci\'on es posible NaN e $\infty$}
    \WHILE{$\abs{f(x_{i})}\geq \epsilon$ \textbf{and} $i\leq N$}
    \STATE $x_{i+1} \leftarrow \displaystyle{\frac{x_{i-1}+x_{i}}{2}}$
    \comm{punto medio (posible overflow)}
    \STATE \comm{Nunca comparar signos as\'i}
    \IF{$f(x_{i-1})f(x_{i+1}) < 0$} 
    \STATE $x_{i}\leftarrow x_{i-1}$ \comm{intervalo $[a,c]$}
    \ELSE
    \STATE $x_{i+1}\leftrightarrow x_{i}$
    \comm{intervalo $[c,b]$}
    \ENDIF
    \STATE $i\leftarrow i+1$
    \ENDWHILE
    \IF{$i>N$} 
    \STATE \textbf{return} ERROR
    \ENDIF
    \STATE \textbf{return} $c\leftarrow x_{i}$
  \end{algorithmic}
  \caption{(Algoritmo de Bisecci\'on)}
  \label{alg:biseccion}
\end{algorithm}


\section{El algoritmo de Newton-Raphson}
Una idea geom\'etrica cl\'asica (y de teor\'ia cl\'asica de la
aproximaci\'on) es, en lugar de calcular una soluci\'on de una
ecuaci\'on $f(x)=0$ directamente, utilizar la \emph{mejor
  aproximaci\'on lineal a $f$}, que es la recta tangente en un
punto. Es decir, en lugar de calcular una ra\'iz de $f$, utilizar un
valor $(x,f(x))$ para trazar la recta tangente a la gr\'afica de $f$
en ese punto y resolver la ecuaci\'on dada por el corte de esta
tangente con el eje $OX$, en lugar de $f(x)=0$. Obviamente, el
resultado no ser\'a una ra\'iz de $f$, pero \emph{en condiciones
  generales}, uno espera que se aproxime algo (la soluci\'on de una
ecuaci\'on aproximada \emph{deber\'ia ser} una soluci\'on
aproximada). Si se repite el proceso de aproximaci\'on mediante la
tangente, uno espera ir acerc\'andose a una ra\'iz de $f$. 
Esta es la idea del algoritmo de Newton-Raphson.

Recu\'erdese que la ecuaci\'on de la recta que pasa por el punto
$(x_{0},y_0)$ y tiene pendiente $b$ es:
\begin{equation*}
Y = b(X-x_{0})+y_{0}
\end{equation*}
as\'i que la ecuaci\'on de la recta tangente a la gr\'afica de $f$ en
el punto $(x_{0},f(x_0))$ es (suponiendo que $f$ es derivable en $x_0$):
\begin{equation*}
Y = f^{\prime}(x_{0})(X-x_0)+f(x_0).
\end{equation*}
El punto de corte de esta recta con el eje $OX$ es, obviamente, 
\begin{equation*}
(x_1,y_1)=\left(x_0-\frac{f(x_0)}{f^{\prime}(x_0)},0 \right)
\end{equation*}
suponiendo que existe (es decir, que $f^{\prime}(x_0)\neq 0$).

La iteraci\'on, si se supone que se ha calculado $x_n$ es, por tanto:
\begin{equation*}
x_{n+1}=x_n-\frac{f(x_n)}{f^{\prime}(x_n)}.
\end{equation*}
As\'i que se tiene el Algoritmo \ref{alg:newton-raphson}. En el
enunciado solo se especifica un posible lugar de error para no
cargarlo, pero hay que tener en cuenta que cada vez que se eval\'ua
$f$ o cada vez que se realiza una operaci\'on (cualquiera) podr\'ia
ocurrir un error de coma flotante. Aunque en los enunciados que
aparezcan de ahora en adelante no se mencionen todos, el alumno debe
ser consciente de que cualquier implementaci\'on ha de emitir una
excepci\'on (\emph{raise an exception}) en caso de que haya un fallo
de coma flotante.

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} una funci\'on $f(x)$ \emph{derivable}, una
    semilla $x_{0}\in {\mathbb R}$, una tolerancia $\epsilon >0$ y un
    l\'imite de iteraciones $N>0$
    \STATE \textbf{Ouput:} o bien un mensaje de error o bien un
    n\'umero real $c$ tal que $\abs{f(c)}< \epsilon$ (es decir, una
    ra\'{\i}z aproximada)
    \PART{Inicio}
    \STATE $i \leftarrow 0$
%    \STATE \comm{comprobar que $f(x_{i})\in {\mathbb R}$, no incluido}
    \WHILE{$\abs{f(x_{i})}\geq \epsilon$ \textbf{and} $i\leq N$}
%    \STATE \comm{punto de corte tangente con eje OX}
    \STATE $x_{i+1} \leftarrow
    x_{i}-\displaystyle{\frac{f(x_{i})}{f^{\prime}(x_{i})}}$
    \comm{posibles NaN e $\infty$}
%    \IF{$x_{i+1}=NaN$
%    \STATE \textbf{return} ERROR
%    \ENDIF
    \STATE $i\leftarrow i+1$
%    \STATE \comm{comprobar que $f(x_{i})\in {\mathbb R}$, no incluido}
    \ENDWHILE
    \IF{$i>N$}
    \STATE \textbf{return} ERROR
    \ENDIF
    \STATE \textbf{return} $c\leftarrow x_{i}$
  \end{algorithmic}
  \caption{(Algoritmo de Newton-Raphson)}
  \label{alg:newton-raphson}
\end{algorithm}

\section{El algoritmo de la secante}

El algoritmo de Newton-Raphson contiene la evaluaci\'on
$\frac{f(x_n)}{f^{\prime}(x_n)}$, para la que hay que
calcular el valor de dos expresiones: $f(x_{n})$ y
$f^{\prime}(x_{n})$, lo cual puede ser exageradamente
costoso. Adem\'as, hay muchos casos en los que ni siquiera se tiene
informaci\'on real sobre $f^{\prime}(x)$, as\'i que puede ser hasta
ut\'opico pensar que el algoritmo es utilizable.

Una soluci\'on a esta pega es aproximar el c\'alculo de la derivada
utilizando la idea geom\'etrica de que \emph{la tangente es el
  l\'imite de las secantes}; en lugar de calcular la recta tangente se
utiliza una aproximaci\'on con dos puntos que se suponen ``cercanos''.
Como todo el mundo sabe, la derivada de $f(x)$ en el punto $c$ es, si
existe, el
l\'imite
\begin{equation*}
\lim_{h\rightarrow 0}\frac{f(c+h)-f(c)}{h}.
\end{equation*}
En la situaci\'on del algoritmo de Newton-Raphson, si en lugar de
utilizar un solo dato $x_n$, se recurre a los dos anteriores, $x_n$ y
$x_{n-1}$, se puede pensar que se est\'a aproximando la derivada
$f^{\prime}(x_n)$ por medio del cociente
\begin{equation*}
f^{\prime}(x_n)\simeq  \frac{f(x_n)-f(x_{n-1})}{x_n-x_{n-1}},
\end{equation*}
y por tanto, la f\'ormula para calcular el t\'ermino $x_{n+1}$
quedar\'ia
\begin{equation*}
x_{n+1}=x_n-f(x_{n})\frac{x_n-x_{n-1}}{f(x_n)-f(x_{n-1})},
\end{equation*}
y con esto se puede ya enunciar el algoritmo
correspondiente. T\'engase en cuenta que, para comenzarlo, hacen falta
\emph{dos semillas}, no basta con una. El factor de $f(x_n)$ en la
iteraci\'on es justamente el inverso de la aproximaci\'on de la
derivada en $x_n$ utilizando el punto $x_{n-1}$ como $x_n+h$.

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} Una funci\'on $f(x)$, una tolerancia $\epsilon
>0$, un l\'imite de iteraciones $N>0$ y \emph{dos} semillas $x_{-1},
x_0\in {\mathbb R}$
\STATE \textbf{Output:} O bien un n\'umero $c\in {\mathbb R}$ tal que
$\abs{f(c)} < \epsilon$ o bien un mensaje de error
\PART{Inicio}
\STATE $i\leftarrow 0$
\WHILE{$\abs{f(x_i)} \geq \epsilon$ \textbf{and} $i\leq N$}
\STATE $x_{i+1}\leftarrow {\displaystyle x_i-
  f(x_{i})\frac{x_i-x_{i-1}}{f(x_i)-f(x_{i-1})}}$ 
\comm{posibles NaN e $\infty$}
\STATE $i\leftarrow i+1$
\ENDWHILE
\IF{$i>N$}
\STATE \textbf{return} ERROR
\ENDIF
\STATE \textbf{return} $c \leftarrow x_i$
\end{algorithmic}
\caption{(Algoritmo de la secante)}
\label{alg:secante}
\end{algorithm}
Es conveniente, al implementar este algoritmo, conservar en memoria no
solo $x_n$ y $x_{n-1}$, sino los valores calculados (que se han
utilizado ya) $f(x_n)$ y $f(x_{n-1})$, para no computarlos m\'as de
una vez.
 
\section{Puntos fijos}
Los algoritmos de punto fijo (que, como veremos, engloban a los
anteriores de una manera indirecta) se basan en la noci\'on de
\emph{contractividad}, que no refleja m\'as que la idea de que una
funci\'on puede hacer que las im\'agenes de dos puntos cualesquiera
est\'en m\'as cerca que los puntos originales (es decir, la funci\'on
\emph{contrae} el conjunto inicial). Esta noci\'on, ligada
directamente a la derivada (si es que la funci\'on es derivable),
lleva de manera directa a la de \emph{punto fijo} de una iteraci\'on
y, mediante un peque\~no artificio, a poder resolver ecuaciones
generales utilizando iteraciones de una misma funci\'on.

\subsection{Contractividad: las ecuaciones $g(x)=x$}
Sea $g$ una funci\'on real de una variable real 
derivable en un punto $c$. Esto significa que, dado cualquier
infinit\'esimo $\oo$, existe otro $\oo_1$ tal que
\begin{equation*}
g(c+\oo) = g(c) + g^{\prime}(c)\oo + \oo\oo_1,
\end{equation*}
es decir, que \emph{cerca de $c$, la funci\'on $g$ se aproxima muy
  bien mediante una funci\'on lineal}.

Supongamos ahora que $\oo$ es la anchura de un intervalo ``peque\~no''
centrado en $c$. Si olvidamos el \emph{error supralineal} (es decir,
el t\'ermino $\oo\oo_{1}$), pues es ``infinitamente m\'as peque\~no''
que todo lo dem\'as, se puede pensar que el intervalo $(c-\oo, c+\oo)$
se transforma en el intervalo $(g(c)-g^{\prime}(c)\oo,
g(c)+g^{\prime}(c)\oo)$: esto es, un intervalo de radio $\oo$ se
transforma aproximadamente, por la aplicaci\'on $g$ en uno de anchura
$g^{\prime}(c)\oo$ (se dilata o se contrae un factor
$g^{\prime}(c)$). Esta es la noci\'on equivalente de derivada que se
utiliza en la f\'ormula de integraci\'on por cambio de variable (el
Teorema del Jacobiano): la derivada (y en varias variables el
\emph{determinante jacobiano}) mide la dilataci\'on o contracci\'on
que sufre la recta real (un abierto de ${\mathbb R}^n$) justo en un
entorno infinitesimal de un punto.

Por tanto, si $\abs{g^{\prime}(c)}<1$, la recta se contrae cerca de
$c$. 

Sup\'ongase que eso ocurre en todo un intervalo $[a,b]$. Para no
cargar la notaci\'on, supongamos que $g^{\prime}$ es continua en
$[a,b]$ y que se tiene $\abs{g^{\prime}(x)}<1$ para todo $x\in [a,b]$
---es decir, $g$ siempre \emph{contrae} la recta. Para empezar, hay un
cierto $\lambda<1$ tal que $\abs{g(x)}<\lambda$, por el Teorema de
Weierstrass (hemos supuesto $g^{\prime}$ continua). Por el Teorema del
Valor Medio (en la forma de desigualdad, pero importa poco), resulta
que, para cualesquiera $x_{1},x_2\in [a,b]$, se tiene que
\begin{equation*}
\abs{g(x_1)-g(x_2)} \leq \lambda \abs{x_1-x_2},
\end{equation*}
en castellano: \emph{la distancia entre las im\'agenes de dos puntos
  es menor que $\lambda$ por la distancia enre los puntos}, pero como
$\lambda$ es menor que $1$, resulta que \emph{la distancia entre las
  im\'agenes es siempre menor que entre los puntos iniciales}. Es
decir: la aplicaci\'on $g$ est\'a \emph{contrayendo} el intervalo
$[a,b]$ \emph{por todas partes}.

Hay muchos \'enfasis en el p\'arrafo anterior, pero son necesarios
porque son la clave para el resultado central: la existencia de un
punto fijo.

Se tiene, por tanto, que la anchura del conjunto $g([a,b])$ es menor o
igual que $\lambda(b-a)$. Si ocurriera adem\'as que $g([a,b])\subset
[a,b]$, es decir, si $g$ transformara el segmento $[a,b]$ en una parte
suya, entonces se podr\'ia calcular tambi\'en $g(g([a,b]))$, que
ser\'ia una parte propia de $g([a,b])$ y que tendr\'ia anchura menor o
igual que $\lambda\lambda(b-a)=\lambda^2(b-a)$. Ahora podr\'ia
iterarse la composici\'on con $g$ y, al cabo de $n$ iteraciones se
tendr\'ia que la imagen estar\'ia contenida en un intervalo de anchura
$\lambda^n(b-a)$. Como $\lambda<1$, resulta que estas anchuras van
haci\'endose cada vez m\'as peque\~nas. Una sencilla aplicaci\'on del
Principio de los Intervalos Encajados probar\'ia que en el l\'imite,
los conjuntos $g([a,b])$, $g(g([a,b]))$, \dots, $g^n([a,b])$, \dots,
terminan cort\'andose en un solo punto $\alpha$. Adem\'as, este punto,
por construcci\'on, debe cumplir que $g(\alpha)=\alpha$, es decir, es
un \emph{punto fijo}. Hemos mostrado (no demostrado) el siguiente

\begin{theorem}\label{the:punto-fijo-aplicacion-derivable}
  Sea $g:[a,b]\rightarrow[a,b]$ una aplicaci\'on de un intervalo
  cerrado en s\'i mismo, continua y derivable en $[a,b]$. Si existe
  $\lambda <1$ positivo tal que para todo
  $x\in[a,b]$ se tiene que $\abs{g^{\prime}(x)}\leq \lambda$, entonces
  existe un \'unico punto $\alpha\in[a,b]$ tal que
  $g(\alpha)=\alpha$.
\end{theorem}

As\'i que, si se tiene una funci\'on de un intervalo en s\'i mismo
cuya deriva se acota por una constante menor que uno, la ecuaci\'on
$g(x)=x$ tiene una \'unica soluci\'on en dicho intervalo. Adem\'as, la
explicaci\'on previa al enunicado muestra que para calcular $\alpha$
basta con tomar cualquier $x$ del intervalo e ir calculando $g(x)$,
$g(g(x))$, \dots: el l\'imite es $\alpha$, independientemente de $x$.

Por tanto, \emph{resolver ecuaciones de la forma $g(x)=x$ para
  funciones contractivas es tremendamente simple}: basta con iterar
$g$. 

\subsection{Aplicaci\'on a ecuaciones cualesquiera
  $f(x)=0$}\label{subs:aplicacion-punto-fijo-raices}
Pero por lo general, nadie se encuentra una ecuaci\'on del tipo
$g(x)=x$; lo que se busca es resolver ecuaciones como $f(x)=0$.

Esto no presenta ning\'un problema, pues:
\begin{equation*}
f(x) = 0 \Leftrightarrow x - f(x) = x 
\end{equation*}
de manera que \emph{buscar un cero de $f(x)$ equivale a buscar un punto
  fijo de $g(x)=x-f(x)$}.

En realidad, si $\phi(x)$ es una funci\'on
que no se anula nunca, \emph{buscar un cero de $f(x)$ equivale a
  buscar un punto fijo de $g(x)=x-\phi(x)f(x)$}. Esto permite, por
ejemplo, \emph{escalar $f$} para que su derivada sea cercana a $1$ en
la ra\'iz y as\'i que $g^{\prime}$ sea bastante peque\~no, para
acelerar el proceso de convergencia. O se puede simplemente tomar
$g(x)=x-cf(x)$ para cierto $c$ que haga que la derivada de $g$ sea
relativamente peque\~na en valor absoluto\margin{Sin terminar: falta
  completar esta secci\'on, obviamente}.

\subsection{El algoritmo}
La implementaci\'on de un algoritmo de b\'usqueda de punto fijo es muy
sencilla. Como todos, requiere una tolerancia $\epsilon$ y un n\'umero
m\'aximo de iteraciones $N$. La pega es que el algoritmo es in\'util
si la funci\'on $g$ no env\'ia un intervalo $[a,b]$ en s\'i
mismo. Esto es algo que hay que comprobar a priori:

\begin{remark}
Sea $g:[a,b]\rightarrow {\mathbb R}$ una aplicaci\'on. Si se quiere
buscar un punto fijo de $g$ en $[a,b]$ utilizando la propiedad de
contractividad, es necesario:
\begin{itemize}
\item Comprobar que $g([a,b])\subset [a,b]$.
\item Si $g$ es derivable\footnote{Si $g$ no es derivable, se requiere
  una condici\'on mucho m\'as complicada de verificar: que
  $\abs{g(x)-g(x^{\prime})}\leq \lambda \abs{g(x)-g(x^{\prime})}$ para
  cierto $0<\lambda<1$ y para todo par $x,x^{\prime}\in [a,b]$. Esto
  es la \emph{condici\'on de Lipschitz con constante menor que $1$}.} 
  en todo $[a,b]$, comprobar que existe
  $\lambda\in {\mathbb R}$ tal que $0<\lambda<1$ y para el que
  $\abs{g^{\prime}(x)}\leq\lambda$ para todo $x\in [a,b]$.
\end{itemize}
\end{remark}

Supuesta esta comprobaci\'on, el algoritmo para buscar el punto fijo de
$g:[a,b]\rightarrow[a,b]$ es el siguiente:
\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} una funci\'on $g$ (que ya se supone
contractiva, etc\dots), una semilla $x_0\in [a,b]$, una tolerancia
$\epsilon>0$ y un n\'umero m\'aximo de iteraciones $N>0$
\STATE \textbf{Output:} o bien un n\'umero $c\in [a,b]$ tal que
$\abs{c-g(c)}<\epsilon$ o bien un mensaje de error
\PART{Inicio}
\STATE $i\leftarrow 0, c\leftarrow x_0$
\WHILE{$\abs{c-g(c)}\geq \epsilon$ \textbf{and} $i\leq N$}
\STATE $c\leftarrow g(c)$
\STATE $i\leftarrow i+1$
\ENDWHILE
\IF{$i>N$}
\STATE \textbf{return} ERROR
\ENDIF
\STATE \textbf{return} c
\end{algorithmic}
\caption{(B\'usqueda de punto fijo)}
\label{alg:punto-fijo}
\end{algorithm}

\subsection{Utilizaci\'on para c\'omputo de ra\'ices}
El algoritmo de punto fijo se puede utilizar para computar ra\'ices,
como se explic\'o en la Secci\'on
\ref{subs:aplicacion-punto-fijo-raices}; para ello, si la ecuaci\'on
tiene la forma $f(x)=0$, puede tomarse cualquier funci\'on $g(x)$ de
la forma
\begin{equation*}
g(x) = x - k f(x)
\end{equation*}
donde $k\in {\mathbb R}$ es un n\'umero conveniente. Se hace esta
operaci\'on para conseguir que $g$ tenga derivada cercana a $0$ cerca
de la ra\'iz, y para que, si $\chi$ es la ra\'iz (desconocida),
conseguir as\'i que $g$ defina una funci\'on
contractiva de un intervalo de la forma $[\chi-\rho,\chi+\rho]$ (que
ser\'a el $[a,b]$ que se utilice).

Lo dif\'icil, como antes, es probar que $g$ es contractiva y env\'ia
un intervalo en un intervalo.


\subsection{Velocidad de convergencia}
Si se tiene la derivada acotada en valor absoluto por una constante
$\lambda<1$, es relativamente sencillo acotar la velocidad de
convergencia del algoritmo del punto fijo, pues como se dijo antes, la
imagen del
intervalo $[a,b]$ es un intervalo de anchura $\lambda(b-a)$. Tras
iterar $i$ veces, la imagen de la composici\'on $g^i([a,b])= g(g(\dots(g([a,b]))))$ (una
composici\'on repetida $i$ veces) est\'a incluida en un intervalo de
longitud $\lambda^i(b-a)$, de manera que se puede asegurar que la
distancia entre la composici\'on $g^i(x)$ y el punto fijo $c$ es como
mucho $\lambda^i(b-a)$. Si $\lambda$ es suficientemente peque\~no, la
convergencia puede ser muy r\'apida.

\begin{example}
Si $[a,b]=[0,1]$ y $\abs(g^{\prime}(x))< {.}1$, se puede asegurar
que tras cada iteraci\'on, hay un decimal exacto en el punto
calculado, sea cual sea la semilla.
\end{example}

\subsection{El algoritmo de Newton-Raphson en realidad es de punto fijo}

Este apartado no se explicar\'a con detalle en estas notas, pero
t\'engase en cuenta que la expresi\'on
\begin{equation*}
x_{n+1}=x_n-\frac{f(x_n)}{f^{\prime}(x_n)}
\end{equation*}
corresponde a buscar un punto fijo de la funci\'on
\begin{equation*}
g(x) = x - \frac{1}{f^{\prime}(x)}f(x)
\end{equation*}
que, como ya se explic\'o, es una manera de convertir una ecuaci\'on
$f(x)=0$ en un problema de punto fijo. La derivada de $g$ es, en este
caso:
\begin{equation*}
g^{\prime}(x) = 1 -
\frac{f^{\prime}(x)^2-f(x)f^{\prime\prime}(x)}{f^{\prime}(x)^2}
\end{equation*}
que, en el punto $\chi$ en que $f(\chi)=0$ vale
$g^{\prime}(\chi)=0$. Es decir, en el punto fijo, la derivada de $g$
es $0$. Esto hace que la convergencia (una vez que uno est\'a ``cerca''
del punto fijo) sea muy r\'apida (se dice \emph{de segundo orden}).

De hecho, se puede probar el siguiente resultado:
\begin{theorem}
\label{the:newton-raphson-cuadratico}
Supongamos que $f$ es una funci\'on derivable dos veces en un
intervalo $[r-\epsilon, r+\epsilon]$, que $r$ es una ra\'iz de $f$ y
que se sabe que
\begin{itemize}
\item La derivada segunda est\'a acotada superiormente:
  $\abs{f^{\prime\prime}(x)}< K$ para $x\in[r-\epsilon, r+\epsilon]$,
\item La derivada primera est\'a acotada inferiormente:
  $\abs{f^{\prime}(x)}>L$ para $x\in[r-\epsilon, r+\epsilon]$
\end{itemize}
Entonces, si $x_k\in[r-\epsilon, r+\epsilon]$, el siguiente elemento
de la iteraci\'on de Newton-Raphson est\'a tambi\'en en el intervalo y
\begin{equation*}
\abs{r-x_{k+1}}<\abs{\frac{K}{2L}}\abs{r-x_k}^2.
\end{equation*}
\end{theorem}
Como corolario, se tiene que:
\begin{corollary}
Si las condiciones del Teorema \ref{the:newton-raphson-cuadratico} se
cumplen y $\epsilon<0{.}1$ y $K< 2L$, entonces a partir de $k$, cada
iteraci\'on de Newton-Raphson obtiene una aproximaci\'on con el doble
de cifras exactas que la aproximaci\'on anterior.
\end{corollary}
\begin{proof}
Esto es porque, si suponemos que $k=0$, entonces $x_0$ tiene al menos
una cifra exacta. Por el Teorema, $x_1$ difiere de $r$ en menos que
$0{.}1^2={.}01$. Y a partir de ahora el n\'umero de ceros en la
expresi\'on se va duplicando.
\end{proof}

Para utilizar el Teorema \ref{the:newton-raphson-cuadratico} o su
corolario, es preciso:
\begin{itemize}
\item Saber que se est\'a cerca de una ra\'iz. La manera com\'un de
  verificarlo es computando $f$ cerca de la aproximaci\'on (hacia la
  derecha o la izquierda): si el signo cambia, ha de haber una ra\'iz
  (pues $f$ es continua, al ser derivable).
\item Acotar la anchura del intervalo (saber que se est\'a a distancia
  menor de $1/10$, por ejemplo.
\item Acotar $f^{\prime\prime}(x)$ superiormente y $f^{\prime}(x)$
  inferiormente (esto puede ser sencillo o no).
\end{itemize}




\section{Anexo: C\'odigo en Matlab/Octave}

Se incluyen a continuaci\'on algunos listados con implementaciones
``correctas'' de los algoritmos descritos en este
cap\'itulo, utilizables tanto en Matlab como en Octave. T\'engase en
cuenta, sin embargo, que, puesto que ambos programas tienen cuidado de
que las operaciones en coma flotante no terminen en un error
irrecuperable, las implementaciones que se presentan \emph{no incluyen
ninguna revisi\'on de posibles errores}. Por ejemplo, en cualquiera de
estos algoritmos, si se utiliza la funci\'on $\log(x)$ y se eval\'ua
en un n\'umero negativo, el algoritmo posiblemente continuar\'a y
quiz\'as incluso alcance una ra\'iz (real o compleja). No se han
incluido estos tests para no complicar la exposici\'on.

\subsection{Implementaci\'on del algoritmo de bisecci\'on}
El siguiente c\'odigo implementa el algoritmo de bisecci\'on en
Octave/Matlab. Los par\'ametros de entrada son:
\begin{description}
\item[\texttt{f}] una funci\'on an\'onima,
\item[\texttt{a}] el extremo izquierdo del intervalo,
\item[\texttt{b}] el extremo derecho del intervalo,
\item[\texttt{epsilon}] una tolerancia (por defecto, \texttt{eps}),
\item[\texttt{n}] un n\'umero m\'aximo de iteraciones (por defecto, 50).
\end{description}
La salida puede ser nula (si no hay cambio de signo, con un mensaje) o una ra\'iz
aproximada en la tolerancia o bien un mensaje (warning) junto con el
\'ultimo valor calculado por el algoritmo (que no ser\'a una ra\'iz
aproximada en la tolerancia). El formato de la salida, para facilitar
el estudio es un par \texttt{[z, N]} donde \texttt{z} es la ra\'iz
aproximada (o el valor m\'as cercano calculado) y \texttt{N} es el
n\'umero de iteraciones hasta calcularla.

\renewcommand{\lstlistingname}{C\'odigo}
\begin{figure}
\begin{lstlisting}
% Algoritmo de biseccion con error admitido y limite de parada
% Tengase en cuenta que en TODAS las evaluaciones f(...) podria
% ocurrir un error, esto no se tiene en cuenta en esta implementacion
% (en cualquier caso, se terimara)
function [z, N] = Bisec(f, x, y, epsilon = eps, n = 50)
  N = 0;
  if(f(x)*f(y)>0)
    warning('no hay cambio de signo')
    return
  end
  % guardar valores en memoria
  fx = f(x);
  fy = f(y);
  if(fx == 0)
    z = x;
    return
  end
  if(fy == 0)
    z = y;
    return
  end
  z = (x+y)/2;
  fz = f(z);
  while(abs(fz) >= epsilon & N < n)
    N = N + 1;
    % multiplicar SIGNOS, no valores
    if(sign(fz)*sign(fx) < 0)
      y = z;
      fy = fz;
    else
      x = z;
      fx = fz;
    end
    % podria haber un error
    z = (x+y)/2;
    fz = f(z);
  end
  if(N >= n)
    warning('No se ha alcanzado el error admitido antes del limite.')
  end
end
\end{lstlisting}
\caption{C\'odigo del algoritmo de Bisecci\'on}
\end{figure}

\subsection{Implementaci\'on del algoritmo de Newton-Raphson}
El algoritmo de Newton-Raphson es m\'as sencillo de escribir (siempre
sin tener en cuenta las posibles \emph{excepciones} de coma flotante),
pero requiere un dato m\'as complejo en el input: la derivada de $f$,
que debe ser otra funci\'on an\'onima.

Como se ha de utilizar la derivada de $f$ y no se quiere suponer que
el usuario tiene un programa con c\'alculo simb\'olico, se requiere
que uno de los par\'ametros sea expl\'icitamente la funci\'on
$f^{\prime}(x)$. En un entorno con c\'alculo simb\'olico esto no
ser\'ia necesario.

As\'i, la entrada es:
\begin{description}
\item[\texttt{f}] una funci\'on an\'onima, 
\item[\texttt{fp}] otra funci\'on an\'onima, la derivada de \texttt{f},
\item[\texttt{x0}] la semilla,
\item[\texttt{epsilon}] una tolerancia (por defecto \texttt{eps}),
\item[\texttt{N}] el n\'umero m\'aximo de iteraciones (por defecto 50).
\end{description}

El formato de la salida, para facilitar
el estudio es un par \texttt{[xn, N]} donde \texttt{xn} es la ra\'iz
aproximada (o el valor m\'as cercano calculado) y \texttt{N} es el
n\'umero de iteraciones hasta calcularla.

\begin{figure}
\begin{lstlisting}
% Implementacion del metodo de Newton-Raphson
function [z n] = NewtonF(f, fp, x0, epsilon = eps, N = 50)
  n = 0;
  xn = x0;
  % Se supone que f y fp son funciones  
  fn = f(xn);
  while(abs(fn) >= epsilon & n <= N)
    n = n + 1;  
    fn = f(xn); % evaluar una sola vez
    % siguiente punto
    xn = xn - fn/fp(xn); % podria haber una excepcion
  end
  z = xn;
  if(n == N)
    warning('No converje en MAX iteraciones');
  end
end
\end{lstlisting}
\caption{Implementaci\'on de Newton-Raphson}
\label{code:newton-raphson}
\end{figure}

