% -*- TeX-master: "notas.ltx" -*-
\chapter{Aritm\'etica finita y an\'alisis del error}
Se revisan muy r\'apidamente las notaciones exponenciales, la coma
flotante de doble precisi\'on, la noci\'on de error y algunas fuentes
comunes de errores en los c\'omputos.
\section{Notaci\'on exponencial}
Los numeros reales pueden tener un n\'umero finito o infinito de
cifras. Cuando se trabaja con ellos, siempre se han de aproximar a una
cantidad con un n\'umero finito (y habitualmente peque\~no) de
d\'{\i}gitos. Adem\'as, conviene expresarlos de una manera uniforme
para que su magnitud y sus d\'{\i}gitos significativos sean
f\'acilmente reconocibles a primera vista y para que el intercambio de
datos entre m\'aquinas sea determinista y eficiente. Esto ha dado
lugar a la notaci\'on conocida como \emph{cient\'{\i}fica} o
\emph{exponencial}. En estas notas se explica de una manera somera; se
pretende m\'as transmitir la idea que describir con precisi\'on las
reglas de estas notaciones (pues tienen demasiados casos particulares
como para que compense una explicaci\'on). A lo largo de este curso,
utilizaremos las siguientes expresiones:
\begin{definition}
  Una \emph{notaci\'on exponencial} de un n\'umero real es una
  expresi\'on del tipo
  \begin{equation*}
    \begin{split}
      & \pm A.B\times 10^{C}\\
      & 10^{C}\times \pm A.B\\
      & \pm A.BeC
    \end{split}
  \end{equation*}
  donde $A,B$ y $C$ son n\'umeros naturales (que pueden ser nulos), 
  $\pm$ indica un signo (que puede omitirse si es $+$). Cualquiera de
  esas expresiones se refiere al n\'umero $(A+0{.}B)\cdot 10^{C}$ (donde
  $0{.}B$ es el n\'umero ``cero coma B\dots''.
\end{definition}
Por ejemplo:
\begin{itemize}
\item El n\'umero $3{.}123$ es el propio $3{.}123$.
\item El n\'umero $0{.}01e-7$ es $0{.}000000001$ (hay ocho ceros
  despu\'es de la coma y antes del $1$).
\item El n\'umero $10^{3}\times -2{.}3$ es $-2300$.
\item El n\'umero $-23{.}783e-1$ es $-2{.}3783$.
\end{itemize}
Pero por lo general, la notaci\'on cient\'{\i}fica asume que el
n\'umero a la derecha del punto decimal es un solo d\'{\i}gito no
nulo:
\begin{definition}
  La notaci\'on exponencial est\'andar es la notaci\'on exponencial en
  la que $A$ es un n\'umero entre $1$ y $9$.
\end{definition}

Finalmente, las m\'aquinas almacenan ---generalmente--- los n\'umeros de una manera
muy concreta, en \emph{coma flotante}:
\begin{definition}
  Un formato en \emph{coma flotante} es una especificaci\'on de una
  notaci\'on exponencial en la que la longitud de $A$ m\'as la
  longitud de $B$ es una cantidad fija y en la que los exponentes (el
  n\'umero $C$) var\'{\i}an en un rango determinado.
\end{definition}
Por tanto, un formato en coma flotante solo puede expresar una
cantidad finita de n\'umeros (aquellos que puedan escribirse seg\'un
la especificaci\'on del formato).

El documento que especifica el est\'andar de coma flotante para los
microprocesadores (y en general para los dispositivos electr\'onicos)
es el IEEE-754 (le\'{\i}do i-e-cubo setecientos cincuenta y
cuatro). Las siglas son de ``Institute of Electrical and Electronic
Engineers''. El documento en cuesti\'on fue actualizado en 2008.

\subsection{El formato binario de doble precisi\'on IEEE-754}
El formato de doble precisi\'on es la especificaci\'on del IEEE sobre
la representaci\'on de n\'umeros reales en secuencias de 16, 32, 64,
128 bits (y m\'as), y su representaci\'on en formato decimal. Se
explican a continuaci\'on las propiedades principales de la doble
precisi\'on en binario.

Para expresar n\'umeros en doble precisi\'on se utilizan 64 bits, es
decir, 64 d\'{\i}gitos \emph{binarios}. El primero indica el signo (un
$0$ es signo positivo, un $1$, signo negativo). Los 11 siguientes se
utilizan para representar el exponente como se indicar\'a, y los 52
restantes se utilizan para lo que se denomina la \emph{mantisa}. De
esta manera, un n\'umero en doble precisi\'on tiene tres partes: $s$,
el signo (que ser\'a $0$ \'o $1$), $e$, el exponente (que variar\'a
entre $0$ y $2047$ (pues $2^{11}=2048$), y $m$, un n\'umero de $52$
bits. Dados tres datos $s, e, m$, el n\'umero real $N$ que representan es:
\begin{itemize}
\item Si $e\neq 0 $ y $e\neq 2047$ (si el exponente no es ning\'un
  valor extremo), entonces
  \begin{equation*}
    N=(-1)^{s}\times 2^{e-1023}\times 1{.}m,
  \end{equation*}
  donde $1{.}m$ indica ``uno-coma-m'' en binario. N\'otese, y esto es
  lo importante, que el exponente \emph{no es el n\'umero representado
  por los $11$ bits de $e$}, sino que ``se desplaza hacia la
derecha''. Un $e=01010101011$, que en decimal es $683$ representa
realmente la potencia de $2$ $2^{683-1023}=2^{-340}$. Los $e$ cuyo
primer bit es cero corresponden a potencias negativas de $2$ y los que
tienen primer bit $1$ a potencias positivas ($2^{10}=1024$).
\item Si $e=0$ entonces, si $m\neq 0$ (si hay mantisa):
  \begin{equation*}
    N = (-1)^{s}\times 2^{-1023}\times 0{.}m,
  \end{equation*}
  donde $0{.}m$ indica ``cero-coma-m'' en binario.
\item Los ceros con signo: si $e=0$ y $m=0$, el n\'umero es $+0$ o
  $-0$, dependiendo de $s$. (es decir, el $0$ tiene signo).
\item El caso de $e=2047$ (es decir, los $11$ digitos del exponente
  son $1$) se reserva para codificar $\pm \infty$ y
  otros objetos que se denominan $NaN$ (Not-a-Number, que indica que
  una operaci\'on es ileg\'{\i}tima, como $1/0$ o $\log(-2)$ o
  $\acos(3)$, en una
  operaci\'on con n\'umeros reales).
\end{itemize}
En realidad, el est\'andar es mucho m\'as largo y completo, como es
natural, e incluye una gran colecci\'on de requisitos para los
sistemas electr\'onicos que realicen c\'alculos en coma flotante (por
ejemplo, especifica c\'omo se han de truncar los resultados de las
operaciones fundamentales para asegurar que si se puede obtener un
resultado exacto, \emph{se obtenga}).

Las ventajas de la coma flotante (y en concreto de la doble
precisi\'on) son, aparte de su estandarizaci\'on, que permite a la vez
operar con n\'umeros muy peque\~nos (el n\'umero m\'as peque\~no que
puede almacenar es $2^{-1023}\simeq 10^{-300}$) y n\'umeros muy grandes (el mayor es
alrededor de $2^{1023}\simeq 10^{300}$). La contrapartida es que, si
se trabaja simult\'aneamente con ambos tipos de datos, los peque\~nos
pierden precisi\'on y \emph{desaparecen} (se produce un error de
cancelaci\'on o de truncaci\'on). Pero \emph{si se tiene cuidado}, es
un formato enormemente \'util y vers\'atil.

\subsection{Conversi\'on de base decimal a base dos y vuelta}
\margin{Poner dos ejemplos}
Es imprescindible saber c\'omo se trasnforma un n\'umero en base
decimal a base dos (binario) y al rev\'es. 

En el pseudoc\'odigo de estas notas, se utilizar\'a la siguiente
notaci\'on:
la expresi\'on $x\leftarrow a$ significa
que $x$ es una variable, $a$ representa un valor (as\'i que puede ser
un n\'umero u otra variable) y a $x$ se
le asigna el valor de $a$. La expresi\'on $u=c$ es la expresi\'on
\emph{condicional} ?`es el valor designado por $u$ igual al valor
designado por $c$? Adem\'as, $m // n$ indica el cociente de dividir
el n\'umero entero  $m\geq 0$ entre el n\'umero entero $n>0$ y $m\% n$
es el \emph{resto} de dicha divis\'on. Es decir,
\begin{equation*}
  m = (m//n) \times n + (m\%n).
\end{equation*}
Finalmente, si $x$ es un n\'umero real $x=A{.}B$, la expresi\'on
$\left\{x\right\}$ indica la \emph{parte fraccionaria de $x$}, es
decir, $0{.}B$.

El Algoritmo \ref{alg:dec-to-bin} es una manera de pasar un n\'umero
$A{.}B$ en decimal con un n\'umero finito de cifras a su forma
binaria. El Algoritmo \ref{alg:bin-to-dec} se utiliza para realizar la
operaci\'on inversa. T\'engase en cuenta que, puesto que hay n\'umeros
con una cantidad finita de digitos decimales que no se pueden expresar
con una cantidad finita de digitos en binario (el ejemplo m\'as obvio
es $0{.}1$), se ha de especificar un n\'umero de cifras decimales para
la salida (as\'{\i} que no se obtiene necesariamente el mismo
n\'umero, sino una truncaci\'on).

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} $A{.}B$ un n\'umero en decimal, con $B$ finito, $k$
    un entero positivo (que indica el n\'umero de digitos que se desea tras la coma en binario)
    \STATE \textbf{Output:} $a.b$ (el n\'umero $x$ en binario, truncado hasta $2^{-k}$)
    \IF{$A=0$}
    \STATE $a\leftarrow 0$ y
    calular solo la {\sc Parte Decimal}
    \ENDIF
    \PART{Parte Entera}
\STATE $i \leftarrow -1$, $n\leftarrow A$
\WHILE{$n>0$}
\STATE{$i\leftarrow i+1$}
\STATE{$x_{i} \leftarrow n \% 2$}
\STATE{$n\leftarrow n//2$}
\ENDWHILE
\STATE{$a \leftarrow x_{i}x_{i-1}\dots x_{0}$} \comm{la secuencia de restos
en orden inverso}
\PART{Parte Decimal}
\IF {$B=0$} 
\STATE $b\leftarrow 0$
\STATE \textbf{return} $a{.}b$
\ENDIF
\STATE $i \leftarrow 0$,  $n\leftarrow 0{.}B$
\WHILE{$n>0$ \textbf{and} $i<k$}
\STATE{$i\leftarrow i+1$}
\STATE{$m \leftarrow 2n$}
\IF{$m\geq 1$}
\STATE{$b_{i} \leftarrow 1$}
\ELSE
\STATE{$b_{i} \leftarrow 0$}
\ENDIF
\STATE $n\leftarrow \left\{m\right\}$ \comm{la parte decimal de $m$}
\ENDWHILE
\STATE $b\leftarrow b_{1}b_{2}\dots b_{i}$
\STATE \textbf{return} a.b
\end{algorithmic}
  \caption{(Paso de decimal a binario, sin signo)}
  \label{alg:dec-to-bin}
\end{algorithm}

Pasar de binario a decimal es ``m\'as sencillo'' pero requiere
ir sumando: no obtenemos un d\'{\i}gito por paso, sino que se han de
sumar potencias de $2$. Se describe este proceso en el Algoritmo
\ref{alg:bin-to-dec}. N\'otese que el n\'umero de decimales de la
salida no est\'a especificado (se podr\'{\i}a, pero solo har\'{\i}a el
algoritmo m\'as complejo). Finalmente, en todos los pasos en que se
suma $A_{i}\times 2^{i}$ o bien $B_{i}\times 2^{-i}$, tanto $A_{i}$
como $B_{i}$ son o bien $0$ o bien $1$, as\'{\i} que ese producto solo
significa ``sumar o no sumar'' la potencia de $2$ correspondiente (que
es lo que expresa un d\'{\i}gito binario, al fin y al cabo).

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} $A{.}B$, un n\'umero positivo en binario, $k$
    un n\'umero entero no negativo (el n\'umero de decimales que se
    quiere utilizar en binario).  
    \STATE \textbf{Ouput:} $a{.}b$, la truncaci\'on hasta precisi\'on
    $2^{-k}$ en decimal del n\'umero $A{.}B$  
    \PART{Parte mayor que $0$}
    \STATE Se escribe $A=A_{r}A_{r-1}\dots A_{0}$ (los d\'{\i}gitos
    binarios)
    \STATE $a\leftarrow 0$, $i\leftarrow 0$
    \WHILE{$i\leq r$}
    \STATE $a\leftarrow a+A_{i}\times 2^{i}$
    \STATE $i\leftarrow i+1$
    \ENDWHILE
    \PART{Parte decimal}
    \IF{$B=0$} 
    \STATE \textbf{return} $a{.}0$
    \ENDIF
    \STATE $b \leftarrow 0$, $i\leftarrow 0$
    \WHILE{$i\leq k$}
    \STATE $i\leftarrow i+1$
    \STATE $b\leftarrow b + B_{i}\times 2^{-i}$
    \ENDWHILE
    \STATE \textbf{return} $a{.}b$
  \end{algorithmic}
  \caption{(Paso de binario a decimal, sin signo)}
  \label{alg:bin-to-dec}
\end{algorithm}

\section{El error, definiciones b\'asicas}
Siempre que se opera con n\'umeros con una cantidad finita de cifras y
siempre que se toman medidas en la realidad, hay que tener en cuenta
que contendr\'an, casi con certeza, un error. Esto no es
grave. Lo prioritario es \emph{tener una idea de su tama\~no} y saber
que seg\'un vayan haci\'endose operaciones puede ir propag\'andose. Al
final, lo que importa es \emph{acotar el error absoluto}, es decir,
conocer un valor (la cota) que sea mayor que el error cometido, para
saber \emph{con certeza} cu\'anto, como mucho, dista el valor real 
del valor obtenido.

En lo que sigue, se parte de un valor exacto $x$ (una constante, un
dato, la soluci\'on de un problema\dots) y de una
aproximaci\'on, $\tilde{x}$.

\begin{definition}
  Se llama \emph{error absoluto} cometido al utilizar $\tilde{x}$ en
  lugar de $x$ al valor absoluto de la diferencia: $\abs{x-\tilde{x}}$.
\end{definition}

Pero, salvo que $x$ sea $0$, uno est\'a habitualmente m\'as interesado
en \emph{el orden de magnitud} del error, es decir, ``cu\'anto se
desv\'{\i}a $\tilde{x}$ de $x$ en proporci\'on a $x$'':

\begin{definition}
  Se llama \emph{error relativo} cometido al utilizar $\tilde{x}$ en
  lugar de $x$, siempre que $x\neq 0$, al cociente
  \begin{equation*}
    \frac{\abs{\tilde{x}-x}}{\abs{x}}
  \end{equation*}
  (que es siempre un n\'umero positivo).
\end{definition}

No vamos a utilizar una notaci\'on especial para ninguno de los dos
errores (hay autores que los llaman $\Delta$ y $\delta$,
respectivamente, pero cuanta menos notaci\'on innecesaria, mejor).
\newcommand{\tpi}{\tilde{\pi}}
\begin{example}
  La constante $\pi$, que es la raz\'on entre la longitud de la
  circunferencia y su di\'ametro, es, aproximadamente
  $3{.}1415926534+$ (con el $+$ final se indica que es mayor que el
  n\'umero escrito hasta la \'ultima cifra). Supongamos que se utiliza
  la aproximaci\'on $\tpi=3{.14}$. Se tiene:
  \begin{itemize}
  \item El error absoluto es $\abs{\pi - \tpi} = 0{.}0015926534+$.
  \item El error relativo es $\frac{\abs{\pi - \tpi}}{\pi}\simeq 10^{-4}\times5{.}069573$.
  \end{itemize}
  Esto \'ultimo significa que se produce un error de 5 diezmil\'esimas
  por unidad (que es m\'as o menos $1/2000$)
  cada vez que se usa $3{.}14$ en lugar de $\pi$. Por tanto, si se
  suma $3{.}14$ dos mil veces, el error cometido al usar esa cantidad en
  lugar de $2000\times \pi$ es aproximadamente de $1$. Este es el
  significado interesante del error relativo: su inverso es el
  n\'umero de veces que hay que sumar $\tilde{x}$ para que el error
  acumulado sea $1$ (que, posiblemente, ser\'a el orden de
  magnitud del problema).
\end{example}

Antes de continuar analizando errores, conviene definir las dos
maneras m\'as comunes de escribir n\'umeros utilizando una cantidad
fija de d\'{\i}gitos: el \emph{truncamiento} y el \emph{redondeo}. No
se van a dar las definiciones precisas porque en este caso la
precisi\'on parece irrelevante. Se parte de un n\'umero real
(posiblemente con un n\'umero infinito de cifras):
\begin{equation*}
  x = a_{1}a_{2}\dots a_{r}{.}a_{r+1}\dots a_{n}\dots
\end{equation*}
Se definen:
\begin{definition}
  El \emph{truncamiento de $x$} a $k$ cifras (significativas) es el
  n\'umero $a_{1}a_{2}\dots a_{k}0\dots 0$ (un n\'umero entero), si $k\leq r$ y
  si no, $a_{1}\dots a_{r}{.}a_{r+1}\dots a_{k}$ si $k>r$. Se trata de
  \emph{cortar} las cifras de $x$ y poner ceros si aun no se ha
  llegado a la coma decimal.
\end{definition}

\begin{definition}
  El \emph{redondeo de $x$} a $k$ cifras (significativas) es el
  siguiente n\'umero:
  \begin{itemize}
  \item Si $a_{k+1}<5$, entonces el redondeo es igual al truncamiento.
  \item Si $5\leq a_{k+1} \leq 9$, entonces el redondeo es igual al
    truncamiento m\'as $10^{r-k+1}$. 
  \end{itemize}
  Este redondeo se denomina \emph{redondeo hacia m\'as infinito},
  porque siempre se obtiene un n\'umero \emph{mayor o igual} que el
  truncamiento. 
\end{definition}
El problema con el redondeo es que pueden cambiar todas las cifras. La
gran ventaja es que el error que se comete al redondear es menor que
el que se comete al truncar (puede ser hasta de la mitad):

\begin{example}
  Si $x=178{.}299$ y se van a usar $4$ cifras, entonces el truncamiento
  es $x_{1}=178{.}2$, mientras que el redondeo es $178{.}3$. El error absoluto
  cometido en el primier caso es $0{.}099$, mientas que en el segundo
  es $0{.}001$.
\end{example}

\begin{example}
  Si $x=999{.}995$ y se van a usar $5$ cifras, el truncamiento es
  $x_{1}=999{.}99$, mientras que el redondeo es $1000{.}0$. Pese a que
  todas las cifras son diferentes, el error cometido es el mismo (en
  este caso, $0.005$)
  y esto es lo importante, no que los digitos ``coincidan''.
\end{example}

?`Por qu\'e se habla entonces de truncamiento? Porque cuando uno
trabaja en coma flotante, es inevitable que se produzca truncamiento
(pues el n\'umero de d\'{\i}gitos es finito) y se hace necesario
tenerlo en cuenta. Actualmente (2012) la mayor\'{\i}a de programas que
trabajan en doble precisi\'on, realmente lo hacen con muchos m\'as
d\'{\i}gitos internamente y posiblemente al reducir a doble
precisi\'on redondeen. Aun as\'{\i}, los truncamientos se producen en
alg\'un momento (cuando se sobrepasa la capacidad de la m\'aquina).

\subsection{Fuentes del error}
El error se origina de diversas maneras. Por una parte, cualquier
medici\'on est\'a sujeta a \'el (por eso los aparatos de medida se
venden con un margen estimado de precisi\'on); esto es intr\'{\i}nseco
a la naturaleza y lo \'unico que puede hacerse es tenerlo en cuenta y
saber su magnitud (conocer una buena cota). Por otro lado, las
operaciones realizadas en aritm\'etica finita dan lugar tanto a la
propagaci\'on de los errores como a la aparici\'on de nuevos,
precisamente por la cantidad limitada de d\'{\i}gitos que se pueden
usar.

Se pueden enumerar, por ejemplo, las siguientes fuentes de error:
\begin{itemize}
\item El error \emph{en la medici\'on}, del que ya se ha hablado. Es
  inevitable.
\item El error de \emph{truncamiento}: ocurre cuando un n\'umero (dato o
  resultado de una operaci\'on) tiene m\'as d\'{\i}gitos que los
  utilizados en las operaciones y se ``olvida'' una parte.
\item El error de \emph{redondeo}: ocurre cuando, por la raz\'on que sea, se
  redondea un n\'umero a una precisi\'on determinada.
\item El error de \emph{cancelaci\'on}: se produce cuando una
  operación da lugar a errores relativos mucho más grandes que los
  absolutos. Habitualmente tiene lugar al sustraer de magnitud muy
  similar. El ejemplo canónico de esto aparece en la
  \emph{inestabilidad de la ecuación cuadrática}.
\item El error de \emph{acumulaci\'on}: se produce al acumular (sumar,
  esencialmente) peque\~nos errores del mismo signo \emph{muchas
    veces}. Es lo que ocurri\'o en el suceso de los Patriot en febrero
  de 1991, en la operaci\'on \emph{Tormenta del Desierto}\footnote{Pueden
  consultarse el siguiente documento de la Univ. de Texas: \par
  \texttt{http://www.cs.utexas.edu/\~{}downing/papers/PatriotA1992.pdf}\par
y el informe oficial: \texttt{http://www.fas.org/spp/starwars/gao/im92026.htm}}.
\end{itemize}
En la aritm\'etica de precisi\'on finita, todos estos errores
ocurren. Conviene quiz\'as conocer las siguientes reglas (que son los
peores casos posibles):
\begin{itemize}\label{item:reglas-operaciones-errores}
\item Al sumar \emph{n\'umeros del mismo signo}, el error absoluto puede ser la suma de los
  errores absolutos y el error relativo, lo mismo.
\item Al sumar \emph{n\'umeros de signo contrario}, el error absoluto
  se comporta como en el caso anterior, pero \emph{el error relativo
    puede aumentar de manera incontrolada}: $1000{.}2-1000{.}1$ solo
  tiene una cifra significativa (as\'{\i} que el error relativo puede
  ser de hasta un $10\%$, mientras  que en los operandos, el error
  relativo era de $1\e{-4}$.
\item Al multiplicar, el error absoluto tiene la magnitud del mayor
  factor por el error en el otro factor. Si los factores son de la
  misma magnitud, puede ser el doble del mayor error absoluto por
  el mayor factor. El error relativo es de la misma magnitud que el
  mayor error relativo (y si son de la misma magnitud, su suma).
\item Al dividir \emph{por un n\'umero mayor o igual que $1$}, el error
  absoluto es aproximadamente el error absoluto del numerador partido
  por el denominador y el error relativo es el error relativo del
  numerador (esencialmente como en la multiplicaci\'on). 
  Al dividir por n\'umeros cercanos a cero, lo que ocurre es que se
  perder\'a precisi\'on absoluta y si luego se opera con n\'umeros de
  magnitud similar, el error de cancelaci\'on puede ser
  importante. Comp\'arense las siguientes cuentas:
  \begin{equation*}
    \begin{split}
          26493-\frac{33}{0{.}0012456}=-0{.}256 \text{ (el resultado
            buscado) }.\\
          26493-\frac{33}{0{.}001245}=-13.024
    \end{split}
  \end{equation*}
  Si bien el error relativo de truncaci\'on es solo de $4\times
  10^{-4}$ (media mil\'esima), el error relativo del resultado es de
  $49{.}8$ (es decir, el resultado obtenido es 50 veces m\'as grande
  que el que deb\'{\i}a ser). Este (esencialmente) es el problema
  fundamental de los m\'etodos de resoluci\'on de sistemas que
  utilizan divisiones (como el de Gauss y por supuseto, el de
  Cramer). Cuando se explique el m\'etodo de Gauss, se ver\'a que es
  conveniente buscar la manera de hacer las divisiones con los
  divisores m\'as grandes posibles (estrategias de \emph{pivotaje}).
\end{itemize}



\section{Acotar el error}
Como se dijo antes, lo importante no es saber con precisi\'on el error
cometido en una medici\'on o al resolver un problema, pues
con casi certeza, esto ser\'a imposible, sino \emph{tener una idea} y,
sobre todo, \emph{tener una buena cota}: saber que el error absoluto cometido
es \emph{menor} que una cantidad y que esta cantidad sea lo m\'as
ajustada posible (ser\'{\i}a in\'util, por ejemplo, decir que $2{.}71$
es una aproximaci\'on de $e$ con un error menor que $400$).

As\'{\i} pues, lo \'unico que se podr\'a hacer realmente ser\'a
estimar un n\'umero mayor que el error cometido y
\emph{razonablemente} peque\~no. Eso es \emph{acotar}.

\subsection{Algunas cotas}
Lo primero que ha de hacerse es acotar el error si se conoce una
cantidad con una variaci\'on aproximada. Es decir, si se sabe que
$x=a\pm \epsilon$, donde $\epsilon>0$, el error que se comete al utilizar $a$ en lugar de
$x$ es desconocido (esto es importante) pero es \emph{a lo sumo,
  $\epsilon$}. Por lo tanto, el error relativo que se comete es,
\emph{a lo sumo} el error absoluto dividido \emph{entre el menor de
  los valores posibles en valor absoluto}: téngase en cuenta que esto
es delicado, pues si $a-\epsilon<0$ pero $a+\epsilon>0$ entonces
\emph{no se puede acotar el error relativo} porque $x$ podría ser $0$
(y, como ya se explicó, el error relativo solo tiene sentido para
cantidades no nulas).
\begin{example}
  Si se sabe que $\pi=3{.}14\pm 0{.}01$, se sabe que el error absoluto m\'aximo
  cometido es $0{.}01$, mientras que el error relativo es como mucho
  \begin{equation*}
    \frac{0{.}01}{\abs{3{.}13}}\simeq {.}003194
  \end{equation*}
  (m\'as o menos $1/300$).
\end{example}

T\'engase en cuenta que, si lo que se busca es una cota superior y se
ha de realizar una divisi\'on, ha de tomarse el divisor \emph{lo m\'as
peque\~no posible} (cuanto menor es el divisor, mayor es el
resultado). Con esto se ha de ser muy cuidadoso.

Las reglas de la p\'agina \pageref{item:reglas-operaciones-errores} son
esenciales si se quiere acotar el error de una serie de operaciones
aritm\'eticas. Como se dice all\'{\i}, ha de tenerse un cuidado muy
especial cuando se realizan divisiones con n\'umeros menores que uno,
pues posiblemente se llegue a resultados in\'utiles (como que ``el
resultado es $7$ pero el error absoluto es $23$'').
