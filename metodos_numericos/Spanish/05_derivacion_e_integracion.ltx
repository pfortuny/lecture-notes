% -*- TeX-master: "notas.ltx" -*-
\chapter{Derivaci\'on e Integraci\'on Num\'ericas}
Se expone con brevedad la aproximaci\'on de la derivada de una
funci\'on mediante la f\'ormula sim\'etrica de incrementos y c\'omo
este tipo de f\'ormula puede generalizarse para derivadas de \'ordenes
superiores. El problema de la inestabilidad de la derivaci\'on
num\'erica se explica someramente.

La integraci\'on se trata con m\'as detalle (pues el problema es mucho
m\'as estable que el anterior), aunque tambi\'en con brevedad. Se
definen las nociones de f\'ormulas de cuadratura de tipo
interpolatorio y de grado de precisi\'on, y
se explican las m\'as sencillas (trapecios y Simpson) en el caso
simple y en el compuesto.

\section{Derivaci\'on Num\'erica: un problema inestable}
A veces se requiere calcular la derivada aproximada de una funci\'on
mediante f\'ormulas que no impliquen la derivada, bien sea porque esta
es dif\'icil de computar ---costosa---, bien porque realmente no se
conoce la expresi\'on simb\'olica de la funci\'on a derivar. En el
primer caso se hace necesario sobre todo utilizar f\'ormulas de
derivaci\'on num\'erica lo m\'as precisas posibles, mientras que en el
segundo caso lo que se requerir\'a ser\'a, casi con certeza, conocer
una cota del error cometido. En estas notas solo se va a mostrar
c\'omo la f\'ormula sim\'etrica para calcular derivadas aproximadas es
\emph{mejor} que la aproximaci\'on na\'if consistente en calcular el
cociente del incremento asim\'etrico. Se expone la f\'ormula
equivalente para la derivada segunda y se da una idea de la
inestabilidad del m\'etodo.

\subsection{La derivada aproximada sim\'etrica}
Una idea simple para calcular la derivada de una funci\'on $f$ en un
punto $x$ es, utilizando la noci\'on de derivada como l\'imite de las
secantes, tomar la siguiente aproximaci\'on:
\begin{equation}
\label{eq:derivada-asimetrica}
f^{\prime}(x) \simeq \frac{f(x+h)-f(x)}{h},
\end{equation}
donde $h$ ser\'a un \emph{incremento peque\~no} de la variable
independiente. 

\begin{figure}
\begin{tikzpicture}
\begin{axis}[
enlargelimits=false,
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={0.2, 0.3, 0.4},
xticklabels={\tiny{$x_{0}-h$}, \tiny{$x_0$}, \tiny{$x_0+h$}},
ytick={2.5, 3.3333, 5},
yticklabels={\tiny{$f(x_0-h)$}, \tiny{$f(x_0)$}, \tiny{$f(x_0+h)$}}
]
\addplot[domain=0.15:0.52, blue, line width={2pt}] {1/x};
\addplot[yellow, line width={1.2pt}] coordinates {(0.2, 5) (0.4, 2.5)};
\addplot[red, line width={1.2pt}] coordinates {(0.3, 3.3333) (0.4, 2.5)};
\addplot[green, line width={1.2pt}] coordinates {(0.3, 3.3333) (0.2, 5)};
\addplot[magenta, line width={1.2pt}] coordinates {(0.19, 4.555) (0.41,
  2.1111)};
\addplot[line width={1pt}, only marks, mark=*] coordinates {
(0.3, 3.3333)
(0.2, 5)
(0.4, 2.5)
};
\addplot[line width={0.3pt}, loosely dashed] coordinates {
(0.2, 0)
(0.2, 5)
(0.15, 5)
};
\addplot[line width={0.3pt}, loosely dashed] coordinates {
(0.3, 0)
(0.3, 3.3333)
(0.15, 3.3333)
};
\addplot[line width={0.3pt}, loosely dashed] coordinates {
(0.4, 0)
(0.4, 2.5)
(0.15, 2.5)
};
\end{axis}
\end{tikzpicture}
\caption{Derivada aproximada: por la derecha (rojo), por la izquierda (verde) y
  centrada (amarillo). La centrada es mucho m\'as parecida a la
  tangente (magenta).}\label{fig:formula-punto-medio-integracion}
\end{figure}


Sin embargo, la propia f\'ormula (\ref{eq:derivada-asimetrica})
muestra su debilidad: ?`se ha de tomar $h$ positivo o negativo? Esto
no es irrelevante. Supongamos que $f(x)=1/x$ y se quiere calcular la
derivada en $x=1/2$. Utilicemos aproximaciones de
$\abs{h}={.}01$. Sabemos que $f^{\prime}(0{.}5)=0{.}25$. Con la
aproximaci\'on ``natural'', se tiene, por
un lado,
\begin{equation*}
\frac{f(x+{.}01)-f(x)}{{.}01}=\frac{\frac{1}{2{.}01}-\frac{1}{2}}{{.}01}
= -0{,}248756+
\end{equation*}
mientras que, por otro,
\begin{equation*}
\frac{f(x-{.}01)-f(x)}{-{.}01}=-\frac{\frac{1}{1{.}99}-\frac{1}{2}}{{.}01}
= -0{,}251256+
\end{equation*}
que difieren en el segundo decimal. ?`Hay alguna de las dos que tenga
preferencia \emph{en un sentido abstracto}? No. 

Cuando se tienen dos datos que aproximan un tercero y no hay ninguna
raz\'on abstracta para elegir uno
por encima de otro, \emph{parece razonable} utilizar la media
aritm\'etica. Probemos en este caso:
\begin{equation*}
\frac{1}{2}\left( \frac{f(x+h)-f(x)}{h}+ \frac{f(x-h)-f(x)}{-h}\right) = 
-0.2500062+
\end{equation*}
que aproxima la derivada real $0{.}25$ con cuatro cifras
significativas (cinco si se trunca). 

Esta es, de hecho, la manera correcta de plantear el problema de la
derivaci\'on num\'erica: utilizar la diferencia sim\'etrica alrededor
del punto y dividir por dos veces el intervalo (es decir, la media
entre la ``derivada por la derecha'' y la ``derivada por la
izquierda'').

\begin{theorem}\label{the:formula-simetrica-derivada-orden-2}
La aproximaci\'on na\'if a la derivada tiene una precisi\'on de orden
$1$, mientras que la f\'ormula sim\'etrica tiene una precisi\'on de
orden $2$.
\end{theorem}
\begin{proof}
Sea $f$ una funci\'on con derivada tercera en el intervalo
$[x-h,x+h]$. 
Utilizando el Polinomio de Taylor de grado uno, se tiene que, para
cierto $\xi\in[x-h,x+h]$,
\begin{equation*}
f(x+h) = f(x) + f^{\prime}(x) h + \frac{f^{\prime\prime}(\xi)}{2}h^2,
\end{equation*}
as\'i que, despejando $f^{\prime}(x)$, queda
\begin{equation*}
f^{\prime}(x) = \frac{f(x+h)-f(x)}{h} - \frac{f^{\prime\prime}(\xi)}{2}h,
\end{equation*}
que es lo que significa que la aproximaci\'on tiene orden $2$. Como se
ve, el t\'ermino de m\'as a la derecha no puede desaparecer.

Sin embargo, para la f\'ormula sim\'etrica se utiliza el Polinomio de
Taylor \emph{dos veces}, y de segundo grado:
\begin{equation*}
\begin{split}
f(x+h) = f(x) + f^{\prime}(x) h +
\frac{f^{\prime\prime}(x)}{2}h^2 + \frac{f^{3)}(\xi)}{6}h^3,\\
f(x-h) = f(x) - f^{\prime}(x) h + 
\frac{f^{\prime\prime}(x)}{2}h^2 - \frac{f^{3)}(\zeta)}{6}h^3
\end{split}
\end{equation*}
para $\xi\in[x-h,x+h]$ y $\zeta\in[x-h,x+h]$. Restando ambas
igualdades queda
\begin{equation*}
f(x+h) - f(x-h) = 2f^{\prime}(x) h + K(\xi, \zeta)h^3
\end{equation*}
donde $K$ es una mera suma de los coeficientes de grado $3$ de la
ecuaci\'on anterior, as\'i que
\begin{equation*}
f^{\prime}(x) = \frac{f(x+h)-f(x-h)}{2h} + K(\xi, \zeta)h^2,
\end{equation*}
que es justo lo que significa que la f\'ormula sim\'etrica es de
segundo orden.
\end{proof}

\section{Integraci\'on Num\'erica: f\'ormulas de cuadratura}
La integraci\'on num\'erica, como corresponde a un problema \emph{en el que los
  errores se acumulan} (un problema global, por as\'i decir) es
\emph{m\'as estable} que la derivaci\'on, por extra\~no que parezca
(pese a que integrar simb\'olicamente es m\'as dif\'icil que derivar,
la aproximaci\'on num\'erica se comporta mucho mejor).

En general, uno est\'a interesado en \emph{dar una f\'ormula} de
integraci\'on num\'erica. Es decir, se busca una expresi\'on
algebraica general tal que, dada una funci\'on
$f:[a,b]\rightarrow {\mathbb R}$ cuya
integral se quiere aproximar, se pueda sustituir $f$ en la expresi\'on
y obtener un valor ---la \emph{integral aproximada}.

\begin{definition}
Una \emph{f\'ormula de cuadratura simple} para un intervalo $[a,b]$ es una
colecci\'on de puntos 
$x_1<x_2<\dots< x_{n+1}\in [a,b]$ y de valores $a_1,\dots, a_{n+1}$. La \emph{integral
aproximada de una funci\'on $f$ en $[a,b]$} utilizando dicha f\'ormula
de cuadratura es la expresi\'on
\begin{equation*}
a_1f(x_1) + a_2f(x_2)+\dots+a_{n+1}f(x_{n+1}).
\end{equation*}
\end{definition}
Es decir, una f\'ormula de cuadratura no es m\'as que ``la
aproximaci\'on de una integral utilizando valores intermedios y
pesos''. Se dice que la f\'ormula de cuadratura es \emph{cerrada} si
$x_{1}=a$ y $x_{n+1}=b$; si ni $a$ ni $b$ est\'an entre los $x_i$, se dice
que la f\'ormula es \emph{abierta}.

Si los puntos $x_i$ est\'an \emph{equiespaciados}, la f\'ormula se
dice que es \emph{de Newton-Coates}.

L\'ogicamente, interesa encontrar las f\'ormulas de cuadratura que
mejor aproximen integrales de funciones conocidas.

\begin{definition}
Se dice que una f\'ormula de cuadratura es de \emph{orden $n$} si para
cualquier polinomio $P(x)$ de grado $n$ se tiene que
\begin{equation*}
\int_a^bP(x)\,dx = a_1P(x_1) + a_2P(x_2) + \dots + a_{n+1}P(x_{n+1}).
\end{equation*}
Es decir, si \emph{integra con exactitud polinomios de grado $n$}.
\end{definition}

Las f\'ormulas de cuadratura m\'as sencillas son: la del punto medio
(abierta, $n=1$), la f\'ormula del trapecio (cerrada, $n=2$) y la
f\'ormula de Simpson (cerrada, $n=3$). Se explican a continuaci\'on.

Se explican la f\'ormulas \emph{simples} a continuaci\'on y luego se
generalizan a sus versiones \emph{compuestas}.

\subsection{La f\'ormula del punto medio} 
\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,3,3.5,4,5},
xticklabels={$2$, $3$, $3{.}5$, $4$, $5$}
ytick={0,1,2}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat}; \addlegendentry{$f(x)$}
\addplot[fill=yellow, draw=black]
coordinates {
(2, 0)
(2, 1.00362)
(5, 1.00362)
(5, 0)
(2, 0)
};
\addplot[mark=none, blue, line legend, line width={2pt}] 
file {../datafiles/integral-line.dat};
\addplot[mark=*, only marks] coordinates {(3.5, 1.00362)};
\end{axis}
\end{tikzpicture}
\caption{F\'ormula del punto medio: se aproxima el \'area debajo de
  $f$ por el rect\'angulo.}\label{fig:formula-punto-medio-integracion}
\end{figure}
Una manera burda pero natural de aproximar una integral es multiplicar
el valor de la funci\'on en el punto medio por la anchura del
intervalo. Esta es la f\'ormula del punto medio:
\begin{definition}
La f\'ormula de cuadratura del punto medio corresponde a $x_1=(a+b)/2$
y $a_1=(b-a)$. Es decir, se aproxima
\begin{equation*}
\int_a^bf(x)\, dx \simeq (b-a)f\left(\frac{a+b}{2}\right).
\end{equation*}
por el \'area del rect\'angulo que tiene un lado horizontal a la
altura del valor de $f$ en el punto medio.
\end{definition}

Es elemental comprobar que \emph{la f\'ormula del punto medio es de
  orden $1$}: integra con precisi\'on funciones lineales pero no
polinomios de segundo grado.


\subsection{La f\'ormula del trapecio}
La siguiente aproximaci\'on natural (no necesariamente \'optima) es
utilizar dos puntos. Puesto que ya hay dos dados, $a$ y $b$, lo
intuitivo es utilizarlos.

Dados dos puntos $a$ y $b$, se tendr\'an los valores de $f$ en cada
uno de ellos. Se puede interpolar linealmente $f$ con una recta y
aproximar la integral por medio de esta o bien aproxmar la integral
con dos rect\'angulos como en la regla del punto medio y hacer la
media. Ambas operaciones producen el mismo resultado. En concreto:

\begin{definition}
La \emph{f\'ormula del trapecio} para $[a,b]$ consiste en tomar
$x_1=a$, $x_2=b$ y pesos $a_1=a_2=(b-a)/2$. Es decir, se aproxima
\begin{equation*}
\int_a^bf(x)\,dx \simeq \frac{b-a}{2}\left(f(a)+f(b)\right)
\end{equation*}
la integral de $f$ por el \'area del trapecio con base en $OX$, 
que une $(a,f(a))$ con
$(b,f(b))$ y es paralelo al eje $OY$.
\end{definition}
\begin{figure}
\begin{tikzpicture}
\begin{axis}[
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,3,4,5},
ytick={0,1,2}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat}; \addlegendentry{$f(x)$}
\addplot[fill=yellow, draw=black]
coordinates {
(2, 0)
(2, 0.31)
(5, 2.2705)
(5, 0)
(2, 0)
};
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat};
\addplot[mark=*, only marks] coordinates {
(2, 0.31)
(5, 2.2705)
};
\end{axis}
\end{tikzpicture}
\caption{F\'ormula del trapecio: se aproxima el \'area debajo de
  $f$ por el trapecio.}\label{fig:formula-trapecio-integracion}
\end{figure}


La f\'ormula del trapecio, pese a incorporar un punto m\'as que la
regla del punto medio, es tambi\'en de orden $1$.

\subsection{La f\'ormula de Simpson}
El siguiente paso \emph{natural} (que no \'optimo) es utilizar, en
lugar de $2$ puntos, $3$ y en vez de aproximar mediante rectas,
utilizar par\'abolas. Esta aproximaci\'on es singularmente eficaz,
pues tiene orden $3$. Se denomina \emph{f\'ormula de Simpson}.

\begin{definition}\label{def:simpson}
La \emph{f\'ormula de Simpson} es la f\'ormula de cuadratura que
consiste en tomar $x_1=a, x_2=(a+b)/2$ y $x_3=b$, y como pesos, los
correspondientes a la interpolaci\'on correcta de un polinomio de
grado $2$. Es decir\footnote{Esto es sencillo de calcular, usando un
  polinomio de grado $3$, por ejemplo.}, $a_1=\frac{b-a}{6}$, $a_2=\frac{4(b-a)}{6}$ y
$a_3=\frac{b-a}{6}$. As\'i pues, consiste en aproximar
\begin{equation*}
\int_a^bf(x)\,dx \simeq \frac{b-a}{6}\left(
f(a) + 4f\left(\frac{a+b}{2}\right) + f(b)
\right)
\end{equation*}
la integral por una media ponderada de \'areas de rect\'angulos
intermedios. 
Esta f\'ormula es sorprendentemente precisa y \emph{conviene
  sab\'ersela de memoria}: un sexto de la anchura por la
ponderaci\'on $1,4,1$ de los valores en extremo, punto medio, extremo.
\end{definition}


\begin{figure}
\begin{tikzpicture}
\begin{axis}[
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,3,3.5,4,5},
xticklabels={$2$, $3$, $3{.}5$, $4$, $5$},
ytick={0,1,2}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat}; \addlegendentry{$f(x)$}
\addplot[fill=yellow, draw=black]
file {../datafiles/simpson-integral.dat};
\addplot[draw=black] coordinates {(5,0) (2,0)};
\addplot[mark=none, black, line legend, line width={0.5pt}, domain=2:5] 
{0.12739*x*x-0.23824*x+0.27691};
\addplot[mark=*, only marks] coordinates {
(2, 0.31)
(3.5, 1.00362)
(5, 2.2705)
};
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat};
\end{axis}
\end{tikzpicture}
\caption{F\'ormula de Simpson: se aproxima el \'area debajo de
  $f$ por la par\'abola que pasa por los puntos extremos y el punto
  medio (en negro).}\label{fig:formula-simpson}
\end{figure}

Lo singular de la f\'ormula de Simpson es que \emph{es de tercer
  orden}: pese a \emph{interpolar con par\'abolas}, se obtiene una
f\'ormula que integra con precisi\'on polinomios de tercer grado. Para
terminar, t\'engase en cuenta que \emph{la ecuaci\'on de la par\'abola
que pasa por los tres puntos es irrelevante}: no hace falta calcular
el polinomio interpolador, basta usar la f\'ormula de la Definici\'on
\ref{def:simpson}. 

\subsection{Las f\'ormulas compuestas}
Las f\'ormulas de cuadratura compuestas consisten en aplicar una
f\'ormula de cuadratura en subintervalos. En lugar de aproximar, por
ejemplo, utilizando la f\'ormula del trapecio
\begin{equation*}
\int_a^bf(x)\,dx \simeq \frac{b-a}{2}\left(f(a)+f(b)\right)
\end{equation*}
se \emph{subdivide} el intervalo $[a,b]$ en subintervalos y se aplica
la f\'ormula en cada uno de estos. 

Podr\'ia hacerse sin m\'as, m\'ecanicamente, pero el caso es que, si
la f\'ormula es cerrada, para las f\'ormulas de Newton-Coates, 
\emph{siempre es m\'as sencillo aplicar la
  f\'ormula general} que ir subintervalo a subintervalo, pues los
valores en los extremos se ponderan. A\'un as\'i, puede perfectamente
llevarse la cuenta intervalo a intervalo, no pasar\'ia nada m\'as que
que estar\'ian haci\'endose operaciones de m\'as.

\subsubsection{F\'ormula compuesta de los trapecios}
\begin{figure}
\begin{tikzpicture}
\begin{axis}[
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,2.5,3,3.5,4,4.5,5},
xticklabels={$2$, $2{.}5$, $3$, $3{.}5$, $4$, $4{.}5$, $5$},
ytick={0,1,2}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat}; \addlegendentry{$f(x)$}
\addplot[fill=yellow] coordinates {
(2, 0)
(2.00000  , 0.30000)
(2.50000  , 1.20397)
(3.00000  , 1.45930)
(3.50000  , 1.00362)
(4.00000  , 0.54320)
(4.50000  , 0.90358)
(5.00000  , 2.27058)
(5, 0)
(2, 0)
};
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat};
\addplot[draw=black] coordinates {
(2, 0)
(2.00000  , 0.30000)
(2.50000  , 1.20397)
(2.5, 0)
(2.5, 1.20397)
(3.00000  , 1.45930)
(3.0, 0)
(3.0, 1.45930)
(3.50000  , 1.00362)
(3.5, 0)
(3.5, 1.00362)
(4.00000  , 0.54320)
(4.0, 0)
(4.0, 0.54320)
(4.50000  , 0.90358)
(4.5, 0)
(4.5, 0.90358)
(5.00000  , 2.27058)
(5, 0)
(2, 0)
};
\end{axis}
\end{tikzpicture}
\caption{F\'ormula del trapecio compuesta: simplemente repetir la
  f\'ormula del trapecio en cada subintervalo.}\label{fig:formula-trapecio-compuesta}
\end{figure}


Si se tienen dos intervalos consecutivos $[a,b]$ y $[b,c]$ \emph{de
  igual longitud} (es decir, $b-a=c-b$) y se aplica
la f\'ormula de los trapecios en $[a,b]$ y en $[b,c]$ para aproximar
la integral total de $f$ entre $a$ y $c$, queda:
\begin{equation*}
\begin{split}
\int_a^cf(x)\,dx = \frac{b-a}{2}\left(
f(b) + f(a)
\right) + \frac{c-b}{2}\left(f(c) + f(b)\right) = \\
\frac{h}{2}\left(f(a) + 2f(b) + f(c)\right),
\end{split}
\end{equation*}
donde $h=b-a$ es la anchura \emph{del subintervalo}, que es una
ponderación de los valores en puntos extremos e intermedio). En el
caso general, queda algo totalmente análogo: 

\begin{definition}[F\'ormula de los trapecios compuesta]
Dado un intervalo $[a,b]$ y un n\'umero de nodos $n+1$, la
\emph{f\'ormula de los trapecios compuesta} para $[a,b]$ con $n$ nodos
viene dada por los nodos $x_{0}=a, x_1=a+h, \dots, x_n=b$, con
$h=(b-a)/(n-1)$\footnote{Esto no es más que la anchura de cada
  subintervalo, no hay que liarse con la $n$.} y por la aproximaci\'on
\begin{equation*}
\int_a^cf(x)\,dx \simeq \frac{h}{2}\left(f(a) + 2f(x_1) +  2f(x_2) + \dots
  + 2f(x_{n-1}) + f(b)\right).
\end{equation*}
Es decir, la mitad de la anchura de cada intervalo por la suma de los
valores en los extremos y los dobles de los valores en los puntos \emph{interiores}.
\end{definition}



\subsubsection{F\'ormula de Simpson compuesta}
De modo an\'alogo, la f\'ormula de Simpson compuesta consiste en
utilizar la f\'ormula de Simpson en varios intervalos que se unen para
formar uno mayor. Como en la anterior, al ser iguales los extremos
final e inicial de intervalos consecutivos, la f\'ormula final
compuesta no es una simple \emph{suma tonta} de todas las f\'ormulas
intermedias: hay cierta combinaci\'on que simplifica el resultado
final.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,2.75,3.5,4.25,5},
xticklabels={$2$, $2{.}75$, $3{.}5$, $4{.}25$, $5$}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/simpson-composite.dat}; \addlegendentry{$f(x)$}
\addplot[draw=black, only marks, mark=*] coordinates {
(2.0000  , 36.9251)
(2.7500  , 25.5415)
(3.5000   , 8.9888)
(4.2500   , 8.7269)
(5.0000  , 82.0992)
};
\addplot[draw=black, domain=2:3.5] {-4.5947*x*x+6.6469*x+42.0103};
\addplot[draw=black, domain=3.5:5] {65.453*x*x-507.608*x+983.821};
\addplot[fill=yellow, draw=black] file {../datafiles/simpson-composite-1.dat};
\addplot[fill=yellow, draw=black] file {../datafiles/simpson-composite-2.dat};
\addplot[draw=black, line width={2pt}] coordinates {(3.5, 0) (3.5, 8.9888)};
\addplot[mark=none, blue, line legend, line width={2pt}, opacity=0.2] file
{../datafiles/simpson-composite.dat};
\end{axis}
\end{tikzpicture}
\caption{F\'ormula de Simpson compuesta: conceptualmente, repetir la
  f\'ormula de Simpson en cada subintervalo.}\label{fig:formula-simpson-compuesta}
\end{figure}

\begin{definition}[F\'ormula de Simpson compuesta]
Dado un intervalo $[a,b]$ dividido en $n$ subintervalos (y por tanto,
dado un n\'umero de nodos equiespaciados $2n+1$), la
\emph{f\'ormula de Simpson compuesta} para $[a,b]$ con $2n+1$ nodos
viene dada por los nodos $x_{0}=a, x_1=a+h, \dots, x_{2n}=b$, (as\'i
que $h=(b-a)/(2n)$) y por la aproximaci\'on
\begin{equation*}
  \begin{split}
    \int_a^cf(x)\,dx \simeq \frac{b-a}{6n}\left(f(a) + 4f(x_1) + 2f(x_2)
      + 4f(x_3) +\dots\right.& \\ 
    \left.\dots + 2f(x_{2n-2}) + 4f(x_{2n-1}) + f(b) \right)&.
  \end{split}
\end{equation*}
Es decir, un tercio del \emph{espacio entre nodos} multiplicado por: el valor en los
extremos m\'as el doble de la suma de los valores en los puntos extremos
de los subintervalos
m\'as cuatro veces la suma de los valores en los puntos medios de los
subintervalos. 
\end{definition}
Es decir, la f\'ormula de Simpson compuesta es la misma que la de
Simpson en todos los aspectos \emph{salvo en los puntos extremos de
  los intervalos,} donde hay que multiplicar por dos ---obviamente,
pues se est\'an sumando.
Es m\'as f\'acil recordar la f\'ormula si simplemente se
considera que hay que aplicar la f\'ormula simple en cada subintervalo
y sumar todos los t\'erminos que salen.

