% -*- TeX-master: "notas.ltx" -*-
\chapter{Interpolaci\'on}
Dada una colecci\'on de datos, la \emph{tentaci\'on} humana es
utilizarlos como fuentes de conocimiento en lugares
desconocidos. Espec\'ificamente, dada una lista de coordenadas ligadas
a un tipo de suceso (un experimento, unas mediciones\dots)
$(x_{i},y_i)$, lo ``natural'' es intentar utilizarla para
\emph{deducir} o \emph{predecir} el valor que tomar\'ia la $y$ si la
$x$ fuera cualquier otra. Este es el \emph{af\'an interpolador y
  extrapolador} del hombre. No se puede hacer nada para evitarlo. Lo
que se puede hacer es calcular las maneras m\'as razonables de llevar
a cabo dicha interpolaci\'on.

En todo este tema se parte de una lista de datos
\begin{equation}
\label{eq:lista-datos}
  \begin{array}{c|c|c|c|c|c}
    \mathbf{x} & x_0 & x_1 & \dots & x_{n-1} & x_n\\
\hline
    \mathbf{y} & y_0 & y_1 & \dots & y_{n-1} & y_n
  \end{array}
\end{equation}
que se supone ordenada en las coordenadas $\mathbf{x}$, que adem\'as
son diferentes: $x_i<x_{i+1}$.
El objetivo es encontrar funciones que ---de alguna manera--- tengan
relaci\'on (cierta \emph{cercan\'ia}) con dicha lista de datos o
\emph{nube de puntos}.

\section{Interpolaci\'on lineal (a trozos)}
La primera idea (sencilla pero funcional) es utilizar la funci\'on
definida a trozos entre $x_0$ y $x_n$ que consiste en ``unir cada
punto con el siguiente con una recta''. Esto se denomina
\emph{interpolaci\'on lineal a trozos} \'o \emph{spline lineal} (se
definir\'a \emph{spline} m\'as adelante con generalidad).

\begin{definition}
La  \emph{funci\'on de interpolaci\'on lineal a trozos} de la tabla
(\ref{eq:lista-datos}) es la funci\'on $f:[x_0,x_n]\rightarrow
{\mathbb R}$ definida de la siguiente manera:
\begin{equation*}
f(x) = \frac{y_i-y_{i-1}}{x_i-x_{i-1}}(x-x_{i-1})+y_{i-1}\,\,\, \mbox{
si } x\in[x_{i-1},x_i]
\end{equation*}
es decir, la funci\'on definida a trozos que consiste en los segmentos
que unen $(x_{i-1},y_{i-1})$ con $(x_{i},y_i)$, definida desde $x_0$
hasta $x_n$, para $i=1,\dots, n$.
\end{definition}


La interpolaci\'on lineal tiene varias caracter\'isticas que la hacen
interesante: 
\begin{itemize}
\item Es \emph{muy} sencilla de calcular.
\item Pasa por todos los puntos de la tabla de datos.
\item Es continua.
\end{itemize}
Por eso se utiliza con frecuencia para representar funciones (es lo
que hace Matlab por defecto), pues si la nube de puntos es densa, los
segmentos ser\'an peque\~nos y las esquinas se notarán poco.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={$f(x)$, interpolaci\'on},
legend style={at={(0.05,0.95)},
anchor = north west}
]
\addplot[line width={1pt},smooth,mark=none,color=green] file {../datafiles/grafica-interp.dat};
\addplot[line width={1pt},color=red, mark=*] coordinates{
(2.8808, 2.9338)
(3.1622, 3.0066)
(4.4701, 3.1489)
(5.1579, 2.6690)
(6.5491, 3.3480)
(7.3023, 3.7617)
(8.5970, 1.8217)
(9.0020, 2.0297)
(10.1275, 4.6187)
};
\end{axis}
\end{tikzpicture}
\caption{Interpolaci\'on lineal de una nube de 9 puntos. Comp\'arese
  con la funci\'on original (en verde).}\label{fig:interpol-lineal-1}
\end{figure}

La pega de la interpolaci\'on lineal son precisamente las esquinas que
aparecen siempre que la tabla de datos no corresponda a puntos de una
recta. 

\section{?`Tendr\'ia sentido interpolar con par\'abolas?}
Est\'a claro que la interpolaci\'on lineal a trozos est\'a abocada a
generar esquinas siempre que la tabla de datos no corresponda a una
funci\'on lineal. En general, la interpolaci\'on busca no solo una
funci\'on que \emph{pase por todos los puntos}, sino que sea
\emph{razonablemente suave} (por motivos no solo visuales sino de
aproximaci\'on a la realidad). Se podr\'ia intentar mejorar la
aproximaci\'on lineal con funciones de grado mayor, exigiendo que las
tangentes en los puntos intermedios fueran iguales. Podr\'ia
intentarse con segmentos parab\'olicos: puesto que tienen tres grados
de libertad, al segmento $i$-\'esimo se le puede exigir que pase por
los puntos $(x_{i-1},y_{i-1})$ y $(x_{i}, y_i)$ y que tenga la misma
derivada en $x_i$ que el siguiente. Esto puede parecer razonable pero
tiene cierta pega dif\'icilmente salvable: genera una asimetr\'ia
intr\'inseca al m\'etodo (si uno plantea las ecuaciones de dicho
sistema, falta exactamente una por imponer para que sea compatible
determinado; esto hace que sea diferente el caso de un n\'umero par de
nodos y un n\'umero impar, o que la funci\'on de interpolaci\'on sea
asim\'etrica para datos sim\'etricos). Sin entrar en detalles, puede
comprobarse que este \emph{spline cuadr\'atico} no es optimo, aunque
aproxime la nube de puntos de manera que no haya esquinas.

Tiene m\'as problemas (por ejemplo, la curva se desv\'ia mucho de la
nube de puntos si hay puntos cercanos en $x$ pero lejanos en $y$ en
sentidos diferentes). No se utiliza pr\'acticamente nunca, salvo en
construcci\'on, por ejemplo, pues los arcos muy tendidos se aproximan
con par\'abolas.

El caso siguiente, el \emph{spline c\'ubico} es el m\'as utilizado: de
hecho, es lo que los programas de dibujo vectorial utilizan para
trazar arcos (aunque no con una tabla como (\ref{eq:lista-datos}),
sino con dos tablas, pues las curvas son parametrizadas como $(x(t),
y(t))$).

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={$f(x)$, \small{spline cuadr\'atico}},
legend style={at={(0.05,0.95)},
anchor = north west}
]
\addplot[line width={1pt},smooth,mark=none,color=green] file {../datafiles/grafica-interp.dat};
\addplot[line width={1pt},smooth,mark=none,color=blue] file {../datafiles/interp-cuadr.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(2.8808, 2.9338)
(3.1622, 3.0066)
(4.4701, 3.1489)
(5.1579, 2.6690)
(6.5491, 3.3480)
(7.3023, 3.7617)
(8.5970, 1.8217)
(9.0020, 2.0297)
(10.1275, 4.6187)
};
\end{axis}
\end{tikzpicture}
\caption{Interpolaci\'on cuadr\'atica de una nube de 9 puntos. Comp\'arese
  con la funci\'on original (en verde).}\label{fig:interpol-cuadratica}
\end{figure}


\section{Splines c\'ubicos: curvatura continua}
Para aproximar la nube de puntos con polinomios de grado tres, se
utiliza la siguiente definici\'on:
\begin{definition}
Un \emph{spline c\'ubico} de la tabla (\ref{eq:lista-datos}) es una
funci\'on $f:[x_0, x_n]\rightarrow {\mathbb R}$ tal que:
\begin{itemize}
\item La funci\'on $f$ es un polinomio de grado $3$ en cada segmento
  $[x_{i-1},x_i]$, para $i=1,\dots, n$.
\item La funci\'on $f$ es derivable dos veces con continuidad en todos
  los puntos $x_{i}$, para $i=1,\dots, n-1$.
\end{itemize}
\end{definition}
De aqu\'i se deduce que, si se llama $P_i$ al polinomio de grado $3$
que coincide con $f$ en $[x_{i-1},x_i]$, entonces
$P_i^{\prime}(x_i)=P_{i+1}^{\prime}(x_i)$ y
$P_i^{\prime\prime}(x_i)=P_{i+1}^{\prime\prime}(x_i)$; esto junto con
el hecho de que $P_i(x_{i})=y_i$ y $P_i(x_{i+1})=y_{i+1}$ impone $4$
condiciones para cada polinomio $P_i$, salvo para el primero y el
\'ultimo, para los que solo hay $3$ (esta es la \emph{simetr\'ia} que
poseen los splines c\'ubicos y no poseen los cuadr\'aticos). Por
tanto, las condiciones de spline c\'ubico determinan \emph{casi}
un\'ivocamente los polinomios $P_i$. Hace falta una condici\'on en
cada polinomio extremal (en $P_1$ y $P_n$) para determinarlo
totalmente. Estas condiciones pueden ser,  \emph{por ejemplo}:
\begin{itemize}
\item Que la derivada segunda en los puntos extremos sea $0$. A esto
  se le denomina el \emph{spline c\'ubico natural}, pero no tiene por
  qu\'e ser el mejor para un problema concreto. Las ecuaciones son
  $P_1^{\prime\prime}(x_0)=0$ y $P_n^{\prime\prime}(x_n)=0$.
\item Que la derivada \emph{tercera} coincida en los puntos casi-extremos:
  $P_1^{\prime\prime\prime}(x_1)=P_2^{\prime\prime\prime}(x_1)$ y
  $P_n^{\prime\prime\prime}(x_{n-1}) =
  P_{n-1}^{\prime\prime\prime}(x_{n-1})$. Se dice que este spline es \emph{extrapolado}.
\item Que haya cierta condici\'on de periodicidad:
  $P_1^{\prime}(x_0)=P^{\prime}_n(x_n)$ y lo mismo con la derivada
  segunda: $P_1^{\prime\prime}(x_0)=P^{\prime\prime}_n(x_n)$. Esto
  tiene su inter\'es, por ejemplo, si se est\'a interpolando una
  funci\'on peri\'odica.
\end{itemize}

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={$f(x)$, \small{spline c\'ubico}},
legend style={at={(0.05,0.95)},
anchor = north west}
]
\addplot[line width={1pt},smooth,mark=none,color=green] file {../datafiles/grafica-interp.dat};
\addplot[line width={1pt},smooth,mark=none,color=orange] file {../datafiles/spline-cubico.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(2.8808, 2.9338)
(3.1622, 3.0066)
(4.4701, 3.1489)
(5.1579, 2.6690)
(6.5491, 3.3480)
(7.3023, 3.7617)
(8.5970, 1.8217)
(9.0020, 2.0297)
(10.1275, 4.6187)
};
\end{axis}
\end{tikzpicture}
\caption{Spline c\'ubico de una nube de 9 puntos. Comp\'arese
  con la funci\'on original (en verde).}\label{fig:interpol-cubica}
\end{figure}


\subsection{El c\'alculo del spline c\'ubico: una matriz tridiagonal}

Para calcular \emph{efectivamente} el spline c\'ubico conviene
normalizar la notaci\'on. Como arriba, se llamar\'a $P_i$ al polinomio
correspondiente al segmento $[x_{i-1},x_i]$ y se escribir\'a
\emph{relativo al punto $x_{i-1}$}:
\begin{equation*}
P_i(x) = a_i + b_i(x-x_{i-1}) + c_i(x-x_{i-1})^2 + d_i(x-x_{i-1})^3,
\end{equation*}
de modo que se han de calcular los $a_i, b_i, c_i$ y $d_i$ a partir de
la tabla de datos $(\mathbf{x}, \mathbf{y})$ que se supone dada como
arriba (\ref{eq:lista-datos}). La siguiente normalizaci\'on consiste
en, en lugar de utilizar continuamente $x_i-x_{i-1}$, llamar
\begin{equation*}
h_i = x_i-x_{i-1}, \mbox{ para } i=1,\dots, n
\end{equation*}
(es decir, utilizar la anchura de los $n$ intervalos en lugar de las
coordenadas $x_i$ propiamente dichas).

Para aclarar todo el razonamiento, los datos conocidos se escribir\'an
en negrita.

El razonamiento es como sigue:
\begin{itemize}
\item Los $a_{i}$ se calculan directamente, pues
  $P_i(\mathbf{x_{i-1}})=a_i$ y tiene que ser igual a $\mathbf{y_{i-1}}$. Por tanto,
\begin{equation*}
a_i = \mathbf{y_{i-1}} \mbox{ para } i=1,\dots, n
\end{equation*}
\item Se ha de cumplir que $P_i(\mathbf{x_{i}})= \mathbf{y_{i}}$,
  as\'i que, utilizando la condici\'on anterior y llevando $\mathbf{y_{i-1}}$
  al miembro de la derecha, queda
\begin{equation}\label{eq:p-i-pasa-por-y-i}
b_i \mathbf{h_i} + c_i \mathbf{h_i}^2 + d_{i}
\mathbf{h_i}^3 = \mathbf{y_{i}} - \mathbf{y_{i-1}}.
\end{equation}
para $i=1,\dots, n$. Esto da $n$ ecuaciones.
\item La condici\'on de derivada continua es $P_i^{\prime}(\mathbf{x_i}) =
  P_{i+1}^{\prime}(\mathbf{x_i})$, as\'i que
\begin{equation}\label{eq:misma-derivada}
 b_i  + 2c_i \mathbf{h_i} + 3d_i \mathbf{h_i}^{2} = b_{i+1},
\end{equation}
para $i=1,\dots n-1$. De aqu\'i salen $n-1$ ecuaciones.
\item Finalmente, las derivadas segundas han de coincidir en los
  puntos intermedios, as\'i que
\begin{equation}\label{eq:misma-derivada-segunda}
2c_{i} + 6 d_{i} \mathbf{h_{i}} = c_{i+1},
\end{equation}
para $i=1,\dots, n-1$. Esto da $n-1$ ecuaciones.
\end{itemize}
En total se obtienen (aparte de las $a_i$, que son directas), $3n-2$
ecuaciones para $3n$ inc\'ognitas (las, $b, c$ y $d$). Como ya se
dijo, se imponen condiciones en los extremos, pero al finalizar todo
el razonamiento.

Una vez enunciadas las ecuaciones, se realizan simplificaciones y
sustituciones para obtener un sistema m\'as \emph{inteligible}. De
hecho, se despejan las $d$ y las $b$ en funci\'on de las $c$ y queda
un sistema lineal en las $c$. Se procede como sigue:

Primero, se utiliza la ecuaci\'on (\ref{eq:misma-derivada-segunda})
para despejar las $d_{i}$:
\begin{equation}
\label{eq:d-i-despejada}
d_{i} = \frac{c_{i+1}-c_i}{3 \mathbf{h_{i}}}
\end{equation}
y sustituyendo en
(\ref{eq:p-i-pasa-por-y-i}), queda (hasta despejar $b_i$):
\begin{equation}\label{eq:b-i-despejada-uno}
b_i = \frac{\mathbf{y_i}-\mathbf{y_{i-1}}}{\mathbf{h_i}} -
\mathbf{h_i} \frac{c_{i+1}+2c_i}{3};
\end{equation}
por otro lado, sustituyendo el $d_i$ en (\ref{eq:p-i-pasa-por-y-i}) y
operando hasta despejar $b_i$, queda
\begin{equation}
\label{eq:b-i-despejada-dos}
b_i = b_{i-1} + \mathbf{h_{i-1}}(c_i+c_{i-1}).
\end{equation}
para $i=2,\dots,n$. Ahora no hay m\'as que utilizar la ecuaci\'on
(\ref{eq:b-i-despejada-uno}) para $i$ y para $i-1$ e
\emph{introducirla} en esta \'ultima, de manera que solo queden
$c$. Tras una colecci\'on de operaciones simples, se obtiene que
\begin{equation}
\label{eq:ecuacion-final}
\mathbf{h_{i-1}}c_{i-1} + (2 \mathbf{h_{i-1}}+ 2 \mathbf{h_i})c_i +
\mathbf{h_i}c_{i+1} = 3\left(
\frac{\mathbf{y_i}-\mathbf{y_{i-1}}}{\mathbf{h_{i}}} - \frac{\mathbf{y_{i-1}}-\mathbf{y_{i-2}}}{\mathbf{h_{i-1}}}
\right)
\end{equation}
para $i=2,\dots, n$.
 
En forma matricial (ya sin utilizar negritas, para no recargar),
queda un sistema de ecuaciones $Ac= \alpha$, donde
$A$ es
\begin{equation}
\label{eq:forma-matricial-c-i-spline}
A=\begin{pmatrix}
h_1 & 2(h_1+h_2) & h_{2} & 0 & \dots & 0 & 0 & 0\\
0 & h_2 & 2(h_2+h_3) & h_3 & \dots & 0 & 0 & 0\\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots
\\
0 & 0 & 0 & 0 & \dots & h_{n-1} & 2(h_{n-1} + h_n) & h_n
\end{pmatrix}
\end{equation}
y $c$ indica el vector columna de $c_1, \dots, c_n$, mientras que
$\alpha$ es el vector columna
\begin{equation*}
\begin{pmatrix}
\alpha_2 \\
\alpha_3 \\
\vdots \\
\alpha_n
\end{pmatrix}
\end{equation*}
con
\begin{equation*}
\alpha_i = 3\left(
\frac{y_i-y_{i-1}}{h_{i}} - \frac{y_{i-1}-y_{i-2}}{h_{i-1}}\right).
\end{equation*}

Como se ve, este sistema es compatible \emph{indeterminado} (le faltan
dos ecuaciones para tener soluci\'on \'unica). Las ecuaciones se
suelen imponer, como se explic\'o, como condiciones en los
extremos. Por ejemplo, el \emph{spline natural} significa que $c_1=0$
y que $c_n=0$, as\'i que $A$ se completar\'ia por arriba con una fila
$(1\; 0 \; 0 \; \dots \; 0)$ y por abajo con otra $(0 \; 0 \; 0
\; \dots \; 1)$, mientras que al vector $\alpha$ se le a\~nadir\'ia
una primera componente igual a $0$ y una \'ultima tambi\'en igual a $0$.
Con estas $n$ ecuaciones se calcular\'ian las $c_i$ y a partir de
ellas, utilizando las ecuaciones (\ref{eq:d-i-despejada}) y
(\ref{eq:b-i-despejada-uno}), se calculan los valores de las dem\'as
variables.

El sistema de ecuaciones de un spline c\'ubico, somo se ve por la
ecuaci\'on (\ref{eq:forma-matricial-c-i-spline}), es
\emph{tridiagonal}: esto significa que solo tiene elementos diferentes
de cero en la diagonal principal y en las dos diagonales
adyacentes. (Esto es cierto para el spline natural y para cualquiera
que imponga condiciones en $c_1$ y $c_n$ directamente). Estos sistemas
se resuelven muy f\'acilmente con una factorizaci\'on $LU$ ---o bien,
puede implementarse directamente la soluci\'on como un algoritmo en
funci\'on de las $\alpha_i$ y las $h_i$. En cualquier caso, este tipo
de sistemas tridiagonales es importantes reconocerlos y saber que su
factorizaci\'on $LU$ es rapid\'isima (pues si se piensa en el
algoritmo de Gauss, cada operaci\'on de \emph{hacer ceros} solo
requiere hacerlo en una fila por debajo de la diagonal).

\subsection{El algoritmo: sencillo, resolviendo sistemas lineales}
Tras todo lo dicho, puede enunciarse el algoritmo para calcular
el spline c\'ubico que interpola una tabla de datos
$\mathbf{x},\mathbf{y}$ de longitud $n+1$, $\mathbf{x}=(x_0,\dots,
x_n)$, $\mathbf{y}=(y_0,\dots,y_n)$, en la que $x_i<x_{i+1}$ (y por
tanto todos los valores de $\mathbf{x}$ son diferentes), como indica
el algoritmo \ref{alg:spline-cubico}.

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} una tabla de datos $\mathbf{x}, \mathbf{y}$
como se especifica arriba y \textbf{dos} condiciones sobre el primer
nodo y el \'ultimo (una en cada uno)
\STATE \textbf{Output:} un spline c\'ubico que interpola los puntos de
dicha tabla. Espec\'ificamente, una lista de $n$ cuaternas $(a_i, b_i,
c_i, d_i)$ tales que los polinomios $P_i(x) = a_i + b_i(x-x_{i-1}) +
c_i(x-x_{i-1})^{2} + d_i(x-x_{i-1})^3 $ constituyen un spline c\'ubico
para la tabla
\PART{Inicio}
\STATE $a_i \leftarrow y_{i-1}$ para $i$ desde $1$ hasta $n$
\STATE $h_{i}\leftarrow x_i-x_{i-1}$ para $i$ desde $1$ hasta $n$
\STATE $i\leftarrow 1$
\WHILE{$i\leq n$}
\IF{$i>1$ \textbf{and} $i<n$}
\STATE $F_i\leftarrow \left( 0 \; \cdots \;  0 \; h_{i-1} \;
  2(h_{i-1}+h_i) \; h_i \; 0 \; \cdots \; 0\right)$
\STATE $\alpha_i = 3(y_i - y_{i-1})/h_i - 3(y_{i-1}-y_{i-2})/h_{i-1}$
\ELSE
\STATE $F_i\leftarrow$ la fila correspondiente a la ecuaci\'on para
$P_i$
\STATE $\alpha_i$ el coeficiente correspondiente a la condici\'on para $P_i$
\ENDIF
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE $M\leftarrow $ la matriz cuyas filas son las $F_i$ para $i=1$
hasta $n$
\STATE $c\leftarrow M^{-1}\alpha$ (resolver el sistema $Mc=\alpha$)
\STATE $i\leftarrow 1$
\WHILE{$i<n$}
\STATE $b_i\leftarrow (y_i-y_{i-1})/h_i - h_i(c_{i+1}+2c_i)/3$
\STATE $d_i\leftarrow (c_{i+1}-c_i)/(3h_i)$
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE $b_n\leftarrow b_{n-1} + h_{n-1}(c_n+c_{n-1})$
\STATE $d_n\leftarrow (y_n-y_{n-1}-b_nh_n -c_nh_n^2)/h_n^3$
\STATE \textbf{return} $(a,b,c,d)$
\end{algorithmic}
\caption{(C\'alculo del spline c\'ubico.)}
\label{alg:spline-cubico}
\end{algorithm}

\subsection{Una cota sobre la aproximaci\'on}
El hecho de que el spline c\'ubico sea \emph{gr\'aficamente}
satisfactorio no significa que sea \emph{t\'ecnicamente \'util}. En
realidad, lo es m\'as de lo que parece. Si una funci\'on se ``comporta
bien'' en la cuarta derivada, entonces el spline c\'ubico la aproxima
razonablemente bien (y mejor cuanto m\'as estrechos sean los
intervalos entre nodos). En concreto:
\begin{theorem}
Sea  $f:[a,b]\rightarrow {\mathbb R}$ una funci\'on derivable $4$
veces y sup\'ongase que $\abs{f^{4)}(x)}<M$ para $x\in [a,b]$. Sea $h$
el m\'aximo de las anchuras $x_i-x_{i-1}$ para $i=1,\dots, n$. Si
$s(x)$ es el spline c\'ubico para los puntos $(x_i, f(x_i))$, entonces
\begin{equation*}
\abs{s(x)-f(x)} \leq \frac{5M}{384}h^4.
\end{equation*}
\end{theorem}
Este resultado puede ser de gran utilidad, por ejemplo, para calcular
integrales o para acotar errores en soluciones de ecuaciones
diferenciales (que viene a ser lo mismo que integrar, vaya).

\subsection{Definici\'on de spline general}
Se prometi\'o incluir la deficini\'on general de spline:
\begin{definition}
Dada una nube de puntos como (\ref{eq:lista-datos}), un \emph{spline
  de grado $n$}, para $n>0$, que los interpola es una funci\'on
$f:[x_0,x_n]\rightarrow {\mathbb R}$ tal que
\begin{itemize}
\item Pasa por todos los puntos: $f(x_i)=y_i$,
\item Es derivable $n-1$ veces,
\item En cada intervalo $[x_i,x_{i+1}]$ coincide con un polinomio de
  grado $n$.
\end{itemize}
Es decir, es una funci\'on polinomial a trozos de grado $n$ que pasa por todos los
puntos y es derivable $n-1$ veces (se entiende que ser derivable $0$
veces significa ser continua).
\end{definition}

De hecho, los que se utilizan son los de grado $1$ y $3$.

\section[Interpolaci\'on de Lagrange]{El polinomio interpolador de Lagrange: un solo polinomio para
todos los puntos}
Hasta ahora se han estudiado \emph{splines}, funciones que son
polin\'omicas a trozos, para interpolar los datos de una tabla como
(\ref{eq:lista-datos}), imponiendo la condici\'on de que la funci\'on
interpoladora pase por todos los puntos de la tabla.

Se puede plantear el problema ``l\'imite'': buscar un polinomio que
pase por todos los puntos. Naturalmente, se procurar\'a que sea de
grado m\'inimo (para, entre otras cosas, simplificar la
b\'usqueda). Es relativamente sencillo comprobar que existe uno de
grado $n$ (hay, recu\'erdese, $n+1$ datos) y que es \'unico con esta
condici\'on. Este es el \emph{polinomio interpolador de Lagrange}:

\begin{theorem}[del polinomio interpolador de Lagrange]
Dada una lista de puntos como (\ref{eq:lista-datos}) (recu\'erdese que
$x_i<x_{i+1}$), existe un
\'unico polinomio de grado $n$ que pasa por cada $(x_i,y_i)$ para
$i=0,\dots, n$.
\end{theorem}

La prueba del resultado es relativamente elemental. T\'omese un
problema algo m\'as ``sencillo'': dados $n+1$ valores
$x_{0}<\dots<x_n$, ?`se puede construir un polinomio $p_i(x)$ de grado $n$ que
valga $1$ en $x_i$ y cero en $x_j$ para $j\neq i$? Con estas
condiciones, se busca un polinomio de grado $n$ que tenga
$n$ ceros ya prefijados; es obvio que ha de ser
m\'ultiplo de $\phi_i(x)=(x-x_0)(x-x_1)\dots(x-x_{i-1})(x-x_{i+1})\dots
(x-x_{n})$. Lo \'unico que se ha de hacer es multiplicar a $\phi_i(x)$
por una constante para que valga $1$ en $x_i$. El valor de $\phi_i(x)$
en $x_i$ es
\begin{equation*}
\phi_i(x_i) = (x_{i}-x_1)\dots(x_{i}-x_{i-1})(x_i-x_{i+1})\dots
(x_i-x_n) = \prod_{j\neq i}(x_i-x_j),
\end{equation*}
as\'i que se ha de tomar
\begin{equation}
\label{eq:polinomios-de-base-de-lagrange}
p_i(x) = \frac{\prod_{j\neq i}(x-x_j)}{\prod_{j\neq i}(x_i-x_j)}.
\end{equation}
Estos polinomios, para $i=0,\dots,n$, se llaman \emph{polinomios de
  base}. Como se ha visto, cada uno de ellos vale $1$ en el $x_i$
correspondiente y $0$ en el resto, as\'i que pueden pensarse como los
vectores de la base est\'andar de ${\mathbb R}^{n+1}$. El vector
$(y_0, y_1, \dots, y_n)$ es el que se quiere expresar como
combinaci\'on lineal de estos vectores, as\'i que el polinomio que
pasa por los todos puntos $(x_i,y_i)$ para $i=0,\dots,n$ es:
\begin{equation}
\label{eq:polinomio-interpolador-de-lagrange}
\begin{split}
P(x) = y_0p_0(x) + y_1p_1(x) + \dots + y_np_n(x) &= \sum_{i=0}^n y_i 
p_i(x) \\ = \sum_{i=0}^ny_i \frac{\prod_{j\neq i}(x-x_j)}{\prod_{j\neq i}(x_i-x_j)}&.
\end{split}
\end{equation}

Para comprobar que es \'unico, basta tener en cuenta que, si hubiera
otro, la diferencia ser\'ia un polinomio de grado $n$ con $n+1$ ceros,
as\'i que la diferencia ser\'ia el polinomio nulo (y por tanto los dos
que pasan por $(x_i,y_i)$ ser\'ian iguales).

El polinomio interpolador de Lagrange tiene algunas ventajas pero
tiene un par de graves inconvenientes:
\begin{itemize}
\item Los denominadores que aparecen pueden ser muy peque\~nos cuando
  hay muchos puntos y dar lugar a errores de redondeo.
\item Es demasiado \emph{curvo}.
\end{itemize}

El primer problema es intr\'inseco, pues los denominadores que
aparecen hay que calcularlos. El segundo depende esencialmente de la
distribuci\'on de puntos. Un ejemplo famoso e importante se debe a
Runge y ejemplifica el \emph{fen\'omeno de Runge}: si se utilizan
puntos equidistantes para interpolar una funci\'on con derivadas
grandes en valor absoluto, el polinomio interpolador de Lagrange se
desv\'ia mucho (mucho, realmente) de la funci\'on, aunque pase por
todos los puntos de interpolaci\'on. Esto no le ocurre a los splines
c\'ubicos (un spline c\'ubico de once puntos de la funci\'on de Runge
es indistinguible de ella, por ejemplo).


\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={\tiny{$f(x)=\frac{1}{1+12x^2}$}, \tiny{polin. interp.}},
legend style={at={(0.5,0.05)},
anchor = south}
]
\addplot[line width={1pt},smooth,mark=none,color=green] file {../datafiles/runge.dat};
\addplot[line width={1pt},smooth,mark=none,color=red] file
{../datafiles/runge-lagrange.dat};
\addplot[line width={0.01pt},loosely dashed, mark=none, color=blue] file {../datafiles/runge-spline.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(-1.00000,0.07692)
(-0.80000,0.11521)
(-0.60000,0.18797)
(-0.40000,0.34247)
(-0.20000,0.67568)
(0.00000,1.00000)
(0.20000,0.67568)
(0.40000,0.34247)
(0.60000,0.18797)
(0.80000,0.11521)
(1.00000,0.07692)
};
\end{axis}
\end{tikzpicture}
\caption{Fen\'omeno de Runge: el polinomio interpolador de Lagrange
  (rojo) se aleja ilimitadamente de la funci\'on si los nodos est\'an
  equiespaciados. Los trazos negros siguen el spline c\'ubico
  (indistinguible de $f(x)$.)}\label{fig:polinom-lagrange-runge}
\end{figure}


Para solventar el problema de la \emph{curvatura} del polinomio
interpolador de Lagrange cuando se trata de interpolar una funci\'on
conocida, se utilizan m\'etodos basados en la idea de
minimizar el valor m\'aximo que pueda tomar el polinomio
\begin{equation*}
P(x)=(x-x_0)(x-x_1)\dots (x-x_n),
\end{equation*}
es decir, se busca una distribuci\'on de los puntos $x_i$ que resuelva
un problema de tipo \emph{minimax} (minimizar un m\'aximo). Sin entrar
en detalles t\'ecnicos, dado el intervalo $[-1,1]$, se tiene que
\begin{lemma}\label{lem:nodos-de-chebychev}
Los puntos $x_0, \dots, x_n$ que minimizan el valor absoluto m\'aximo
del polinomio $P(x)$ en el intervalo $[-1,1]$ vienen dados por la f\'ormula
\begin{equation*}
x_i = \cos \left(
\frac{2k+1}{n+1} \frac{\pi}{2}
\right)
\end{equation*}
Por tanto, los puntos correspondientes para el intervalo $[a,b]$,
donde $a,b\in {\mathbb R}$, son
\begin{equation*}
\tilde{x_i} = \frac{(b-a)x_i + (a+b)}{2}.
\end{equation*}
\end{lemma}
A los puntos del lemma se les denomina \emph{nodos de Chebychev} de un
intervalo $[a,b]$. Son los que se han de utilizar si se quiere
aproximar una funci\'on por medio del polinomio interpolador de
Lagrange.


\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={\tiny{$f(x)=\frac{1}{1+12x^2}$}, \tiny{polin. interp.}},
legend style={at={(0.5,0.05)},
anchor = south}
]
\addplot[line width={1pt},smooth,mark=none,color=green] file
{../datafiles/runge.dat};
\addplot[line width={1pt},smooth,mark=none,color=magenta] file
{../datafiles/runge-chebychev.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(9.8982e-01,7.8389e-02)
(9.0963e-01,9.1498e-02)
(7.5575e-01,1.2733e-01)
(5.4064e-01,2.2185e-01)
(2.8173e-01,5.1217e-01)
(6.1232e-17,1.0000e+00)
(-2.8173e-01,5.1217e-01)
(-5.4064e-01,2.2185e-01)
(-7.5575e-01,1.2733e-01)
(-9.0963e-01,9.1498e-02)
(-9.8982e-01,7.8389e-02)
};
\end{axis}
\end{tikzpicture}
\caption{Aproximaci\'on de la funci\'on de Runge por el polinomio de
  Lagrange utilizando los nodos de Chebychev. El error m\'aximo
  cometido es mucho menor.}\label{fig:runge-chebychev}
\end{figure}


\section{Interpolaci\'on aproximada}
En problemas relacionados con estudios estad\'isticos o experimentales, los datos se
suponen sujetos a errores, as\'i que tratar de interpolar una nube de
puntos por medio de funciones que pasan por todos ellos puede no tener
ning\'un interes (de hecho, casi nunca lo tiene, en este
contexto). Para delimitar el problema con precisi\'on, hace falta
indicar \emph{qu\'e significa interpolar}, pues cada problema puede
tener una idea distinta de lo que significa que \emph{una funci\'on se
parezca a otra}. En el caso discreto (en el que se tiene una tabla de
datos como (\ref{eq:lista-datos}), lo habitual es intentar minimizar
la distancia cuadr\'atica de $f(x_i)$ a $y_i$, donde $f$ ser\'ia la
funci\'on de interpolaci\'on. Pero podr\'ia interesar algo diferente
(p.ej. minimizar la distancia de la gr\'afica de $f$ a los puntos
$(x_i,y_i)$, un problema diferente del anterior), o cualquier otro
criterio ---el que mejor corresponda a los datos.


\subsection{Interpolaci\'on por m\'inimos cuadrados}
El m\'etodo m\'as utilizado de interpolaci\'on, sin duda alguna, es el
de \emph{m\'inimos cuadrados}. Se parte de una nube de puntos
$\mathbf{x}, \mathbf{y}$, donde $\mathbf{x}$ e $\mathbf{y}$ son
vectores de tama\~no $n$ arbitrarios (es decir, puede haber valores
repetidos en las $\mathbf{x}$ y el orden es irrelevante). Dada una
funci\'on $f:{\mathbb R}\rightarrow {\mathbb R}$, se define:
\begin{definition*}
El \emph{error cuadr\'atico}
de $f$ en el punto $x_i$ (una componente
de $\mathbf{x}$) es el valor $(f(x) - y_i)^2$. El \emph{error
  cuadr\'atico total} de $f$ en la nube dada por $\mathbf{x}$ e
$\mathbf{y}$ es la suma
\begin{equation*}
\sum_{i=0}^n (f(x_i) - y_i) ^2.
\end{equation*}
\end{definition*}

El problema de la interpolaci\'on por m\'inimos cuadrados consiste en,
dada la nube de puntos, encontrar, \emph{en un conjunto determinado de
funciones} una que minimice el error cuadr\'atico total. El matiz de
``en un conjunto de funciones'' es crucial. De hecho, en estas notas se
estudiar\'a con precisi\'on el problema en un espacio vectorial de
funciones y se tratar\'a de manera aproximada el de algunos conjuntos que no
son espacios vectoriales.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[]
\addplot[only marks, mark=x] file {../datafiles/rand-line.dat};
\addplot[domain=-1:1,line width={1pt}, red] {2*x+3.25};
\end{axis}
\end{tikzpicture}
\caption{Interpolaci\'on de una nube de puntos por m\'inimos cuadrados
lineales por una ecuaci\'on del tipo $y=ax+b$.}
\label{fig:minimos-cuadrados-1}
\end{figure}


\subsubsection{M\'inimos cuadrados lineales}
Sup\'ongase que ha de aproximarse una nube de puntos de tama\~no $N$
mediante una funci\'on $f$ que pertenece a un espacio vectorial $V$ de
funciones y que se conoce una base de $V$: $f_1, \dots, f_n$. Se
supone siempre que $N>n$ (y de hecho, habitualmente, $N$ es mucho
mayor que $n$). Es decir, se busca la funci\'on $f\in V$ tal que
\begin{equation*}
\sum _{i=1}^N (f(x_i)-y_i)^2
\end{equation*}
es m\'inimo. Pero, puesto que $V$ es un espacio vectorial, dicha
funci\'on es una combinaci\'on lineal de los elementos de la base:
\begin{equation*}
f = a_1f_1 + a_2f_2 + \dots + a_nf_n.
\end{equation*}
Y, en realidad, se puede plantear el problema como la b\'usqueda de
los coeficientes $a_1, \dots, a_n$ que hacen m\'inima la funci\'on
\begin{equation*}
F(a_1, \dots, a_n) = \sum_{i=1}^N (a_1f_1(x_i) + \dots + a_nf_n(x_i) -
y_i)^2,
\end{equation*}
es decir, dada $F(a_1,\dots, a_n)$, una funci\'on de $n$ variables, se
ha de calcular un m\'inimo. La expresi\'on de $F$ indica claramente
que es una funci\'on diferenciable; por tanto, el punto que d\'e el
m\'inimo resuelve las ecuaciones correspondientes a hacer las
parciales iguales a $0$. Es decir, hay que resolver el sistema
\begin{equation*}
\frac{\partial F}{\partial a_1} = 0, \frac{\partial F}{\partial
  a_2}=0, \dots, \frac{\partial F}{\partial a_n} =0.
\end{equation*}
Si se escribe la parcial de $F$ con respecto a $a_j$, queda
\begin{equation*}
\frac{\partial F}{\partial a_j} = \sum_{i=1}^N 2 f_j(x_i)(a_1f_1(x_i) + \dots
+ a_nf_n(x_i) - y_i) = 0.
\end{equation*}
Si, para simplificar, se denomina 
\begin{equation*}
\mathbf{y_j}=\sum_{i=1}^N f_j(x_i)y_{i}, 
\end{equation*}
uniendo todas las ecuaciones y escribiendo en forma matricial, sale el
sistema
\scriptsize
\begin{equation}
\label{eq:sistema-de-minimos-cuadrados}
\begin{split}
  \begin{pmatrix}
    f_1(x_1) & f_1(x_2) & \dots & f_1(x_N) \\
    f_2(x_1) & f_{2}(x_2) & \dots & f_2(x_N) \\
    \vdots & \vdots & \ddots & \vdots\\
    f_n(x_1) & f_{n}(x_2) & \dots & f_n(x_N) \\
  \end{pmatrix}
  \begin{pmatrix}
    f_1(x_1) & f_2(x_1) & \dots & f_n(x_1) \\
    f_1(x_2) & f_2(x_2) & \dots & f_n(x_{2}) \\
    \vdots & \vdots & \ddots & \vdots\\
    f_1(x_N) & f_2(x_N) & \dots & f_n(x_N)
  \end{pmatrix}
  \begin{pmatrix}
    a_1\\
    a_2 \\
    \vdots\\
    a_n
  \end{pmatrix}=\\
  =
  \begin{pmatrix}
    \mathbf{y_1}\\
    \mathbf{y_2}\\
    \vdots\\
    \mathbf{y_n}
  \end{pmatrix},
\end{split}
\end{equation}
\normalsize
que puede escribirse
\begin{equation*}
XX^ta=Xy
\end{equation*}
donde se sobreentiende que la matriz $X$ es la lista (por filas) de
los valores de cada $f_i$ en los puntos $x_j$, y la $y$ es el vector
columna $(y_0, y_1,\dots, y_n)^T$, es decir:
\begin{equation*}
X =   
\begin{pmatrix}
    f_1(x_1) & f_1(x_2) & \dots & f_1(x_N) \\
    f_2(x_1) & f_{2}(x_2) & \dots & f_2(x_N) \\
    \vdots & \vdots & \ddots & \vdots\\
    f_n(x_1) & f_{n}(x_2) & \dots & f_n(x_N) \\
  \end{pmatrix},\,\,\,
y =
\begin{pmatrix}
y_0\\
y_1\\
\vdots\\
y_n
\end{pmatrix}
\end{equation*}
Este sistema es compatible determinado si hay al menos tantos
puntos como la dimensi\'on de $V$ y las funciones $f_i$ generan filas
independientes en la matriz $X$. 

Es bastante probable que el sistema dado por
(\ref{eq:sistema-de-minimos-cuadrados}) no est\'e muy bien
acon\-di\-cio\-nado.

\subsubsection{Interpolaci\'on no lineal: aproximaciones}
En bastantes casos se requiere aproximar una nube de puntos por una
funci\'on que pertenecza a una familia que no forme un espacio
vectorial. Un ejemplo cl\'asico es tratar de encontrar una funci\'on
del tipo
\begin{equation}\label{eq:gauss-family}
f(x) = ae^{bx^2}
\end{equation}
que aproxime una cierta nube de puntos. Las funciones de ese tipo dan
lugar (para $a,b>0$) a ``campanas de Gauss''. Est\'a claro que dichas
funciones no forman un espacio vectorial, as\'i que no se pueden
aplicar las t\'ecnicas del apartado anterior.

Sin embargo, puede intentar trasladarse el problema a una familia
lineal, aproximar esta por m\'inimos cuadrados y calcular la funci\'on
original equivalente.

Por seguir con el ejemplo, si se toman logaritmos en ambos lados de la
ecuaci\'on (\ref{eq:gauss-family}), se obtiene la expresi\'on
\begin{equation*}
\log(f(x)) = \log(a) + bx^2 = a^{\prime} + bx^2,
\end{equation*}
y, considerando las funciones $1$ y $x^2$, se puede ahora  aproximar
la nube de puntos $\mathbf{x}, \log(\mathbf{y})$, en lugar de la
original, utilizando m\'inimos cuadrados lineales (pues $1$ y $x^2$
forman un espacio vectorial). Si se obtiene la soluci\'on $\log(a_{0})$,
$\log(b_0)$, entonces puede pensarse que
\begin{equation*}
g(x) = e^{a_0}e^{b_0x^2}
\end{equation*}
ser\'a una buena aproximaci\'on de la nube de puntos original
$\mathbf{x}, \mathbf{y}$. Pero \emph{es importante ser consciente de
  que} esta aproximaci\'on posiblemente no sea la mejor respecto del
error cuadr\'atico total. Pi\'ensese que, al tomar logaritmos, los
puntos de la nube cercanos a $0$ estar\'an cerca de $-\infty$, con lo
cual tendr\'an un peso mayor que los que al principio est\'an cerca de
$1$ (que pasan a estar cerca de $0$): al tomar logaritmos, los
errores absolutos y relativos cambian de manera no
lineal. M\'as aun, si uno de los valores de $\mathbf{x}$ es $0$, 
el problema no puede convertirse con el logaritmo, y ha de eliminarse
ese valor o cambiarse por otro aproximado\dots

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={datos,$3e^{-2x^2}$},
legend style={at={(0.95,0.95)},
anchor = north}]
\addplot[only marks, mark=o, red] file {../datafiles/psf-data.dat};
\addplot[line width={1pt}, blue] file {../datafiles/psf-function.dat};
\addplot[line width={1pt}, green] file {../datafiles/log-interpolated-psf.dat};
\end{axis}
\end{tikzpicture}
\caption{Interpolaci\'on no lineal tomando logaritmos: la nube de
  puntos se parece mucho a la funci\'on $f(x)$, pero la
  interpolaci\'on lineal tomando logaritmos y luego haciendo la exponencial
  es \emph{muy mala} (verde). El problema reside en los valores de $y$ muy
  cercanos a $0$: al tomar logaritmos, adquieren una importancia desproporcionada.}
\label{fig:minimos-cuadrados-1}
\end{figure}


\section{C\'odigo de algunos algoritmos}

\subsection{El spline c\'ubico natural}
Matlab, por defecto, calcula splines c\'ubicos con la condici\'on
\emph{not-a-knot}, que es cierta relaci\'on entre las derivadas
terceras en los puntos segundo y penúltimo. Solo se pueden calcular splines
naturales con un \emph{toolbox}. El c\'odigo que sigue implementa el
c\'alculo del spline c\'ubico natural de una colecci\'on de puntos.
La entrada es:
\begin{description}
\item[\texttt{x}] la lista de las coordenadas $x$ de los puntos
\item[\texttt{y}] la lista de las coordenadas $y$ de los puntos
\end{description}

Devuelve un ``objeto'' de Matlab que se denomina \emph{polinomio a
  trozos}, que describe exactamente un objeto polinomial definido a
trozos: los intervalos en los que est\'a definido vienen dados por el
vector \texttt{x} y su valor en un $t$ (que hay que computar utilizando la
funci\'on \texttt{ppval}) viene dado por el valor del polinomio
correspondiente al intervalo de las \texttt{x} que contiene a $t$.


\renewcommand{\lstlistingname}{C\'odigo}
\begin{figure}
%\begin{lstlisting}
\lstinputlisting{../matlab/spline_cubico.m}
%\end{lstlisting}
\caption{C\'odigo del algoritmo para calcular un spline c\'ubico}
\end{figure}


Un ejemplo de uso podr\'ia ser el siguiente, para comparar el spline
c\'ubico con la gr\'afica de la funci\'on seno:
\lstset{numbers=none}
\begin{lstlisting}
> x = linspace(-pi, pi, 10);
> y = sin(x);
> f = spline_cubico(x, y);
> u = linspace(-pi, pi, 400);
> plot(u, ppval(f, u)); hold on;
> plot(u, sin(u), 'r');
\end{lstlisting}
\lstset{numbers=left}

\subsection{El polinomio interpolador de Lagrange}
El c\'alculo del Polinomio interpolador de Lagrange en Matlab/Octave
es extremadamente simple, como se puede ver mirando el C\'odigo
\ref{cod:lagrange-polynomial}. 

La entrada es la siguiente:
\begin{description}
\item[\texttt{x}] El vector de las coordenadas $\mathbf{x}$ de la nube de
  puntos que se quiere interpolar
\item[\texttt{y}] El vector de las coordenadas $\mathbf{y}$ de la nube
\end{description}

La salida es un polinomio \emph{en forma vectorial}, es decir, una
lista de los coeficientes $a_n, a_{n-1}, \dots, a_0$ tal que el
polinomio $P(x)=a_nx^n+\dots +a_1x+a_0$ es el polinomio interpolador
de Lagrange para la nube de puntos $(\mathbf{x}, \mathbf{y})$.

\renewcommand{\lstlistingname}{C\'odigo}
\begin{figure}
%\begin{lstlisting}
\lstinputlisting{../matlab/lagrange.m}
%\end{lstlisting}
\caption{C\'odigo del algoritmo para calcular el polinomio
  interpolador de Lagrange}
\label{cod:lagrange-polynomial}
\end{figure}


\subsection{Interpolaci\'on lineal}
Para la interpolaci\'on lineal en Matlab/Octave se ha de utilizar un
objeto especial: un \emph{array de celdas}: la lista de funciones que
forman la base del espacio vectorial se ha de introducir \emph{entre
  llaves}. 

La entrada es la siguiente:
\begin{description}
\item[\texttt{x}] El vector de las coordenadas $\mathbf{x}$ de la nube de
  puntos que se quiere interpolar
\item[\texttt{y}] El vector de las coordenadas $\mathbf{y}$ de la nube
  \item[\texttt{F}] Un array de celdas de funciones an\'onimas. Cada
    elemento es una funci\'on de la base del espacio vectorial que se
    utiliza para interpolar
\end{description}

La salida es un vector de coeficientes $c$, tal que
$c_1F_1+\dots+c_nF_n$ es la funci\'on de interpolaci\'on lineal de
m\'inimos cuadrados de la nube $(\mathbf{x},\mathbf{y})$.

\renewcommand{\lstlistingname}{C\'odigo}
\begin{figure}
%\begin{lstlisting}
\lstinputlisting{../matlab/interpol.m}
%\end{lstlisting}
\caption{C\'odigo del algoritmo para calcular la interpolaci\'on
  lineal por m\'inimos cuadrados.}
\label{cod:least-squares}
\end{figure}


Un ejemplo de uso podr\'ia ser el siguiente, para aproximar con
funciones trigonom\'etricas
\lstset{numbers=none}
\begin{lstlisting}
> f1=@(x) sin(x); f2=@(x) cos(x); f3=@(x) sin(2*x); f4=@(x) cos(2*x);
> f5=@(x)sin(3*x); f6=@(x)cos(3*x);
> F={f1, f2, f3, f4, f5, f6};
> u=[1,2,3,4,5,6];
> r=@(x) 2*sin(x)+3*cos(x)-4*sin(2*x)-5*cos(3*x);
> v=r(u)+rand(1,6)*.01;
> interpol(u,v,F)
ans =
   1.998522   2.987153  -4.013306  -0.014984  -0.052338  -5.030067
\end{lstlisting}
\lstset{numbers=left}
