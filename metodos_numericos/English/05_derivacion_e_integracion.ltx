% -*- TeX-master: "notas.ltx" -*-
\chapter{Numerical Differentiation and
Integration}\label{cha:derivation-and-integration}
Numerical differentiation is explained in these notes using the symmetrical
increments formula and showing how this can be generalized for higher
order derivatives. A brief glimpse into the instability of the problem
is given.

Integration is dealt with in deeper detail (because it is easier and
much more stable) but also briefly. A simple definition of quadrature
formula is given and the easiest ones (the trapezoidal and
Simpson's rules) are explained.

\section{Numerical Differentiation}
Sometimes (for example, when approximating the solution of a
differential equation) one has to approximate the value of the
derivative of a function at point. A symbolic approach may be
unavailable (either because of the software or because of its
computational cost) and a numerical recipe may be required. Formulas
for approximating the derivative of a function at a point are
available (and also for higher order derivatives) but one also needs,
in general, bounds for the error incurred. In these notes we shall
only show the symmetric rule and explain why the naive approximation is
suboptimal.\margin{instability?}
%A brief idea about the instability is given.

\subsection{The Symmetric Formula for the Derivative}
The first (simple) idea for approximating the derivative of $f$ at $x$
is to use the definition of derivative as a limit, that is, the
following formula:
\begin{equation}
\label{eq:derivada-asimetrica}
f^{\prime}(x) \simeq \frac{f(x+h)-f(x)}{h},
\end{equation}
where $h$ is a \emph{small increment} of the $x$ variable.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[
enlargelimits=false,
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={0.2, 0.3, 0.4},
xticklabels={\tiny{$x_{0}-h$}, \tiny{$x_0$}, \tiny{$x_0+h$}},
ytick={2.5, 3.3333, 5},
yticklabels={\tiny{$f(x_0-h)$}, \tiny{$f(x_0)$}, \tiny{$f(x_0+h)$}}
]
\addplot[domain=0.15:0.52, blue, line width={0.5pt}] {1/x};
\addplot[dashed, line width={1.2pt}] coordinates {(0.2, 5) (0.4, 2.5)};
\addplot[dotted, line width={1.2pt}] coordinates {(0.3, 3.3333) (0.4, 2.5)};
\addplot[dotted, line width={1.2pt}] coordinates {(0.3, 3.3333) (0.2, 5)};
\addplot[black, line width={1.2pt}] coordinates {(0.19, 4.555) (0.41,
  2.1111)};
\addplot[line width={1pt}, only marks, mark=*] coordinates {
(0.3, 3.3333)
(0.2, 5)
(0.4, 2.5)
};
\addplot[line width={0.3pt}, loosely dashed] coordinates {
(0.2, 0)
(0.2, 5)
(0.15, 5)
};
\addplot[line width={0.3pt}, loosely dashed] coordinates {
(0.3, 0)
(0.3, 3.3333)
(0.15, 3.3333)
};
\addplot[line width={0.3pt}, loosely dashed] coordinates {
(0.4, 0)
(0.4, 2.5)
(0.15, 2.5)
};
\end{axis}
\end{tikzpicture}
\caption{Approximate derivative: on the right \& left (dotted)
  and symmetric (dashed). The symmetric one is much similar to
  the tangent (solid straight line).}\label{fig:formula-punto-medio-integracion}
\end{figure}

However, the very expression in formula (\ref{eq:derivada-asimetrica})
shows its weakness: should one take $h$ positive or negative? This is
not irrelevant. Assume $f(x)=1/x$ and try to compute its derivative at
$x=2$. We shall take
$\abs{h}={.}01$. Obviously $f^{\prime}(0{.}5)=0{.}25$. Using the ``natural''
approximation one has, for $h>0$:
\begin{equation*}
\frac{f(x+{.}01)-f(x)}{{.}01}=\frac{\frac{1}{2{.}01}-\frac{1}{2}}{{.}01}
= -0{.}248756+
\end{equation*}
whereas, for $h<0$:
\begin{equation*}
\frac{f(x-{.}01)-f(x)}{-{.}01}=-\frac{\frac{1}{1{.}99}-\frac{1}{2}}{{.}01}
= -0{.}251256+
\end{equation*}
which are already different at the second decimal digit. Which of the
two is to be preferred? There is no abstract principle leading to
choosing one or the other. 

Whenever there are two values which approximate a third one and there
is no good reason for preferring one to the other, it is reasonable to
expect that \emph{the mean value} should be a better approximation
than either of the two. In this case:
\begin{equation*}
\frac{1}{2}\left( \frac{f(x+h)-f(x)}{h}+ \frac{f(x-h)-f(x)}{-h}\right) = 
-0.2500062+
\end{equation*}
which is an approximation to the real value $0{.}25$  to five
significant figures.

This is, actually, the correct way to state the approximation problem
to numerical differentiation: to use the symmetric difference around
the point and divide by the double of the width of the interval. This
is exactly the same as taking the mean value of the ``right'' and
``left'' hand derivative.

\begin{theorem}\label{the:formula-simetrica-derivada-orden-2}
The naive approximation to the derivative has order
$1$ precision, whereas the symmetric formula has order $2$.
\end{theorem}
\begin{proof}
Let $f$ be a three times differentiable function on 
$[x-h,x+h]$. 
Using the Taylor polynomial of degree $1$, one has
\begin{equation*}
f(x+h) = f(x) + f^{\prime}(x) h + \frac{f^{\prime\prime}(\xi)}{2}h^2,
\end{equation*}
for some $\xi$ in the interval, so that, solving $f^{\prime}(x)$, one gets
\begin{equation*}
f^{\prime}(x) = \frac{f(x+h)-f(x)}{h} - \frac{f^{\prime\prime}(\xi)}{2}h,
\end{equation*}
which is exactly the meaning of ``having order $1$''. Notice that the
rightmost term cannot be eliminated.

However, for the symmetric formula one can use the Taylor polynomial
of degree $2$, twice:
\begin{equation*}
\begin{split}
f(x+h) = f(x) + f^{\prime}(x) h +
\frac{f^{\prime\prime}(x)}{2}h^2 + \frac{f^{3)}(\xi)}{6}h^3,\\
f(x-h) = f(x) - f^{\prime}(x) h + 
\frac{f^{\prime\prime}(x)}{2}h^2 - \frac{f^{3)}(\zeta)}{6}h^3
\end{split}
\end{equation*}
for some $\xi\in[x-h,x+h]$ and $\zeta\in[x-h,x+h]$. Subtracting:
\begin{equation*}
f(x+h) - f(x-h) = 2f^{\prime}(x) h + K(\xi, \zeta)h^3
\end{equation*}
where $K$ is a sum of the degree $3$ coefficients in the previous
equation, so that
\begin{equation*}
f^{\prime}(x) = \frac{f(x+h)-f(x-h)}{2h} + \frac{K(\xi, \zeta)}{2}h^2,
\end{equation*}
which is the meaning of ``having order $2$''. 
\end{proof}

\section{Numerical Integration---Quadrature Formulas}
Numerical integration, being a problem in which \emph{errors
  accumulate} (a ``global'' problem) is \emph{more stable} than
differentiation, surprising as it may seem (even though taking
derivatives is much simpler, symbolically, than integration).

In general, one wishes to state an \emph{abstract formula} for
performing numerical integration: an algebraic expression such that,
given a function
$f:[a,b]\rightarrow {\mathbb R}$ whose integral is to be computed, one
can substitute $f$ in the expression and get a value ---the
\emph{approximated integral}.

\begin{definition}
A \emph{simple quadrature formula} for an interval $[a,b]$ is a family
of points
$x_1<x_2<\dots< x_{n+1}\in [a,b]$ and coefficients (also called
weights) $a_1,\dots,
a_{n+1}$.
The  \emph{approximate integral of a function $f$ on $[a,b]$ using
that formula} is the expression
\begin{equation*}
a_1f(x_1) + a_2f(x_2)+\dots+a_{n+1}f(x_{n+1}).
\end{equation*}
\end{definition}
That is, a quadrature formula is no more than ``a way to approximate
an integral using intermediate points and weights''. The formula is
\emph{closed} if $x_1=a$ and $x_{n+1}=b$; if neither $a$ nor $b$ are
part of the intermediate points, then it is \emph{open}.

If the $x_i$ are evenly spaced, then the formula is a
\emph{Newton-Coates} formula. These are the only ones we shall explain
in this course.

Obviously, one wants the formulas which best approach the integrals of
known functions.

\begin{definition}
  A quadrature formula with coefficients $a_1, \dots, a_{n+1}$
  is \emph{of order $m$} if for any polynomial of
degree $m$, one has
\begin{equation*}
\int_a^bP(x)\,dx = a_1P(x_1) + a_2P(x_2) + \dots + a_{n+1}P(x_{n+1}).
\end{equation*}
That is, if the formula is exact for \emph{polynomials of degree $m$}. 
\end{definition}

The basic quadrature formulas are: the \emph{midpoint} formula
(open, $n=0$), the trapezoidal rule (closed, two points, $n=1$) and Simpson's
formula (closed, three points, $n=2$).

We first show the \emph{simple} versions and then generalize them to
their \emph{composite} versions.

\subsection{The Midpoint Rule} 
\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,3,3.5,4,5},
xticklabels={$2$, $3$, $3{.}5$, $4$, $5$},
ytick={0,1,2}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat}; \addlegendentry{$f(x)$}
\addplot[fill=blue, draw=black, pattern=north west lines]
coordinates {
(2, 0)
(2, 1.00362)
(5, 1.00362)
(5, 0)
(2, 0)
};
\addplot[mark=none, blue, line legend, line width={2pt}] 
file {../datafiles/integral-line.dat};
\addplot[mark=*, only marks] coordinates {(3.5, 1.00362)};
\end{axis}
\end{tikzpicture}
\caption{Midpoint rule: the area under $f$ is approximated by the
  rectangle.}\label{fig:rule-punto-medio-integracion}
\end{figure}

A coarse but quite natural way to approximate an integral is to
multiply the value of the function at the midpoint by the width of the
interval. This is the \emph{midpoint formula}:
\begin{definition}
  The \emph{midpoint quadrature formula} corresponds to  $x_1=(a+b)/2$
and $a_1=(b-a)$. That is, the approximation
\begin{equation*}
\int_a^bf(x)\, dx \simeq (b-a)f\left(\frac{a+b}{2}\right).
\end{equation*}
given by the area of the rectangle having a horizontal side at
$f((b-a)/2)$. 
\end{definition}

One checks easily that the midpoint rule is of order $1$: it
is exact for linear polynomials but not for quadratic ones.

\subsection{The Trapezoidal Rule}
The next natural approximation (which is not necessarily better) is to
use two points. As there are two already given ($a$ 
and $b$), the naive idea is to use them.

Given $a$ and $b$, one has the values of $f$ at them. One could
interpolate $f$ linearly (using a line) and approximate the value of
the integral with the area under the line. Or one could use the mean
value between two rectangles (one with height $f(a)$ and the other
with height $f(b)$). The fact is that both methods give the same value.

\begin{definition}
The \emph{trapezoidal rule} for $[a,b]$ corresponds to
$x_1=a$, $x_2=b$ and weights $a_1=a_2=(b-a)/2$. That is, the approximation
\begin{equation*}
\int_a^bf(x)\,dx \simeq \frac{b-a}{2}\left(f(a)+f(b)\right)
\end{equation*}
for the integral of $f$ using the trapeze with a side on $[a,b]$,
joining $(a,f(a))$ with $(b,f(b))$ and parallel to $OY$.
\end{definition}
\begin{figure}
\begin{tikzpicture}
\begin{axis}[
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,3,4,5},
ytick={0,1,2}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat}; \addlegendentry{$f(x)$}
\addplot[fill=yellow, draw=black, pattern=north west lines]
coordinates {
(2, 0)
(2, 0.31)
(5, 2.2705)
(5, 0)
(2, 0)
};
\addplot[mark=none, blue, line legend, line width={2pt}, ] file
{../datafiles/integral-line.dat};
\addplot[mark=*, only marks] coordinates {
(2, 0.31)
(5, 2.2705)
};
\end{axis}
\end{tikzpicture}
\caption{trapezoidal rule: approximate the area under
  $f$ by that of the trapeze.}\label{fig:formula-trapecio-integracion}
\end{figure}

Even though it uses one point more than the midpoint rule, the
trapezoidal rule is also of order $1$.

\subsection{Simpson's Rule}
The next natural step involves $3$ points instead of $2$ and using
a parabola instead of a straight line. This method is remarkably
precise (it has order $3$) and is widely used. It is called
\emph{Simpson's Rule}.

\begin{definition}\label{def:simpson}
\emph{Simpson's rule} is the quadrature formula corresponding to
the nodes $x_1=a, x_2=(a+b)/2$ and $x_3=b$, and the weights,
corresponding to the correct interpolation of a degree $2$
polynomial. That is\footnote{This is an easy computation which the
  reader should perform by himself.}, $a_1=\frac{b-a}{6}$,
$a_2=\frac{4(b-a)}{6}$ and
$a_3=\frac{b-a}{6}$. Hence, it is the approximation of the integral of
$f$ by
\begin{equation*}
\int_a^bf(x)\,dx \simeq \frac{b-a}{6}\left(
f(a) + 4f\left(\frac{a+b}{2}\right) + f(b)
\right),
\end{equation*}
which is a weighted mean of the areas of three intermediate
rectangles. \emph{This rule must be memorized}: one sixth of the
length times the values of the function at the endpoints and midpoint
with weights $1, 4, 1$.
\end{definition}


\begin{figure}
\begin{tikzpicture}
\begin{axis}[
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,3,3.5,4,5},
xticklabels={$2$, $3$, $3{.}5$, $4$, $5$},
ytick={0,1,2}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat}; \addlegendentry{$f(x)$}
\addplot[draw=black, pattern=north west lines]
file {../datafiles/simpson-integral.dat};
\addplot[draw=black] coordinates {(5,0) (2,0)};
\addplot[mark=none, black, line legend, line width={0.5pt}, domain=2:5] 
{0.12739*x*x-0.23824*x+0.27691};
\addplot[mark=*, only marks] coordinates {
(2, 0.31)
(3.5, 1.00362)
(5, 2.2705)
};
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat};
\end{axis}
\end{tikzpicture}
\caption{Simpson's rule: approximating the area under $f$ by the
  parabola passing through the endpoints and the midpoint (black).}
\label{fig:rule-simpson}
\end{figure}

The remarkable property of Simpson's rule is that it has order $3$:
even though a parabola is used, the rule integrates correctly
polynomials of degree up to $3$. Notice that \emph{one does not need
  to know the equation of the parabola}: the values of $f$ and the
weights ($1/6, 4/6, 1/6$) are enough.

\subsection{Composite Formulas}
Composite quadrature formulas are no more than ``applying the simple
ones in subintervals''. For example, instead of using the trapezoidal
rule for approximating an integral, like
\begin{equation*}
\int_a^bf(x)\,dx \simeq \frac{b-a}{2}\left(f(a)+f(b)\right)
\end{equation*}
one subdivides $[a,b]$ into subintervals and performs the
approximation on each of these.

One might do this mechanically, one subinterval after another. The
fact is that, due to their additive nature, \emph{it is always simpler
to apply the general formula} than to apply the simple one step by
step, for closed Newton-Coates formulas (not for open ones, for which
the computations are the same one way or the other). 

\subsubsection{Composite Trapezoidal Rule}
\begin{figure}
\begin{tikzpicture}
\begin{axis}[
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,2.5,3,3.5,4,4.5,5},
xticklabels={$2$, $2{.}5$, $3$, $3{.}5$, $4$, $4{.}5$, $5$},
ytick={0,1,2}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat}; \addlegendentry{$f(x)$}
\addplot[pattern=north west lines] coordinates {
(2, 0)
(2.00000  , 0.30000)
(2.50000  , 1.20397)
(3.00000  , 1.45930)
(3.50000  , 1.00362)
(4.00000  , 0.54320)
(4.50000  , 0.90358)
(5.00000  , 2.27058)
(5, 0)
(2, 0)
};
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/integral-line.dat};
\addplot[draw=black] coordinates {
(2, 0)
(2.00000  , 0.30000)
(2.50000  , 1.20397)
(2.5, 0)
(2.5, 1.20397)
(3.00000  , 1.45930)
(3.0, 0)
(3.0, 1.45930)
(3.50000  , 1.00362)
(3.5, 0)
(3.5, 1.00362)
(4.00000  , 0.54320)
(4.0, 0)
(4.0, 0.54320)
(4.50000  , 0.90358)
(4.5, 0)
(4.5, 0.90358)
(5.00000  , 2.27058)
(5, 0)
(2, 0)
};
\end{axis}
\end{tikzpicture}
\caption{Composite trapezoidal rule: just repeat the simple one on
  each subinterval.}\label{fig:formula-trapecio-compuesta}
\end{figure}

If there are two consecutive intervals $[a,b]$ and $[b,c]$ \emph{of
  equal length} (that is, $b-a=c-b$) and the trapezoidal rule is
applied on both in order to approximate the integral of $f$ on
$[a,c]$, one gets:
\begin{equation*}
\begin{split}
\int_a^cf(x)\,dx = \frac{b-a}{2}\left(
f(b) + f(a)
\right) + \frac{c-b}{2}\left(f(c) + f(b)\right) = \\
\frac{h}{2}\left(f(a) + 2f(b) + f(c)\right),
\end{split}
\end{equation*}
where $h=b-a$ is the width of each subinterval, either $[a,b]$ or
$[b,c]$: the formula just a weighted mean of the values of $f$ at the
endpoints and the midpoint. In the general case, when there are more
than two subintervals, one gets an analogous formula:

\begin{definition}[Composite trapezoidal rule]
Given an interval $[a,b]$ and $n+1$ nodes, the
\emph{composite trapezoidal rule} for $[a,b]$ with $n+1$ nodes
is given by the nodes $x_{0}=a, x_1=a+h, \dots, x_n=b$, the value
$h=(b-a)/n$ and the approximation
\begin{equation*}
\int_a^cf(x)\,dx \simeq \frac{h}{2}\left(f(a) + 2f(x_2) +  2f(x_3) + \dots
  + 2f(x_{n}) + f(b)\right).
\end{equation*}
That is, half the width of the subintervals times the sum of the
values at the endpoints and the double of the values at the interior
points.
\end{definition}



\subsubsection{Composite Simpson's Rule}
In a similar way, the composite Simpson's rule is just the application
of Simpson's rule in a sequence of subintervals (which, for the
Newton-Coates formula, have all the same width). As before, as the
left endpoint of each subinterval is the right endpoint for the next
one, the whole composite formula is somewhat simpler to implement than
the mere addition of the simple formula on each subinterval.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={2,2.75,3.5,4.25,5},
xticklabels={$2$, $2{.}75$, $3{.}5$, $4{.}25$, $5$}
]
\addplot[mark=none, blue, line legend, line width={2pt}] file
{../datafiles/simpson-composite.dat}; \addlegendentry{$f(x)$}
\addplot[draw=black, only marks, mark=*] coordinates {
(2.0000  , 36.9251)
(2.7500  , 25.5415)
(3.5000   , 8.9888)
(4.2500   , 8.7269)
(5.0000  , 82.0992)
};
\addplot[draw=black, domain=2:3.5] {-4.5947*x*x+6.6469*x+42.0103};
\addplot[draw=black, domain=3.5:5] {65.453*x*x-507.608*x+983.821};
\addplot[pattern=north west lines, draw=black] file {../datafiles/simpson-composite-1.dat};
\addplot[pattern=north west lines, draw=black] file {../datafiles/simpson-composite-2.dat};
\addplot[draw=black, line width={2pt}] coordinates {(3.5, 0) (3.5, 8.9888)};
\addplot[mark=none, blue, line legend, line width={2pt}, opacity=0.2] file
{../datafiles/simpson-composite.dat};
\end{axis}
\end{tikzpicture}
\caption{Composite Simpson's rule: conceptually, it is just the
  repetition of the simple rule on each subinterval.}\label{fig:formula-simpson-compuesta}
\end{figure}

\begin{definition}[Composite Simpson's rule]
  Given an interval $[a,b]$, divided into $n$ subintervals (so, given
  $2n+1$ evenly distributed nodes), \emph{Simpson's composite rule}
  for $[a,b]$ with $2n+1$ nodes is given by the nodes $x_{0}=a,
  x_1=a+h, \dots, x_{2n}=b$, (where $h=(b-a)/(2n)$) and the
  approximation
\begin{equation*}
  \begin{split}
    \int_a^cf(x)\,dx \simeq \frac{b-a}{6n}\left(f(a) + 4f(x_2) + 2f(x_3)
      + 4f(x_3) +\dots\right.& \\ 
    \left.\dots + 2f(x_{2n-1}) + 4f(x_{2n}) + f(b) \right)&.
  \end{split}
\end{equation*}
\end{definition}
This means that the composite Simpson's rule is the same as the simple
version taking into account that one multiplies all by $h/6$, where
$h$ is the width of each subinterval and the coefficients are $1$ for
the two endpoints, $4$ for the inner midpoints and $2$ for the
inner endpoints.

