% natural cubic spline: second derivative at both
% endpoints is 0. Input is a pair of lists describing
% the cloud of points.
function [f] = natural_spline(x, y)
  n = length(x)-1;

  % variables and coefficients for the linear system,
  % these are the ordinary names. Initialization
  h = diff(x);
  dy = diff(y);
  F = zeros(n);
  a = y(1:end-1);
  alpha = [0 3*(dy(2:end-1)./h(2:end-1)-dy(1:end-2)./h(1:end-2)) 3/2*(-dy(end-1)/h(end-1)+dy(end)/h(end))]';
  A1 = diag([h(1:end-2) h(end-1)/2], -1);
  A2 = diag([0 h(2:end-1)], 1);
  A3 = diag([1 2*(h(1:end-2)+h(2:end-1)) h(end)+h(end-1)]);
  A  = A1+A2+A3;

  % Solve the c coefficients:
  c = (A\alpha)';

  % Initialize b and d
  b = zeros(1,n);
  d = zeros(1,n);

  % unroll all the coefficients as in the theory
  k = 1;
  while(k<n)
    b(k) = (y(k+1)-y(k))/h(k) - h(k) *(c(k+1)+2*c(k))/3;
    k=k+1;
  end
  d(1:end-1) = diff(c)./(3*h(1:end-1));

  % the last b and d have explicit expressions:
  b(n) = b(n-1) + h(n-1)*(c(n)+c(n-1));
  d(n) = (y(n+1)-y(n)-b(n)*h(n)-c(n)*h(n)^2)/h(n)^3;

  % finally, build the piecewise polynomial (a Matlab function)
  % we might implement it by hand, though
  f = mkpp(x,[d; c; b ;a ]');
end
