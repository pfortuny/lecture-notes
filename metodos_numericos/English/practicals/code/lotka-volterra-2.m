% Define the function (same parameters)
% Notice that because x is a vector (albeit a column one),
% one can reference each component with a single index:
f = @(t, x) [0.8*x(1) - 0.4*x(1)*x(2) ; -2*x(2) + 0.2*x(1)*x(2)];

% Set up the time
T = [0:.1:12];
% Initial conditions
X0 = [18; 3];
% Solve
X = eulervector(f, T, X0);
% Plot. Notice how Matlab plots each row using different colours
plot(T, X)
