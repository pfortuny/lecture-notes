% Simulation of a pendulum, for two different
% initial conditions. Mass = 1

% The pendulum equation: theta'' = - sin(theta)
P = @(t, X) [X(2) ; -sin(X(1))];

% Time:
T=[0:.01:40];
% Initial condition: pi/4
X0=[pi/4; 0];

% Solve
Y = heunvector(P, T, X0);
% Plot both angle Y(1,:) and angular speed Y(2,:)
plot(T, Y)
hold on

% Initial condition: pi/6
X1=[pi/6;0];

% Solve & plot
Y2 = heunvector(P, T, X1);
plot(T, Y2)
