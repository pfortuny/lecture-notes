% profile_euler(x, yp, y0):
%
% Given lists of x-coordinates and yp-slopes and an initial height y0,
% return the heights at each stretch (coordinate x) of a road having
% slopes yp, starting at height y0.
function [y] = profile_euler(x, yp, y0)
    y = zeros(size(x));
    y(1) = y0;
    for s = 1:length(x)-1
      y(s+1) = y(s) + yp(s)*(x(s+1) - x(s));
    end
end
