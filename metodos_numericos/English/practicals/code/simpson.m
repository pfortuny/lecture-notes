% Numerical quadrature using Simpson's.
% Input: a, b, f, where
%   a, b are real numbers (the endpoints of the integration interval)
%   f    is an anonymous function

function [v] = simpson(a, b, f)
    v = (b-a)./6*(f(a) + 4*f((a+b)./2) + f(b));
end
