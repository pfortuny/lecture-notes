% Euler's method for numerical integration of ODEs, vector version
% INPUT:
% 1) an anonymous function f(t, x)
%    of two variables:
%    t: numerical
%    x: column vector
% 2) a vector T of t-positions (including t0 as the first one)
% 3) a column vector x0 of initial values of x for t=t0

% OUTPUT:
% a matrix of as many rows as x0 and as many columns as T above,
% which approximate the solution to the (vector) ODE given by f.
function [y] = eulervector(f, x, y0)
    % first of all, create 'y' with the adequate size
    y = zeros(length(y0),length(x));
    % and store the initial condition in the first position
    y(:,1) = y0;

    % Run Euler's loop
    for s = 2:length(x)
         y(:,s) = y(:,s-1) + (x(s) - x(s-1)) * f(x(s-1),y(:,s-1));
    end
end
