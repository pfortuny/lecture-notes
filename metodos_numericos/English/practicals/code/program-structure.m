function [o1, o2, ..., on] = fName(p1, p2, ...., pn)
    o1 = reasonable default value
    o2 = reasonable default value
    % ...
    % and so on until all the output values have been initialized
    
    % fi a counter is needed, initialize it here
    k = 1

    % Some computations may have to be performed before the main
    % loop, they would go here. For example: the FIRST iteration
    % may have to be carried out BEFORE starting the loop.

    % Now a WHILE or FOR loop will come.
    % It will generally be a WHILE() unless the number of iterations
    % or the set of values on which to iterate is already known. Or
    % In a WHILE loop there will be several stopping conditions, the
    % last one will be of the form
    %     k < Number of Iterations
    % IF THERE IS A COUNTER, obviously

    while( CONDITION1 && CONDITION2 ... && CONDITIONn )

         % The body of the algorithm goes here, this is
         % "The algorithm itself".
         % (bisection, Newton-Raphson, Euler...)
         %
         % Here one may find ifs, assignations, anything

    end

    % Before finishing, one has to verify that the answer is
    % at least coherent:
    % If there is a counter and the maximum number of iterations
    % has been reached, then one MUST warn the user of this fact
    % like this:
    if( k >= N) % assuming the maximum number is N
        warning('Maximum number of iterations reached')
    end

    % end of the program, do not forget the trailing 'end'
end
