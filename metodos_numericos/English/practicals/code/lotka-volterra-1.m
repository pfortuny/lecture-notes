% Lotka-Volterra simulation, first version
% Time from 0 to 12 seconds
t=[0:.1:12];
% Create an empty list of the adequate size for both variables
x=zeros(size(t));
y=zeros(size(t));
% Initial values
x(1)=18;
y(1)=3;

% The differential equation (Notice 3 variables)
xp = @(t,x,y) 0.8*x - 0.4*x*y;
yp = @(t,x,y) -2*y + 0.2*x*y;

% Do the Euler step for each time
for k=1:length(t)-1
    x(k+1) = x(k) + xp(t(k),x(k),y(k)) * (t(k+1) - t(k));
    y(k+1) = y(k) + yp(t(k),x(k),y(k)) * (t(k+1) - t(k));
end

% Finally, plot both species on the same graph
plot(t,x)
hold on
plot(t,y,'r')
