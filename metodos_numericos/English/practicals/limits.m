% my first complicated matlab function:
% given a cloud of points, with ordered x-coordinates
% and all of them different (the x-coordinates), return:
%
% 1) the minimal distance bewteen points and to which indices it corresponds
% 2) the Maximal distance bewteen points...
% 3) the limits (end values) of the x-coordinates
% 4) the limits (end values) of the y-coordinates
function [m, M, xlim, ylim] = limits(x,y)
    % first of all: create the return variables with
    % "default" values (corresponding to wrong inputs);
    xlim = [min(x), max(x)];
    ylim = [min(y), max(y)];

    % defaults for min, max and indices
    dmin = +inf;
    dmax = 0;
    im = 0;
    jm = 0;
    iM = 0;
    jM = 0;

    m    = [dmin, im, jm];
    M    = [dmax, iM, jM];

    % sanity check, display an explanation error
    if(any(diff(x)<=0))
        disp("Error: the x-coordinates must be ordered.");
        return;
    end

    % basic combinatorics: looping through different pairs
    % is the same as looping through the first index and
    % then only through the ones greater than that.
    for k = 1:length(x)
        for l = k+1:length(x)

            d = ((x(k)-x(l)).^2 + (y(k)-y(l)).^2); 

            % minimum?
            if d < dmin
                dmin = d;
                im = k;
                jm = l;
            end

            % maximum?
            if d > dmax
                dmax = d;
                iM = k;
                jM = l;
            end

        end
    end
    
    % store the right values in m & M
    m = [dmin, im, jm];
    M = [dmax, iM, jM];
end
