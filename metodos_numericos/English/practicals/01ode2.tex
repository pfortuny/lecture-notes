% -*- TeX-master: "practicals.ltx" -*-
\chapter{Ordinary Differential Equations (II)}\label{cha:ode2}
We are going to work out some examples in this chapter.

\section{The Lotka-Volterra model}
The first non-trivial differential equation describing a biological
system which we are going to study is called the ``Lotka-Volterra''
equation and is used to model an environment in which two species
live, one which behaves as a prey and the other as its predator.

Let $x(t)$ denote the population of a ``prey'' species at time
$t$, and $y(t)$ the population of the ``predator'' species. The
differential equation describing this model is based on the following
(quite simplistic) assumptions:

\begin{enumerate}
\item\label{lv-prey-breed} The prey species breeds proportionally to its number.
\item\label{lv-prey-die} A prey dies only as a consequence of being eaten by
  some predator, with some constant probability.
\item\label{lv-pred-die} The predator species dies proportionally to its number.
\item\label{lv-pred-breed} Predators only breed in proportion to their
  eating the preys.
\end{enumerate}

From item \ref{lv-prey-breed} we infer that there is a number $\alpha>0$
such that
\begin{equation*}
  \dot{x}(t) = \alpha x(t) + \dots
\end{equation*}
for some expression instead of the dots. This is an exponential
increase in the number of preys (apart from the interaction with the
predators).

From item \ref{lv-pred-die} we conclude that there is a number
$\gamma>0$ such that
\begin{equation*}
  \dot{y}(t) = -\gamma y(t) + \dots
\end{equation*}
for some other (different from the above) expression instead of the
dots. This gives an exponential decrease in the number of predators
(in the absence of feeding).

It is easy to show that the probability of a predator meeting a prey
at some point in space is proportional to the product of the number of
predators and preys. Because not every
meeting ends up in a prey being eaten, we model the probability of
this event as $\beta x(t) y(t)$ (with $\beta >0$) and because not every feeding of a
predator gives rise to breeding, we model the probability of breeding
(for predators) as $\delta x(t) y(t)$ for some $\delta > 0$.
Hence, we can substitute the
dots above by the corresponding values and get the Lotka-Volterra
differential equation:
\begin{equation}
  \label{eq:lotka-volterra}
  \begin{array}{l}
    \dot{x}(t) = \alpha x(t) - \beta x(t) y(t)\\[5pt]
    \dot{y}(t) = -\gamma y(t) + \delta x(t) y(t) 
  \end{array}
\end{equation}

\begin{example}\label{exa:lotka-volterra-1}
  Model a Lotka-Volterra system with parameters $\alpha=0{.}8$, $\beta=0{.}4$,
  $\gamma=2$, $\delta=0{.}2$ and initial populations $x(0)=18$,
  $y(0)=3$. Use Euler's method to compute an approximation
  with a timestep of $0.1$ units and
  compute up to $12$ seconds.

  We first do it by hand and then shall try to write a general
  program. Listing \ref{lst:lotka-volterra-1.m}  is a
  complicated way to work out this example and plot the evolution
  of both populations in time.

  \lstinputlisting[language=Matlab, label=lst:lotka-volterra-1.m,
  caption={A first approximation to the Lotka-Volterra
    equations.}]{code/lotka-volterra-1.m}

  Notice how the populations have an increasing and decreasing
  behaviour, with a shift.

  Also, with some effort one can verify approximately that the critical points of
  $x(t)$ are reached when $y(t)= \gamma/\delta$ and that the critical
  points of $y(t)$ happen when $x(t)=\alpha/\beta$. (How would you do
  this? It is not so easy but not so difficult either).

  However, it is known that the Lotka-Volterra system is periodic
  (this is something known, not something that we have proved in the
  theory classes) and the approximation we have plotted is not
  periodic (if one goes on plotting, the graphs become more and more
  separated and spiky). This anomaly is due also to the intrinsic
  imprecision of Euler's method.
\end{example}

It should be easy to realize that the process above can be simplified
if one writes a function to perform Euler's algorithm with ordinary
differential equations in several variables.

First of all, the expression
\begin{equation*}
  \begin{array}{l}
    \dot{x}(t) = \alpha x(t) - \beta x(t) y(t)\\[5pt]
    \dot{y}(t) = -\gamma y(t) + \delta x(t) y(t) 
  \end{array}
\end{equation*}
can be written in vector form as
\begin{equation*}
  \begin{pmatrix}
    \dot{x}(t)\\
    \dot{y}(t)
  \end{pmatrix}=
  \begin{pmatrix}
    f_1(t,x,y) \\
    f_2(t,x,y)
  \end{pmatrix}
\end{equation*}
for adequate values of $f_1$ and $f_2$. In general, it does not need to
be a $2-$dimensional vector: it can be of any size. And, to make
things even more general, one might write the above as:
\begin{equation*}
  \begin{pmatrix}
    \dot{x}_{1}(t)\\
    \vdots\\
    \dot{x}_n(t)
  \end{pmatrix}=
  F(t, x_1, \dots, x_n)
\end{equation*}
where $F$ is a \emph{column vector function} of $n$ components. Thus, in
order to describe the initial value problem, one needs:
\begin{itemize}
\item An $n-$compoment \emph{column vector function} of $n+1$ variables.
\item The $n$ initial values, one for each $x_i$.
\end{itemize}
Notice that we work with column vectors, which is the way one usually
writes ordinary differential equations on paper.

One might define then a function \texttt{eulervector} which receives:
\begin{itemize}
\item an
  anonymous function returning a column vector (the $F$ above),
\item a vector
  of time-positions and
\item a column vector of initial positions
\end{itemize}
and which
returns a list of column vectors with the same length as the list of
time-positions
(that is, a matrix of $n$ rows, as many as variables and $l$ columns,
as many as time-positions).

\begin{example}
  The code in Listing \ref{lst:eulervector.m} shows a possible
  implementation of the function \texttt{eulervector} described above.

  \lstinputlisting[language=Matlab, label=lst:eulervector.m,
  caption={A vector implementation of the Euler method.}]
  {code/eulervector.m}

  Notice in the code that the only difference with our previous
  implementation of Euler's algorithm, as in Listing
  \ref{lst:euler-method-ode}, is the explicit apparition of the rows
  in the \texttt{y} vector, both at the beginning, in the line
  \begin{lstlisting}
    y = zeros(length(y0), length(x));
  \end{lstlisting}
  and all the other times, with the colon in the expresssions
  \texttt{y(:, ...)}.

  Using this function, the Lotka-Volterra model of Example
  \ref{exa:lotka-volterra-1} can be worked out with the code in
  Listing \ref{lst:lotka-volterra-2.m}.

    \lstinputlisting[language=Matlab, label=lst:lotka-volterra-2.m,
  caption={Lotka-Volterra with \texttt{eulervector}.}]{code/lotka-volterra-2.m}

  It should be noted that one no longer uses two different variables,
  \texttt{x} and \texttt{y}, but a single ``column vector''
  \texttt{x}, and references each component using the appropriate
  index. So, when defining \texttt{f}, instead of \texttt{x} one uses
  \texttt{x(1)} and instead of \texttt{y}, one uses
  \texttt{x(2)}. Also, the solution is stored in a single variable
  (\texttt{X} in the example) which (in the example)
  has two rows, one for each
  component of the system.

  Finally, notice how Matlab plots each row in a matrix with a
  different colour, as though it were a list. This makes unnecessary
  the use of \texttt{hold on}.  
\end{example}

\begin{exercise}\label{ex:heunvector}
  Write a function \texttt{heunvector} which implements Heun's method
  for vector differential equations. It should be obvious that the
  only difference with Listing \ref{lst:eulervector.m} should be
  changing the following line
\begin{lstlisting}
         y(:,s) = y(:,s-1) + (x(s) - x(s-1)) * f(x(s-1),y(:,s-1));
\end{lstlisting}
  for something a bit more complicated (the Heun step of ``going
  forward like Euler, computing the velocity and then using the mean
  value of both velocities at the starting point.'')
\end{exercise}

\begin{exercise}
  Using the function \texttt{heunvector} which has just been defined,
  plot the approximate solution to the Lotka-Volterra equation of
  Example \ref{exa:lotka-volterra-1}. Plot, also, the solution given
  by Euler's method and compare them. Which looks more reasonable?
  Why? What are the absolute and relative differences after $12$
  seconds?

  Verify (somehow) that the maxima and minima of each variable
  correspond to the quotients of the parameters of the other one. How
  could you do this?
  
  You should notice how Heun's solution looks more periodic than
  Euler's. Actually, this reflects the true solution to the system
  more accurately, as it is known that the solutions to the
  Lotka-Volterra equations are periodic, indeed.
\end{exercise}

\section{Epidemic models (SIR)}

\begin{exercise}\label{ex:epidemic-sir}
  The $SIR$ epidemic model for an infectious disease follows the differential
  equation:
  \begin{equation*}
    \begin{array}{l@{\;=\;}l}
      \dot{S}(t) & -\alpha S(t)I(t)\\
      \dot{I}(t) & -\beta I(t) + \alpha S(t)I(t)\\
      \dot{R}(t) & \beta I(t)
    \end{array}
  \end{equation*}
  for some positive numbers $\alpha$ and $\beta$. The variables
  $S, I$ and $R$
  stand for ``susceptible'', ``infected'' and ``removed.'' Use the
  Matlab function \texttt{heunvector} defined in Exercise
  \ref{ex:heunvector} to plot several models of this equation, for
  different initial values and parameters. An interesting example is
  $S(0)=1$, $I(0)=0{.}001$, $R(0)=0$ and $\alpha=0{.}1$, $\beta=0{.}05$,
  time from $0$ to $1500$ using $2000$ points, for example.
  Compare the evolution of that system with another one having
  $\alpha=0{.}05$ and $\beta=0{.}01$. Does the system behave
  differently if  $\alpha > \beta$ or if $\alpha < \beta$? In what
  way?
  
  Compare also the difference between using \texttt{heunvector} and
  using \texttt{eulervector}. Which seems more precise? Why?
\end{exercise}

\begin{exercise}\label{ex:epidemic-sir-2}
  Modify the model in Exercise \ref{ex:epidemic-sir} to allow that
  some of the infected people become susceptible, with
  a coefficient $\gamma$. Compare this model to the previous one,
  using \texttt{heunvector}.
\end{exercise}

\begin{exercise}\label{ex:epidemic-sir-3}
  Allow for births in the model of Exercise \ref{ex:epidemic-sir-2}
  (i.e. $S(t)$ increases proportionally to the sum of $S(t), I(t)$ and
  $R(t)$) with some small coefficient
  $\delta$). Compare this model with the ones of Exercises
  \ref{ex:epidemic-sir-2} and \ref{ex:epidemic-sir}. Explain the plots
  and the (remarkable) difference between $R(t)$ in both previous
  exercises and this one.
\end{exercise}

\begin{exercise}\label{ex:epidemic-sir-4}
  Allow for deaths in the model of Exercise \ref{ex:epidemic-sir-3}:
  let each of $S(t), R(t)$ and $I(t)$ decrease with different probabilities
  $\epsilon_1$, $\epsilon_2$ and $\epsilon_3$, with $\epsilon_1 =
  \epsilon_3$ and $\epsilon_2 > \epsilon_1$. Does the model change a
  lot? Why? Compare with the previous ones.
\end{exercise}

\section{Physical systems}

\begin{example}\label{exa:pendulum-1}
  A simple pendulum without friction can be simulated using the
  following (polar) equation
  \begin{equation*}
    l\ddot{\theta} = -g\sin(\theta)
  \end{equation*}
  where $\theta$ is the angle with respect to the vertical and $l$ is
  the (constant) length of the pendulum. Notice how the mass of the
  weight is irrelevant. We can use
  \texttt{heunvector} to compute an approximation to the solution of
  this equation for example, using $l=1$, $g=9.81$ and two different initial
  conditions: $\theta(0)=pi/4$ and $\theta(0)=pi/3$ with initial
  velocities $0$ in both cases, with time going
  from $0$ to $50$ in steps of $0{.}01$. The code in
  Listing \ref{lst:pendulum-1.m} shows how.

  \lstinputlisting[language=Matlab, label=lst:pendulum-1.m,
  caption={Simulation of a pendulum with Heun's method.}]
  {code/pendulum-1.m}
  
\end{example}


\begin{exercise}\label{ex:pendulum-2}
  Compute approximate solutions to the same systems as in Example
  \ref{exa:pendulum-1} using \texttt{eulervector} and compare. Which
  seems more apt? Why? 
\end{exercise}

\begin{exercise}
  Consider the pendulum of Example \ref{exa:pendulum-1} but with
  friction. Assume friction is proportional to the angular speed (and
  with opposite direction), with proportionality constant
  $0{.}1$. The equation needs to be rewritten taking into account the
  mass at the end of the pendulum. Plot the graph and compare with the
  previous exercise (use $l=1$ everywhere and the mass of your
  choice). The effect is called
  ``damping,'' in this case ``exponential damping'' because the
  damping term (what makes the sine waves diminish in amplitude) is
  exponential.
\end{exercise}


\begin{exercise}\label{ex:harmonic}
  Harmonic motion is defined by the second order differential equation
  \begin{equation*}
    \ddot{x} = -\frac{k}{m} x
  \end{equation*}
  for some elasticity constant $k$ and mass $m$ of the moving body.
  It is \emph{damped} if there is a damping term which goes against motion:
  \begin{equation*}
    \ddot{x} = -\frac{k}{m} x - r\dot{x}
  \end{equation*}
  for some damping constant $r>0$. Study the behaviour of the
  oscillator with and without damping, using both \texttt{eulervector}
  and \texttt{heunvector}. What happens with \texttt{eulervector}?

  What happens if you set $r$ to some negative value?
\end{exercise}

\begin{exercise}\label{ex:ballistic}
  The ballistic equation describes the motion of a body under
  gravity. If $(x(t), y(t))$ is its position vector, then the equation
  is
  \begin{equation*}
    \begin{array}{l}
    \ddot{x} = 0\\
    \ddot{y} = -g.
    \end{array}
  \end{equation*}
  Given an initial position $(0,0)$ and speed $(1,1)$, plot the
  $(x(t),y(t))$ coordinates of the corresponding motion for $t$ from
  $0$ to $20$ in steps of $0.1$. Use \texttt{heunvector}.

  What is the differential equation if there is friction and it is
  proportional to the velocity (but in opposite direction)? Plot the
  trajectory corresponding to this motion for the same initial
  conditions as before.

  Finally, compute and plot the trajectory if friction is proportional
  to \emph{the square} of each component of the velocity, in each
  component.
\end{exercise}