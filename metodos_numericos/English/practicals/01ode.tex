% -*- TeX-master: "practicals.ltx" -*-
\chapter{Ordinary Differential Equations}\label{cha:ode}

This practical sessions are devoted to the numerical integration of
ordinary differential equations. We shall mostly deal with
one-variable problems but we might make an incursion into
several variables, if time allows.

\section{Cycling race profile}
The most important idea to recall is that the derivative
$y^{\prime}(x)$ of a function
of one variable, $y(x)$ at a point $x_0$ is \emph{the slope of the
  graph of $y(x)$ at $(x_0,y(x_0))$}. This is so important that we
shall take some time to chisel it on our minds.

%\begin{example}
  Let $\mathbf{y}^{\prime}=(y^{\prime}_0,y^{\prime}_1,\dots,y^{\prime}_{n-1})$
  denote the slopes of the road
  at different points in a bicycle race and $\mathbf{x}=(x_0,x_1,
  \dots, x_n)$ the horizontal coordinates of each point. Notice that
  $\mathbf{x}$ is not the list of km marks at each point but
  the $OX-$axis coordinate of each point. Notice also that
  $\mathbf{y}^{\prime}$ has one coordinate less than $\mathbf{x}$.

  If the race starts at height $y_0$, how would one compute the
  approximate heights at each point $x_i$ for $i=1,\dots, n$?
  
  This is an easy example with several possible solutions (depending
  on the reader's preference). The most obvious way to tackle this
  problem is by assuming that the slope on each interval $[x_i,
  x_{i+1})$ is constant and has value $y^{\prime}_i$. This way, the height at
  $x_1$ would be computed as $y_1=y_0 + y^{\prime}_0(x_1-x_0)$,
  because the line passing through $(x_0,y_0)$ with slope
  $y^{\prime}_0$ has equation
  \begin{equation*}
    y = y_0+y^{\prime}_0(x-x_0).
  \end{equation*}
  From here one can now compute the approximate height at $x_2$, using
  the same reasoning: $y_2=y_1+(x_2-x_1)y^{\prime}_1$, and
  iteratively,
  \begin{equation*}
    y_i = y_{i-1}+(x_i-x_{i-1})y^{\prime}_{i-1},\,\,\,\mbox{ for }i=1,\dots,n.
  \end{equation*}
  This shows clearly why the initial height $y_0$ is necessary:
  without it there is no way to compute $y_1$ and hence, 
  $y_i$ for $i\geq 1$.
%\end{example}

\begin{example}
  A simple example with just $5$ nodes. Let a road have the following list
  of slopes: $(+4\%, -2\%, +7\%, +12\%)$ at the horizontal coordinates
  (in \emph{km}):
  $(0, 2, 5, 7)$. The road reaches up to km $8$ on the $OX$
  axis. The race starts at a height of $850m$. Draw a profile of the race.

  This example can be done by hand:
  \begin{enumerate}
  \item The first stretch is $2km$ long horizontally and has a slope
    of $+4\%$. This means that, if the slope is constant, the road goes
    up by $2km\times 4\%=80m$. As the race starts at $850m$, after this
    stretch, the road will be approximately $850m+80m=930m$ high.
  \item The second stretch is $3km$ long horizontally and its slope is
    $-2\%$, so that the road goes \emph{down} by $3km\times
    2\%=60m$. Hence, after this stage the road is $930m-60m=870m$ high.
  \item The third stage is $2km$ long and has slope $+7\%$. This gives
    $140m$ up, so the road ends at $870m+140m=1010m$.
  \item Finally, $1km\times 12\%=120m$, so that the road ends at
    $1010m+120m=1130m$.
  \end{enumerate}
  The approximate profile is plotted in Figure
  \ref{fig:simple-profile}.
  \end{example}
  \begin{figure}[h]
  \centering
  \begin{tikzpicture}
    \begin{axis}[ymin=630,
      axis y discontinuity = crunch,
      ylabel=Height (m), xlabel=Horizontal
      stretch (Km)]
      \addplot[color=orange, line width=1.5pt] coordinates
      {(0, 850) (2,930) (5, 870) (7,1010) (8,1130)};
    \end{axis}
  \end{tikzpicture}
  \caption{Approximate profile of a short race.}
  \label{fig:simple-profile}
\end{figure}


\begin{example}\label{ex:cycling-stage-euler}
  The same example, with many more data. Construct $\mathbf{x}$ as
  a vector with $25$ components between $0$ and $200$. Construct now
  a random vector $\mathbf{y}^{\prime}$ with values between $-15\%$ and $15\%$
  (which are normal slopes for a road). Choose a height $y_0$ as the
  starting value. Compute the successive heights for each $x_i$ and
  draw the profile of the stage.

  A solution to this can be found in Listing \ref{lst:cycling-stage-euler}
  \lstinputlisting[language=matlab, label=lst:cycling-stage-euler,
  caption={Approximate profile of a cycling race, first
    version.}]{code/stage.m}

  A possible profile is plotted in Figure
  \ref{fig:detailed-race-profile}.
  \end{example}
  \begin{figure}[h]
  \centering
  \begin{tikzpicture}
    \begin{axis}[ymin=860,
      axis y discontinuity = crunch,
      ylabel=Height (m), xlabel=Horizontal
      stretch (Km)]
      \addplot[color=orange, line width=1.5pt] file
      {fig/detailed-race-profile.dat};
    \end{axis}
  \end{tikzpicture}
  \caption{Approximate profile of a long race.}
  \label{fig:detailed-race-profile}
\end{figure}

Of course, the method explained is one way to solve the stage
profile problem approximately. There are more (although with the
data that is available, there are not \emph{many} more which are reasonable).

\begin{example}\label{ex:cycling-stage-mean-value}
  The same problem can be tackled differently. Notice that in Example
  \ref{ex:cycling-stage-euler} we are using just the slope \emph{at the
  beginning of the interval}, which may be too little information. One
  might think ``why use the left endpoint and not the right
  one?''. As a matter of fact, a more reasonable solution would be to
  somehow use the information at both endpoints for each
  interval. Instead of taking $y^{\prime}_{i-1}$ or $y^{\prime}_i$ as
  the slope for interval $[x_{i-1},x_i],$ one could use the mean value
  as the ``mean slope'' on that interval:
  \begin{equation*}
    \tilde{y}^{\prime}_{i-1}=\frac{y^{\prime}_{i-1}+y^{\prime}_i}{2}
  \end{equation*}
  and compute the height at step $i$ as
  \begin{equation*}
    y_i=y_{i-1} + (x_i-x_{i-1})\tilde{y}^{\prime}_{i-1}.
  \end{equation*}
  Notice (and this is important) that in order to perform these
  calculations, we \emph{need to know} the slope at the last point, so
  that $\mathbf{y}^{\prime}$ must have as many components as $\mathbf{x}$
  for this method to work.
  
  This leads to the code of Listing
  \ref{lst:cycling-stage-mean-value}.
\end{example}
 
\lstinputlisting[language=matlab, label=lst:cycling-stage-mean-value,
  caption={Approximate profile of a cycling race, ``mean value''
    version.}]{code/stage-mean-value.m}

\begin{exercise}\label{exer:compare-cycling-one}
  Using the codes of Listings \ref{lst:cycling-stage-euler} and
  \ref{lst:cycling-stage-mean-value}, compare the solutions to the
  same problem using both methods; ``compare'' both analytically
  (using absolute and relative differences) and graphically. 
\end{exercise}

\begin{exercise}\label{exer:m-file-for-ciclying}
  Write two \texttt{m-}files, one for each of the methods in examples
  \ref{ex:cycling-stage-euler} and
  \ref{ex:cycling-stage-mean-value}. In each file, a function should
  be defined, taking as arguments two vectors of the same length,
  \texttt{x} and \texttt{yp} and a
  real number \texttt{y0}. The output should be a vector \texttt{y}
  containing the heights of the profile at each point in
  \texttt{x}. Call them \texttt{profile\_euler.m} and
  \texttt{profile\_mean.m}.

  Use them several times with the same data and compare the
  plots. Which is smoother? Why?

  A sample \texttt{profile\_euler.m} file might read as Listing
  \ref{lst:profile-euler.m}
  \lstinputlisting[language=Matlab, label=lst:profile-euler.m,
  caption={Sample code for
    \texttt{profile\_euler.m}.}]{code/profile_euler.m}
\end{exercise}

\section{Numerical Integration of ODEs}
The previous section was just an introduction to the problem of
numerical integration of Ordinary Differential Equations (ODE from now
on). As we saw in class, we shall consider only\footnote{Or rather,
  \emph{mainly}.} ODEs of the form
\begin{equation*}
  y^{\prime} = f(x,y).
\end{equation*}
We are now going to translate this expression into ordinary language.

First of all, the derivative of a function $y(x)$
\emph{the slope of the graph $(x,y(x))$ at each point}: let
$y(x)$ be a differentiable function and $x_0$ a real number. Then
$y^{\prime}(x_0)$ is exactly the \emph{slope} (as the slope of a road,
the same concept) of the graph of $y(x)$ at $(x_0,y(x_0))$.

In Figure \ref{fig:sin-and-tangent} the graph of the function
$y(x)=\sin(x)$ has been plotted together with the tangent line at
$(\frac{2\pi}{6}, \frac{\sqrt{3}}{2})$. Notice how the slope of this
line is $0{.}5$ (as a matter of fact, notice how it goes up by $2$ units
vertically along a $4$ units long interval) which is exactly the value
of $y^{\prime}(x)=\cos(x)$ at $x_0=\frac{2\pi}{6}$. This should
reinforce the idea that ``derivative means slope.''
\begin{figure}[h]
  \centering
  \begin{tikzpicture}
    \begin{axis}[xmin=-1,
      xmax=3,
      ymin=-1,
      ymax=3,
      ]
      \addlegendentry{$y(x)=\sin(x)$};
      \addlegendentry{$y=0{.}5(x-1{.}047)+0{.}866$};
      \addplot[line width={1.2pt}, color=blue] file
      {code/sin--1-3.dat};
      \addplot[line width={1pt}, color=black, dashed] file
      {code/sin-1-3-tangent.dat};
      \addplot[line width={1pt}, mark=*, color=red] coordinates
      {(1.0472, 0.86603)};
    \end{axis}
  \end{tikzpicture}
  \caption{Graph of $y(x)=\sin(x)$ and its tangent at
    $\left(\frac{2\pi}{6},\frac{\sqrt{3}}{2}\right)$.}
  \label{fig:sin-and-tangent}
\end{figure}

With this mindset, the expression
\begin{equation*}
  y^{\prime}=f(x,y)
\end{equation*}
can only have an interpretation: ``the function $y(x)$ is such
that its slope at each point $(x_0,y(x_0))$ is exactly
$f(x_0,y(x_0))$.'' Briefly stated, ``the slope of the curve $y(x)$
at $x$ is $f(x,y)$.''

As in the case of the cycling race, giving the slopes of the road at
each point is not enough to compute the heights: one needs an initial
height in order for the problem to have a definite solution. The same happens
with an ODE: the single statement $y^{\prime}=f(x,y)$ cannot be enough
to provide a solution. One needs a piece of data more:
the ``initial height'' of the graph of $y(x)$. This gives rise to the
notion of \emph{initial value problem}.

\begin{definition}\label{def:initial-value-problem}
  An initial value problem is an ODE $y^{\prime}=f(x,y)$ together with
  a pair $(x_0,y_0)$ called the \emph{initial condition} or
  \emph{initial value}.
\end{definition}

Once an initial value is given for $y(x_0)$ at some $x_0$, the ODE
$y^{\prime}=f(x,y)$ has a unique solution\footnote{Under natural
  conditions which are outside the scope of the practicals.}. 

\begin{example}\label{ex:ode-exponential}
  The initial value problem
  \begin{equation*}
    y^{\prime}=y,\,\,\, y(0)=1,
  \end{equation*}
  has as solution the function $y(x)=e^x$. Check this.

  If instead of $y(0)=1$ one has $y(0)=K$, then the solution to the
  corresponding initial value problem is
  \begin{equation*}
    y(x) = Ke^x.
  \end{equation*}

  What happens if the initial value is $y(1)=0{.}5$. Is it necessary
  that $x_0=0$ or can one compute the solution anyway?\Endexample
\end{example}

As the reader will have already noticed, in order to state an initial
value problem, one needs the following data:
\begin{enumerate}
\item The function $f(x,y)$, of two real variables.
\item The pair $(x_0,y_0)$, that is, two real values, one for the $x$
  and another one for the corresponding $y$.
\end{enumerate}
However, for a numerical approach, one also needs the \emph{network of
$x-$coordinates} on which approximations of $y(x)$ will be
computed. As in the cycling examples above, this may be a vector
$\mathbf{x}$ of ``horizontal positions.'' Thus, in order to compute a
numerical approximation to the solution $y(x)$, one requires
\begin{enumerate}
\item[(3)] A vector $\mathbf{x}$ of $x-$coordinates on which to
  approximate $y(x)$.
\end{enumerate}

The ``solution'' to the numerical integration of the ODE will be the
list of values $y(x)$ for each $x\in \mathbf{x}$. The first one will
be, obviously, $y_0$, the remaining ones will be approximations to the
true solution.

\subsection{Euler's method}
The first naive numerical method is Euler's algorithm which follows
literally Example \ref{ex:cycling-stage-euler}. Let $n$ be the length
of the vector $\mathbf{x}$. Then one can describe Euler's method as
Algorithm \ref{alg:euler}, which is as easy as it gets. Notice that
$x_0$ and $y_0$ are \emph{part of the data}.
\begin{algorithm}
  \begin{algorithmic}
    \FOR{$i=1\dots n$}
    \STATE $y_i = y_{i-1}+(x_i-x_{i-1})f(x_{i-1},y_{i-1})$
    \ENDFOR
    \STATE \textbf{return} $(y_0,y_1,\dots,y_n)$
  \end{algorithmic}
  \caption{Euler's method.}
  \label{alg:euler}
\end{algorithm}

The statement inside the \textbf{for} loop is exactly the same as for
the cycling race profile: the height at position $x_i$ is approximated as
the height at
 $x_{i-1}$ plus the slope at this point (which is
$f(x_{i-1},y_{i-1})$) times the horizontal step (which is
$x_i-x_{i-1}$). Finally, the algorithm returns the vector of heights.

Implementing the above as a Matlab \texttt{m}-function should be
straightforward.
However, we are including the complete code in Example
\ref{ex:euler-method-ode} for the benefit of the reader. We have
called the function \texttt{euler} and, as a consequence, the file
must be called \texttt{euler.m}.

\begin{example}\label{ex:euler-method-ode}
  Given a function $f$ ---which we shall assume is given as an
  \emph{anonymous function}---, a vector $\mathbf{x}$ of horizontal
  positions and an
  initial value $y_0$, an \texttt{m-}file
  which implements Euler's method for numerical integration of ODEs as
  a Matlab function may contain the code in Listing
  \ref{lst:euler-method-ode}. We emphasize that \emph{the file name name must be}
  \texttt{euler.m}, otherwise it would not work.\Endexample
\end{example}

\lstinputlisting[label=lst:euler-method-ode, language=matlab,
  caption={Matlab code for Euler's numerical integration method.}]
  {code/euler.m}


\begin{exercise}\label{exer:euler-1}
  Use the function \texttt{euler} just implemented to solve
  numerically the following initial value problems. Use as many points
  as you wish (not more than $100$, though) when not specified. Plot the solutions as you
  compute them.
  \begin{itemize}
  \item For $x\in[0,1]$, solve $y^{\prime}=2x$ with $y(0)=1$. Use
    $10$ points. Plot, on the same graph, the true solution
    $y(x)=x^2+1$.
  \item For $x\in[0,1]$, solve $y^{\prime}=y$ with $y(0)=1$. Use $15$
    points. Plot, on the same graph, the true solution $y(x)=e^x$.
  \item For $x\in[0,1]$, solve $y^{\prime}=xy$ with $y(0)=1$. Plot, on
    the same graph, the true solution $y(x)=e^{\frac{x^2}{2}}$.
  \item For $x\in[-1,1]$, solve $y^{\prime}=x+y$ with $y(-1)=0$.
  \item For $x\in[-\pi,0]$, solve $y^{\prime}=\cos(y)$ with
    $y(-\pi)=-1$.
  \item For $x\in[1,3]$, solve $y^{\prime}=y-x$ with $y(1)=-1$. 
  \end{itemize}
\end{exercise}

However, Euler's method is not exactly \emph{the best way} to
approximate solutions to ODEs. As the first three initial value problems
in Exercise \ref{exer:euler-1}
show, solutions computed using Euler's method are usually below the
true solution if this is convex (or above it, when
concave). This is because convexity means $y^{\prime}(x)$ is an
increasing function (and concavity means $y^{\prime}(x)$ is
decreasing), so that Euler's algorithm is always short of the true
solution. This lack of precision can be overcome partly using an
intermediate point instead of the left endpoint of the interval, which
is \emph{modified Euler's} method. 

\subsection{Modified Euler's method}
Instead of using the value of $f(x,y)$ at the left endpoint of each
interval, one can perform the following ``improvement:''
\begin{enumerate}
\item Assume $y_{i-1}$ has been computed.
\item Let $k=f(x_{i-1},y_{i-1})$ (the slope of Euler's method).
\item Let $\tilde{x}=\frac{x_i+x_{i-1}}{2}$ be the midpoint of
  $[x_{i-1},x_i]$.
\item Let $z=\frac{k}{2}(x_{i}-x_{i-1})$ be half the vertical step
  corresponding to Euler's approximation.
\item Let $r=f(\tilde{x}, y_{i-1}+z)$ be the slope described by
  $f(x,y)$ at the midpoint of Euler's approximation, that is:
  $(\tilde{x},z)$.
\item Finally, $y_i=y_{i-1}+r(x_i-x_{i-1})$ is the next approximate
  value of the solution.
\end{enumerate}

Although the description seems confusing, the above method can be
described as follows: ``Use Euler's method to compute the next
\emph{midpoint}, then use the value of $f$ at this midpoint as the slope at
the present point.'' So, instead of using the slope at the left
endpoint, one uses the slope at some ``midpoint'' in the hope that it
will give a better approximation. As a matter of fact, this happens in
most cases. Formally, the above could be described with Algorithm
\ref{alg:modified-euler}.

\begin{algorithm}
  \begin{algorithmic}
    \FOR{$i=1\dots n$}
    \STATE $k=f(x_{i-1},y_{i-1})$
    \STATE $\tilde{x}=\frac{x_{i-1}+x_i}{2}$
    \STATE $z=\frac{k}{2}(x_i-x_{i-1})$
    \STATE $r=f(\tilde{x}, y_{i-1}+z)$
    \STATE $y_i=y_{i-1}+r(x_i-x_{i-1})$
    \ENDFOR
    \STATE \textbf{return} $(y_0,\dots, y_n)$
  \end{algorithmic}
  \caption{Modified Euler's method.}
  \label{alg:modified-euler}
\end{algorithm}

Notice how $f(x,y)$ needs to be evaluated twice in this new
algorithm. This is essentially what gives the enhanced accuracy of
this method. As a general rule, the more evaluations of $f(x,y)$ are
carried out (reasonably),
the more accurate a method will be, and vice versa: the
more accurate a method, the more evaluations of $f(x,y)$ it will
require. Efficient algorithms for numerical integration of
ODEs  balance speed and accuracy.

\begin{exercise}\label{exer:modified-euler}
  Implement modified Euler's method in an \texttt{m-}file, call it
  \texttt{modified\_euler.m}
  and use it
  to solve the initial value problems in Exercise
  \ref{exer:euler-1}. Compare (graphically and analytically) both
  approximate solutions and the exact one in the cases where this is
  given. Is this method better or worse? Is it always so or just some
  times?
\end{exercise}

\subsection{Heun's method (``improved Euler'')}
The third method for numerical integration of ODEs which we shall explain
is the equivalent of the \emph{mean-value} algorithm for the cycling
race explained in Example~\ref{ex:cycling-stage-mean-value}: use the
mean of the slopes at the left and right endpoints of each
interval. This is known as Heun's or \emph{improved Euler's} method.

However, there is a difference with respect to the cycling race
example. In the race, \emph{we already know the slope at the right
  endpoint}: as a matter of fact, we know the slopes at each point on
the $x-$axis. On the contrary, if we are given the differential equation
\begin{equation*}
  y^{\prime}=f(x,y)
\end{equation*}
and the initial value $(x_0,y_0)$, we can compute the slope at this
point $f(x_0,y_0)$ but the question arises: \emph{what is the ``next
  point''?} There is no
such thing because the solution to the ODE is unknown. We only know the next
$x-$coordinate, $x_1$ but we lack the $y-$coordinate (otherwise we
would know the solution to the problem). We need to \emph{guess} (or,
more precisely, to \emph{predict}) a value for $y_1$ and then
``correct'' it somehow. Heun's method follows the following mental
process:
\begin{enumerate}
\item Start at $(x_0,y_0)$.
\item Compute $(x_1, \tilde{y}_1)$ using Euler's method as a
  ``guess.'' This requires using $k_1=f(x_0,y_0)$ as slope.
\item Compute $k_2=f(x_{1},\tilde{y}_1)$, the slope at Euler's guess.
\item Instead of using either $k_1$ or $k_2$, take the mean value of
  both slopes, $k=(k_1+k_2)/2$.
\item Use $k$ as the slope from $(x_0,y_0)$. That is,
  \begin{equation*}
    y_1 = y_0 + k(x_1-x_0).
  \end{equation*}
\end{enumerate}

An example should illustrate things a bit.

\begin{example}
  Consider the initial value problem (IVP)
  \begin{equation*}
    y^{\prime} = x-y,\,\,\,y(2)=0.
  \end{equation*}
  and let $\mathbf{x}=(2,2{.}25,2{.}5)$ be a sequence of
  $x-$coordinates. Let us use Heun's method to find an approximate solution
  to the IVP at $x=2{.}25$ and $x=2{.}5$.

  We need to perform two steps of Heun's method. We shall detail the
  first one and run through the second one.

  \textbf{First step}: $x_0=2, y_0=0$.
  \begin{itemize}
  \item Using Euler's method, one has $k_1=f(x_0,y_0)=2$ (this point
    $(x_0,y_0)$ is the
    initial condition) and so, $\tilde{y}_1=0+2\times 0{.}25=0{.}5$.
  \item Using the previous data, $k_2=f(2{.}25, 0{.}5)=1{.}75$.
  \item The mean value of $k_1$ and $k_2$ is $k=1{.}875$.
  \item Finally, $y_1=y_0+k(x_1-x_0)=0+1{.}875\times 0{.}25$ = 0{.}46875.
  \end{itemize}
  \hspace*{\parindent}Hence, $(x_1,y_1)=(2{.}25, 0{.}46875)$.\\
  \indent \textbf{Second step}: $x_1=2{.}25, y_1=0{.}46875$.\\
  \indent From this data, we get $\bm{k}_1=f(x_1,y_1)=1{.}7812$ and
  $\bm{\tilde{y}}_{2}=0{.}46875+1{.}7812\times 0{.}25=0{.}91405$, so that
  $\bm{k}_2=f(x_2, \bm{\tilde{y}}_2)=1{.}5859$. This gives $\bm{k}=1{.}6835$ and the
  result is $(x_2,y_2)=(2{.}5, 0{.}88962)$.

  Taking into account that the exact solution to the problem is
  $y(x)=x-e^{2-x}-1$, which gives $y(2.5)=0.89347$, the relative error
  incurred is $\frac{\abs{0.89347-0.88962}}{0.89347}\simeq 0.004$, which is
  rather small (even more if we consider that the steps are quite
  large, $0{.}25$ units each).
  \Endexample
\end{example}

Algorithm \ref{alg:heun} is a more formal expression of the method.

\begin{algorithm}[h!]
  \begin{algorithmic}
    \FOR{$i=1\dots n$}
    \STATE $k_1=f(x_{i-1},y_{i-1})$
    \STATE $\tilde{y}_i=y_{i-1}+k_1(x_i-x_{i-1})$
    \STATE $k_2=f(x_i, \tilde{y}_i)$
    \STATE $k = \frac{k_1+k_2}{2}$
    \STATE $y_i=y_{i-1}+k(x_i-x_{i-1})$
    \ENDFOR
    \STATE \textbf{return} $(y_0,\dots, y_n)$
  \end{algorithmic}
  \caption{Heun's method, also called ``improved Euler's'' method.}
  \label{alg:heun}
\end{algorithm}

\begin{exercise}
  In an \texttt{m-}file, implement Heun's method with a function called
  \texttt{heun}. (Remember that the file must then be named
  \texttt{heun.m}). Use it to find approximate solutions to the IVPs of Exercise
  \ref{exer:euler-1} and compare these to the ones found using Euler's
  and Modified Euler's. Which are better? Always? When?
\end{exercise}

\begin{exercise}
  Use a spreadsheet (like Excel or LibreOffice Calc) to implement
  Euler's method. Explain how it might be done (and do it) for the
  IVPs of Exercise \ref{exer:euler-1}. Compare the results with the
  ones produced by Matlab/Octave. Can Modified Euler's and
  Heun's methods be implemented in a spreadsheet? If they can, do so
  for at least one of them.

  Use the charting utility of the spreadsheet to plot the graphs of
  the solutions.
\end{exercise}
