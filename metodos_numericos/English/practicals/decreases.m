% detect if a list of numbers decreases and which is the
% first index when it happens (i.e. x(k) < x(k-1)).
function [dec when howmuch] = decreases(x)
  % create the returned variables first of all
  dec = 0;     % false by default
  when = 0;    % start outside boundaries
  howmuch = 0; % no decrease

  % basic sanity check
  if length(x) < 1
    return
  end

  % start position counter
  k = 1;
  l = length(x);

  % basic while loop, might as well use a for, how?
  while (k < l)
    if x(k) <= x(k+1)
      k = k+1;
    else
      % decrease detected: compute return values
      % AND RETURN IMMEDIATELY
      when = k+1;
      dec  = 1;
      howmuch = x(when) - x(when-1);
      return
    end
  end
end
