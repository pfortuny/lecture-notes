% Given a function f and a vector x, get the initial segment
% of x for which f is non-negative. Empty if f(x(1)) < 0
function [v] = positive_start(f, x)
    v = []; % empty by default
    % we need an index for this problem
    k = 1;
    while k <= length(x) && f(x(k)) >= 0
        v = [v, x(k)];
        k = k + 1; % never forget this in a while loop...
    end
end
