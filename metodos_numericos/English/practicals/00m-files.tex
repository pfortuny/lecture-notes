% -*- TeX-master: "practicals.ltx" -*-
\chapter[A primer on files as functions]{A primer on Matlab
  \texttt{m-}functions}\label{cha:primer-m-functions}
Along this course we are going to use constantly a Matlab/Octave tool
for defining complex functions ---more complex than the simple
\emph{anonymous functions}: \texttt{m-}files and functions defined in
them.

An \texttt{m-}file is no more than a file whose name ends in
\texttt{.m} and containing a number of Matlab/Octave commands. For
example, the following, if called \texttt{trial1.m} might be called an
\texttt{m-}file:

\begin{lstlisting}[language=matlab,label=lst:simple-m-file,
caption={A rather simple \texttt{m-}file}]
% This is just a simple file
e = exp(1);
b = linspace(-2,2,1000);
plot(b, e.\^b)
\end{lstlisting}

The power of \texttt{m-}files comes from two properties:
\begin{itemize}
\item If they are saved in the \texttt{PATH} directory (which includes
  the default \texttt{Documents/MATLAB}) then, running a command with
  the name of the file (without the \texttt{.m} extension) runs the
  contents of the file. That is, if after saving the above file, one runs
\begin{lstlisting}[language=matlab]
> trial1
\end{lstlisting}
  then the plot of the function $e^x$ for $x\in[-2,2]$ using $1000$
  points is drawn.
\item They can be used to define complex functions.
\end{itemize}

Instead of just a sequence of commands, one can define a function
inside an \texttt{m-}file. Consider Listing
\ref{lst:first-m-function}, which defines a function returning the two
maximum values of a list, in increasing order.

The structure of the file is the following:
\begin{enumerate}
\item Several comment lines (those starting with a \texttt{\%}
  symbol). These comments describe what the function defined
  afterwards does.
\item A line like
\begin{lstlisting}[language=matlab]
function [y1, y2,...] = name(p1, p2,...)
\end{lstlisting}
  where the word \texttt{function} appears at the beginning, then a
  list of names between square brackets (these are the \emph{output
    variables}), an equal (\texttt{=}) sign, then \emph{name of the
    function} (in Listing \ref{lst:first-m-function}, \texttt{max2})
  and a list of parameters (the \emph{input parameters}) between
  parentheses.
\item A sequence of lines of Matlab commands, which implement the
  function.
\item The \texttt{end} keyword at the end.
\item Finally, the name of the file must be the name of the function
  with a trailing \texttt{.m}. For Listing \ref{lst:first-m-function},
  it should be \texttt{max2.m}.
\end{enumerate}

A file following all those rules defines a new Matlab/Octave command
which can be used as any other command.

\begin{lstlisting}[language=matlab,label=lst:first-m-function,
caption={A first \texttt{m-}file defining a function. Save it as \texttt{max2.m}}]
% max2(x)
% return the 2 maximum values in x, in increasing order
% if length(x) == 1, [-inf, x] is returned

function [y] = max2(x)

  % initialize: the first element is always greater than -inf
  y = [-inf, x(1)];

  % early exit test
  if(length(x) == 1)
    return;
  end

  % for each element, only do something if it is greater than y(1)
  for X=x(2:end)
    if(X > y(1))
      if(X > y(2))
        y(1) = y(2);
        y(2) = X;
      else
        y(1) = X;
      end
    end
  end

end
\end{lstlisting}

For example, if one saves the code in Listing
\ref{lst:first-m-function} in a file named \texttt{max2.m} inside the
\texttt{Documents/MATLAB} directory, then one can run the following
commands:

\begin{lstlisting}[language=matlab]
> x = [-1 2 3 4 -6 9 -8 11]
x =

   -1    2    3    4   -6    9   -8   11

> max2(x)
ans =

    9   11
\end{lstlisting}

which show that a new function, called \texttt{max2}, has been
defined, and performs the instructions in the file \texttt{max2.m}.

In this course, we are going to use \texttt{m-}files and functions
defined therein continuously, so the student is encouraged to write as
many examples as possible. Learning to program in Matlab/Octave should
be easy taking into account that the students have already undergone a
course on Python.

\begin{remark}\label{rem:programming-constructs}
  The main programming constructs we shall need are included in the
  Cheat Sheet (ask the professor for it if you do not have the
  link). They are the following:
  \begin{itemize}
  \item The \texttt{if...else} statement. It has the following syntax
\begin{lstlisting}[language=matlab]
if CONDITION
... % sequence of commands if CONDITION holds
else
... % sequence of commands if CONDITION does not hold
end
\end{lstlisting}
    There are more possibilities (the \texttt{elseif} construct) but
    we are not going to detail them here.
  \item The \texttt{while} loop. Syntax:
\begin{lstlisting}[language=matlab]
while CONDITION
... % sequence of commands while CONDITION holds
end
\end{lstlisting}
    will perform the commands inside the loop as long as the condition
    holds.
  \item The \texttt{for} loop. Syntax:
\begin{lstlisting}[language=matlab]
for var=LIST
... % sequence of commands
end
\end{lstlisting}
    will assign sequentially to \texttt{var} each of the values of
    \texttt{LIST} and perform the commands inside the loop.
  \item Logical expressions. The conditions in \texttt{if} statements
    and \texttt{while} loops can be simple expressions (like \texttt{x
    < 3}, which means ``x is less than $3$'') or expressions built
  with logical operators: \texttt{and}, \texttt{or}, \texttt{not} or
  their \emph{shortcut} versions \texttt{\&\&}, \texttt{||}, \texttt{~}.
  \end{itemize}
\end{remark}

\begin{exercise}
  Implement a function \texttt{min3} which, given a list as input,
  returns its \emph{three} minimal elements. Use \texttt{max2} defined
  above as a guide.
\end{exercise}

\begin{exercise}\label{exer:increases}
  Implement a function \texttt{increases} which, given a list as
  input, returns \texttt{1} if the list is in non-decreasing order and
  \texttt{0} if it is not. How would you implement this?
\end{exercise}

\begin{exercise}\label{exer:increases-enhanced}
  Enhance the function of Exercise \ref{exer:increases} so that it
  outputs two values: first of all, the same as \texttt{increases} and
  the length of the ``increasing sequence'' at the beginning of the
  input. Call it \texttt{increases2}. For example:
\begin{lstlisting}[language=matlab]
> [a,b] = incresases2(7, -1, 2, 3 4)
  a = 0
  b = 1
> [u,v] = increases2(-1, 2, 5, 8, 9)
  u = 1
  v = 5
> [a,b] = increases2(3, 4, 5, -6, 8, 10)
  a = 1
  b = 3
\end{lstlisting}
\end{exercise}

\begin{exercise}\label{exer:positive}
  Define a function called \texttt{positive} which, given a list as
  input, returns two values: the number of \emph{positive} (strictly
  greater than $0$) elements in the list and the list of those
  elements. Examples:
\begin{lstlisting}[language=matlab]
> [a,b] = positive(-2, 3, 4, -5, 6, 7, 0)
  a = 4
  b = [3 4 6 7]
> [a,b] = positive(-1, -2, -3, -exp(1), -pi)
  a = 0
  b = []
> [a,b] = positive(4, 2, 1, 3 2)
  a = 5
  b = [4 2 1 3 2]
\end{lstlisting}
  would you use a \texttt{while} loop or a \texttt{for}? Why?
\end{exercise}

Notice that functions returning several values can be requested to
return any number of them. For example, the function \texttt{positive}
defined in Exercise \ref{exer:positive} might be requested to return
just one value:
\begin{lstlisting}[language=matlab]
> positive(1, 2, -2, 0, 4)
  3
> x = positive(-2, 1, 3, 7, 2)
  x = 4
\end{lstlisting}
or it can be forced to return both. This requires using square
brackets for the assignment of variables:
\begin{lstlisting}[language=matlab]
> [n x] = positive(pi, -2, 3, -5, 0)
  n = 2
  x = [pi 3]
\end{lstlisting}

If the function is called without assigning its output to a variable,
it will return just one value. This will be relevant for many of the
functions dealing with linear systems of equations (where one usually
needs to know not just the final answer to a problem but the
intermediate steps leading to it).