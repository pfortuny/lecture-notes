% -*- TeX-master: "practicals.ltx" -*-
\chapter{Approximate solutions to nonlinear equations}\label{cha:solutions-equations}
This chapter deals with finding approximate solutions to equations of
one variable:
\begin{equation}
  \label{eq:main-equation}
  f(x) = 0
\end{equation}
where $f$ is a ``reasonably behaved'' function. The meaning of this
expression depends
on the context but one usually requires $f$ to be \emph{at the very
  least}, continuous.

\section{Bisection method}
One of the first methods for finding approximate roots of
\emph{continuous} functions is based on Bolzano's theorem. Given a
continuous function $f:[a,b]\rightarrow \mathcal{R}$ with $f(a)f(b)<0$
(that is, which changes sign between $a$ and $b$), there must be a
point $c\in(a,b)$ such that $f(c)=0$. From this, one infers that
either $z=(a+b)/2$ is a root of $f$ or $f$ satisfies the same
conditions on either the left interval
$[a,z]$ or the right one $[z,b]$. This lets one repeat the procedure
(taking the midpoint) until a tolerance is reached. This method is
called the Bisection algorithm. It can be described more formally as
in Algorithm \ref{alg:bisection}.

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} A function $f(x)$, a pair of real numbers
    $a,b$ with $f(a)f(b)<0$, a tolerance $\epsilon >0$ and
    a limit of iterations $N>0$ 
    \STATE \textbf{Output:} either an error message or a real number
    $c$ between $a$ and $b$ such that $\abs{f(c)} <
    \epsilon$ (i.e. an approximate root)
    \PART{Precondition}
    \IF{$f(a)\not\in {\mathbb R}$
      \textbf{of} $f(b)\not\in \mathbb{R}$}
    \STATE \textbf{return} ERROR
    \ENDIF
    \PART{Start}
    \STATE $i\leftarrow 0$
    \STATE $c\leftarrow \displaystyle{\frac{a+b}{2}}$ 
    \WHILE{$\abs{f(c)}\geq \epsilon$ \textbf{and} $i\leq N$}
    \IF{$f(a)f(c) < 0$} 
    \STATE $b\leftarrow c$ \comm{interval $[a,c]$}
    \ELSE
    \STATE $a\leftarrow c$ \comm{interval $[c,b]$}
    \ENDIF
    \STATE $i\leftarrow i+1$
    \STATE $c \leftarrow \displaystyle{\frac{a+b}{2}}$
    \comm{middle point}
    \ENDWHILE
    \IF{$i>N$} 
    \STATE \textbf{return} ERROR
    \ENDIF
    \STATE \textbf{return} $c$
  \end{algorithmic}
  \caption{Bisection Algorithm.}
  \label{alg:bisection}
\end{algorithm}

\begin{exercise}\label{exer:bisection}
  Implement the Bisection algorithm in an \texttt{m}-file called
  \texttt{bisection.m}.
\end{exercise}

\begin{exercise}\label{exer:bisection-functions}
  Use the bisection algorithm to find approximate roots to the
  following functions in the specified intervals.
  Notice that the algorithm may not work for some of them
  even though they have roots in that interval.
  \begin{equation*}
    \begin{array}[h]{r@{\;=\;}l}
      f(x) & \cos(e^x), x\in [0,2]\\
      g(x) & x^3-2, x\in[0,3]\\
      h(x) & e^x-2\cos(x), x\in[0,\pi]\\
      r(t) & t^2-1, x\in[-2,2]\\
      s(z) & \sin(z) + \cos(z), z\in[0,2\pi]\\
      u(t) & t^2-3, t\in[-2,2]\\
      f(t) & \atan(t-2), t\in[-3,3]
    \end{array}
  \end{equation*}
    Can you use a different interval for those functions for which the
    algorithm does not work in order to find a root of them?
\end{exercise}

\section{The Newton-Raphson method}
The Newton-Raphson method for finding roots of an equation $f(x)=0$
requires $f$ to be, at least, differentiable (and hence,
continuous). It can be described as in Algorithm \ref{alg:newton-raphson}.
\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} A differentiable function $f(x)$, a
    \emph{seed} $x_{0}\in {\mathbb R}$, a tolerance $\epsilon >0$ and
    a limit for the number of iterations $N>0$
    \STATE \textbf{Output:} Either an error message or
    a real number $c$ such that $\abs{f(c)}< \epsilon$ (i.e. an
    approximate root)
    \PART{Start}
    \STATE $i \leftarrow 0$
%    \STATE \comm{make sure that $f(x_{i})\in {\mathbb R}$, not included}
    \WHILE{$\abs{f(x_{i})}\geq \epsilon$ \textbf{and} $i\leq N$}
%    \STATE \comm{meeting point of tangent and $OX$ axis}
    \STATE $x_{i+1} \leftarrow
    x_{i}-\displaystyle{\frac{f(x_{i})}{f^{\prime}(x_{i})}}$
    \comm{possible NaN or $\infty$}
%    \IF{$x_{i+1}=NaN$
%    \STATE \textbf{return} ERROR
%    \ENDIF
    \STATE $i\leftarrow i+1$
%    \STATE \comm{make sure that $f(x_{i})\in {\mathbb R}$, not included}
    \ENDWHILE
    \IF{$i>N$}
    \STATE \textbf{return} ERROR
    \ENDIF
    \STATE $c\leftarrow x_i$
    \STATE \textbf{return} $c$
  \end{algorithmic}
  \caption{Newton-Raphson.}
  \label{alg:newton-raphson}
\end{algorithm}

\begin{example}\label{ex:newton-raphson-implementation}
  In order to implement the Newton-Raphson method \emph{without using
    symbolic operations} in Matlab, one needs to define a function
  whose input includes the seed \texttt{x0}, the function \texttt{f}
  (as an anonymous function), its derivative \texttt{fp}
  (because one is not using symbolic derivation) and both the
  tolerance \texttt{epsilon} and the limit for the iterations
  \texttt{N}. Listing \ref{lst:newton-raphson} shows a possible
  implementation.
  \lstinputlisting[language=matlab, caption={A possible (simple)
    implementation of the Newton-Raphson method.},
  label=lst:newton-raphson]{code/newtonraphson.m}
  Notice that, as the function is called \texttt{newtonraphson}, the
  file should be called \texttt{newtonraphson.m}.
\end{example}

\begin{exercise}\label{exer:solutions-newton-raphson}
  Use the \texttt{newtonraphson} function defined in Example
  \ref{ex:newton-raphson-implementation} to compute approximate
  solutions to the functions of Exercise
  \ref{exer:bisection-functions}. Use different values for the seed,
  epsilon and limit of iterations.

  Does the Newton-Raphson method converge always?
\end{exercise}

\begin{exercise}\label{exer:newton-raphson-plot-points}
  Modify the code of \texttt{newtonraphson} (and create a new function
  called \texttt{newtonraphson\_plot}, and the corresponding file) so
  that, at each step, the point $(x_i, 0)$ is plotted on a graph, with
  a big enough mark. Use this to keep track of different calls to the
  function for the equations of Exercise
  \ref{exer:bisection-functions}.
\end{exercise}

\begin{exercise}\label{exer:newton-raphson-plot-lines}
  \textbf{Remark: only do this exercise if you have enough time to spare.}
  As in Exercise \ref{exer:newton-raphson-plot-points}, create a new function called
  \texttt{newtonraphson\_graph} which plots not only each $(x_i,0)$
  but also the tangent line from $(x_{i-1},f(x_{i-1}))$ to $(x_i,0)$
  to visualize the process. Use the command \texttt{pause} between
  each plot to wait for the user to press some key (so that the
  process is seen step by step). Use this function for the equations
  in Exercise \ref{exer:bisection-functions}.
\end{exercise}


\section{The Secant method}
The secant method is a slight modification of the Newton-Raphson
algorithm when the derivative of $f$ is either unknown or too
expensive to compute. Instead of using $f^{\prime}(x_i)$ at each step,
the algorithm approximates that value as
\begin{equation*}
  f^{\prime}(x_i) \simeq \frac{f(x_{i})-f(x_{i-1})}{x_i-x_{i-1}},
\end{equation*}
which requires keeping track not only of $x_i$ but also of $x_{i-1}$
at each step.

\begin{exercise}\label{exer:secant}
  Write a function \texttt{secant} in a file called \texttt{secant.m}
  which implements the secant algorithm. Notice that, unlike the
  Newton-Raphson function, this requires only an anonymous function
  \texttt{f} as input but two seeds \texttt{x0} and \texttt{x1} (for
  example).

  Use this \texttt{secant} function to compute approximate roots to
  the equations of Exercise \ref{exer:bisection-functions}.
\end{exercise}


\section{The fixed-point method}
The fixed-point point method is based on the following result
\begin{theorem}\label{the:fixed-point-differentiable}
  Let $g:[a,b]\rightarrow[a,b]$ be a map of $[a,b]$ into itself, which
  is continuous and differentiable on $[a,b]$. If there is a positive
  $\lambda <1$ such that for any
  $x\in[a,b]$, $\abs{g^{\prime}(x)}\leq \lambda$, then
  there exists one and only one $\alpha\in[a,b]$ for which
  $g(\alpha)=\alpha$.
\end{theorem}

That requires verifying quite a few conditions before it can be used
for solving an equation (and, as a matter of fact, turning that
equation into a fixed-point problem). Essentially, if one starts with
\begin{equation*}
  f(x) = 0,
\end{equation*}
in order to use the fixed-point method one has to transform that
equation into a ``fixed-point problem,'' that is, an equation of the
form
\begin{equation*}
  \tilde{f}(x) = x
\end{equation*}
(which means that one is looking for a value $c$ which is
\emph{fixed} by $\tilde{f}$: $\tilde{f}(c)=c$, whence the name). The usual
transformation is easy
\begin{equation*}
  f(x) = 0 \Leftrightarrow f(x) + x = x.
\end{equation*}
Obviously $f(c)=0$ if and only if $f(c)+c=c$. Thus, generally,
$\tilde{f}(x)=f(x)+x$. 

Instead of trying to verify all the necessary conditions in Theorem
\ref{the:fixed-point-differentiable}, one may simply try to find a
solution to $f(x)+x=x$, given an equation $f(x)=0$.

\begin{exercise}\label{exer:fixed-point}
  Write a function \texttt{fixed\_point} which implements the
  fixed-point method. The input should be an anonymous function
  \texttt{f}, a seed \texttt{x0}, a tolerance \texttt{epsilon} and a
  limit for the number of iterations \texttt{N}. The output should be
  either a warning or error or a number \texttt{c} such that
  \texttt{abs(f(c))<epsilon}.

  \textbf{Remark:} the iteration for this problem is not just
  $x_{i+1}=f(x_i)$ because we are looking for a \emph{root} of $f(x)$!

  Use this function to compute (if possible) approximate roots to the
  equations of Exercise \ref{exer:bisection-functions}. Does it work?
  When? Why?
\end{exercise}


