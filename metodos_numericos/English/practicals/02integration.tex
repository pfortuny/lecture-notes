% -*- TeX-master: "practicals.ltx" -*-
\chapter{Numerical Integration}
Unlike in the theory classes, we are not going to study numerical
differentiation. However, the student should be able to understand and
implement algorithms using both right- and left-sided methods and
the symmetric ones (at least for the first and second order
derivatives). It is possible that one informal practical be used to
show examples of the three methods, for solving
ordinary differential equations.

This is one of the simplest practicals, as the topic ---numerical
integration--- will be covered quickly and only the simplest formulas
will be implemented. All of them are assumed to be known (because they
will have been explained in the theory classes).

\section{The simple quadrature formulas}
Numerical integration (or \emph{quadrature}, the classical term) is
first dealt with as the problem of finding a suitable formula for the whole
integration interval. This gives rise to the three most known methods: the
midpoint rule, the trapeze rule and Simpson's rule. Start with a
function $f:[a,b]\rightarrow \mathbb{R}$. The following are the three
basic quadrature formulas:

\begin{enumerate}
\item The \emph{midpoint rule} uses the value of $f$ at the midpoint
  $\frac{a+b}{2}$:
  \begin{equation*}
    \int_a^b f(x)\,dx \simeq (b-a)f\left(\frac{a+b}{2}\right).
  \end{equation*}
\item The \emph{trapeze rule} uses the value of $f$ at both endpoints:
  \begin{equation*}
    \int_a^b f(x)\,dx \simeq (b-a)\frac{(f(a)+f(b))}{2}.
  \end{equation*}
  We prefer stating it like this to emphasize that the area is
  computed as the width of the interval $(b-a)$ times the mean value
  of $f$ at the endpoints.
\item \emph{Simpson's rule} uses three values: at both endpoints and
  at the midpoint:
  \begin{equation*}
    \int_a^bf(x)\,dx \simeq
    (b-a)\frac{\left(f(a)+4f\left(\frac{a+b}{2}\right)+f(b)\right)}{6}.
  \end{equation*}
  Notice that the denominator $6$ is the sum of the coefficients
  $1+4+1$ at each point.
\end{enumerate}

\begin{example}\label{ex:midpoint-rule}
  A possible implementation of the midpoint rule, which receives three
  parameters, \texttt{a}, \texttt{b} and \texttt{f} (the last one an
  anonymous function) is included in listing
  \ref{lst:midpoint-rule}. In order for it to define a proper
  function, the corresponding file should be called
  \texttt{midpoint.m}.
  \lstinputlisting[language=matlab, caption={Implementation of the
    midpoint rule.}, label=lst:midpoint-rule]{code/midpoint.m}
  A usage example could be:
  \begin{lstlisting}
    > f = @(x) cos(x);
    > midpoint(0, pi, f)
    ans = 0
  \end{lstlisting}
\end{example}


\begin{exercise}\label{exer:trapeze-rule}
  Implement the trapeze rule in an \texttt{m-}file called
  \texttt{trapeze.m}. Use it to compute approximations to the
   following:
  \begin{itemize}
  \item The integral of $\tan(x)$ from $x=0$ to $x=\pi/2$.
  \item The integral of $e^x$ from $x=-1$ to $x=1$.
  \item The integral of $\sin(x)$ from $x=0$ to $x=\pi$.
  \item The integral of $\cos(x)+\sin(x)$ from $x=0$ to $x=\pi$.
  \item The integral of $x^2+2x+1$ from $x=-2$ to $x=3$.
  \item The integral of $x^3$ from $x=2$ to $x=6$.
  \end{itemize}
  Use also the function \texttt{midpoint} as defined in example
  \ref{ex:midpoint-rule} to compute the same integrals. Use Matlab's
  \texttt{int} function to compute the exact values (or WolframAlpha
  or your own ability) and compare the accuracy of both methods.
\end{exercise}

\begin{exercise}\label{exer:simpson-rule}
  Implement Simpson's rule in a file called \texttt{simpson.m} and use
  it to compute approximations to the integrals in Exercise
  \ref{exer:trapeze-rule}. Compare the accuracy of this method to that
  of the other two. Which is best? Why? Does it always give the best
  approximation?
\end{exercise}

As the reader will have noticed, the above are rules which use a
formula applied to one, two or three points in the interval and
approximate the value of the integral on the whole stretch
$[a,b]$. One can get more precise results dividing $[a,b]$ into a
number of subintervals and applying each formula to each of
these. This way one gets the \emph{composite} quadrature formulas.

\section{Composite rules}
Of course, dividing the interval requires somehow knowing how many
subintervals are needed. We shall assume the user provides this number
as an input.
Hence, the functions we shall implement will receive
\emph{four} parameters: the endpoints, the function and another one,
the number of subintervals. 

\begin{example}\label{ex:composite-midpoint-rule}
  For instance, a possible implementation of the \emph{composite
    midpoint rule} can be read in listing
  \ref{lst:composite-midpoint-rule}. Notice how the \texttt{sum}
  operation adds all the values of a vector (in this case the vector
  \texttt{f(m)} of values of \texttt{f} at the midpoints).
  \lstinputlisting[language=matlab, caption={An implementation of the
    composite midpoint rule.},
  label=lst:composite-midpoint-rule]{code/composite_midpoint.m}
\end{example}

\begin{exercise}\label{exer:composite-midpoint-evaluation}
  Use the code of \texttt{composite\_midpoint} to compute
  approximations to the
  integrals in Exercise~\ref{exer:trapeze-rule}, with different values
  for the parameter \texttt{n}. Compare the results with the ones
  obtained before.
\end{exercise}

\begin{exercise}\label{exer:composite-trapeze-simpson}
  Implement the composite trapeze and Simpson's rule (in two different files called,
  respectively, \texttt{composite\_trapeze.m} and
  \texttt{composite\_simpson.m}). Use these implementations to compute
  approximations to the integrals in
  Exercise~\ref{exer:trapeze-rule}. Compare the results.
\end{exercise}

\begin{exercise}\label{exer:compare-composite-simpson-trapeze}
  If one uses the composite trapeze rule with $2n$ subintervals and
  Simpson's rule with $n$ (for example, setting \texttt{n} to $6$ for
  the trapeze rule and to $3$ for Simpson's), one is using the same evaluation points
  (verify this). Are the approximations obtained equally good? Which
  is better? Why do you think this happens?
\end{exercise}


