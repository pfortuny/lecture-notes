% -*- TeX-master: "notas.ltx" -*-
\chapter{Multivariate and higher order
  ODEs}\label{cha:multivariate-ode}
In Chapter \ref{cha:ode} we dealt with the simplest case of Ordinary
Differential Equations: those of a single dependent variable. As a
matter of fact, in most cases, the number of dependent variables is
more than one and, as we shall see later, any ODE of order greater
than one can be transformed into a system of equations
of order one with more than one
dependent variable. This chapter gives a summary introduction to the
numerical solution of these problems. The expectation is that the
student gets acquainted with problems of order greater than one and
understands their transformation into order one equations.

\section{A two-variable example}
Let us consider a problem in two variables. Usually ---and we shall
follow the convention in this chapter--- the independent variable is
called $t$, reflecting the common trait of ODEs of being equations
which define a motion in terms of time.

Consider a dimensionless body $B$ ---a mass point--- moving with velocity
$v$ in the plane. Hence, if the coordinates of $B$ at time $t$ are
$B(t)=(x(t), y(t))$, we shall denote $v=(\dot{x}(t), \dot{y}(t))$. Assume
we know, for whatever reason, that the velocity vector satisfies some
condition $F$ which depends on the position of $B$ in the plane. This
can be expressed as $v=F(x,y)$. The function $F$ is then a vector
function of two variables, $F(x,y)=(F_1(x,y), F_2(x,y))$, and we can
write the condition on $v$ as:
\begin{equation}
  \label{eq:two-dim-ode-1}
  \left\{
  \begin{array}{r@{\,=\,}l}
    \dot{x}(t) & F_1(x(t), y(t))\\[2pt]
    \dot{y}(t) & F_2(x(t), y(t))
  \end{array}
  \right.
\end{equation}
which means, exactly, that the $x-$component of the velocity depends
on the position at time $t$ as the value of $F_1$ at the point
and that the $y-$component depends as the value of $F_2$. If we want,
writing $B(t)=(x(t),y(t))$ as above,
expression \ref{eq:two-dim-ode-1} can be written, in a compact way
as
\begin{equation*}
  v(t) = F(B(t))
\end{equation*}
which reflects the idea that \emph{what we are doing is just the same
  as in Chapter \ref{cha:ode}, only with more coordinates}. This is
something that has to be clear from the beginning: the only added
difficulty is the number of computations to be carried out.

Notice that in the example above, $F$ depends only on $x$ and $y$
---not on $t$. However, it might as well depend on $t$ (because the
behaviour of the system may depend on time), so that in general, we
should write
\begin{equation*}
  v(t) = F(t, B(t)).
\end{equation*}
Just for completeness, if $F$ does not depend on $t$, the system is
called \emph{autonomous}, whereas if it does depend on $t$, it is
called \emph{autonomous}.

\subsection{A specific example}
Consider, to be more precise, the same autonomous problem
as above but with $F(x,y)=(-y, x)$. This gives the following equations
\begin{equation}
  \label{eq:two-dim-ode-2}
  \left\{
    \begin{array}{r@{\,=\,}l}
      \dot{x}(t) & -y(t)\\[2pt]
      \dot{y}(t) & x(t).
    \end{array}
    \right.
\end{equation}
It is clear that, in order to have an initial value problem, we need
an initial condition. Because the equation is of order one, we need
just the starting point of the motion, that is: two cordinates. Let us
take $x(0)=1$ and $y(0)=0$ as the initial position for time $t=0$. The
initial value problem is, then
\begin{equation}
  \label{eq:two-dim-ode-2}
  \left\{
    \begin{array}{r@{\,=\,}l}
      \dot{x}(t) & -y(t)\\[2pt]
      \dot{y}(t) & x(t).
    \end{array}
  \right.
  \,\,\mathrm{with}\,\,
  (x(0), y(0)) = (1,0)
\end{equation}
which, after some scaling (so that the plot looks more or less nice)
describes the vector field depicted in Figure \ref{fig:circles}. It
may be easy to guess that a body whose velocity is described by the
arrows in the diagram follows a circular motion. This is what we are
going to prove, actually.
\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{axis}[
      enlargelimits=false,legend style={at={(0.05,0.95)},anchor =
        north west},
      xtick={},ytick={},xticklabels={},yticklabels={},
      restrict x to domain=-1:1,
      restrict y to domain=-1:1]
      \input{../datafiles/circles.vect}
      \addplot[mark=*] coordinates {(0.92,0.0)};
      \addplot[red, ->] coordinates {(0.92,0) (0.92, 0.17)};
\end{axis}
\end{tikzpicture}
  \caption{Vector field representing (after scaling) the ODE of
    Equation \eqref{eq:two-dim-ode-2}. The initial condition (with the
    vector at that point) is marked with a black dot.}
  \label{fig:circles}
\end{figure}

The initial value problem \eqref{eq:two-dim-ode-2} means, from a
symbolic point of view, that the functions $x(t)$ and $y(t)$ satisfy
the following conditions:
\begin{itemize}
\item First, the derivative of $x(t)$ with respect to $t$ is $-y(t)$.
\item Then, the derivative of $y(t)$ with respect to $t$ is $x(t)$.
\item And finally, the initial values are $1$ and $0$, for $t=0$,
  respectively.
\end{itemize}
The first two conditions imply that $\ddot{x}(t)=-x(t)$ and
$\ddot{y}(t)=-y(t)$. This leads one to think of trigonometric
functions and, actually, it is easy to check that $x(t)=\cos(t)$,
$y(t)=\sin(t)$ verify the conditions above. That is, the solution to
the initial value problem \eqref{eq:two-dim-ode-2} is
\begin{equation}
  \label{eq:solution-to-two-dim-ode-2}
  \left\{
    \begin{array}{r@{\,=\,}l}
      x(t) & \cos(t)\\
      y(t) & \sin(t)
    \end{array}
    \right.
\end{equation}
which, as the reader will have alredy realized, is a circular
trajectory around the origin, passing through $(1,0)$ at time
$t=0$. Any other initial condition $(x(0),y(0))=(a,b)$ gives rise to a
circular trajectory starting at $(a,b)$.

However, our purpose, as we have stated repeatedly in these notes, is
not to find a \emph{symbolic} solution to any problem, but to
approximate it using the tools at hand. In this specific case, which
is of dimension two, one can easily describe the generalization of
Euler's method of Chapter \ref{cha:ode} to the problem under study. Let us
fix a discretization of $t$, say in steps of size $h$. Then, a rough
approximation to a solution would be:
\begin{enumerate}
\item We start with $x_0=1, y_0=0$.
\item At that point, the differential equation means that
  the velocity $v(t)=(\dot{x}(t), \dot{y}(t))$ is
  \begin{equation*}
    \dot{x}(0)=0,\,\,\dot{y}(0)=1.
  \end{equation*}
\item Because $t$ moves in steps of size $h$, the trajectory
  $(x(t),y(t))$ can only be approximated by moving as much as the
  vector at $t=0$ says multiplied by the timespan $h$, hence the next
  approximate position, $(x_1, y_1)$ is given by
  \begin{equation*}
    (x_1, y_1) = (x_0, y_0) + h\cdot (\dot{x}(0), \dot{y}(0)) =
    (1, 0) + (h\cdot 0, h \cdot 1) = (0,h).
  \end{equation*}
\item Now, at the point $(x_1, y_1) = (0,h)$, we carry out the
  analogous computations, taking into account that at this point,
  $v=(-\cos(h),
  \sin(0))$\dots
\end{enumerate}

If we follow the steps just described, we get a picture like Figure
\ref{fig:circles-euler}, in which the step is $h=0.1$ and the error
incurred is easily noticed.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{axis}[
      enlargelimits=false,legend style={at={(0.05,0.95)},anchor =
        north west},
      xtick={},ytick={},xticklabels={},yticklabels={},
      restrict x to domain=-1.5:1.5,
      restrict y to domain=-1.5:1.5,
      xmin=-2,xmax=2,ymin=-2,ymax=2,
      axis equal=true]
      \input{../datafiles/circle-euler.vect}
\end{axis}
\end{tikzpicture}
  \caption{Approximation to the solution of Problem
    \eqref{eq:two-dim-ode-2} using Euler's method. Notice the
    (obvious) error.}
  \label{fig:circles-euler}
\end{figure}
The fact that this method, for this example, gives what looks like a
bad approximation needs not make one reject it straightway. There is a
clear reason for this, which we shall not delve into but which has to
deal with the solutions being always convex ---i.e. the trajectories
describing the true solutions are always curved in the same way. This
makes the Euler method accumulate the errors always, instead of having
some positive errors and some negative ones.

\section{Multivariate equations: Euler and Heun's methods}
The example with two coordinates above is easily generalized to $n$
coordinates. Consider an initial value problem
\begin{equation}
  \label{eq:n-dimension-ivp}
  \left\{
    \begin{array}{ll}
      \dot{x}_1 = f_1(t, x_1, \dots, x_n), & x_1(0) = a_1 \\[3pt]
      \dot{x}_2 = f_2(t, x_1, \dots, x_n), & x_2(0) = a_2 \\
      \vdots 
      \\
      \dot{x}_n = f_n(t, x_1, \dots, x_n), & x_n(0) = a_n
    \end{array}
  \right.\,\,\,  
\end{equation}
where $f_1(t, x_1, \dots, x_n), \dots, f_n(t, x_1, \dots, x_n)$ are
continuous functions of $n+1$ variables ---``time'', so to say, and
``position''.  We do not assume the system is autonomous.
Our aim is to compute an
approximate solution using increments of the independent variable $t$
of size $h$. The generalization of Euler's method is just a copy of
what was described above:
\begin{enumerate}
\item Start with $i=0$ and let $x_{0,1}=a_1, \dots, x_{0,n}=a_n$.
  Notice that the first subindex indicates the number of
  the iteration and the second is the coordinate.
\item Set $t_i=h\cdot i$.
\item Compute the coordinates of the derivative: that is,
  compute $v_1 = f_1(t_{i}, x_{i,1}(t_i),\dots, x_{i,n}(t_i)), \dots, v_n =
  f_n(t_{i}, x_{i,1}(t_i), \dots, x_{i,n}(t_i))$. This is much simpler than
  it seems.
\item Compute the next point: $x_{i+1,1}=x_{i,1}+h \cdot v_1,
  x_{i,2}=x_{i,2}+h \cdot v_2, \dots,
  x_{i+1,n}=x_{i,n}+h\cdot v_n$.
\item Increase $i=i+1$ and goto step $2$ until one stops.
\end{enumerate}
These steps are formally stated in Algorithm
\ref{alg:euler-n-dimensions}.
\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} $n$ functions of $n+1$
    variables: $f_1(t, x_{1},\dots, x_n), \dots, f_n(t, x_1, \dots, x_n)$,
    an initial condition
    $(a_1,a_2, \dots, a_n)$,
    an interval $[A,B]$ and a step $h=(B-A)/N$
    \STATE \textbf{Output: } A family of vector values $\mathbf{x}_0,
    \dots, \mathbf{x_N}$ (which approximate the solution to the
    corresponding initial value problem for $t\in[A,B]$)
    \PART{Start}
    \STATE $i\leftarrow 0, t_0\leftarrow A, \mathbf{x}_0\leftarrow
    (a_1, \dots, a_n)$
    \WHILE{$i<N$}
    \STATE $\mathbf{v}\leftarrow (f_1(t_i, \mathbf{x}_i), \dots,
    f_n(t_i,\mathbf{x}_i))$
    \STATE $\mathbf{x}_{i+1}\leftarrow \mathbf{x}_i + h\cdot \mathbf{v}_i$
    \STATE $i\leftarrow i+1$
    \ENDWHILE
    \STATE \textbf{return} $\mathbf{x}_0,\dots, \mathbf{x}_N$
  \end{algorithmic}
  \caption{Euler's algorithm for $n$ coordinates. Notice that boldface
  elements denote vectors.}
  \label{alg:euler-n-dimensions}
\end{algorithm}

As we did in Chapter \ref{cha:ode}, we could state a \emph{modified}
version of Euler's method but we prefer to go straightway for
Heun's. It is precisely stated in Algorithm
\ref{alg:heun-n-dimensions}. 
\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} $n$ functions of $n$
    variables: $f_1(t, x_{1},\dots, x_n), \dots, f_n(t, x_1, \dots, x_n)$,
    an initial condition
    $(a_1,a_2, \dots, a_n)$,
    an interval $[A,B]$ and a step $h=(B-A)/N$
    \STATE \textbf{Output: } A family of vector values $\mathbf{x}_0,
    \dots, \mathbf{x_N}$ (which approximate the solution to the
    corresponding initial value problem for $t\in[A,B]$)
    \PART{Start}
    \STATE $i\leftarrow 0, t_0 \leftarrow A, \mathbf{x}_0 \leftarrow
    (a_1, \dots, a_n)$
    \WHILE{$i<N$}
    \STATE $\mathbf{v}\leftarrow (f_1(t_i, \mathbf{x}_i), \dots,
    f_n(t_i, \mathbf{x}_i))$
    \STATE $\mathbf{z}\leftarrow \mathbf{x}_i + h\cdot \mathbf{v}_{i}$
    \STATE $\mathbf{w}\leftarrow (f_1(t_i, \mathbf{z}_{i}), \dots,
    f_n(t_i, \mathbf{z}_{i}))$
    \STATE $\mathbf{x}_{i+1}\leftarrow \mathbf{x}_i + \frac{h}{2}\cdot
    (\mathbf{v}+\mathbf{w})$
    \STATE $i\leftarrow i+1$
    \ENDWHILE
    \STATE \textbf{return} $\mathbf{x}_0,\dots, \mathbf{x}_N$
  \end{algorithmic}
  \caption{Heun's algorithm for $n$ coordinates. Notice that boldface
  elements denote vectors.}
  \label{alg:heun-n-dimensions}
\end{algorithm}

In Figure \ref{fig:circles-heun} a plot of the approximate solution to
the initial value problem of Equation \ref{eq:two-dim-ode-2} is
shown. Notice that the approximate trajectory is ---on the printed page---
indistinguishable from a true circle (we have plotted more than a
single loop to show how the approximate solution overlaps to the naked
eye). This shows the improved accuracy
of Heun's algorithm ---although it does not behave so nicely in the
general case. 

\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{axis}[
      enlargelimits=false,legend style={at={(0.05,0.95)},anchor =
        north west},
      xtick={},ytick={},xticklabels={},yticklabels={},
      restrict x to domain=-1.5:1.5,
      restrict y to domain=-1.5:1.5,
      xmin=-2,xmax=2,ymin=-2,ymax=2,
      axis equal=true]
      \input{../datafiles/circle-heun.vect}
\end{axis}
\end{tikzpicture}
  \caption{Approximation to the solution of Problem
    \eqref{eq:two-dim-ode-2} using Heun's method. The
    error is unnoticeable on the printed page.}
  \label{fig:circles-heun}
\end{figure}

\section{From greater order to order one}
Ordinary differential equations are usually of order greater than
$1$. For instance, one of the most usual settings is a mechanical
problem in which forces act on a body to determine its motion. The
equation that governs this system is, as the reader should know,
Newton's second law:
\begin{equation*}
  \vec{a} = m\cdot \vec{F}
\end{equation*}
which is stated as ``acceleration is mass times force.'' If the
position of the body at time $t$ is $(x_1(t), \dots, x_n(t))$, then
the acceleration vector is $\vec{a}=(\ddot{x}_1(t), \dots,
\ddot{x}_n(t))$ and if the resultant force is $(F_1, \dots, F_n)$
(which would depend on $(x_1, \dots, x_n)$) then Newton's Law becomes,
in coordinates:
\begin{equation*}
  \left\{
    \begin{array}{l}
      \ddot{x}_1(t) = F_1(x_1(t), \dots, x_n(t))\cdot m\\[3pt]
      \ddot{x}_2(t) = F_2(x_1(t), \dots, x_n(t))\cdot m\\
      \vdots\\
      \ddot{x}_n(t) = F_n(x_1(t), \dots, x_n(t))\cdot m
    \end{array} 
  \right.
\end{equation*}
which is an ordinary differential equation but has order greater than
one so that we cannot apply the methods explained above
straightaway. Notice that \emph{the system is autnomous} because Newton's
second law is time-invariant (it depends only on the positions of the
bodies, not on time).

However, one can look at a body in motion under the influence of
forces as a system whose ``state'' is not just the position ---that is,
the set of coordinates $(x_1(t), \dots, x_n(t))$--- but as a system
whose state is described by position \emph{and} velocity. As a matter
of fact, everybody knows by experience that the trajectory of a body
(say, a stone) influenced by gravity depends not only on its initial
position (from where it is thrown away) but also on its initial velocity
(how fast it is thrown away). So, in mechanics, \emph{velocities} are
intrinsic elements of the system of coordinates. Thus, the body $B$
under study has $2n$ coordinates to account for its state: its
position $(x_1, \dots, x_n)$ and its velocity $(v_1, \dots,
v_n)$. By their own nature, these $2n$ coordinates satisfy the
following equations:
\begin{equation*}
  \dot{x}_1(t) = v_1(t),\, \dot{x}_2(t)= v_2(t),\,  \dots,\,
  \dot{x}_n(t) = v_n(t).
\end{equation*}
And now Newton's law can be expressed as a true ordinary differential
equation of order one
\begin{equation}
  \label{eq:newton-order-1}
  \left\{
    \begin{array}{l}
      \dot{x}_1(t) = v_1(t)\\[3pt]
      \dot{v}_1(t) = F_1(x_1(t), \dots, x_n(t))\cdot m\\[3pt]
      \dot{x}_2(t) = v_2(t)\\[3pt]
      \dot{v}_2(t) = F_2(x_1(t), \dots, x_n(t))\cdot m\\
      \vdots\\
      \dot{x}_n(t) = v_n(t)\\[3pt]
      \dot{v}_n(t) = F_n(x_1(t), \dots, x_n(t))\cdot m
    \end{array}
  \right.
\end{equation}
to which the methods explained above can be applied. Notice that one
is usually only interested in knowing the positions $(x_1(t), \dots,
x_n(t))$ but one \emph{must} solve the system for both positions and
velocities. After computing both values, one may discard the latter,
if it is truly unnecessary.

For an initial value problem, one needs (as can be seen from Equation
\ref{eq:newton-order-1}) not only the position at time $t_0$ but also
the velocity at that time.

\subsection{The general case is similar}
In general, one starts with an ordinary differential equation of some
order $k$ ---the maximum order of derivation that appears for some
variable and transforms the problem having $n$ variables and the
derivatives up to order $k-1$ into one
having $kn$ variables by adding $(k-1)$ new variables.

One may state the general case as a differential equation
\begin{equation}
  \left\{
  \label{eq:ode-order-k}
  \begin{array}{l}
    \displaystyle\frac{d^k x_1}{dt^k} = F_1(t, \mathbf{x},
    \mathbf{x}^{\prime}, \dots, \mathbf{x}^{k-1)})\\[9pt]
    \displaystyle\frac{d^k x_2}{dt^k} = F_2(t, \mathbf{x},
    \mathbf{x}^{\prime}, \dots, \mathbf{x}^{k-1)})\\
    \vdots\\
    \displaystyle\frac{d^kx_n}{dt^k} = F_n(t, \mathbf{x},
    \mathbf{x}^{\prime}, \dots, \mathbf{x}^{k-1)})
  \end{array}
  \right.
\end{equation}
where the functions $F_1, \dots, F_n$ depend on $t$ and the variables $x_1,
\dots, x_n$ and their derivatives with respect to $t$ up to order
$k-1$. It is easier to understand than to write down.

In order to turn it into a system of equations of order one, one just
defines $k-1$ new systems of variables, which we enumerate with
superindices
\begin{equation*}
  (u_1^1, \dots, u_n^1), \, \dots, (u_1^{k-1}, \dots, u_n^{k-1})
\end{equation*}
and specify that each system corresponds to the derivative
of the previous one (and the first one is the derivative of the
coordinates),
\begin{equation*}
  \left\{
    \begin{array}{l}
      \dot{x}_1 = u_1^1\\
      \vdots\\
      \dot{x}_n = u_n^1
    \end{array}
  \right.,
  \left\{
    \begin{array}{l}
      \dot{u}^1_1 = u_1^2\\
      \vdots\\
      \dot{u}^1_n = u_n^2
    \end{array}
  \right.,\dots,
  \left\{
    \begin{array}{l}
      \dot{u}^{k-2}_1 = u_1^{k-1}\\
      \vdots\\
      \dot{u}^{k-2}_n = u_n^{k-1}
    \end{array}
  \right.
\end{equation*}

Using this technique, one ends up with a (very long) ordinary
differential equation of order one in the variables
\begin{equation*}
(x_1, \dots,
x_n, u^1_1, \dots, u^1_n, \dots, u^{k-1}_1, \dots, u^{k-1}_n).  
\end{equation*}


\subsection{The two-body problem}
As an example, let us consider a typical problem of newtonian
mechanics, the motion of a system of two bodies ---which is, again,
autonomous. Let $B_1$ and $B_2$
be two dimensionless objects with masses $m_1$ and  $m_2$ 
which we suppose are placed on the same plane (so that we only need
two coordinates to describe their positions). The usual approach is to
consider one of the bodies fixed and the other movable but we want to
show how both bodies do move and how they do interact with each
other. If $(x_1, y_1)$ and $(x_2, y_2)$ denote the coordinates of each
body, and dots indicate derivation with respect to the time variable,
Newton's equations of motion state that, respectively:
\begin{equation}
  \label{eq:two-body-1}
  \begin{array}{r@{\,=\,}l}
   (\ddot{x}_1, \ddot{y}_1) &G
    \displaystyle \frac{-m_2}{\left((x_2-x_1)^2+(y_2-y_1)^2\right)^{3/2}}(x_2-x_1,
    y_2-y_1)\\
    (\ddot{x}_2, \ddot{y}_2) &G
    \displaystyle \frac{-m_{1}}{\left((x_2-x_1)^2+(y_2-y_1)^2\right)^{3/2}}(x_1-x_2,
    y_1-y_2)
  \end{array}
\end{equation}
where $G$ is the gravitational constant. We shall use units in which
$G=1$ in order to simplify the exposition. There are $4$ equations in
\eqref{eq:two-body-1}, one for each second derivative. Notice that the
first derivative of each $x_i, y_i$ does not appear explicitely but
this is irrelevant: the problem is of order two and to turn it into
one of order one, they have to be made explicit. Given that there are
four coordinates, we need another four variables, one for each first
derivative. Let us call them $(u_x, u_y)$ and $(v_x, v_y)$. Writing
all the equations one after the other for $G=1$, we get
\begin{equation}
  \label{eq:two-body-2}
  \begin{array}{r@{\,=\,}l}
    \dot{x}_{1} & u_x\\
    \dot{u}_x & \displaystyle
    \frac{-m_2}{\left((x_2-x_1)^2+(y_2-y_1)^2\right)^{3/2}}(x_2-x_1)\\
    \dot{y}_1 & u_y\\
    \dot{u}_y & \displaystyle
    \frac{-m_2}{\left((x_2-x_1)^2+(y_2-y_1)^2\right)^{3/2}}(y_2-y_1)\\
    \dot{x_2} & v_x\\
    \dot{v}_x & \displaystyle
    \frac{-m_1}{\left((x_2-x_1)^2+(y_2-y_1)^2\right)^{3/2}}(x_1-x_2)\\
    \dot{y_2} & v_y\\
    \dot{v}_y & \displaystyle
    \frac{-m_1}{\left((x_2-x_1)^2+(y_2-y_1)^2\right)^{3/2}}(y_1-y_2)
  \end{array}
\end{equation}
which is a standard ordinary differential equation of the first
order. An initial value problem would require both a pair of initial
positions (for $(x_1,y_1)$ and $(x_2, y_2)$) and a pair of initial
velocities (for $(u_x, u_y)$ and $(v_x, v_y)$): this gives eight
values, apart from the masses, certainly.

In Listing \ref{lst:two-body-heun} we show an implementation of Heun's
method for the two-body problem (with $G=1$). In order to use it, the
function \texttt{twobody} must receive the values of the masses, a
vector of initial conditions as $(x_1(0), y_1(0), x_2(0),
y_2(0), u_x(0), u_y(0), v_x(0), v_y(0))$), a step size $h$ and the
number of steps to compute.

\lstinputlisting[language=matlab, caption={An implementation of the
  two-body problem with $G=1$ using Heun's method.},
label=lst:two-body-heun]
{../matlab/twobody.m}

In Figure \ref{fig:two-body-1}, a plot for the run of the function
\texttt{twobbody} for masses $4, 400$ and initial conditions $(-1,0)$,
$(1, 0)$, $(0, 14)$, $(0, -0{.}1)$ is given, using $h=0{.}01$ and
$40000$ steps. The difference in mass is what makes the trajectories
so different from one another, more than the difference in initial
speed. Notice that the blue particle turns elliptically around the red
one. 

\begin{figure}
  \centering
  \begin{tikzpicture}
%    \begin{axis}[
%      enlargelimits=false,legend style={at={(0.05,0.95)},anchor =
%        north west},
%      xtick={},ytick={},xticklabels={},yticklabels={},
%      restrict x to domain=-1.5:1.5,
%      restrict y to domain=-1.5:1.5,
%      xmin=-,xmax=2,ymin=-4,ymax=2
%      ]
    \input{../datafiles/twobody-blue.dat}
    \input{../datafiles/twobody-red.dat}
%\end{axis}
\end{tikzpicture}
  \caption{Approximation to the solution of the two-body problem with
    initial values as in the text. One of the bodies (in red) is more massive
    than the other.}
  \label{fig:two-body-1}
\end{figure}

Finally, in Figure \ref{fig:two-body-2}, we see both the plot of the
motion of two particles and, next to it, the relative motion of one of
them with respect to the other, in order to give an idea of:
\begin{itemize}
\item The fact that trajectories of one body with respect to the other
  are ellipses.
\item The error incurred in the approximation (if the solution were
  exact, the plot on the right would be a closed ellipse, not the open
  trajectory shown).
\end{itemize}

\begin{figure}
  \centering
  \begin{tikzpicture}[scale=0.3]
    \input{../datafiles/two-body-2-1.dat}
    \input{../datafiles/two-body-2-2.dat}
  \end{tikzpicture}
  \hspace*{80pt}
  \begin{tikzpicture}[scale=1]
    \input{../datafiles/two-body-2-3.dat}
  \end{tikzpicture}
  \caption{Absolute and relative motion of two particles. On the left,
    the absolute trajectories, on the right, the relative ones.}
  \label{fig:two-body-2}
\end{figure}

