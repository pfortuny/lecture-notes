% -*- TeX-master: "notas.ltx" -*-
\chapter{Interpolation}
Given a set of data ---generally a cloud of points on a plane---, the
human \emph{temptation} is to use them as source of knowledge and
forecasting. Specifically, given a list of coordinates associated to
some kind of event (say, an experiment or a collection of
measurements) 
$(x_{i},y_i)$, the ``natural'' thing to do is to use it for
\emph{deducing} or \emph{predicting} the value $y$ would take if $x$
took some other value not in the list. This is the \emph{interpolating
and extrapolating tendency} of humans. There is no helping it. The
most that can be done is studying the most reasonable ways to perform
those forecasts.

We shall use, along this whole chapter, a list of $n+1$ pairs $(x_i,y_i)$ 
\begin{equation}
\label{eq:lista-datos}
  \begin{array}{c|c|c|c|c|c}
    \mathbf{x} & x_0 & x_1 & \dots & x_{n-1} & x_n\\
\hline
    \mathbf{y} & y_0 & y_1 & \dots & y_{n-1} & y_n
  \end{array}
\end{equation}
which is assumed ordered on the $\mathbf{x}$ coordinates, which are
all different, as well: $x_i<x_{i+1}$.
The aim is to find functions which somehow have a relation (a
kind of \emph{proximity}) to that \emph{cloud of points}.

\section{Linear (piecewise) interpolation}
The first, simplest and useful idea is to use a piecewise defined
function from $x_0$ to $x_n$ consisting in ``joining each point to the
next one by a straight line.'' This is called \emph{piecewise linear
  interpolation} or \emph{linear spline} ---we shall define
\emph{spline} in general later on.

\begin{definition}
  The piecewise linear interpolating function for list
(\ref{eq:lista-datos}) is the function $f:[x_0,x_n]\rightarrow
{\mathbb R}$ defined as follows:
\begin{equation*}
f(x) = \frac{y_i-y_{i-1}}{x_i-x_{i-1}}(x-x_{i-1})+y_{i-1}\,\,\, \mbox{
if } x\in[x_{i-1},x_i]
\end{equation*}
that is, the piecewise defined function whose graph is the union of
the linear segments joining $(x_{i-1},y_{i-1})$ to $(x_{i},y_i)$,
for $i=1,\dots, n$.
\end{definition}

Piecewise linear interpolation has a set of properties which make it quite
interesting: 
\begin{itemize}
\item It is \emph{very} easy to compute.
\item It passes through all the data points.
\item It is continuous.
\end{itemize}
That is why it is frequently used for drawing functions (it is what
Matlab does by default): if the data cloud is dense, the segments are
short and corners will not be noticeable on a plot.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={$f(x)$, linear interpolation},
legend style={at={(0.05,0.95)},
anchor = north west}
]
\addplot[line width={1.5pt},smooth,dashed, color=black] file {../datafiles/grafica-interp.dat};
\addplot[line width={1pt},color=red, mark=*] coordinates{
(2.8808, 2.9338)
(3.1622, 3.0066)
(4.4701, 3.1489)
(5.1579, 2.6690)
(6.5491, 3.3480)
(7.3023, 3.7617)
(8.5970, 1.8217)
(9.0020, 2.0297)
(10.1275, 4.6187)
};
\end{axis}
\end{tikzpicture}
\caption{Piecewise linear interpolation of a $9$-point cloud.}
\label{fig:interpol-lineal-1}
\end{figure}

The main drawback of this technique is, precisely, the corners which
appear anywhere the cloud of points does not correspond to a straight
line. Notice also that this (and the following splines which we shall
explain) are \emph{interpolation} methods only, not suitable for
\emph{extrapolation}: they are used to approximate values
\emph{between the endpoints} $x_0, x_n$, never outside that interval.

\section{Can parabolas be used for this?}
Linear interpolation is deemed to give rise to
corners whenever the data cloud is not on a straight line. In general,
interpolation requirements do not only include the graph to pass
through the points in the cloud but also to be \emph{reasonably
  smooth} (this is not just for aesthetic reasons but also because
reality usually behaves this way). One could try and improve linear
interpolation with higher degree functions, imposing that the tangents
of these functions match at the intersection points. For example, one
could try with parabolic segments (polynomials of degree two). As they
have three degrees of freedom, one could make them not only pass
through $(x_{i-1},y_{i-1})$ and $(x_{i}, y_i)$, but also have the same
derivative at $x_i$. This may seem reasonable but
has an inherent undesirable property: it is intrinsically asymmetric
(we shall not explain why, the reader should be able to realize this
after some computations). Without delving into the details, one can
verify that this \emph{quadratic spline} is not optimal although it
approximates the data cloud without corners.

It has also some other problems (it tends to get far from the data
cloud if there are points whose $x$-coordinates are near but whose
$y$-coordinates differ in different directions). It is almost never
used except where the cloud of points is known a priori to resemble a
parabola. 

The next case, that of \emph{cubic splines} is by far the most used:
as a matter of fact, computer aided design (CAD) software uses cubic
splines any time it needs to draw a smooth curve, not exactly using a
table like (\ref{eq:lista-datos}) but with two tables, because curves
are parametrized as  $(x(t), y(t))$.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={$f(x)$, \small{quadratic spline}},
legend style={at={(0.05,0.95)},
anchor = north west}
]
\addplot[line width={1.5pt},smooth,mark=none,dashed,color=black] file {../datafiles/grafica-interp.dat};
\addplot[line width={1pt},smooth,mark=none,color=red] file {../datafiles/interp-cuadr.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(2.8808, 2.9338)
(3.1622, 3.0066)
(4.4701, 3.1489)
(5.1579, 2.6690)
(6.5491, 3.3480)
(7.3023, 3.7617)
(8.5970, 1.8217)
(9.0020, 2.0297)
(10.1275, 4.6187)
};
\end{axis}
\end{tikzpicture}
\caption{Quadratic interpolation of a $9$-point cloud. Compare with
  the original function.}\label{fig:interpol-cuadratica}
\end{figure}


\section{Cubic Splines: Continuous Curvature}
In order to approximate clouds of points with polynomials of degree $3$,
the following definition is used:
\begin{definition}
A \emph{cubic spline} for a table like (\ref{eq:lista-datos}) is a function
$f:[x_0, x_n]\rightarrow {\mathbb R}$ such that:
\begin{itemize}
\item  It matches a degree $3$ polynomial on every segment
  $[x_{i-1},x_i]$, for $i=1,\dots, n$.
\item It is two times differentiable with continuity at every
  $x_{i}$, for $i=1,\dots, n-1$ (the inner points).
\end{itemize}
\end{definition}
From this follows that, if $P_i$ is the polynomial of degree $3$
matching $f$ on $[x_{i-1},x_i]$, then
$P_i^{\prime}(x_i)=P_{i+1}^{\prime}(x_i)$ and
$P_i^{\prime\prime}(x_i)=P_{i+1}^{\prime\prime}(x_i)$; these facts, together with
$P_i(x_{i})=y_i$ and $P_i(x_{i+1})=y_{i+1}$ give $4$
conditions for each $P_i$, except for the first and the last ones, for
which there are only $3$ (this symmetry of behavior at the endpoints
is what quadratic splines
lack). Thus, the conditions for being a cubic spline \emph{almost}
determine the polynomials $P_i$ (and hence $f$). Another condition for
$P_1$ and $P_n$ is needed for a complete specification. There are
several alternatives, some of which are:
\begin{itemize}
\item That the second derivative at the endpoints be $0$. This
  gives rise to the \emph{natural cubic spline}, but needs not be the
  best option for a problem. The corresponding equations are
  $P_1^{\prime\prime}(x_0)=0$ and $P_{n}^{\prime\prime}(x_{n})=0$.
\item That the \emph{third} derivative matches at the ``almost''
  extremal points:
  $P_1^{\prime\prime\prime}(x_1)=P_2^{\prime\prime\prime}(x_1)$ and
  $P_{n}^{\prime\prime\prime}(x_{n-1}) =
  P_{n-1}^{\prime\prime\prime}(x_{n-1})$. This is the
  \emph{extrapolated spline}. 
\item A periodicity condition:
  $P_1^{\prime}(x_0)=P^{\prime}_n(x_n)$ and the same for the second
  derivative: $P_1^{\prime\prime}(x_0)=P^{\prime\prime}_n(x_n)$. This
  may be relevant if, for example, one is interpolating a periodic
  function. 
\end{itemize}

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={$f(x)$, \small{cubic spline}},
legend style={at={(0.05,0.95)},
anchor = north west}
]
\addplot[line width={1pt},smooth,mark=none,dashed,color=black] file {../datafiles/grafica-interp.dat};
\addplot[line width={1pt},smooth,mark=none,color=red] file {../datafiles/spline-cubico.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(2.8808, 2.9338)
(3.1622, 3.0066)
(4.4701, 3.1489)
(5.1579, 2.6690)
(6.5491, 3.3480)
(7.3023, 3.7617)
(8.5970, 1.8217)
(9.0020, 2.0297)
(10.1275, 4.6187)
};
\end{axis}
\end{tikzpicture}
\caption{Cubic spline for a $9$-point cloud. Compare with the original
  function.}\label{fig:interpol-cubica}
\end{figure}


\subsection{Computing the Cubic Spline: Tridiagonal Matrices}
Before proceeding with the explicit computation of the cubic spline,
let us introduce some useful notation which will allow us to simplify
most of the expressions that will appear.

We shall denote by $P_i$ the polynomial corresponding to the interval
$[x_{i-1},x_i]$ and we shall always write it \emph{relative to}
$x_{i-1}$, as follows:
\begin{equation}
  \label{eq:i-th-polynomial}
P_i(x) = a_i + b_i(x-x_{i-1}) + c_i(x-x_{i-1})^2 + d_i(x-x_{i-1})^3.
\end{equation}
We wish to compute  $a_i, b_i, c_i$ and $d_i$  for $i=1,\dots, n$ from
the cloud of points $(\mathbf{x}, \mathbf{y})$ above
(\ref{eq:lista-datos}).

The next normalization is to rename $x_i-x_{i-1}$ and use
\begin{equation*}
h_i = x_i-x_{i-1}, \mbox{ for } i=1,\dots, n
\end{equation*}
(that is, use the width of the $n$ intervals instead of the
coordinates $x_i$).

Finally, in an attempt to clarify further, known data will be written
in boldface.

We reason as follows:
\begin{itemize}
\item The values $a_{i}$ are directly computable, because
  $P_i(\mathbf{x_{i-1}})=a_i$ and, by hypothesis, this must equal
  $\mathbf{y_{i-1}}$. Hence,
\begin{equation*}
a_i = \mathbf{y_{i-1}} \mbox{ for } i=1,\dots, n
\end{equation*}
\item As $P_i(\mathbf{x_{i}})= \mathbf{y_{i}}$,
  using now that $a_i\mathbf{y_{i-1}}$
  on the right hand side of the equality \eqref{eq:i-th-polynomial},
  one gets
\begin{equation}\label{eq:p-i-pasa-por-y-i}
b_i \mathbf{h_i} + c_i \mathbf{h_i}^2 + d_{i}
\mathbf{h_i}^3 = \mathbf{y_{i}} - \mathbf{y_{i-1}}.
\end{equation}
for $i=1,\dots, n$, which gives $n$ linear equations.
\item The condition on the continuity of the derivative is $P_i^{\prime}(\mathbf{x_i}) =
  P_{i+1}^{\prime}(\mathbf{x_i})$, so that
\begin{equation}\label{eq:misma-derivada}
 b_i  + 2c_i \mathbf{h_i} + 3d_i \mathbf{h_i}^{2} = b_{i+1},
\end{equation}
for $i=1,\dots n-1$. This gives other $n-1$ equations.
\item Finally, the second derivatives must match at the intermediate
  points, so that
\begin{equation}\label{eq:misma-derivada-segunda}
2c_{i} + 6 d_{i} \mathbf{h_{i}} = 2 c_{i+1},
\end{equation}
for $i=1,\dots, n-1$, which gives other $n-1$ equations.
\end{itemize}
Summing up, there are (apart from the known $a_i$), $3n-2$ linear
equations for  $3n$ unknowns (all of the $b_i, c_i$ and $d_i$). We have already
remarked that one usually imposes two conditions at the endpoints in order
to get $3n$ linear equations.

However, before including the other conditions, one simplifies and
rewrites all the above 
equations in order to obtain a more \emph{intelligible} system. What
is done is to solve the $d$'s and the $b$'s in terms of the $c$'s,
obtaining a system in which there are only $c$'s.

First of all, Equations  (\ref{eq:misma-derivada-segunda}) are used to
solve the $d_{i}$:
\begin{equation}
\label{eq:d-i-despejada}
d_{i} = \frac{c_{i+1}-c_i}{3 \mathbf{h_{i}}}
\end{equation}
and substituting in 
(\ref{eq:p-i-pasa-por-y-i}), one gets (solving for $b_i$):
\begin{equation}\label{eq:b-i-despejada-uno}
b_i = \frac{\mathbf{y_i}-\mathbf{y_{i-1}}}{\mathbf{h_i}} -
\mathbf{h_i} \frac{c_{i+1}+2c_i}{3};
\end{equation}
on the other hand, substituting $d_i$ in (\ref{eq:p-i-pasa-por-y-i})
and computing until solving $b_i$, one gets
\begin{equation}
\label{eq:b-i-despejada-dos}
b_i = b_{i-1} + \mathbf{h_{i-1}}(c_i+c_{i-1}).
\end{equation}
for $i=2,\dots,n$. Now one only needs to use Equations
(\ref{eq:b-i-despejada-uno}) for $i$ and $i-1$ and 
\emph{introduce} them into \eqref{eq:b-i-despejada-dos},
so that there are only
$c$'s. After some other elementary calculations, one gets
\begin{equation}
\label{eq:ecuacion-final}
\mathbf{h_{i-1}}c_{i-1} + (2 \mathbf{h_{i-1}}+ 2 \mathbf{h_i})c_i +
\mathbf{h_i}c_{i+1} = 3\left(
\frac{\mathbf{y_i}-\mathbf{y_{i-1}}}{\mathbf{h_{i}}} - \frac{\mathbf{y_{i-1}}-\mathbf{y_{i-2}}}{\mathbf{h_{i-1}}}
\right)
\end{equation}
for $i=2,\dots, n-1$.
 
This is a system of the form $Ac= \alpha$, where
$A$ is the $n-2$ by $n$ matrix
\begin{equation}
\label{eq:forma-matricial-c-i-spline}
A=\begin{pmatrix}
h_1 & 2(h_1+h_2) & h_{2} & 0 & \dots & 0 & 0 & 0\\
0 & h_2 & 2(h_2+h_3) & h_3 & \dots & 0 & 0 & 0\\
\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots
\\
0 & 0 & 0 & 0 & \dots & h_{n-1} & 2(h_{n-1} + h_n) & h_n
\end{pmatrix}
\end{equation}
and $c$ is the  column vector $(c_1, \dots, c_n)^t$, whereas 
$\alpha$ is
\begin{equation*}
\begin{pmatrix}
\alpha_2 \\
\alpha_3 \\
\vdots \\
\alpha_{n-1}
\end{pmatrix}
\end{equation*}
with
\begin{equation*}
\alpha_i = 3\left(
\frac{y_i-y_{i-1}}{h_{i}} - \frac{y_{i-1}-y_{i-2}}{h_{i-1}}\right).
\end{equation*}

It is easy to see that this is a consistent system (with infinite
solutions, because there are two missing equations). The
equations which are usually added were explained above. If one sets,
for example,
$c_1=0$ and $c_n=0$, so that $A$ is completed above with a row
$(1 \; 0 \; \dots \; 0) $and below with  $(0 \; 0 \; 0
\; \dots \; 1)$, whereas $\alpha$ gets a top and bottom $0$. From
these $n$ equations, one computes all the $c_i$ and, using
(\ref{eq:d-i-despejada}) and 
(\ref{eq:b-i-despejada-uno}), one gets all the $b$'s and $d$'s.

The above system (\ref{eq:forma-matricial-c-i-spline}), which has only
nonzero elements on the diagonal 
and the two adjacent lines is called \emph{tridiagonal}. These
systems are easily solved using $LU$ factorization and one can even
compute the solution directly, solving the $c$'s in terms of the
$\alpha$'s and $h$'s. One might as well use iterative methods but for
these very simple systems, $LU$ factorization is fast enough.

\subsection{The Algorithm}
We can now state the algorithm for computing the interpolating cubic
spline for a data list 
$\mathbf{x},\mathbf{y}$ of length  $n+1$, $\mathbf{x}=(x_0,\dots,
x_n)$, $\mathbf{y}=(y_0,\dots,y_n)$, in which $x_i<x_{i+1}$ (so that
all the values in $\mathbf{x}$ are different). This is Algorithm
\ref{alg:spline-cubico}.

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} a data table $\mathbf{x}, \mathbf{y}$
as specified in \eqref{eq:lista-datos}
and \textbf{two} conditions, one on the first and
one on the last node, usually. 
\STATE \textbf{Output:} an interpolating cubic spline for the table.
Specifically, a $n$ lists of four numbers $(a_i, b_i,
c_i, d_i)$ such that the polynomials $P_i(x) = a_i + b_i(x-x_{i-1}) +
c_i(x-x_{i-1})^{2} + d_i(x-x_{i-1})^3 $ give an interpolating cubic
spline for the table, satisfying the two extra conditions of the input.
\PART{Start}
\STATE $a_i \leftarrow y_{i-1}$ for $i$ from $1$ to $n$
\STATE $h_{i}\leftarrow x_i-x_{i-1}$ for $i$ from $1$ to $n$
\STATE $i\leftarrow 1$
\STATE \comm{Build the tridiagonal system:}
\WHILE{$i\leq n$} 
\IF{$i>1$ \textbf{and} $i<n$}
\STATE $F_i\leftarrow \left( 0 \; \cdots \;  0 \; h_{i-1} \;
  2(h_{i-1}+h_i) \; h_i \; 0 \; \cdots \; 0\right)$
\STATE $\alpha_i = 3(y_i - y_{i-1})/h_i - 3(y_{i-1}-y_{i-2})/h_{i-1}$
\ELSE
\STATE $F_i\leftarrow$ the row corresponding to the equation for
$P_i$
\STATE $\alpha_i$ the coefficient corresponding to the equation for
$P_i$
\ENDIF
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE $M\leftarrow $ the matrix whose rows are $F_i$ for $i=1$
to $n$
\STATE $c\leftarrow M^{-1}\alpha$ \comm{solve the system $Mc=\alpha$}
\STATE $i\leftarrow 1$
\STATE \comm{Solve the $b'$s and $d'$s:}
\WHILE{$i<n$}
\STATE $b_i\leftarrow (y_i-y_{i-1})/h_i - h_i(c_{i+1}+2c_i)/3$
\STATE $d_i\leftarrow (c_{i+1}-c_i)/(3h_i)$
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE $b_n\leftarrow b_{n-1} + h_{n-1}(c_n+c_{n-1})$
\STATE $d_n\leftarrow (y_n-y_{n-1}-b_nh_n -c_nh_n^2)/h_n^3$
\STATE \textbf{return} $(a,b,c,d)$
\end{algorithmic}
\caption{Computation of the Cubic Spline}
\label{alg:spline-cubico}
\end{algorithm}

\subsection{Bounding the Error}
The fact that the cubic spline is graphically \emph{satisfactory} does
not mean that it is \emph{technically useful}. As a matter of fact, it
is much more useful than what it might seem. If a function is ``well
behaved'' on the fourth derivative, then the cubic spline is a very
good approximation to it (and as the intervals get smaller, the better the
approximation is). Specifically, \emph{for clamped cubic
  splines}, we have:
\begin{theorem}
Let  $f:[a,b]\rightarrow {\mathbb R}$ be a $4$ times differentiable function
with $\abs{f^{4)}(x)}<M$ for $x\in [a,b]$. Let $h$
be the maximum of $x_i-x_{i-1}$ for $i=1,\dots, n$. If
$s(x)$ is a the clamped cubic spline for $(x_i, f(x_i))$, then
\begin{equation*}
\abs{s(x)-f(x)} \leq \frac{5M}{384}h^4.
\end{equation*}
\end{theorem}
This result can be most useful for computing integrals and bounding
the error or for bounding the error when interpolating values of
solutions of differential equations. Notice that the clamped cubic
spline for a function $f$ is such that
$s^{\prime}(x_0)=f^{\prime}(x_0)$ and $s^{\prime}(x_n) =
f^{\prime}(x_n)$, that is, the first derivative at the endpoints is
given by the first derivative of the interpolated function. 

Notice that this implies, for example, that if $h=0{.}1$ and $M<60$
(which is rather common: a derivative greater than $60$ is huge),
then the distance \emph{at any point} between the original function
$f$ and the interpolating cubic spline is less than $10^{-4}$.

\subsection{General Definition of Spline}
We promised to give the general definition of spline:
\begin{definition}
Given a data list as (\ref{eq:lista-datos}), an \emph{interpolating spline
  of degree $m$} for it, (for $m>0$), is a function
$f:[x_0,x_n]\rightarrow {\mathbb R}$ such that
\begin{itemize}
\item It passes through all the points: $f(x_i)=y_i$,
\item Is $m-1$ times differentiable
\item On every interval $[x_i,x_{i+1}]$ it coincides with a polynomial
  of degree at most $m$.
\end{itemize}
That is, a piecewise polynomial function passing through all the
points and $m-1$ times differentiable (where $0$ times differentiable
means continuity).
\end{definition}

The most used are linear splines (degree $1$) and cubic splines
(degree $3$). Notice that there is \emph{not} a unique spline of
degree $m$ for $m\geq 2$.

\section[Lagrange Interpolation]{The Lagrange Interpolating
  Polynomial}
Splines, as has been explained, are \emph{piecewise} polynomial
functions which pass through a given set of points, with some
differentiability conditions.

One could state a more stringent problem: finding a true polynomial of
the least degree
which passes through each of the points given by
\eqref{eq:lista-datos}.
That is, given $n+1$
points, find a polynomial of degree at most $n$ which passes through
each and every point. This is the \emph{Lagrange
  interpolation problem}, which has a solution:

\begin{theorem}[Lagrange's interpolation polynomial]
Given a data list as (\ref{eq:lista-datos}) (recall that
$x_i<x_{i+1}$), there is a unique polynomial of degree at most $n$
passing through each $(x_i,y_i)$ for
$i=0,\dots, n$.
\end{theorem}

The proof is elementary. First of all, one starts with a simpler
version of the problem: given
$n+1$ values $x_{0}<\dots<x_n$, is there a polynomial of degree at
most $n$ whose value is $0$ at every $x_j$ for $j=0,\dots, n$ but for
$x_i$ where its value is $1$?  This is easy because there is a
simple polynomial of degree $n$ whose value is $0$ at each $x_j$ for
$j\neq i$: namely $\phi_i(x)=(x-x_0)(x-x_1)\dots(x-x_{i-1})(x-x_{i+1})\dots
(x-x_{n})$. Now one only needs multiply $\phi_i(x)$ by a constant so
that its value at $x_i$ is $1$. The polynomial $\phi_i(x)$ has, at
$x_i$ the value
\begin{equation*}
\phi_i(x_i) = (x_{i}-x_1)\dots(x_{i}-x_{i-1})(x_i-x_{i+1})\dots
(x_i-x_n) = \prod_{j\neq i}(x_i-x_j),
\end{equation*}
so that the following polynomial $p_{i(x)}$ takes the value $1$ at
$x_i$ and $0$ at any $x_j$ for $j\neq i$.
\begin{equation}
\label{eq:polinomios-de-base-de-lagrange}
p_i(x) = \frac{\prod_{j\neq i}(x-x_j)}{\prod_{j\neq i}(x_i-x_j)}.
\end{equation}
These polynomials $p_0(x), \dots, p_n(x)$ are called the
\emph{Lagrange basis polynomials}
(there are $n+1$ of them, one for each $i=0,\dots, n$). The collection
$\{p_0(x), \dots, p_n(x)\}$ can be viewed as the basis of the vector
space $\mathbb{R}^{n+1}$. From this point of view,
now the vector $P(x) = (y_0, y_1, \dots, y_n)$ is to be expressed as
a linear combination of them, but there is only one way to do so:
\begin{equation}
\label{eq:polinomio-interpolador-de-lagrange}
\begin{split}
P(x) = y_0p_0(x) + y_1p_1(x) + \dots + y_np_n(x) &= \sum_{i=0}^n y_i 
p_i(x) \\ = \sum_{i=0}^ny_i \frac{\prod_{j\neq i}(x-x_j)}{\prod_{j\neq i}(x_i-x_j)}&.
\end{split}
\end{equation}
One verifies easily that this $P(x)$ passes through all the points
$(x_i, y_i)$ for $i=0, \dots, n$.

The fact that there is only one polynomial of the same degree as
$P(x)$ satisfying that condition
can proved as follows: if there existed $Q(x)$ of degree less than or
equal to $n$ passing through all those points, the difference $P(x)-Q(x)$
between them would be a polynomial of degree at most $n$ with $n+1$
zeros and hence, would be $0$. So, $P(x)-Q(x)$ would be $0$, which
implies that $Q(x)$ equals $P(x)$.

Compare the cubic spline interpolation with
the Lagrange interpolating polynomial for the same function as
before in figure \ref{fig:comparison-spline-lagrange}.


\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={$f(x)$, \small{cubic spline}, \small{Lagrange}},
legend style={at={(0.05,0.95)},
anchor = north west}
]
\addplot[line width={1pt},smooth, mark=none,color=black] file {../datafiles/grafica-interp.dat};
\addplot[line width={1.5pt}, mark=none, smooth,dotted,color=blue] file
{../datafiles/spline-cubico.dat};
\addplot[line width={1pt}, smooth, mark=none, dashed, color=red]
file {../datafiles/lagrange-for-comparison.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(2.8808, 2.9338)
(3.1622, 3.0066)
(4.4701, 3.1489)
(5.1579, 2.6690)
(6.5491, 3.3480)
(7.3023, 3.7617)
(8.5970, 1.8217)
(9.0020, 2.0297)
(10.1275, 4.6187)
};
\end{axis}
\end{tikzpicture}
\caption{Comparison between a cubic spline and the Lagrange
  interpolating polynomial for the same $f(x)$ as above. Notice the
  relatively large spike of the Lagrange polynomial at the last
  interval.}\label{fig:comparison-spline-lagrange}
\end{figure}


The main drawbacks of Lagrange's interpolating polynomial are:
\begin{itemize}
\item There may appear very small denominators, which may give rise to
  (large) rounding errors.
\item It is too \emph{twisted}.
\end{itemize}

The first problem is intrinsic to the way we have computed
it\footnote{There are different ways, but we shall not describe them.}.
The second one depends on the
distribution of the $x_i$ in the segment $[x_0, x_n]$. A remarkable
example is given by \emph{Runge's phenomenon}: whenever a function with
large derivatives is approximated by a Lagrange interpolating
polynomial with the $x-$coordinates evenly distributed, then this
polynomial gets values which differ greatly from those of the original
function (even though it passes through the interpolation points).
This phenomenon is much less frequent in splines.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={\tiny{$f(x)=\frac{1}{1+12x^2}$}, \tiny{Lagrange}},
legend style={at={(0.5,0.05)},
anchor = south}
]
\addplot[line width={1pt},smooth,mark=none, dashed] file {../datafiles/runge.dat};
\addplot[line width={1pt},smooth,mark=none] file
{../datafiles/runge-lagrange.dat};
\addplot[line width={0.01pt},mark=none, color=blue]
file {../datafiles/runge-spline.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(-1.00000,0.07692)
(-0.80000,0.11521)
(-0.60000,0.18797)
(-0.40000,0.34247)
(-0.20000,0.67568)
(0.00000,1.00000)
(0.20000,0.67568)
(0.40000,0.34247)
(0.60000,0.18797)
(0.80000,0.11521)
(1.00000,0.07692)
};
\end{axis}
\end{tikzpicture}
\caption{Runge's Phenomenon: the Lagrange interpolating polynomial
  takes values very far away from the original function if the nodes
  are evenly distributed. There is a thin blue line which corresponds
  to the cubic spline, which is indistinguishable from the original
  function.}
  \label{fig:polinom-lagrange-runge}
\end{figure}

One may try to avoid this issue (which is essentially one of
curvature) using techniques which try to minimize the maximum value
taken by the polynomial
\begin{equation*}
R(x)=(x-x_0)(x-x_1)\dots (x-x_n),
\end{equation*}
that is, looking for a distribution of the $x_i$ which solves a
\emph{minimax} problem. We shall not get into details. Essentially,
one has:
\begin{lemma}\label{lem:nodos-de-chebychev}
  The points $x_0, \dots, x_n$ minimizing the largest maximum value of
  $R(x)$ on the interval $[-1,1]$ are given by the formula
\begin{equation*}
x_i = \cos \left(
\frac{2k+1}{n+1} \frac{\pi}{2}
\right)
\end{equation*}
So that, the corresponding points for  $[a,b]$,
for $a,b\in {\mathbb R}$, are
\begin{equation*}
\tilde{x_i} = \frac{(b-a)x_i + (a+b)}{2}.
\end{equation*}
\end{lemma}
The points $x_i$ in the lemma are called \emph{Chebychev's nodes} for
the interval $[a,b]$. They are the ones to be used if one wishes to
interpolate a function using Lagrange's polynomial and get a good
approximation. 

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={\tiny{$f(x)=\frac{1}{1+12x^2}$},
    \tiny{Lagrange}},
legend style={at={(0.5,0.05)},
anchor = south}
]
\addplot[line width={1pt},smooth,mark=none,dashed, color=black] file
{../datafiles/runge.dat};
\addplot[line width={1pt},smooth,mark=none,color=red] file
{../datafiles/runge-chebychev.dat};
\addplot[line width={1pt},color=red, mark=*, only marks] coordinates{
(9.8982e-01,7.8389e-02)
(9.0963e-01,9.1498e-02)
(7.5575e-01,1.2733e-01)
(5.4064e-01,2.2185e-01)
(2.8173e-01,5.1217e-01)
(6.1232e-17,1.0000e+00)
(-2.8173e-01,5.1217e-01)
(-5.4064e-01,2.2185e-01)
(-7.5575e-01,1.2733e-01)
(-9.0963e-01,9.1498e-02)
(-9.8982e-01,7.8389e-02)
};
\end{axis}
\end{tikzpicture}
\caption{Approximation of Runge's function using the Lagrange
  polynomial and Chebychev's nodes. The maximum error is much less
  than in \ref{fig:polinom-lagrange-runge}.}\label{fig:runge-chebychev}
\end{figure}


\section{Approximate Interpolation}
In experimental and statistical problems, data are always considered
inexact, so that trying to find a curve fitting them all exactly makes no
sense at all. This gives rise to a different interpolation concept:
one is no longer interested in making a curve pass through each point
in a cloud but in studying what curve in a family \emph{resembles} that cloud
in the best way. In our case, when one has a data table like
(\ref{eq:lista-datos}), this ``resemblance'' is measured using the
minimal quadratic distance from $f(x_i)$ to $y_i$, where $f$ is the
interpolating function, but this is not the only way to measure the
``nearness'' (however, it is the one we shall use).

\subsection{Least Squares Interpolation}
The most common approximate interpolating method is \emph{least
  squares interpolation}. One starts with a cloud of points
$\mathbf{x}, \mathbf{y}$, where $\mathbf{x}$ e $\mathbf{y}$ are
arbitrary vectors of length $N$. This is different from
\eqref{eq:lista-datos}: now we enumerate $x_i$ and $y_i$ from $1$ to
$N$ (notice also that there may be repeated
$x-$values and they need not be sorted). Given  $f:{\mathbb
  R}\rightarrow {\mathbb R}$, one defines
\begin{definition*}
The \emph{quadratic error}
of $f$ at $x_i$ (one of the coordinates of $\mathbf{x}$)) is the
number $(f(x_i) - y_i)^2$. The \emph{total quadratic error} of $f$ on
the cloud $(\mathbf{x},\mathbf{y})$ is the sum
\begin{equation*}
\sum_{i=1}^N (f(x_i) - y_i) ^2.
\end{equation*}
\end{definition*}

The \emph{least squares linear interpolation problem} consists in, given the
cloud of points $(x_i,y_i)$ and \emph{a family of functions}, to find
the function \emph{among those in the family}
which minimizes the total quadratic error. This family is assumed to be
a finite-dimensional vector space (whence the term \emph{linear}
interpolation).

We shall give an example of \emph{nonlinear} interpolation problem,
just to show the difficulties inherent to its lack of vector
structure.


\begin{figure}
\begin{tikzpicture}
\begin{axis}[]
\addplot[only marks, mark=x] file {../datafiles/rand-line-2.dat};
\addplot[domain=-1:1,line width={1pt}, red] {2*x+3.25};
\end{axis}
\end{tikzpicture}
\caption{Least squares interpolation of a cloud of points using
  functions of the form $y=ax+b$.}
\label{fig:minimos-cuadrados-1}
\end{figure}

\subsection{Linear Least Squares}
Assume one has to approximate a cloud of points of size $N$ with
a function $f$ belonging to a vector space $V$ of
dimension $n$ and that a
basis of $V$ is known: $f_1, \dots, f_n$. We always assume $N>n$ (it
is actually much greater, in general). That is, we try to find a
function  $f\in V$ such that
\begin{equation*}
\sum _{i=1}^N (f(x_i)-y_i)^2
\end{equation*}
is minimal. As  $V$ is a vector space, this $f$ must be a linear
combination of the elements of the basis:
\begin{equation*}
f = a_1f_1 + a_2f_2 + \dots + a_nf_n.
\end{equation*}
And the problem consists in finding the coefficients $a_1,
\dots, a_n$ minimizing the value of the $n-$variable function
\begin{equation*}
F(a_1, \dots, a_n) = \sum_{i=1}^N (a_1f_1(x_i) + \dots + a_nf_n(x_i) -
y_i)^2,
\end{equation*}
which is an $n-$dimensional optimization problem. It is obvious that
$F$ is differentiable function, so that the minimum
will annul all the partial derivatives (notice that the variables are
the $a_i$!). The following system, then, needs to be solved (the
critical values of $F$):
\begin{equation*}
\frac{\partial F}{\partial a_1} = 0, \frac{\partial F}{\partial
  a_2}=0, \dots, \frac{\partial F}{\partial a_n} =0.
\end{equation*}
The expansion of the partial derivative of $F$ with respect to $a_j$ is
\begin{equation*}
\frac{\partial F}{\partial a_j} = \sum_{i=1}^N 2 f_j(x_i)(a_1f_1(x_i) + \dots
+ a_nf_n(x_i) - y_i) = 0.
\end{equation*}
Writing
\begin{equation*}
\mathbf{y_j}=\sum_{i=1}^N f_j(x_i)y_{i}, 
\end{equation*}
and stating the equations in matrix form, one gets the system
\scriptsize
\begin{equation}
\label{eq:sistema-de-minimos-cuadrados}
\begin{split}
  \begin{pmatrix}
    f_1(x_1) & f_1(x_2) & \dots & f_1(x_N) \\
    f_2(x_1) & f_{2}(x_2) & \dots & f_2(x_N) \\
    \vdots & \vdots & \ddots & \vdots\\
    f_n(x_1) & f_{n}(x_2) & \dots & f_n(x_N) \\
  \end{pmatrix}
  \begin{pmatrix}
    f_1(x_1) & f_2(x_1) & \dots & f_n(x_1) \\
    f_1(x_2) & f_2(x_2) & \dots & f_n(x_{2}) \\
    \vdots & \vdots & \ddots & \vdots\\
    f_1(x_N) & f_2(x_N) & \dots & f_n(x_N)
  \end{pmatrix}
  \begin{pmatrix}
    a_1\\
    a_2 \\
    \vdots\\
    a_n
  \end{pmatrix}=\\
  =
    \begin{pmatrix}
    f_1(x_1) & f_1(x_2) & \dots & f_1(x_N) \\
    f_2(x_1) & f_{2}(x_2) & \dots & f_2(x_N) \\
    \vdots & \vdots & \ddots & \vdots\\
    f_n(x_1) & f_{n}(x_2) & \dots & f_n(x_N) \\
  \end{pmatrix}
  \begin{pmatrix}
    y_1\\
    y_2\\
    \vdots\\
    y_N\\
  \end{pmatrix}
  =
  \begin{pmatrix}
    \mathbf{y_1}\\
    \mathbf{y_2}\\
    \vdots\\
    \mathbf{y_n}
  \end{pmatrix},
\end{split}
\end{equation}
\normalsize
which is of the form
\begin{equation*}
XX^ta=Xy
\end{equation*}
where $X$ is the matrix whose row $i$ is the list of values of $f_i$
at $x_1, \dots, x_N$ and $y$ is the column vector $(y_1, y_2,\dots,
y_N)^t$, that is:
\begin{equation*}
X =   
\begin{pmatrix}
    f_1(x_1) & f_1(x_2) & \dots & f_1(x_N) \\
    f_2(x_1) & f_{2}(x_2) & \dots & f_2(x_N) \\
    \vdots & \vdots & \ddots & \vdots\\
    f_n(x_1) & f_{n}(x_2) & \dots & f_n(x_N) \\
  \end{pmatrix},\,\,\,
y =
\begin{pmatrix}
y_1\\
y_1\\
\vdots\\
y_N
\end{pmatrix}
\end{equation*}
System \eqref{eq:sistema-de-minimos-cuadrados}
is consistent system and has a unique solution if there are at
least as many points as the dimension of $V$ and the functions $f_i$
generate linearly independent rows (i.e. if the rank of $X$ is $n$).

However, system
(\ref{eq:sistema-de-minimos-cuadrados}) will \emph{very likely} be
\emph{ill-conditioned}.

\subsection{Non-linear Least Squares... Danger}
There are many cases in which the interpolating family of functions is
not a vector space. A well-known example is the set given
by the functions:
\begin{equation}\label{eq:gauss-family}
f(x) = ae^{bx^2}.
\end{equation}
These are \emph{Gaussian ``bells''} for $a,b>0$. They obviously do not
form a vector space and the techniques of last section do not apply.

One could try to ``transform'' the problem into a linear one, perform
the least squares approximation on the transformed problem and
``revert'' the result. This may be \emph{catastrophic}.

Taking logarithms on both sides of (\ref{eq:gauss-family}), one gets:
\begin{equation*}
\log(f(x)) = \log(a) + bx^2 = a^{\prime} + bx^2,
\end{equation*}
and, if instead of considering functions $ae^{bx^2}$, one considers
$c+dx^2$, and tries to approach the cloud of points $\mathbf{x},
\log(\mathbf{y})$, then one has a classical linear interpolation
problem, for which the techniques above apply. If one obtains a
solution $a^{\prime}=\log(a_{0})$,
$b^{\prime}=b_0$, then one could think that
\begin{equation*}
g(x) = a_0e^{b_0x^2}
\end{equation*}
would be a good approximation to the original cloud of points
$(\mathbf{x}, \mathbf{y})$. It might be. But it \emph{might not
  be}. One has to be aware that this approximation may not minimize
the total quadratic error.

Notice that, when taking logarithms, $y-$values
near $0$ are transformed into values near $-\infty$, and these will
have a \emph{much greater} importance than they had before the
transformation. This is because, when taking logarithms, absolute and
relative errors behave in a totally unrelated way. Moreover, if one
of $y-$values is $0$, there is no way to take logarithms and it would
have to be discarded.

What is at stake here is, essentially, the fact that $\log(x)$ is not
a linear function of $x$, that is: $\log(a+b)\neq
\log(a)+\log(b)$. Then, performing a linear computation on the
logarithms and then computing the exponential is not the same as
performing the linear operation on the initial data.

\begin{figure}
\begin{tikzpicture}
\begin{axis}[legend entries={data,$3e^{-2x^2}$,\small{``linear'' interp.}},
legend style={at={(0.95,0.95)},
anchor = north}]
\addplot[only marks, mark=*, red] file {../datafiles/psf-data.dat};
\addplot[line width={1pt}, blue] file {../datafiles/psf-function.dat};
\addplot[line width={1pt}, black] file {../datafiles/log-interpolated-psf.dat};
\end{axis}
\end{tikzpicture}
\caption{Non-linear interpolation using logarithms: the cloud of
  points resembles the function $f(x)$, but the linear interpolation
  taking logarithms and performing the inverse transform afterwards
  (the ``low bell'') is
  noticeably bad. This is because there are $y-$values too near to $0$.}
\label{fig:minimos-cuadrados-1}
\end{figure}


\section{Code for some of the Algorithms}

\subsection{Cubic Splines}
Matlab computes \emph{not-a-knot} splines by default (which arise
from imposing a condition on the third derivative at the second and
last-but-one points)
and \emph{natural splines} can only be computed using a
\emph{toolbox}. Listing \ref{lst:spline} implements the computation of
natural cubic splines for a cloud of points.
Input is:
\begin{description}
\item[\texttt{x}] the list of $x-$coordinates
\item[\texttt{y}] the list of $y-$coordinates
\end{description}

It returns a Matlab/Octave ``object'' which implements a \emph{piecewise
  polynomial}. There is no need to completely understand this
structure, only that if one wants to compute its value at a point, one
uses the function \texttt{ppval}.

%\renewcommand{\lstlistingname}{Code}
%\begin{figure}
%\begin{lstlisting}
\lstinputlisting[caption={Natural cubic spline computation using
  Matlab/Octave}, label=lst:spline]{../matlab/spline_cubico.m}
%\end{lstlisting}
%\caption{}
%\end{figure}


The following lines are an example of the usage of Listing
\ref{lst:spline} for approximating the
graph of the sine function.
\lstset{numbers=none}
\begin{lstlisting}
> x = linspace(-pi, pi, 10);
> y = sin(x);
> f = spline_cubico(x, y);
> u = linspace(-pi, pi, 400);
> plot(u, ppval(f, u)); hold on;
> plot(u, sin(u), 'r');
\end{lstlisting}
\lstset{numbers=left}

\subsection{The Lagrange Interpolating Polynomial}
The computation of the Lagrange Interpolating Polynomial in
Matlab/Octave is rather simple, as one can confirm reading listing
\ref{lst:lagrange-polynomial}. 

Input is:
\begin{description}
\item[\texttt{x}] The list of $\mathbf{x}-$coordinates of the cloud of
  points
\item[\texttt{y}] The list of $\mathbf{y}-$coordinates of the cloud of
  points
\end{description}

Output is a polynomial \emph{in vector form}, that is, a list of the
coefficients $a_n, a_{n-1}, \dots, a_0$ such that
$P(x)=a_nx^n+\dots +a_1x+a_0$ is the Lagrange interpolating polynomial
for $(\mathbf{x}, \mathbf{y})$.

%\renewcommand{\lstlistingname}{Code}
%\begin{lstlisting}
\lstinputlisting[caption={Code for computing the Lagrange
  interpolating polynomial},label=lst:lagrange-polynomial]{../matlab/lagrange.m}
%\end{lstlisting}


\subsection{Linear Interpolation}
For linear interpolation in Matlab/Octave, one has to use a special
object, a \emph{cell array}: the list of functions which comprise the
basis of the vector space $V$ has to be input \emph{between curly
  braces}. See Listing \ref{cod:least-squares}.

Input:
\begin{description}
\item[\texttt{x}] The list of $\mathbf{x}-$coordinates of the cloud
\item[\texttt{y}] The list of $\mathbf{y}-$coordinates of the cloud
  \item[\texttt{F}] A cell array of anonymous functions. Each elements
    is one of the functions of the basis of the linear space used for
    interpolation.
\end{description}

The output is a vector \texttt{c} with coordinates $(c_1, \dots, c_n)$,
such that $c_1F_1+\dots+c_nF_n$ is the least squares interpolating function in
$V$ for the cloud $(\mathbf{x},\mathbf{y})$.

\lstinputlisting[caption={Code for least squares interpolation},
  label=cod:least-squares]{../matlab/interpol.m}

Follows an example of use, interpolating with trigonometric functions
\lstset{numbers=none}
\begin{lstlisting}
> f1=@(x) sin(x); f2=@(x) cos(x); f3=@(x) sin(2*x); f4=@(x) cos(2*x);
> f5=@(x)sin(3*x); f6=@(x)cos(3*x);
> F={f1, f2, f3, f4, f5, f6};
> u=[1,2,3,4,5,6];
> r=@(x) 2*sin(x)+3*cos(x)-4*sin(2*x)-5*cos(3*x);
> v=r(u)+rand(1,6)*.01;
> interpol(u,v,F)
ans =
   1.998522   2.987153  -4.013306  -0.014984  -0.052338  -5.030067
\end{lstlisting}
\lstset{numbers=left}
