% -*- TeX-master: "notas.ltx" -*-
\chapter{Numerical Solutions to Linear Systems of Equations}
Some of the main classical algorithms for (approximately) solving
systems of linear equations are discussed in this
chapter. We begin with Gauss' reduction method (and its
interpretation as the $LU$ factorization).
The notion of \emph{condition number} of a
matrix and its relation with relative error is introduced (in a very
simplified way, without taking into account the errors in the
matrix). Finally, the fixed-point algorithms are introduced and two of
them (Jacobi's and Gauss-Seidel's) are explained.

All three algorithms are relevant by themselves but knowing the scope
of each one
is as important, at least from the theoretical standpoint; for
example, the requirement that the matrix be \emph{convergent} (the
analogue to contractivity). We shall not speak about the spectrum nor
use the eigenvalues; these are of the utmost importance but the
students have no true knowledge of them. We prefer to insist on the
necessity of convergence (and the student will be able to apply it to
each case when need comes).

In this chapter, the aim is to (approximately) solve a system of
linear equations of the form
\begin{equation}
\label{eq:sistema-original-lineal}
Ax=b
\end{equation}
where $A$ is a \emph{square} matrix of order $n$ and $b$ is a vector in
${\mathbb R}^n$. We always assume unless explicitely stated
that \emph{$A$ is non-singular}, so
that the system is consistent and has a unique solution. 

\section{Gauss' Algorithm and $LU$ Factorization}
Starting from system (\ref{eq:sistema-original-lineal}), Gauss' method
transforms $A$ and $b$ by means of ``simple'' operations into an upper
triangular matrix $\tilde{A}$ and a vector $\tilde{b}$ such that the
new system $\tilde{A}x=\tilde{b}$ is solvable by \emph{regressive
  substitution} (i.e. $x_n$ is directly solvable and each variable
$x_k$ is solvable in terms of $x_{k+1}, \dots, x_{n}$). Obviously, in
order that this transformation make sense, the
main requirement is that the solution to the new system
be the same as that of the original\footnote{However, it is important to
  realize that no numerical algorithm intends to find the \emph{exact}
solutions to any equation or system of equations. So this ``sameness'' of
the solutions relates only to the \emph{theoretical} algorithm}.
To this end, only the following
operation is permitted:
\begin{itemize}
\item Any equation  $E_{i}$ (the $i-$th row of $A$ together with the
  $i-$th element of $b$) may be substituted
  by a linear combination of the form  $E_i+\lambda E_k$ for some
  $k<i$ and $\lambda\in\mathbb{R}$. In this case, 
  $b_i$ is substituted with $b_i+\lambda b_k$.
\end{itemize}
The fact that $E_i$ appears with coefficient $1$ in the substituting
expression ($E_i+\lambda E_k)$ is what
guarantees that the new system has the same solution as the original one.

Let $\overline{A}$ be the augmented matrix $\overline{A}=\left(A|b\right)$.

\begin{lemma}
  \label{lem:combinacion-de-gauss-matriz-tonta}
  In order to transform a matrix $\overline{A}$ into a matrix $\tilde{A}$ using
  the above operation, it is enough to multiply $\overline{A}$ on the left by the
  matrix $L_{ik}(\lambda)$ whose elements are:
\begin{itemize}
\item If $m=n$, then $(L_{ik}(\lambda))_{mn}=1$ (diagonal with $1$).
\item If $m=i,n=k$, then $(L_{ik}(\lambda))_{mn}=\lambda$ (the element
  $(i,k)$ is $\lambda$).
\item Any other element is $0$.
\end{itemize}
\end{lemma}

\begin{example}
Starting with the following $\overline{A}$
\begin{equation*}
\overline{A}=\left(
\begin{array}{cccc|c}
3 & 2 & -1 & 4 & -1\\
0 & 1 & 4 & 2  & 3\\
6 & -1 & 2 & 5 & 0\\
1 & 4 & 3 & -2 & 4  
\end{array}
\right)
\end{equation*}
and combining row $3$ with row $1$ times $-2$ (in order to ``make a
zero at the $6$''), then one has to multiply by $L_{31}(-2)$
\begin{equation*}
\begin{pmatrix}
1 & 0 & 0 & 0  \\
0 & 1 & 0 & 0  \\
-2 & 0 & 1 & 0 \\
0 & 0 & 0 & 1  \\
\end{pmatrix}
\begin{pmatrix}
3 & 2 & -1 & 4  & -1\\
0 & 1 & 4 & 2  & 3  \\
6 & -1 & 2 & 5 & 0  \\
1 & 4 & 3 & -2 & 4  
\end{pmatrix}
=
\begin{pmatrix}
  3 & 2 & -1 &  4 & -1\\
  0 & 1 & 4  &  2 & 3\\
  0 &-5 & 4  & -3 & 2\\
  1 & 4 & 3  & -2 & 4  
\end{pmatrix}.
\end{equation*}
\end{example}

Algorithm  \ref{alg:gauss-simplificado-sistemas-lineales} is a
simplified statement of Gauss' reduction method.
The line with a comment \texttt{[*]} 
\ref{alg:gauss-simplificado-sistemas-lineales} is precisely the
multiplication of  $\tilde{A}$ on the left by
$L_{ji}(-m_{ji})$. In the end, $\tilde{A}$, which is upper triangular,
is a product of these matrices:
\begin{equation}
\label{eq:gauss-como-producto}
\small{\tilde{A} =
L_{n,n-1}(-m_{n,n-1})L_{n,n-2}(-m_{n,n-2})\cdots
L_{2,1}(-m_{2,1}) A
} = \tilde{L}A
\end{equation}
and $\tilde{L}$ is a lower triangular matrix with $1$'s on the diagonal (this
is a simple exercise to verify). It is easy to check (although this
looks a bit like \emph{magic}) that
\begin{lemma*}
  The inverse matrix of $\tilde{L}$ in
  (\ref{eq:gauss-como-producto}) is the lower triangular matrix whose
  $(i,j)$ entry is $m_{ij}$. That is,
  \begin{equation*}
    \begin{split}
      \left(L_{n,n-1}(-m_{n,n-1})L_{n,n-2}(-m_{n,n-2})\cdots
        L_{2,1}(-m_{2,1})\right)^{-1}=\\
      \begin{pmatrix}
        1 & 0 & 0 & \dots & 0\\
        m_{21} & 1 & 0 & \dots & 0\\
        m_{31} & m_{32} & 1 & \dots & 0\\
        \vdots & \vdots &  \vdots & \ddots & \vdots\\
        m_{n1}  & m_{n2} & m_{n3} &\dots & 1
      \end{pmatrix}
    \end{split}
  \end{equation*}
\end{lemma*}


\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} A square matrix $A$ and a vector
$b$, of order $n$
\STATE \textbf{Output:} Either an error message or a matrix
$\tilde{A}$ and a vector $\tilde{b}$ 
such that $\tilde{A}$ is upper triangular and the system
$\tilde{A}x=\tilde{b}$ has the same solutions as $Ax = b$
\PART{Start}
\STATE $\tilde{A}\leftarrow A, \tilde{b}\leftarrow b, i\leftarrow 1$
\WHILE{$i<n$}
\IF{$\tilde{A}_{ii}=0$}
\STATE \textbf{return} ERROR \comm{division by zero}
\ENDIF
\STATE \comm{combine rows underneath $i$ with row $i$}
\STATE{$j\leftarrow i+1$}
\WHILE{$j \leq n$}
\STATE $m_{ji}\leftarrow \tilde{A}_{ji}/ \tilde{A}_{ii}$
\STATE \comm{Next line is an operation on a \textbf{row}}
\STATE $\tilde{A}_j \leftarrow \tilde{A}_{j} - m_{ji} \tilde{A}_i$
\,\,\,\comm{*}
\STATE $\tilde{b}_j\leftarrow \tilde{b}_j-m_{ji} \tilde{b}_{i}$
\STATE $j\leftarrow j+1$
\ENDWHILE
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $\tilde{A}, \tilde{b}$
\end{algorithmic}
\caption{Gauss' Algorithm for linear systems}
\label{alg:gauss-simplificado-sistemas-lineales}
\end{algorithm}


We have thus proved the following result:
\begin{theorem}
\label{the:lu-factorization}
If in the Gauss' reduction process no element of the diagonal is $0$
at any step, then there is a lower triangular matrix $L$ whose
elements are the corresponding coefficients at their specific place
and an upper triangular matrix $U$ such that
\begin{equation*}
A = LU
\end{equation*}
and such that the system $Ux = \tilde{b} = L^{-1}b$ is equivalent to
the initial one $Ax=b$.
\end{theorem}
With this result, a factorization of $A$ is obtained which simplifies
the resolution of the original system, as $Ax=b$ can be rewritten
$LUx=b$ and one can proceed step by step:
\begin{itemize}
\item First the system $Ly=b$ is solved by \emph{direct substitution}
  ---that is, from top to bottom, without even dividing.
\item Then the system $Ux = y$ is solved by \emph{regressive
    substitution}
  ---from bottom to top; divisions will be needed at this point.
\end{itemize}
This solution method only requires storing the matrices $L$ and $U$ in
memory and is very fast.

%
% The following subsection is obsolete as it is written
%


%\subsection{Complexity of Gauss' Algorithm}
% Classically, the complexity of algorithms involving arithmetic
% operations was measured in terms of the number of multiplications
% required. Today this has become irrelevant, as multiplications take
% essentially the same time as additions. The slow operation (which will
% probably remain slow for quite a while) is division, which must be
% used as sparingly as possible.

% De esta manera, si \emph{se supone que las divisiones pueden hacerse
%   con exactitud}\footnote{Lo cual es, como ya se ha dicho muchas
%   veces, demasiado suponer.}, se tiene el siguiente resultado:
% \begin{lemma}
% Si en el proceso de simplificaci\'on de Gauss no aparece ning\'un cero
% en la diagonal, tras una cantidad de a lo sumo $(n-1)+(n-2)+\dots+1$
% combinaciones de filas, se obtiene un sistema $\tilde{A}x=\tilde{b}$
% donde $\tilde{A}$ es triangular superior.
% \end{lemma}

% Cada combinaci\'on de filas en el paso $k$, tal como se hace el algoritmo requiere
% una multiplicaci\'on por cada elemento de la fila (descontando el que
% est\'a justo debajo del pivote, que se sabe que hay que sustituir por $0$), que en este paso
% da $n-k$ productos, m\'as una divisi\'on (para calcular el
% multiplicador de la fila) y otra multiplicaci\'on
% para hacer la combinaci\'on del vector. Es decir, en el paso $k$ hacen
% falta
% \begin{equation*}
% (n-k)^2 + (n-k ) + (n-k)
% \end{equation*}
% operaciones ``complejas'' (multiplicaciones, esencialmente) 
% y hay que sumar desde $k=1$ hasta $k=n-1$, es decir, hay que sumar
% \begin{equation*}
% \sum_{i=1}^{n-1}i^2+2\sum_{i=1}^{n-1}i.
% \end{equation*}
% La suma de los primeros $r$ cuadrados es (cl\'asica) $r(r+1)(2r+1)/6$,
% mientras que la suma de los primeros $r$ n\'umeros es
% $r(r+1)/2$. As\'i pues, en total, se tienen
% \begin{equation*}
% \frac{(n-1)n(2(n-1)+1)}{6} + (n-1)n = \frac{n^3}{3}+\frac{n^2}{2}-\frac{5n}{6}.
% \end{equation*}
% Para ahora resolver el sistema triangular superior
% $\tilde{A}x\tilde{b}$, hace falta una divisi\'on por cada l\'inea y
% $k-1$ multiplicaciones en la fila $n-k$ (con $k$ desde $0$ hasta $n$),
% as\'i que hay que sumar $n+1+\dots+(n-1)= \frac{n(n+1)}{2}$. Por tanto,
% para resolver un sistema con el m\'etodo de Gauss, hacen falta en
% total
% \begin{equation*}
% \frac{n^3}{3} + n^2- \frac{n}{3} \mbox{ operaciones para $Ax=b$.}
% \end{equation*}

% Si el algoritmo se utiliza para resolver $m$ sistemas de la forma
% $Ax=b_i$ (para diferentes $b_i$), todas las operaciones son iguales
% para triangular $A$ y simplemente hay que recomputar $\tilde{b}_i$ y
% resolver, Esto requiere $(n-1)+\dots +1=n(n-1)/2$ multiplicaciones (las del
% proceso de triangulaci\'on) y $1+2+\dots + n$ para
% ``despejar''. Descontando las que se hicieron en la resoluci\'on de
% $b$, resulta que para resolver los $m$ sistemas, hacen falta:
% \begin{equation}\label{eq:complexity-of-m-systems-gauss}
% \frac{n^3}{3} + mn^{2}-\frac{n}{3} \mbox{ operaciones para $m$ sistemas.}
% \end{equation}

% \subsubsection{Comparaci\'on con utilizar $A^{-1}$}
% Es sencillo comprobar que el c\'alculo general de la inversa de una
% matriz requiere, utilizando el algoritmo de Gauss-Jordan 
% (o por ejemplo, resolviendo los sitemas $Ax=e_i$ para la
% base est\'andar) al menos $n^3$ operaciones. Hay mejores algoritmos
% (pero se est\'an intentando comparar m\'etodos an\'alogos). Una vez
% calculada la inversa, resolver un sistema $Ax=b$ consiste en
% multiplicar $b$ a la izquierda por $A^{-1}$, que requiere (obviamente)
% $n^2$ productos. Por tanto, la resoluci\'on de $m$ sistemas requiere
% \begin{equation*}
% n^3 +mn^2 \mbox{ operaciones complejas.}
% \end{equation*}
% Que siempre es m\'as grande que
% (\ref{eq:complexity-of-m-systems-gauss}). As\'i que, si se utiliza el
% m\'etodo de Gauss, es mejor conservar la factorizaci\'on $LU$ y
% utilizarla para ``la sustituci\'on'' que calcular la inversa y
% utilizarla para multiplicar por ella.

% Claro est\'a que \emph{esta comparaci\'on es entre m\'etodos
%   an\'alogos}: hay maneras de computar la inversa de una matriz en
% menos (bastante menos) de $n^3$ operaciones (aunque siempre m\'as que
% un orden de $n^2\log(n)$).

\subsection{Pivoting Strategies and the  $\mathrm{\emph{LUP}}$ Factorization}
If during Gaussian reduction a pivot appears
(the element which determines the multiplication) of value
approximately $0$, either the process cannot be continued or one
should expect large errors to take place, due to rounding and
truncation\footnote{Recall Example \ref{ex:division-truncation} of
  Chapter \ref{cha:arithmetic}, where
a tiny truncation error in a denominator became a huge error in the
final result.}. This problem
can be tackled by means of \emph{pivoting strategies}, either swapping
rows or both rows and columns. If only rows are swapped, the operation
is called \emph{partial pivoting}. If swapping both rows and columns
is allowed, \emph{total pivoting} takes place.

\begin{definition}
A \emph{permutation matrix} is a square matrix whose entries are all
$0$ but for each row and column, in each of which there is exactly a $1$.
\end{definition}
Permutation matrices can be built from the identity matrix by
\emph{permuting} rows (or columns).

A permutation matrix $P$ has, on each row, a single $1$ and the rest
entries are $0$. If on row $i$ the only $1$ is on column $j$, the
multiplication $PA$ means ``swap rows $j$ and $i$ of $A$.''

Obviously, the determinant of a permutation matrix is not zero (it is
either $1$ or $-1$). It is not so easy to show that the inverse of a
permutation matrix is another permutation matrix and, as a matter of
fact, that $P^{-1}=P^T$ (the transpose) if $P$ is a permutation matrix.

\begin{lemma}
  If $A$ is an $n\times m$ matrix and $P$ is a permutation matrix of
  order $n$ having only $2$ non-zero elements out of the diagonal, say
  $(i,j)$ and $(j,i)$, then $PA$ is the matrix obtained from $A$ by
  swapping \emph{rows} $i$ and $j$. On the other hand, if $P$ is of
  order $m$, then $AP$ is the
  matrix $A$ with \emph{columns} $i$ and $j$ swapped.
\end{lemma}


\begin{definition}
  A \emph{pivoting strategy} is followed in Gauss' reduction process
  if the pivot element chosen at step $i$ is the one with
  greatest absolute
  value. 
\end{definition}

In order to perform Gauss' algorithm with partial pivoting, one can
just follow the same steps but choosing, at step $i$, the row $j>i$ in
which the pivot has the greatest absolute value and swapping rows $i$
and $j$.

Pivoting strategies give rise to a factorization which differs from
$LU$ in that permutation matrices are allowed as factors.

\begin{lemma}
Given the linear system of equations $Ax=b$ with $A$ nonsingular,
there is a permutation matrix $P$ and two matrices $L$, $U$, the
former lower triangular (with only $1$
on the diagonal) and the latter upper triangular  such that
\begin{equation*}
PA = LU.
\end{equation*}
\end{lemma}

This result is proved indirectly by recurrence (we are not going to do
it).

Both Matlab and Octave include the function \texttt{lu} which, given
$A$, returns three values: $L$, $U$ and $P$.

For example, if
\begin{equation*}
A =
\begin{pmatrix}
  1 & 2  &3  &4\\
  -1 & -2  &5  &6\\
  -1 & -2  &-3  &7\\
  0 & 12  &7  &8\\
\end{pmatrix}
\end{equation*}
then
\begin{equation*}
L=  \begin{pmatrix}
    1 & 0 & 0 & 0\\
    0 & 1 & 0 & 0\\
    -1 & 0 & 1 & 0\\
    -1 & 0 & 0 & 1\\
  \end{pmatrix}\,\,\,
U=
\begin{pmatrix}
1 & 2 & 3 & 4\\
0 & 12 & 7 & 8\\
0 & 0 & 8 & 10\\
0 & 0 & 0 & 11\\
\end{pmatrix},\,\,
P=
\begin{pmatrix}
 1 & 0 & 0 & 0\\
 0 & 0 & 0 & 1\\
 0 & 1 & 0 & 0\\
 0 & 0 & 1 & 0\\
\end{pmatrix}
\end{equation*}

In order to compute $L$, $U$ and $P$ one only needs to follow Gauss'
algorithm and \emph{whenever a swap of rows $i$ and $j$ is performed},
the same swap has to be performed on $L$ \emph{up to the $i-1$
  column} (that is, if a swap is needed on $A$ at step $i$ then the
rows $i$ and $j$ are swapped \emph{but only the columns $1$ to $i-1$}
are involved). Also, the $P$ computed up to step $i$ has to be
multiplied on the left by $P_{ij}$ (the permutation matrix for $i$ and
$j$).

This can be stated as Algorithm \ref{alg:lup}.

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} A matrix $A$ of order $n$
\STATE \textbf{Output:} Either an error or three matrices:
$L$ lower triangular with $1$ on the diagonal, $U$ upper
triangular and a permutation matrix $P$ such that $LU=PA$
\STATE \textbf{Comment:} $P_{ip}$ is the permutation matrix permuting
rows $i$ and $p$.
\PART{Start}
\STATE $L\leftarrow\rm{Id}_n, U\leftarrow\rm{Id}_n,
P\leftarrow\rm{Id}_n$, $i\leftarrow 1$
\WHILE{$i<n$}
\STATE $p\leftarrow $ row index such that $\abs{U_{pi}}$ is maximum, with
$p\geq i$
\IF{$U_{pi}=0$}
\STATE \textbf{return} ERROR \comm{division by zero}
\ENDIF
\STATE \comm{swap rows $i$ and $p$}
\STATE $P\leftarrow P_{ip}P$
\STATE $U\leftarrow P_{ip} U$
\STATE \comm{on $L$ swap only rows $i$ and $p$ on the
  submatrix $n\times (i-1)$ at the left, see the text}
\STATE $L\leftarrow \tilde{L}$
\STATE \comm{combine rows on $U$ and keep track on $L$}
\STATE{$j\leftarrow i+1$}
\WHILE{$j<=n$}
\STATE $m_{ji}\leftarrow U_{ji}/ U_{ii}$
\STATE $U_j \leftarrow U_{j} - m_{ij} U_i$
\STATE $L_{ji} \leftarrow m_{ji}$
\STATE $j\leftarrow j+1$
\ENDWHILE
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $L,U,P$
  \end{algorithmic}
\caption{$\mathrm{\emph{LUP}}$ Factorization for a matrix A}
\label{alg:lup}
\end{algorithm}


\section{Condition Number: behavior of the relative error}
We now deal briefly with the question of the \emph{stability} of the
methods for solving systems of linear equations. This means: if
instead of starting with a vector $b$ (which might be called the
``initial condition''), one starts with a slight modification
$\tilde{b}$, how 
much does the solution to the new system differ from the original one?
The proper way to ask this question uses the \emph{relative} error,
not the absolute one. Instead of system
(\ref{eq:sistema-original-lineal}), consider the modified one
\begin{equation*}
Ay = b+\delta_{b}
\end{equation*}
where $\delta_{b}$  is a  \emph{small vector}. The new solution will
have the form $x+\delta_{x}$, for some $\delta_{x}$ (which one \emph{expects} to
be small as well).

\begin{example}
  Consider the system of linear equations
  \begin{equation*}
    \begin{pmatrix}
      2 & 3\\
      4 & 1\\
    \end{pmatrix}
    \begin{pmatrix}
      x_{1}\\
      x_{2}
    \end{pmatrix}
    =
    \begin{pmatrix}
      2\\
      1
    \end{pmatrix}
  \end{equation*}
  whose solution is $(x_{1},x_{2})=(0{.}1, 0{.}6)$. If we take
  $\tilde{b}=(2{.}1, 1{.}05)$ then $\delta_b=(0{.}1, 0{.}05)$ and the
  solution of the new system
  \begin{equation*}
    \begin{pmatrix}
      2 & 3\\
      4 & 1\\
    \end{pmatrix}
    \begin{pmatrix}
      x_{1}\\
      x_2
    \end{pmatrix}
    =
    \begin{pmatrix}
      2{.}1\\
      1{.}05
    \end{pmatrix}    
  \end{equation*}
  is $(x_{1},x_2)=(0{.}105, 0{.}63)$ so that $\delta_x=(0{.}005,
  0{.}03)$. In this case, a small increment $\delta_b$ gives rise to a
  small increment $\delta_x$ but this needs not be so.

  However, see Example \ref{ex:ill-conditioned-system} for a system
  with a very different behaviour.
\end{example}

The size of vectors is measured using \emph{norms} (the most common
one being \emph{length}, in Euclidean space, but we shall not make use
of this one). As $x$ is a solution, one has
\begin{equation*}
A(x+\delta_{x}) = b+\delta_{b},
\end{equation*}
so that
\begin{equation*}
A \delta_{x} = \delta_{b},
\end{equation*}
but, as we want to compare $\delta_{x}$ with $\delta_{b}$, we can
write 
\begin{equation*}
\delta_{x} = A^{-1} \delta_{b}
\end{equation*}
so that, taking \emph{sizes} (i.e. \emph{norms}, which are denoted
with $\| \|$), we get
\begin{equation*}
\| \delta_{x} \| = \| A^{-1}\delta_{b} \|.
\end{equation*}
Recall that we are trying to asses the \emph{relative displacement}, not the
absolute one. To this end we need to include $\| x \|$ in the left
hand side of the equation. The available information is that $Ax = b$,
from which $\| Ax \| = \| b
\|$. Hence,
\begin{equation}\label{eq:first-condition-eq}
\frac{\| \delta_{x} \|}{\| A x \| } = \frac{\| A^{-1} \delta_{b}\|}{\| b \|},
\end{equation}
but this is not very useful (it is obvious, as it stands). However,
assume there is some kind of \emph{matrix norm} $\| A \|$ which
satisfies that $\| Ax \|\leq \| A \|\| x \|$. Then, performing this
substitution in Equation \eqref{eq:first-condition-eq}, one gets
\begin{equation*}
\frac{\| A^{-1} \delta_{b}\|}{\| b \|}= \frac{\| \delta_{x} \|}{\| A x \|
} \geq \frac{\| \delta_{x} \|}{\|A\| \|x\|}
\end{equation*}
and, applying the same reasoning to the right hand side of
\eqref{eq:first-condition-eq}, one gets
\begin{equation*}
\frac{\| A^{-1}\| \|\delta_{b}\|}{\| b \|} \geq \frac{\| A^{-1} \delta_{b}\|}{\| b \|},
\end{equation*}
and combining everything,
\begin{equation*}
\frac{\| A^{-1}\| \|\delta_{b}\|}{\| b \|} \geq \frac{\| \delta_{x} \|}{\|A\| \|x\|}
\end{equation*}
so that, finally, one gets an upper bound for the \emph{relative
  displacement of $x$}:
\begin{equation*}
\frac{\| \delta_{x} \|}{\| x \|} \leq \| A \| \| A^{-1} \| \frac{\|
  \delta_{b} \| }{\| b \|}
\end{equation*}
The ``matrix norm'' which was mentioned above does exist. In fact,
there are many of them. We shall only make use of the following one in
these notes:
\begin{definition}
\label{def:norma-infinito}
The \emph{infinity norm} of a square matrix $A=(a_{ij})$ is the number
\begin{equation*}
\| A \|_{\infty} = \max_{1\leq i \leq n} \sum_{j=1}^n \abs{a_{ij}},
\end{equation*}
that is, the maximum of the sums of absolute values of each row.
\end{definition}
The following result relates the infinity norms of matrices and vectors:
\begin{lemma}
The \emph{infinity norm} is such that, for any vector $x$, $\| Ax
\|_{\infty} \leq \|A\|_{\infty}\|x\|_{\infty}$, where $\|x\|_{\infty}$
is the norm given by the maximum of the absolute values of the
coordinates of $x$.
\end{lemma}
This means that, if one measures the size of a vector by its largest
coordinate (in absolute value), and one calls it $\|x\|_{\infty}$, then
\begin{equation*}
\frac{\| \delta_{x} \|_{\infty}}{\| x \|_{\infty}} \leq \| A \|_{\infty}
\|A^{-1}\|_{\infty} \frac{\|\delta_{b} \|_{\infty}}{\| b \|_{\infty}}.
\end{equation*}
The product $\| A \|_{\infty} \| A^{-1}\|_{\infty}$ is called the
\emph{condition number of $A$ for the infinity norm}, is denoted
$\kappa(A)$ and is a bound for the maximum possible displacement of
the solution when 
the initial vector gets displaced.
%De hecho, se tiene el siguiente resultado:
%\begin{lemma}
%Siempre hay un desplazamiento $\delta_{b}$ para el que 
%\begin{equation*}
%\frac{\| \delta_{x} \|_{\infty}}{\| x \|_{\infty}} = \kappa(A)
% \frac{\|\delta_{b} \|_{\infty}}{\| b \|_{\infty}}.
%\end{equation*}
%Es decir, el n\'umero de condici\'on es una cota que se cumple alguna
%vez. 
% \end{lemma}
The greater the condition number, the greater (to be expected) the displacement of the
solution when the initial condition (independent term) changes a little.

The condition number also bounds \emph{from below} the relative displacement:

\begin{lemma}
  Let $A$ be a nonsingular matrix of order $n$ and $x$ a solution of
  $Ax=b$. Let $\delta_{b}$ be a ``displacement'' of the initial
  conditions and $\delta_{x}$ the corresponding ``displacement'' in the
  solution. Then:
\begin{equation*}
\frac{1}{\kappa (A)} \frac{\| \delta_{b}\|}{\| b \|}\leq \frac{\| \delta
x\|}{\| x \|} \leq \kappa (A) \frac{\| \delta_{b} \|}{\| b \|}.
\end{equation*}
So that the relative displacement (or error) can be bounded using the
\emph{relative residue}
 (the number $\| \delta_{b}\| / \| b \|$).
\end{lemma}


The following example shows how large condition numbers are usually an
indicator that solutions may be strongly dependent on the initial values.

\begin{example}\label{ex:ill-conditioned-system}
Consider the system
\begin{equation}\label{eq:ill-conditioned-2-system}
\begin{matrix}
0{.}853 x + 0{.}667 y & = & 0{.}169\\
0{.}333 x + 0{.}266 y & = & 0{.}067
\end{matrix}
\end{equation}
whose condition number for the infinity norm is $376{.}59$, so that a
relative change of a thousandth of unit in the initial conditions
(vector $b$) is expected to give rise to a relative change of more
than $37\%$ in the solution. The exact solution of
\eqref{eq:ill-conditioned-2-system} is
$x=0{.}055+, y=0{.}182+$.

However, the size of the condition number is a
tell-tale sign that a small perturbation on the system will modify the
solutions greatly. If, instead of $b=(0{.}169, 0{.}067)$, one uses
$b=(0{.}167, 0{.}067)$ (which is a relative displacement of just
$1{.}1\%$ in the first coordinate), the new solution is
$x=-0{.}0557+, y=0{.}321+$, for which $x$ has not even the same sign
as in the original problem and $y$ is displaced $76\%$ from its
original value. This is clearly unacceptable. If the equations describe
a static system, for example, and the coefficients have been measured
with up to $3$ significant digits, then the system of equations
\emph{is useless},
as one cannot be certain that the measuring errors are meaningful.
\end{example}


\section{Fixed Point Algorithms}
Gauss' reduction method is a first attempt at a fast algorithm for
solving linear systems of equations. It has two drawbacks: it is quite
complex (in the technical sense, which means it requires \emph{a lot
  of operations} to perform) and it depends greatly on the accuracy of
divisions (and when small divisors do appear, large relative errors
are expected to come up). However, the first issue (complexity) is the main one. A
rough approximation shows that for a system with $n$ variables, Gauss'
algorithm requires approximately $n^3$ operations. For $n$ in the tens
of thousands this soon becomes impractical.

Fixed point algorithms tackle this problem by not trying to find an
``exact'' solution (which is the aim of Gauss' method) but to
approximate one using the techniques explained in Chapter
\ref{cha:ecuaciones-no-lineales}. 

Start with a system like
(\ref{eq:sistema-original-lineal}), of the form $Ax=b$. One can
transform it into a fixed point problem by means of a ``decomposing''
of matrix $A$ into two: $A=N-P$, where $N$ is some invertible matrix.
This way,  $Ax=b$ can be written $(N-P)x=b$, i.e.
\begin{equation*}
(N-P)x = b \Rightarrow Nx = b + Px \Rightarrow x = N^{-1}b + N^{-1}P x.
\end{equation*}
If one calls $c = N^{-1}b$ and $M=N^{-1}P$, then one obtains the
following fixed point problem:
\begin{equation*}
x = M x + c
\end{equation*}
which can be solved (if at all) in the very same way as in Chapter
\ref{cha:ecuaciones-no-lineales}: start with a seed $x_0$ and iterate
\begin{equation*}
x_n = M x_{n-1} + c,
\end{equation*}
until a sufficient precision is reached.

In what follows, the infinity norm $\|\,\|_{\infty}$ is assumed
whenever the concept of ``convergence'' appears\footnote{However, all
  the results below apply to any matrix norm.}.

One needs the following results:
\begin{theorem}
\label{the:condiciones-necesarias-convergencia-iteraciones-lineales}
Assume $M$ is a matrix of order $n$ and that $\| M
\|_{\infty}<1$. Then the equation $x=Mx+c$ has a unique solution for
any $c$ and the iteration  $x_n = Mx_{n-1}+c$ converges to it for any
initial value $x_0$.
\end{theorem}

\begin{theorem}
\label{the:cota-para-punto-fijo-matricial}
Given $M$ with $\| M\|_{\infty}<1$, and given a seed $x_0$ for the
iterative method of Theorem
\ref{the:condiciones-necesarias-convergencia-iteraciones-lineales}, if
$s$ is the solution to $x = Mx + c$, then the following bound holds:
\begin{equation*}
\| x_n - s \|_{\infty} \leq \frac{\| M \|_{\infty}^n}{1- \| M
  \|_{\infty}}\| x_1 - x_0\|_{\infty}.
\end{equation*}
Recall that for vectors, the infinity norm  $\| x \|_{\infty}$ is the
maximum of the absolute values of the coordinates of $x$.
\end{theorem}

We now proceed to explain the two basic iterative methods for solving
linear systems of equations: Jacobi's and Gauss-Seidel. Both rely on
``decomposing'' $A$ in different ways: Jacobi takes $N$ as the
diagonal of $A$ and Gauss-Seidel as the lower triangular part of $A$,
including its diagonal.

\subsection{Jacobi's Algorithm}
If each coordinate $x_i$ is solved ex\-pli\-ci\-tly in terms of the others,
for the system $Ax=b$, then something like what follows appears:
\begin{equation*}
x_i = \frac{1}{a_{ii}} (b_i - a_{i1}x_1 - \dots - a_{ii-1}x_{i-1} -
a_{ii+1}x_{i+1} - \dots -a_{in}x_{n}),
\end{equation*}
in matrix form,
\begin{equation*}
x = D^{-1}(b - (A-D)x),
\end{equation*}
where $D$ is the diagonal matrix whose nonzero elements are the
diagonal of $A$. One can write,
\begin{equation*}
x = D^{-1}b - D^{-1}(A-D)x,
\end{equation*}
which is a fixed point problem.

If $D^{-1}(A-D)$ satisfies the
conditions of Theorem
\ref{the:condiciones-necesarias-convergencia-iteraciones-lineales},
then the iterations corresponding to the above expression converge for
any choice of seed
$x_0$ and the bound of Theorem 
\ref{the:cota-para-punto-fijo-matricial} holds. This is called
Jacobi's method.
In order to verify the necessary conditions, the computation of $D^{-1}(A-D)$
is required, although there are other sufficient conditions (especially Lemma
\ref{lem:jacobi-gs-convergentes-estrictamente-diag}).

\subsection{Gauss-Seidel's Algorithm}
If instead of using the diagonal of $A$ one takes its lower
triangular part (including the diagonal) then one gets a system of the
form 
\begin{equation*}
x = T^{-1}b - T^{-1}(A-T)x,
\end{equation*}
which is also a fixed point problem.

If $T^{-1}(A-T)$ satisfies the conditions of Theorem
\ref{the:condiciones-necesarias-convergencia-iteraciones-lineales},
then the corresponding iteration (called the Gauss-Seidel iteration)
converges for any initial seed $x_0$
and the bound of Theorem
\ref{the:cota-para-punto-fijo-matricial} holds. In order to verify the
conditions one should compute $T^{-1}(A-T)$, but there are other sufficient conditions.

\subsection{Sufficient conditions for convergence}
We shall use two results which guarantee convergence for the iterative
methods explained above. One requires a technical definition, the
other applies to positive definite matrices.

\begin{definition}
A matrix  $A=(a_{ij})$ is \emph{strictly diagonally dominant} by rows if
\begin{equation*}
\abs{a_{ii}}>\sum_{j\neq i}\abs{a_{ij}}
\end{equation*}
for any $i$ from $1$ to $n$. That is, if the elements on the diagonal
are greater in absolute value than the sum of the rest of the elements
of their rows in absolute value.
\end{definition}

For these matrices, the convergence of both Jacobi's and
Gauss-Seidel's methods is guaranteed:
\begin{lemma}\label{lem:jacobi-gs-convergentes-estrictamente-diag}
If $A$ is a \emph{strictly diagonally dominant} matrix by rows then
both Jacobi's and Gauss-Seidel's methods converge for any system of
the form $Ax=b$ and any seed.
\end{lemma}

For Gauss-Seidel, the following also holds:
\begin{lemma}
If $A$ is a positive definite symmetric matrix, then Gauss-Seidel's
method converges for any system of the form $Ax=b$.
\end{lemma}


\section{Annex: Matlab/Octave Code}

Code for some of the algorithms explained is provided; it should work both
in Matlab and Octave.

\subsection{Gauss' Algorithm without Pivoting}
Listing \ref{lst:gauss} implements Gauss' reduction algorithm for a system
$Ax=b$, returning $L$,  $U$ and the new $b$ vector, \emph{assuming
  the multipliers are never $0$}; if some is $0$, then the process
terminates with a message.

Input:
\begin{description}
\item[\texttt{A}] a square matrix (if it is not square, the output
  gives the triangulation under the principal diagonal),
\item[\texttt{b}] a vector with as many rows as \texttt{A}.
\end{description}

The output is a trio \texttt{[L, At, bt]}, as follows:
\begin{description}
\item[\texttt{L}] the lower triangular matrix (with the multipliers),
\item[\texttt{At}] the transformed matrix (which is $U$ in the
  $LU$-factorization) and which is upper triangular.
\item[\texttt{bt}] the transformed initial vector.
\end{description}
The new system to be solved is, hence, $At\times x = bt$.

%\renewcommand{\lstlistingname}{Listing}
%\begin{figure}
%\begin{lstlisting}
\lstinputlisting[caption={Gauss' Reduction Algorithm}, label=lst:gauss]{../matlab/gauss.m}
%%\end{lstlisting}
%\caption{Gauss' Reduction Algorithm}
%\end{figure}


\subsection{$\mathrm{\emph{LUP}}$ Factorization}
Gauss' Reduction depends on the non-appearing of zeros on the diagonal
and it can also give rise to large rounding errors if pivots are
small. Listing \ref{lst:lup} implements $\mathrm{\emph{LUP}}$
factorization, which provides matrices $L$, $U$ and $P$ such that
$LU=PA$, $L$ and $U$
being respectively lower and upper triangular and $P$ a permutation
matrix. Its input is
\begin{description}
\item[\texttt{A}] a square matrix of order $n$.
\item[\texttt{b}] An $n$-row vector. 
\end{description}

The output is a vector \texttt{[L, At, P, bt]} where,
\texttt{L}, \texttt{At}, \texttt{P} and \texttt{bt},
are three matrices and a vector corresponding to $L$, $U$, $P$
and the transformed vector $\tilde{b}$, according to the algorithm.

%\renewcommand{\lstlistingname}{Code}
%\begin{figure}
%\begin{lstlisting}
\lstinputlisting[caption={$\mathrm{\emph{LUP}}$ Factorization},
label=lst:lup]{../matlab/gauss_pivotaje.m}
%\end{lstlisting}
%\caption{}
%\end{figure}


