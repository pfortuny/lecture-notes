% -*- TeX-master: "notas.ltx" -*-
\chapter{Differential Equations}
\label{cha:ode}
There is little doubt that the main application of numerical methods
is for \emph{integrating differential equations} (which is the
technical term for ``solving them numerically'').

\section{Introduction}
A differential equation is a special kind of equation: one
in which one of the unknowns is a function. We have already
studied some: any integral is a differential equation (it is the
simplest kind). For example,
\begin{equation*}
y^{\prime}=x
\end{equation*}
is an equation in which one seeks a function $y(x)$ whose derivative
is $x$. It is well-known that the solution is not unique: there is an
\emph{integration constant} and the general solution is written as
\begin{equation*}
y = \frac{x^2}{2} + C.
\end{equation*}

This integration constant can be better understood graphically. When
computing the primitive of a function, one is trying to find a
function whose derivative is known. A function can be thought of as a
graph on the $X,Y$ plane. The integration constant specifies at which
height the graph is. This does not change the derivative, obviously.
On the
other hand, if one is given the specific problem
\begin{equation*}
y^{\prime}=x, \,\,\, y(3) = 28,
\end{equation*}
then one is trying to find a function whose derivative is $x$,
\emph{with a condition at a point}: that its value at $3$ be
$28$. Once this value is fixed, \emph{there is only one graph having
  that shape and passing through} $(3,28)$. The condition $y(3)=28$
is called an
\emph{initial condition}.
One imposes that the graph of $f$ passes through a point and then
there is only one $f$ solving the integration problem, which means
there is only one suitable constant $C$. As a matter of fact, $C$ can
be computed by substitution: 
\begin{equation*}
28 = 3^2+C \,\,\,\Rightarrow\,\,\, C = 19.
\end{equation*}
The same idea gives rise to the term \emph{initial condition} for a
differential equation.

Consider the equation
\begin{equation*}
y^{\prime}=y
\end{equation*}
(whose general solution should be known). This equation means: find
a function $y(x)$ whose derivative is equal to the same function
$y(x)$ at every point.
One tends to
think of the solution as $y(x)=e^x$ but\dots is this the only possible
solution? A geometrical approach may be more useful; the equation
means ``the function $y(x)$ whose derivative is equal to the height
$y(x)$ at each point.'' From this point of view it seems obvious that
there must be more than one solution to the problem: at each point
one should be able to draw the corresponding tangent, move a little to
the right and do the same. There is nothing special on the points $(x,
e^x)$ for them to be the only solution to the problem. Certainly, the
general solution to the equation is
\begin{equation*}
y(x) = Ce^x,
\end{equation*}
where $C$ is an \emph{integration constant}. If one also specifies an
initial condition, say $y(x_0)=y_0$, then necessarily
\begin{equation*}
y_0=Ce^{x_0}
\end{equation*}
so that $C=y_0/e^{x_0}$ is the solution to the \emph{initial value
  problem} (notice that the denominator is not $0$).

This way, in order to find a \emph{unique} solution to a differential
equation, one needs to specify \emph{at least} one initial
condition. As a matter of fact, the number of these must be the same
as the order of the equation.

Consider
\begin{equation*}
y^{\prime\prime} = -y,
\end{equation*}
whose solutions should also be known: the functions of the form
$y(x)=a\sin(x) + b\cos(x)$, for two constants $a,b\in {\mathbb R}$. 
In order for the solution to be unique, \emph{two} initial conditions
must be specified. They are usually stated as the value of $y$ at some
point, and the value of the derivative at that same place. For example,
$y(0)=1$ and 
$y^{\prime}(0)=-1$. In this case, the solution is
$y(x)=-\sin(x)+\cos(x)$.

This chapter deals with \emph{approximate solutions} to differential
equations. Specifically, \emph{ordinary differential equations}
(i.e. with functions $y(x)$ of a single variable $x$).

\section{The Basics}
The first definition is that of differential equation
\begin{definition}
  An \emph{ordinary differential equation} is an equality $A=B$ in
  which the only unknown is a function of one variable
  whose derivative of 
  some order appears explicitly.
\end{definition}
The adjective \emph{ordinary} is the condition on the unknown of being a function of
a single variable (there are no partial derivatives).

\begin{example}
We have shown some examples above. Differential equations can get many
forms:
\begin{equation*}
\begin{split}
y^{\prime}=\sin(x)\\
xy=y^{\prime}-1\\
(y^{\prime})^{2}-2y^{\prime\prime}+x^{2}y=0\\
\frac{y^{\prime}}{y}-xy=\cos(y)
\end{split}
\end{equation*}
etc.
\end{example}

In this chapter, the unknown in the equation will always be denoted with
the letter $y$. The variable on which it depends will usually be either $x$
or $t$.

\begin{definition}
  A differential equation is of order $n$ if $n$ is the highest order
  derivative of $y$ appearing in it.
\end{definition}

The specific kind of equations we shall study in this chapter are
the \emph{solved ones} (which does not mean that they are already
solved, but that they are written in a specific way):
\begin{equation*}
y^{\prime}=f(x,y).
\end{equation*}

\begin{definition}
An \emph{initial value problem} is a differential equation together
with an initial condition of the form
$y(x_0)=y_0$, where $x_0,y_0\in {\mathbb R}$.
\end{definition}

\begin{definition}
The \emph{general solution} to a differential equation $E$ is a family
of functions $f(x,c)$, where $c$ is one (or several) constants such that:
\begin{itemize}
\item Any solution of $E$ has the form $f(x,c)$ for some $c$.
\item Any expression $f(x,c)$ is a solution of $E$,
\end{itemize}
except for possibly a finite number of values of $c$.
\end{definition}

If integrating functions of a real variable is already a complex
problem, the exact
integration a differential equation is, in general,
impossible. That is, the explicit computation of the symbolic solution
to a differential equation is a problem which is usually not
tackled. What  one seeks is to know an
approximate solution and a \emph{reasonably good} bound for the error
incurred when using that approximation instead of the ``true'' solution.

Notice that, in reality, most of the numbers appearing in the equation
describing a problem will already be \emph{inexact}, so trying to get
an ``exact'' solution is already a mistake.


\section{Discretization}
We shall assume a two-variable function $f(x,y)$ is
given, which is defined on a region $x\in[x_0, x_n]$,
$y\in [a,b]$, and which satisfies the following condition (which the
reader is encouraged to \emph{forget}):
\begin{definition}
  A function $f(x,y)$ defined on a set $X\in {\mathbb R}^2$ satisfies
  \emph{Lipschitz's condition} if there exists $K>0$ such that
\begin{equation*}
\abs{f(x_{1})-f(x_2)}\leq K \abs{x_1-x_2}
\end{equation*}
for any $x_1, x_2, \in X$, where $\abs{\,\,}$ denotes the absolute
value of a number.
\end{definition}
This is a kind of ``strong continuity condition'' (i.e. it is easier
for a function to be continuous than to be Lipschitz). What matters is
that this condition has a very important consequence for differential
equations: it guarantees the uniqueness of the solution. Let $X$ be a
set $[x_0,x_n]\times [a,b]$ (a strip, or a rectangle)
and $f(x,y):X\rightarrow {\mathbb R}$ a function on $X$ which
satisfies Lipschitz's condition. Then
\begin{theorem}[Cauchy-Kovalevsky]
  Under the conditions above, any differential equation
$y^{\prime}=f(x,y)$ with an initial condition
$y(x_0)=y_0$ for $y_0\in(a,b)$ has a unique solution
$y=y(x)$ defined on
$[x_0,x_0+t]$ for some $t\in {\mathbb R}$ greater than $0$.
\end{theorem}

Lipschitz's condition is not so strange. As a matter of fact,
polynomials and all the ``analytic functions'' (exponential,
logarithms, trigonometric functions, etc\dots) and their inverses
(where they are defined and continuous) satisfy it. An example which
does not is  $f(x)=\sqrt{x}$ on an interval containing $0$, because
$f$ at that point has a ``vertical'' tangent line. The reader should not
worry about this condition (only if he sees a derivative becoming
infinity or a point of discontinuity, but we shall not discuss them in
these notes). We give just an example:

\begin{example}[Bourbaki ``Functions of a Real Variable'',
  Ch. 4, \S 1]
  The differential equation $y^{\prime}=2\sqrt{\abs{y}}$ with initial
  condition $y(0)=0$ has an infinite number of solutions. For example,
  any of the following, for $a,b>0$:
\begin{enumerate}
\item $y(t)=0$ for any interval $(-b,a)$,
\item $y(t)=-(t+b)^2$ for $t\leq -b$,
\item $y(t)=(t-a)^2$ for $t\geq a$
\end{enumerate}
is a solution of that equation. This is because the function on the
right hand side, $\sqrt{\abs{y}}$, \emph{is not Lipschitz} near
$y=0$. (The reader is suggested to verify both assertions).
\end{example}

In summary, any ``normal'' initial value problem has a unique
solution. What is difficult is finding this.

And what about an approximation?

\subsection{The derivative as an arrow}
One is usually told that the derivative of a function of a real
variable is \emph{the slope} of its graph at the corresponding
point. However, a more useful idea for the present chapter
is to think of it as \emph{the $Y$
  coordinate of the velocity vector of the graph}.

When plotting a function, one should imagine that one is drawing a
curve with constant horizontal speed (because the $OX-$axis is
homogeneous, one goes from left to right at uniform speed). This way,
the graph of $f(x)$ is actually the plane curve $(x,f(x))$. Its
tangent vector at any point is $(1, f^{\prime}(x))$: the derivative
$f^{\prime}(x)$
of $f(x)$ is the vertical component of this vector.

From this point of view, a differential equation in solved form
$y^{\prime}=f(x,y)$
can be interpreted as the statement ``find a curve
$(x,y(x))$ such that the velocity vector at each point is
$(1,f(x,y))$.'' One can then draw the family of ``velocity'' vectors
on the plane $(x,y)$ given by $(1,f(x,y))$ (the function $f(x,y)$
is known, remember). This visualization, like in Figure \ref{fig:ecuadiff-example-1},
already gives an idea of the shape of the solution.

Given the arrows ---the vectors $(1,f(x,y))$--- on a plane, drawing a
curve whose tangents are those arrows should not be too hard. Even
more, if what one needs is just an approximation, instead of drawing a
curve, one could draw ``little segments going in the direction of the
arrows.'' If these segments have a very small $x-$coor\-di\-na\-te, one
reckons that an approximation to the solution will be obtained.
\begin{figure}[h]
  \centering
  \begin{tikzpicture}
\begin{axis}[
enlargelimits=false,
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={},
ytick={},
xticklabels={},
yticklabels={},
restrict x to domain=-0.1:0.1,
restrict y to domain=-0.105:0.105
]
\input{../datafiles/vector.dat}
%\addplot[only marks, mark=*] coordinates {(0,0) (1,1)};
\end{axis}
\end{tikzpicture}
  \caption{Arrows representing vectors of a solved differential
    equation $y^{\prime}=f(x,y)$. Notice how the horizontal component
    is constant while the vertical one is not. Each arrow corresponds
    to a vector $(1, f(x,y))$ where $f(x,y)\simeq x\cos(y)$.}
  \label{fig:ecuadiff-example-1}
\end{figure}

This is exactly Euler's idea.

Given a plane ``filled with arrows'' indicating the velocity vectors
at each point, the simplest idea for drawing the corresponding curve
that:
\begin{itemize}
\item Passes through a specified point (initial condition $y(x_0)=y_0$)
\item Whose $y-$velocity component is $f(x,y)$ at each point\dots
\end{itemize}
consists in \emph{discretizing} the
$x-$coordinates. Starting at $x_0$, one assumes that $OX$ is quantized
in intervals of width $h$ (constant, by now) ``small enough.'' Now,
instead of drawing a \emph{smooth curve}, one approximates it by small
steps (of width $h$) on the $x-$variable.

As the solution has to verify
$y(x_0)=y_0$, one needs only
\begin{itemize}
\item Draw the point $(x_0, y_0)$ (the initial condition).
\item Compute $f(x_0,y_0)$, the \emph{vertical} value of the velocity
  vector of the solution at the initial point.
\item As the $x-$coordinate is quantized, the \emph{next} point of the
  approximation will have $x-$coordinate equal to
  $x_0+h$.
\item As the velocity at $(x_0,y_0)$ is $(1, f(x_0,y_0))$, the
  simplest approximation to the \emph{displacement of the curve in an
    interval of width $h$ on the $x-$coordinate} is $(h,
  hf(x_0,y_0))$. Let $x_1=x_0+h$ and $y_1=y_0+hf(x_0,y_0)$.
\item Draw the segment from $(x_0,y_0)$ to $(x_1,y_1)$: this is the
  first ``approximate segment'' of the solution.
\item At this point one is in the same situation as at
  the beginning but
  with $(x_1, y_1)$ instead of $(x_0,y_0)$. Repeat.
\end{itemize}
And so on as many times as one desires. The above is \emph{Euler's}
algorithm for numerical integration of differential equations.

\section{Sources of error: truncation and rounding}
It is obvious that the solution to an equation
$y^{\prime}=f(x,y)$ will never (or practically so) be a line composed of
straight segments. When using Euler's method, there is an intrinsic
error which can be analyzed, for example, with Taylor's
formula. Assume $f(x,y)$ admits a sufficient number of partial
derivatives. Then
$y(x)$ will be differentiable also and
\begin{equation*}
y(x_0+h)=y(x_0)+hy^{\prime}(x_0)+\frac{h^2}{2}y^{\prime\prime}(\theta)
\end{equation*}
for some $\theta\in[x_0,x_0+h]$. What Euler's method does is to
\emph{remove the last term}, so that the error incurred is exactly
that: a term of order $2$ in $h$ in the first iteration. This error,
which arises from \emph{truncating} the Taylor expansion is called
\emph{truncation error} in this setting.

In general, one assumes that the interval along which the integration
is performed has width of magnitude $h^{-1}$. Usually, starting from
an interval $[a,b]$ one posits a number of subintervals $n$ (or
intermediate points, $n-1$) and takes $x_0=a$, $x_n=b$ and
$h=(b-a)/n$, so that going from $x_0$ to $x_n$ requires $n$ iterations
and the \emph{global truncation error} has order $h^{-1}O(h^2)\simeq
O(h)$.
% \footnote{This has to be used with care, because $(b-a)^{10}h$ is of order
% $O(h)$, but if $b\gg a$, then it is much greater than $h$. In all
% these problems, constants may be quite large and one has to be able to
% bound them as well.}

However, truncation is not the only source of error. Floating-point
operations incur always
\emph{rounding} errors.

\subsection{Details on the truncation and rounding errors.}
Specifically, if using IEEE-754, one considers
that the smallest \emph{significant} quantity (what is called, the
\emph{epsilon}) is $\epsilon=2^{-52}\simeq 2.2204\times
10^{-16}$. Thus, the rounding error in each operation is less than
$\epsilon$. If ``operation'' means a step in the Euler algorithm
(which may or may not be the case), then each step from $x_i$ to
$x_{i+1}$ incurs an error of at most $\epsilon$ and hence, the \emph{global
  rounding error} is bounded by $\epsilon/h$. This is very tricky
because it implies that \emph{the smaller the interval $h$, the larger
the rounding error incurred}, which is counterintutive. So,
\textbf{making $h$ smaller does not necessarily improve the accuracy of the
method!}

Summing up, the addition of the truncation and rounding errors can be
approximated as
\begin{equation*}
E(h) \simeq \frac{\epsilon}{h} + h,
\end{equation*}
which grows both when $h$ becomes larger and when it gets smaller. The
minimum $E(h)$ is given by 
$h\simeq \sqrt{\epsilon}$: which means that there is no point in using
intervals of width less than $\sqrt{\epsilon}$ in the Euler method (actually,
ever). Taking smaller intervals may perfectly lead to huge errors.

% For double precision, the optimal width is about $10^{-8}$: smaller
% intervals overload the processor and do not improve the precision
% significantly.

This explanation about truncation and rounding errors is relevant for
any method, not just Euler's. One needs to bound both for every method
and know how to choose the best $h$. There are even problems for which
a single $h$ is not useful and it has to be modified during the
execution of the algorithm. We shall not deal with these problems
here (the interested reader should look for the term ``stiff
differential equation'').

% \subsection{Anchura variable}
% Hasta ahora se ha supuesto que el intervalo $[a,b]$ (o, lo que es lo
% mismo, $[x_0,x_n]$) se divide en subintervalos de la misma anchura
% $h$. Esto no es necesario en general. Pero simplifica la exposici\'on
% lo suficiente como para que siempre supongamos que los intervalos son
% de anchura constante, salvo que se indique expl\'icitamente lo
% contrario (y esto es posible que no ocurra).

\section{Quadratures and Integration}
Assume that in the differential equation $y^{\prime}=f(x,y)$, the
function $f$ depends only on the variable $x$. Then, the equation
would be written
\begin{equation*}
y^{\prime}=f(x),
\end{equation*}
expression which means \emph{$y$ is the function whose derivative is
  $f(x)$}. That is, the problem is that of computing a primitive. We
have already dealt with it in Chapter
\ref{cha:derivation-and-integration} but it is inherently related to
what we are doing for differential equations.

When $f(x,y)$ depends also on $y$, the relation between the
equation and a primitive is harder to perceive but we know (by the
Fundamental Theorem of Calculus) that, if $y(x)$ is the solution to
the initial value problem $y^{\prime}=f(x,y)$ with $y(x_0)=y_0$, then,
integrating both sides, one gets
\begin{equation*}
y(x) = \int_{x_{0}}^x f(t, y(t))\,dt + y_0,
\end{equation*}
which is not a primitive but \emph{looks like it}. In this case, there
is no way to approximate the integral using the values of $f$ at
intermediate points \emph{because one does not know the value of
  $y(t)$}. But one can take a similar approach.

\section[Euler's Method]{Euler's Method: Integrate Using the
  Left Endpoint}
One can state Euler's method as in Algorithm
\ref{alg:euler-numerical-integration}. If one reads carefully, the
gist of each step is to approximate each value $y_{i+1}$ as the
previous one $y_i$ plus $f$ evaluated at $(x_i,y_i)$ times the
interval width. That is:
\begin{equation*}
\int_{x_i}^{x_{i+1}}f(t,y(t))\,dt \simeq (x_{i+1}-x_i)f(x_i,y_i) = h f(x_i,y_i).
\end{equation*}
If $f(x,y)$ were independent of $y$, then one would be performing the
following approximation (for an interval $[a,b]$):
\begin{equation*}
\int_a^bf(t)\,dt = (b-a)f(a),
\end{equation*}
which, for lack of a better name, could be called \emph{the
  left endpoint rule}: the integral is approximated by the area of the
rectangle of height $f(a)$ and width $(b-a)$.

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} A function $f(x,y)$, an initial condition
 $(x_0,y_0)$, an interval $[a,b]=[x_0,x_n]$ and a step $h=(x_n-x_0)/n$
\STATE \textbf{Output: } A family of values $y_0,y_1, \dots,
y_n$ (which approximate the solution to
$y^{\prime}=f(x,y)$ on the net $x_0, \dots, x_n$)
\PART{Start}
\STATE $i\leftarrow 0$
\WHILE{$i\leq n$}
\STATE $y_{i+1}\leftarrow y_i+hf(x_i,y_i)$
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $(y_0,\dots, y_n)$
\end{algorithmic}
\caption{Euler's algorithm assuming exact arithmetic.}
\label{alg:euler-numerical-integration}
\end{algorithm}

One might try (as an exercise) to solve the problem ``using the right
endpoint'': this gives rise to what are called the \emph{implicit}
methods which we shall not study (but which usually perform better
than the \emph{explicit} ones we are going to explain).

%Como se ha visto arriba, el m\'etodo de Euler tiene un error de
%$O(h)$, que, cuanto m\'as grande sea el intervalo, mayor se hace y
%puede f\'acilmente llegar a ser \emph{macrosc\'opico}.

\section{Modified Euler: the Midpoint Rule}
Instead of using the left endpoint of 
$[x_i,x_{i+1}]$ for integrating and computing $y_{i+1}$, one might use
(and this would be better, as the reader should verify) the midpoint
rule somehow. As there is no way to know the intermediate
values of $y(t)$ further than $x_0$,
some kind of guesswork has to be done. The
method goes as follows:

\begin{itemize}
\item Use a point near  $P_i = (x_i,y_i)$ whose $x-$coordinate is the
  midpoint of $[x_i,x_{i+1}]$.
\item For lack of a better point, the first approximation is done
  using Euler's algorithm and one takes a point
  $Q_i = [x_{i+1}, y_i+hf(x_i,y_i)]$.
\item Compute the midpoint of the segment $\overline{P_iQ_i}$, which is
  $(x_i+h/2, y_i+h/2f(x_i,y_i))$. Let $k$ be its $y-$coordinate.
\item Use the value of $f$ at that point in order to compute
  $y_{i+1}$: this gives the formula $y_{i+1}=y_i+hf(x_i+h/2,k)$.
\end{itemize}

% Como se explic\'o en el Cap\'itulo \ref{cha:derivacion-e-integracion},
% es m\'as preciso aproximar la derivada en un punto por la diferencia
% sim\'etrica que por la diferencia incremental directa. Lo que hace el
% paso descrito arriba es tomar en $(x_i,y_i)$ como valor de la derivada
% el valor en el punto medio aproximado por la diferencia sim\'etrica
% (aunque no lo parezca).
If, as above, $f(x,y)$ did not depend on $y$, one verifies easily that
the corresponding approximation for the integral is
\begin{equation*}
\int_a^bf(x)\,dx \simeq (b-a)f(\frac{a+b}{2}),
\end{equation*}
which is exactly the \emph{midpoint rule} of numerical integration.

This method is called \emph{modified Euler's method} and its
\emph{order} is $2$, the same as Euler's,
which implies that the accrued error at $x_n$ is
$O(h)$. It is described formally in Algorithm
\ref{alg:modified-euler}.


\begin{figure}[h!]
  \centering
  \begin{tikzpicture}
\begin{axis}[
%enlargelimits=false,
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={0, 0.5, 1},
ytick={0, 0.25, 0.3, 0.55, 0.6},
xticklabels={$x_i$, $x_i+\frac{h}{2}$, $x_{i+1}$},
yticklabels={$y_i$, \small{$y_{i+1}$}, \small{$z^2$},\small{$z_2+k^2$},\small{$y_i+k^1$}},
restrict x to domain=-0.1:1.6,
restrict y to domain=-0.1:0.7
]
%\input{../datafiles/vector.dat}
\addplot[only marks, mark=*] coordinates {(0,0)};
\addplot[only marks, mark=o, line width=1.5pt] coordinates {(0.5, 0.3)};
\addplot[blue, ->, line width=1.5pt, dashed] coordinates {(0,0) (1,0.6)};
\addplot[red, ->, dotted, line width=1.5pt] coordinates {(0.5, 0.3) (1.5, 0.55)};
\addplot[gray, loosely dashed] coordinates {(1.5, 0.55) (1, 0.25)}; 
\addplot[red, ->, dotted, line width=1.5pt] coordinates {(0,0) (1, 0.25)};
\addplot[brown, only marks, mark=*] coordinates {(1,0.25)};
\addplot[gray, dotted] coordinates {(0.5, -0.1) (0.5, 0.3) (-0.1, 0.3)};
\addplot[gray, dotted] coordinates {(1, -0.1) (1, 0.25) (-0.1, 0.25)};
%\addplot[only marks, black, mark=.] coordinates {(1.7, 0.7) };
\end{axis}
\end{tikzpicture}
  \caption{Modified Euler's Algorithm. Instead of using the vector
    $(h,hf(x_i,y_i))$ (dashed), one sums at $(x_i,y_i)$ the dotted vector,
    which is the tangent vector at the midpoint.}
  \label{fig:modified-euler}
\end{figure}

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} A function $f(x,y)$, an initial condition
 $(x_0,y_0)$, an interval $[x_0,x_n]$ and a step $h=(x_n-x_0)/n$
\STATE \textbf{Output: } A family of values $y_0,y_1, \dots,
y_n$ (which approximate the solution of
$y^{\prime}=f(x,y)$ on the net $x_0, \dots, x_n$)
\PART{Start}
\STATE $i\leftarrow 0$
\WHILE{$i\leq n$}
\STATE $k_1 \leftarrow f(x_i,y_i)$
\STATE $z_2\leftarrow y_i+\frac{h}{2}k_1$
\STATE $k_2\leftarrow f(x_i+\frac{h}{2}, z_2)$
\STATE $y_{i+1}\leftarrow y_i+h k_2$
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $(y_0,\dots, y_n)$
\end{algorithmic}
\caption{Modified Euler's algorithm, assuming exact arithmetic.}
\label{alg:modified-euler}
\end{algorithm}

As shown in Figure \ref{fig:modified-euler}, this method consist in
first using Euler's in order to get an initial guess,
then computing the
value of $f(x,y)$ at the midpoint between $(x_0,y_0)$ and the guess
and using this
value of $f$ as the approximate slope of the solution at $(x_0,y_0)$.
There is still an error but,
as the information is gathered ``a bit to the right'', it is less than
the one of Euler's method, analogue to the trapezoidal rule.

The next idea is, instead of using a single vector, compute two of
them and use a ``mean value:'' as a matter of fact, the mean between
the vector at the origin and the vector at the end of Euler's method.

\section{Heun's Method: the Trapezoidal Rule}
Instead of using the midpoint of the segment of Euler's method, one
can take the vector corresponding to Euler's method and also
the vector at the endpoint of Euler's method
and compute the mean of both vectors and use this mean for
approximating. This way, one is using the information at the point and
some information ``later on.'' This \emph{improves} Euler's method and
is called accordingly \emph{improved Euler's method} or \emph{Heun's
  method}. It is described in Algorithm \ref{alg:heun}.

At each step one has to perform the following operations:
\begin{itemize}
\item Compute $k_1=f(x_i,y_i)$.
\item Compute $z_2=y_j+hk_1$. This is the coordinate
  $y_{i+1}$ in Euler's method.
\item Compute $k_2=f(x_{i+1}, z_2)$. This would be the slope at
  $(x_{i+1},y_{i+1})$ with Euler's method.
\item Compute the mean of $k_1$ and $k_2$: $\frac{k_1+k_2}{2}$ and use
  this value as ``slope''. That is, set
  $y_{i+1}=y_i+\frac{h}{2}(k_1+k_2)$.
\end{itemize}

Figure \ref{fig:heun} shows a graphical representation of Heun's method. 

\begin{figure}[h]
  \centering
  \begin{tikzpicture}
\begin{axis}[
enlargelimits=false,
legend style={at={(0.05,0.95)},
anchor = north west},
xtick={0,1,2},
ytick={0,0.1,0.25,0.4,0.5},
xticklabels={$x_i$, $x_{i+1}$, $x_{i+1}+h$},
yticklabels={$y_i$, \small{$y_{i}+hk_2$}, \small{$y_{i+1}$},\small{$y_i+hk_1$},},
restrict x to domain=-0.1:2.1,
restrict y to domain=-0.05:0.55
]
%\input{../datafiles/vector.dat}
\addplot[only marks, mark=*] coordinates {(0,0)};
\addplot[blue, ->, dashed, line width=1.5pt] coordinates {(0,0) (1,0.4)};
\addplot[red, ->, dotted, line width=1.5pt] coordinates {(1, 0.4) (2, 0.5)};
\addplot[gray, loosely dashed, line width=1pt] coordinates {(2, 0.5) (1, 0.1)};
\addplot[red, ->, dotted, line width=1.5pt] coordinates {(0,0) (1, 0.1)};
\addplot[only marks, mark=*, olive] coordinates {(1, 0.25)};
\addplot[brown, ->, line width=1.5pt] coordinates {(0,0) (1, 0.25)};
\addplot[gray, dotted] coordinates {(1, -0.05) (1, 0.4)};
\addplot[gray, dotted] coordinates {(1,0.1) (-0.1, 0.1)};
\addplot[gray, dotted] coordinates {(1,0.25) (-0.1, 0.25)};
\addplot[gray, dotted] coordinates {(1, 0.4) (-0.1, 0.4)}; 
\addplot[only marks, black, mark=.] coordinates {(2.1, 0.55) };
\end{axis}
\end{tikzpicture}
  \caption{Heun's method. At $(x_i,y_i)$ one uses the mean
    displacement (solid) between the vectors at 
    $(x_i,y_i)$ (dashed) and at the next point $(x_{i+1}, y_i+hk_1)$
    in Euler's method (dotted).}
  \label{fig:heun}
\end{figure}



\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} A function $f(x,y)$, an initial condition
 $(x_0,y_0)$, an interval $[x_0,x_n]$ and a step $h=(x_n-x_0)/n$
\STATE \textbf{Output: } A family of points $y_0,y_1, \dots,
y_n$( which approximate the solution to
$y^{\prime}=f(x,y)$ on the net $x_0, \dots, x_n$)
\PART{Start}
\STATE $i\leftarrow 0$
\WHILE{$i\leq n$}
\STATE $k_1 \leftarrow f(x_i,y_i)$
\STATE $z_2\leftarrow y_i+hk_1$
\STATE $k_2\leftarrow f(x_{i}+h, z_2)$
\STATE $y_{i+1}\leftarrow y_i+\frac{h}{2} (k_1+k_2)$
\STATE $i\leftarrow i+1$
\ENDWHILE
\STATE \textbf{return} $(y_0,\dots, y_n)$
\end{algorithmic}
\caption{Heun's algorithm assuming exact arithmetic.}
\label{alg:heun}
\end{algorithm}

Figures \ref{fig:comparison-ode-1} and \ref{fig:comparison-ode-2} show
the approximate solutions to the equations $y^{\prime}=y$ and
$y^{\prime}=-y+\cos(x)$, respectively, using the methods explained in
the text, together with the exact solution. 



\begin{figure}[h]
  \centering
  \begin{tikzpicture}
\begin{axis}[
enlargelimits=false,
legend style={at={(0.05,0.95)},
  anchor = north west},
legend entries={Euler, Modified, Solution},
]
% \input{../datafiles/vector.dat}
\addplot[blue, dashed, mark=*, line width=1pt] file {../datafiles/ode1-euler.dat};
\addplot[black, mark=o, line width=1pt] file {../datafiles/ode1-modified.dat};
\addplot[red, line width=1pt] file {../datafiles/ode1-sol.dat};
\end{axis}
\end{tikzpicture}
\caption{Comparison between Euler's and Modified Euler's methods
  and the true solution to the ODE $y^{\prime}=y$ for $y(-2)=10^{-1}$.}
  \label{fig:comparison-ode-1}
\end{figure}


\begin{figure}[h]
  \centering
  \begin{tikzpicture}
\begin{axis}[
enlargelimits=false,
legend style={at={(0.55,0.75)},
  anchor = south west},
legend entries={Euler, Modified, Heun, Solution},
]
% \input{../datafiles/vector.dat}
\addplot[blue, dashed, mark=*, line width=1pt] file {../datafiles/ode2-euler.dat};
\addplot[black, mark=*, dashed, line width=1pt] file {../datafiles/ode2-modified.dat};
\addplot[black, mark=o, line width=1pt] file {../datafiles/ode2-heun.dat};
\addplot[red, line width=1pt] file {../datafiles/ode2-sol.dat};
\end{axis}
\end{tikzpicture}
\caption{Comparison between Euler's, Modified Euler's and Heun's methods
  and the true solution to the ODE $y^{\prime}=-y+\cos(x)$ for $y(0)=-1$.}
  \label{fig:comparison-ode-2}
\end{figure}

