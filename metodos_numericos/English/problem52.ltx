% -*- TeX-master: "problem52.ltx" -*-
\documentclass[a4paper, 12pt]{amsart}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{color}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{ %
  language=Matlab,                % the language of the code
  basicstyle=\small\ttfamily,           % the size of the fonts that are used for the code
%  numbers=left,                   % where to put the line-numbers
%  numberstyle=\tiny\color{gray},  % the style that is used for the line-numbers
%  stepnumber=10,                   % the step between two line-numbers. If it's 1, each line 
                                  % will be numbered
%  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{white},      % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  %frame=single,                   % adds a frame around the code
  rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. commens (green here))
  tabsize=4,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
%  title=\lstname,                   % show the filename of files included with \lstinputlisting;
                                  % also try caption instead of title
  keywordstyle=\color{blue},          % keyword style
  commentstyle=\color{dkgreen},       % comment style
  stringstyle=\color{mauve},         % string literal style
  comment=[l]\%,
  morecomment=[s]{\%\{}{\%\}},
%  escapeinside={\%*}{*)},            % if you want to add a comment within your code
  morekeywords={*,...}               % if you want to add more keywords to the set
}


\title{Problem 52}
\author{P. Fortuny Ayuso}
\begin{document}
\maketitle

This is a remarkable problem because it shows, once again, that \emph{most
of real life problems are not linear}. In this case, if $f(x)$ is
the least squares 
approximation to a function of the form
\begin{equation*}
  H(x) = a\,e^{bx^2},
\end{equation*}
can be totally different from the function $l(x)$ obtained ``linearizing the
problem'' using logarithms.

The statement goes as follows:

\textbf{Exercise:\/}
  Consider the following data list:
\begin{equation*}
  \begin{array}{r|l}
  x & y\\
\hline
 -2 & 0{.}0338\\
 -1{.}5 & 0{.}0397\\
 -1 & 0{.}4119\\
 -0{.}5 & 1{.}862\\
 0 & 3{.}013\\
 0{.}5 & 1{.}856\\
 1 & 0{.}4240\\
 1{.}5 & 0{.}0485\\
 2 & 0{.}0249\\
\end{array}
\end{equation*}
And perform the following:
\begin{itemize}
\item Compute the table $(x,\log(y))$ ($\log$ is \emph{always} the
  natural logarithm).
\item Compute the linear interpolation of the new table using the
  functions $1$ y $x^2$. Let $g(x)$ be the interpolation function.
\item Let $f(x)$ be the function $e^{g(x)}$, that is,
  $e^ae^{bx^2}$, where $a$ and $b$ are the previous ones. Compute the
  total quadratic error of $f(x)$ for the \emph{original} list.
\item Compute the total quadratic error of
   $h(x)=3e^{-2x^2}$ for the original list.
\item Compare and comment the two last results.
\end{itemize} 

\textbf{Long explanation using \texttt{Octave/Matlab}.} First, we need
to write the data cloud as a matrix in Matlab:
\begin{lstlisting}[language=Matlab]
  > data=[-2 0.0338; -1.5 0.0397; -1 0.4119; -0.5 1.862; 0 3.013; 0.5 1.856; 1 0.4240; 1.5 0.0485; 2 0.0249];
\end{lstlisting}

Taking logarithms of the second column should be easy:
\begin{lstlisting}
  > logdata = [data(:,1) log(data(:,2))];
\end{lstlisting}

In order to compute the interpolation of the table \texttt{logdata},
we use the results from the Theory classes. We need the matrix
\begin{equation*}
  X =
  \begin{pmatrix}
    f_1(x_1) & \dots & f_1 (x_N)\\
    f_2(x_1) & \dots & f_2 (x_N)
  \end{pmatrix}
\end{equation*}
where, in this case, $f_1(x) = 1$ and $f_2(x)=x^2$, as in the
statement. We first define these functions:
\begin{lstlisting}
  > f1 = @(t) 1 + 0.*t;
  > f2 = @(t) t.^2;
\end{lstlisting}
Notice how \emph{the constant function has to be defined with an
  artifact to make it vectorial}. The $X$ matrix is now easily
defined:
\begin{lstlisting}
  > X = [f1(logdata(:,1)'); f2(logdata(:,1)')];
\end{lstlisting}
Both the coefficient matrix and the independent terms are computed
from $X$ and the $(y_1, \dots, y_n)^t$ vector:
\begin{lstlisting}
  > A = X * X';
  > Y = X * logdata(:,2);
\end{lstlisting}
Solving linear systems in Octave/Matlab is quite easy:
\begin{lstlisting}
  > sol = A\Y
  sol =

   0.54169
  -1.17400
\end{lstlisting}
Which means that the solution to this problem (the one with $\log(y)$)
is $h(x) =
0{.}54169 - 1{.}17400 x^{2}$. Hence, computing the inverse of the
logarithm, one would obtain a ``solution'' to the initial problem:
\begin{equation*}
  l(x) = e^{h(x)} = e^{0{.}54169}e^{-1{.}17400x^{2}},
\end{equation*}
which \emph{might} be near to the true solution or not. Let us compute
the total quadratic error of $l(x)$ for the original cloud of points:
\begin{lstlisting}
  > l = @(t) exp(0.54169) .* exp(-1.17400.*t.^2);
  > l_err = sum((l(data(:,1))' - data(:,2)').^2)
  l_err =  2.3798
\end{lstlisting}

\textbf{However:} we are going to verify that there are functions
which interpolate the cloud with a much less quadratic error than
that. Specifically,
\begin{lstlisting}
  > f = @(t) 3.*exp(-2.*t.^2);
  > f_err = sum((f(data(:,1))' - data(:,2)').^2)
  f_err =  0.0055687
\end{lstlisting}
which is \emph{much less} than \texttt{l\_err}.

\textbf{What is the problem:} we must take a close look at the graphs
of the cloud of points and the two functions we have computed in order
to understand what is happening:
\begin{lstlisting}
  > plot(data(:,1)', data(:,2)', '*r');
  > hold on
  > u = linspace(-2, 2, 1000);
  > plot(u, l(u));
  > plot(u, f(u), 'g');
\end{lstlisting}
\begin{figure}
  \centering
  \includegraphics[width=0.8\textwidth]{log-least-sq-fig.eps}
  \caption{Comparison between the true solution to a non-linear least squares problem (green) and the solution obtained linearizing the problem.}
  \label{fig:two-solutions}
\end{figure}

The blue plot corresponds to the ``solution from the logarithm'',
while the green one corresponds to the function $3e^{-2x^{2}}$. It is
obvious that the green graph \emph{matches better} the cloud of
points. Why is the blue line so \emph{bad}? Because the linearization
taking logarithms of the $y-$values solves the problem for the
following matrix:
\begin{equation*}
  \begin{array}{r|l}
    x & y\\
    \hline
  -2.0 & -3.38729\\
  -1.5 & -3.22640\\
  -1.0 & -0.88697\\
  -0.5 &  0.62165\\
   0.0 &  1.10294\\
   0.5 &  0.61842\\
   1.0 & -0.85802\\
   1.5 & -3.02619\\
   2.0 & -3.69289
  \end{array}
\end{equation*}
Notice how the values which correspond to $y$ near $0$ in the original
cloud have become \emph{large} values, which turns them into
\emph{very important values} for the least squares interpolation. In
the original problem, those values, which are very near zero and arise
for $x$ large (in absolute value) are almost irrelevant (all the
functions of the form $ae^{-bx^2}$ have small values for large $x$.
If one looks at the first graph near $x=-2$, one notices that the bad
function interpolates those points very well, but performs quite badly
for the rest of the points. However, the other function, the
\emph{good one}, is a much better fit for all the points, despite
being \emph{a bit worse} at the endpoints.

Hence: the least squares interpolation of the linearized problem
\emph{may be quite far away} from the linearized of the least squares
interpolation. \emph{Caveat emptor}.
\end{document}
