% -*- TeX-master: "notas.ltx" -*-
\chapter{Arithmetic and error analysis}\label{cha:arithmetic}
Exponential notations, double precision floating point, the notion of
error and some common sources of error in computations are summarily
reviewed. The student should at least get a feeling of the importance
of \emph{normalization} of floating point arithmetic and that mistakes in its use
can be critical.
\section{Exponential notation}
Real numbers can have a finite or infinite number of digits. When
working with them they have to be approximated using a finite (and
usually small) number of digits. Even more, they should be expressed
in a standard way so that their magnitude and significant digits are
easily noticed at first sight and so that sharing them among machines
is a deterministic and efficient process. These requirements have led
to what is known as \emph{scientific} or \emph{exponential}
notation. We shall explain it roughly; what we intend is to transmit the
underlying idea more than to detail all the rules (they have lots of
particularities and exceptions). We shall use, along these notes, the
following expressions:
\begin{definition}
  An \emph{exponential notation} of a real number is an expression of
  the form
  \begin{equation*}
    \begin{split}
      & \pm A.B\times 10^{C}\\
      & 10^{C}\times \pm A.B\\
      & \pm A.B\mathbf{e}C
    \end{split}
  \end{equation*}
  where $A,B$ and $C$ are natural numbers (possibly $0$), 
  $\pm$ is a sign (which may be elided if $+$). Any of those
  expressions refers to the real number $(A+0{.}B)\cdot 10^{C}$ (where
  $0{.}B$ is ``nought dot B\dots'').
\end{definition}
For example:
\begin{itemize}
\item The number $3{.}123$ is the same $3{.}123$.
\item The number $0{.}01e-7$ is $0{.}000000001$ (eight zeroes after
  the dot and before the $1$).
\item The number $10^{3}\times -2{.}3$ is $-2300$.
\item The number $-23{.}783e-1$ is $-2{.}3783$.
\end{itemize}
In general, scientific notation assumes the number to the left of the
decimal point is a single non-zero digit:
\begin{definition}
  The \emph{standard exponential notation} is the exponential notation
  in which $A$ is between $1$ and $9$.
\end{definition}

Finally, machines usually store real numbers in a very specific way called
\emph{floating point}.
\begin{definition}
  A \emph{floating point format} is a specification of an exponential
  notation in which the length of $A$ plus the length of $B$ is
  constant and in which the exponents (the number $C$) vary in a
  specific range.
\end{definition}
Thus, a floating point format can only express a finite amount of
numbers (those which can be written following the specifications of
the format).

The blueprint for the floating point standard for microchips (and for
electronic devices in general) is document IEEE-754 (read ``I-E-cube
seven-five-four''). The acronym stands for ``Institute of Electrical
and Electronic 
Engineers''. The last version of the document dates from 2008.

\subsection{The binary double precision format of IEEE-754}
The double precision format is the IEEE specification for representing
real numbers in sequences of $16$, $32$, $64$, $128$ bits (and more
cases) and their decimal representation. The main properties of binary
double precision with $64$ bits are roughly explained in this section.

In order to write numbers in double precision, $64$ bits are used,
that is, $64$ \emph{binary digits}. The first one stands for the sign
(a $0$ means $+$, a $1$ means $-$). The $11$ following ones are used
for the exponent (as will be shown) and the remaining $52$ are used
for what is called the \emph{mantissa}. Thus, a double precision
number has three parts: $s$, the sign, which can be $0$ or $1$; $e$,
the exponent, which varies between $0$ and $2^{11}-1=2047$; and $m$, a
$52$-bit number. Given three values for $s$, $e$ and $m$, the real
number represented by them is:
\begin{itemize}
\item If $e\neq 0$ and $e\neq 2047$ (i.e. if the exponent is not one
  of the end values), then
  \begin{equation*}
    N=(-1)^{s}\times 2^{e-1023}\times 1{.}m,
  \end{equation*}
  where $1{.}m$ means ``one-dot-m'' in binary. Notice ---and this is
  the key--- that the exponent \emph{is not the number represented by
    the $11$ bits of $e$} but that it is ``shifted to the right''. The
  exponent $e=01010101011$, which in decimal is $683$ represents, in
  double precision format, the number $2^{683-1023}=2^{-340}$. Those
  $e$ with a starting $0$ bit correspond to negative powers of $2$ and
  those having a starting $1$ bit to positive powers (recall that
  $2^{10}=1024$).
\item If $e=0$ then, if $m\neq 0$ (if there is a mantissa):
  \begin{equation*}
    N = (-1)^{s}\times 2^{-1023}\times 0{.}m,
  \end{equation*}
  where $0{.}m$ means ``zero-dot-m'' in binary.
\item If $e=0$ and $m=0$, the number is either $+0$ or $-0$, depending
  on the sign $s$. This means that $0$ has a sign in double precision
  arithmetic (which makes sense for technical reasons).
\item The case $e=2047$ (when all the eleven digits of $e$ are $1$) is
  reserved for encoding ``exceptions'' like $\pm\infty$ and $NaN$
  (not-a-number-s). We shall not enter into details.
\end{itemize}
As a matter of fact, the standard is much longer and thorough and
includes a long list of requisites for electronic devices working in
floating point (for example, it specifies how truncations of the main
mathematical operations have to be performed to ensure exactness of the
results whenever possible).

The main advantage of floating point (and of double precision
specifically) is, apart from standardization, that it enables
computing with very small and very large numbers with a single
format (the smallest storable number is $2^{-1023}\simeq 10^{-300}$
and the largest one is $2^{1023}\simeq 10^{300}$). The trade off is
that if one works \emph{simultaneously} with both types of quantities,
the first lose precision and tend to disappear (a \emph{truncation
  error} takes place). However, used \emph{carefully}, it is a hugely
useful and versatile format.

\subsection{Binary to decimal (and back) conversion}
\margin{Examples?}
In this course, it is \emph{essential} to be able to convert a number
from binary to decimal representation and back.

In these notes, we shall use the following notation: the expression
$x\leftarrow a$ means that $x$ is a variable, $a$ is a value (so that
it can be a number or another variable) and that $x$ gets assigned the
value of $a$. The expression $u=c$ is the \emph{conditional} statement
``the value designed by $u$ is the same as that designed by $c$'',
which can be either true or false. We also denote $m // n$ as the
quotient of dividing the natural number $m>0$ by the natural number
$n>0$, and $m \% n$ is the \emph{remainder} of that division. That is,
\begin{equation*}
  m = (m//n) \times n + (m\%n).
\end{equation*}
Finally, if $x$ is a real number $x=A{.}B$, the expression
$\left\{x\right\}$ means the \emph{fractional part of $x$}, i.e.,
the number $0{.}B$.

\begin{example}
  The following table shows the decimal and binary expansions of several
  numbers. The last binary expansion is approximate because it has an
  infinite number of figures.
  \begin{equation*}
    \begin{array}{rr}
      \mathrm{\emph{Decimal}} & \mathrm{\emph{Binary}}\\
      1 & 1\\
      2 & 10\\
      10 & 1010\\
      0{.}5 & 0{.}1\\
      7{.}25 & 111{.}01\\
      0{.1} & 0{.}000110011+
    \end{array}
  \end{equation*}
\end{example}

Algorithm \ref{alg:dec-to-bin} (in page \pageref{alg:dec-to-bin})
is a way to convert a decimal number
$A{.}B$ with a finite number of digits to its binary form. Algorithm
\ref{alg:bin-to-dec} performs the reverse conversion. Notice that, as
there are numbers with a finite quantity of decimal digits which
cannot be expressed with a finite quantity of binary digits (the most
obvious example being $0{.}1$), one has to specify a number of fractional
digits for the output (which implies that one does not necessarily
obtain the very same number but a truncation).

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} $A{.}B$ a number in base $10$, with $B$ of
    finite length, $k$
    a positive integer (which is the desired number of fractional
    digits after the binary dot)
    \STATE \textbf{Output:} $a.b$ (the number $x$ in binary format,
    truncated to $2^{-k}$)
    \IF{$A=0$}
    \STATE $a\leftarrow 0$ and
    go to the {\sc Fractional Part}
    \ENDIF
    \PART{Integral part}
\STATE $i \leftarrow -1$, $n\leftarrow A$
\WHILE{$n>0$}
\STATE{$i\leftarrow i+1$}
\STATE{$x_{i} \leftarrow n \% 2$} \comm{remainder}
\STATE{$n\leftarrow n//2$} \comm{quotient}
\ENDWHILE
\STATE{$a \leftarrow x_{i}x_{i-1}\dots x_{0}$} \comm{the sequence of
  remainders in reverse order}
\PART{Fractional part}
\IF {$B=0$} 
\STATE $b\leftarrow 0$
\STATE \textbf{return} $a{.}b$
\ENDIF
\STATE $i \leftarrow 0$,  $n\leftarrow 0{.}B$
\WHILE{$n>0$ \textbf{and} $i<k$}
\STATE{$i\leftarrow i+1$}
\STATE{$m \leftarrow 2n$}
\IF{$m\geq 1$}
\STATE{$b_{i} \leftarrow 1$}
\ELSE
\STATE{$b_{i} \leftarrow 0$}
\ENDIF
\STATE $n\leftarrow \left\{m\right\}$ \comm{the fractional part of $m$}
\ENDWHILE
\STATE $b\leftarrow b_{1}b_{2}\dots b_{i}$
\STATE \textbf{return} a.b
\end{algorithmic}
  \caption{Conversion from decimal to binary, without sign.}
  \label{alg:dec-to-bin}
\end{algorithm}

Converting from binary to decimal is ``simpler'' but requires
additions on the go: one does not obtain a digit at each step because
one has to add powers of two. This process is described in Algorithm
\ref{alg:bin-to-dec}. Notice that the number of digits of the output
is not specified (it could be done but it would only make the
algorithm more complex). Finally, in all those steps in which
$A_{i}\times 2^{i}$ or $B_{i}\times 2^{-i}$ is added, both $A_{i}$
and $B_{i}$ are either $0$ or $1$, so that this multiplication just
means ``either add or do not add'' the corresponding power of $2$
(this is what a binary digit means, anyway).

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} $A{.}B$, a number in binary format, $k$
    a non-negative integer (the number of the fractional binary digits
    to be used).
    \STATE \textbf{Output:} $a{.}b$, the decimal expression of the
    truncation up to precision 
    $2^{-k}$ of the number $A{.}B$  
    \PART{Integral part}
    \STATE Write $A=A_{r}A_{r-1}\dots A_{0}$ (the binary digits)
    \STATE $a\leftarrow 0$, $i\leftarrow 0$
    \WHILE{$i\leq r$}
    \STATE $a\leftarrow a+A_{i}\times 2^{i}$
    \STATE $i\leftarrow i+1$
    \ENDWHILE
    \PART{Fractional part}
    \IF{$B=0$} 
    \STATE \textbf{return} $a{.}0$
    \ENDIF
    \STATE $b \leftarrow 0$, $i\leftarrow 0$
    \WHILE{$i\leq k$}
    \STATE $i\leftarrow i+1$
    \STATE $b\leftarrow b + B_{i}\times 2^{-i}$
    \ENDWHILE
    \STATE \textbf{return} $a{.}b$
  \end{algorithmic}
  \caption{Conversion from binary to decimal, without sign.}
  \label{alg:bin-to-dec}
\end{algorithm}

\section{Error, basic definitions}
Whenever numbers with a finite quantity of digits are used in
computations and whenever real measurements are performed, one has to
acknowledge that errors will be made. This is not grave by
itself. What matters is \emph{having an estimate of their size} and
knowing that, as calculations are made, they can propagate. In the
end, the best one can do is \emph{to bound the absolute error}, that is,
to know a \emph{reasonable} value (the bound) which is larger than the error, in order
to assess, with certainty, how far the real value can be from the
computed one.

In what follows, an exact value $x$ is assumed (a constant, a datum,
the exact solution to a problem\dots) and an approximation will be
denoted $\tilde{x}$.

\begin{definition}
  The \emph{absolute error} incurred when using $\tilde{x}$ instead of
  $x$ is the absolute value of their difference $\abs{x-\tilde{x}}$.
\end{definition}

But, unless $x$ is $0$, one is usually more interested in the
\emph{order of magnitude} of the error; that is, ``how \emph{relatively}
large the error is'' as compared to the real quantity.

\begin{definition}
  The \emph{relative error} incurred when using $\tilde{x}$ instead of
  $x$, assuming $x\neq 0$, is the quotient
  \begin{equation*}
    \frac{\abs{\tilde{x}-x}}{\abs{x}}
  \end{equation*}
  (which is always positive).
\end{definition}

We are not going to use a specific notation for them (some people use
$\Delta$ and $\delta$, respectively).
\newcommand{\tpi}{\tilde{\pi}}
\begin{example}
  The constant $\pi$, which is the ratio between the length of the
  circumference and its diameter, is, approximately
  $3{.}1415926534+$ (the trailing $+$ indicates that the real number
  is larger than the representation). Assume one uses the
  approximation $\tpi=3{.14}$. Then
  \begin{itemize}
  \item The absolute error is $\abs{\pi - \tpi} = 0{.}0015926534+$.
  \item The relative error is $\frac{\abs{\pi - \tpi}}{\pi}\simeq
    10^{-4}\times5{.}069573$.
  \end{itemize}
  This last statement means that one is incurring an error of $5$
  ten-thousandths per unit (approx. $1/2000$) each time $3{.}14$ is
  used for $\pi$. Thus, if one adds $3{.}14$ two thousand times, the
  error incurred when using this quantity instead of
  $2000\times\pi$ will be approximately $\pi$. This is the meaning of
  the relative error: its reciprocal is the number of times one has to
  add $\tilde{x}$ in order to get an accrued error equal to the number
  being approximated.
\end{example}

Before proceeding with the analysis of errors, let us define the two
most common ways of approximating numbers using a finite quantity of
digits: \emph{truncation} and \emph{rounding}. The precise definitions
are too detailed for us to give.
Start with a real number (with possibly an infinite quantity of
digits): 
\begin{equation*}
  x = a_{1}a_{2}\dots a_{r}\,{\mathbf{.}}\,a_{r+1}\dots a_{n}\dots
\end{equation*}
notice that there are $r$ digits to the left of the decimal point.
Define:
\begin{definition}
  The \emph{truncation of $x$} to $k$ (significant) digits is:
  \begin{itemize}
  \item   the
  number $a_{1}a_{2}\dots a_{k}0\dots 0$ (an integer with $r$ digits),
  if $k\leq r$,
  \item  the number $a_{1}\dots a_{r}{.}a_{r+1}\dots a_{k}$ if $k>r$. 
  \end{itemize}
  That is, one just cuts
  (i.e. \emph{truncates}) the numerical expression  of $x$ and adds
  zeroes to the right if the decimal part has not been reached.
\end{definition}

\begin{definition}
  The \emph{rounding of $x$} to $k$ (significant) digits is the
  following number:
  \begin{itemize}
  \item If $a_{k+1}<5$, then the rounding is the same as the
    truncation. 
  \item If $5\leq a_{k+1} \leq 9$, then the rounding is the truncation
    plus $10^{r-k+1}$. 
  \end{itemize}
  Remark: the rounding described here is called \emph{biased to plus
    infinity} because when the $k+1-$th digit is greater than or equal to
  $5$, the approximation is always greater than the true value.
\end{definition}
The problem with rounding is that \emph{all the digits of the rounded
  number} can be different from the original value (think of
$0{.}9999$ rounded to $3$ significant digits). The great advantage is
that the error incurred when rounding is less than the one
incurred
when truncating (it can even be a half of it):
\begin{example}
  If $x=178{.}299$ and one needs $4$ significant\footnote{We have not
    explained the meaning of this term, but we shall not do
    so. Suffice it to say that ``significant figures''
  means ``exact figures'' in an expression.} figures,
  then the truncation is
  $x_{1}=178{.}2$, whereas the rounding is $178{.}3$. The absolute
  error
  in the former case is $0{.}099$, while it is
  $0{.}001$ in the latter.
\end{example}

\begin{example}
  If $x=999{.}995$ and $5$ digits are being used, the truncation is 
  $x_{1}=999{.}99$, while the rounding is $1000{.}0$. Even though all
  the digits are different the absolute error incurred in both cases is the
  same: $0.005$. This is what matters, not that the digits ``match''.
\end{example}

Why is then truncation important if rounding is always at least as
precise and half the time better? Because when using floating point,
one cannot prevent truncation from happening and it must be taken into
account to bound the global error. As of today (2013) most of the
computer programs working in double precision, perform their computations
internally with many more digits and round their results to double
precision. However, truncations always happen (there is a limited
amount of available digits to the processor).

\subsection{Sources of error}
Error appears in different ways. On one hand, any measurement is
subject to it (this why any measuring device is sold with an estimated
margin of precision); this is intrinsic to Nature and one can only 
take it into account and try to assess its magnitude (give a
bound). On the other hand, computations performed in finite precision
arithmetic both propagate these errors and give rise to new ones precisely
because the quantity of available digits is finite.

The following are some of the sources of error:
\begin{itemize}
\item \emph{Measurement error}, already mentioned. This is
  unavoidable. 
\item \emph{Truncation error}: happens whenever a number (datum or the
  result of a computation) has more digits than available
  and some of them must be ``forgotten''.
\item \emph{Rounding error}: takes place when a
  number is rounded to a specified precision.
\item \emph{Loss of significance} (also called \emph{cancellation
    error}): this appears when an operation on two numbers increases
  relative error substantially more than the absolute error. 
  For example, when numbers of very
  similar magnitude are subtracted. The digits lost to truncation or
  rounding are too relevant and the relative error is huge.
  A classical example is the
  \emph{instability of the solution of the quadratic equation}.
\item \emph{Accumulation error}: which appears when accumulating
  (with additions, essentially) small errors of the same sign \emph{a lot
    of times}. This is what happened with the Patriot Missile Stations
  in 1991, during the \emph{Desert Storm} operation\footnote{One can
    read a summary at \par
    \texttt{http://www.cs.utexas.edu/\~{}downing/papers/PatriotA1992.pdf}\par
    and the official report is also available:\par
    \texttt{http://www.fas.org/spp/starwars/gao/im92026.htm}.}.
\end{itemize}
All the above errors may take place when working with finite
arithmetic. The following rules (which describe the worst-case
behaviors) apply:

\begin{itemize}\label{item:reglas-operaciones-errores}
\item When adding \emph{numbers of the same sign}, the absolute error
  can be up to the sum of the absolute errors and the same can happen
  to the relative error.
\item When adding \emph{numbers of different sign}, the absolute error
  behaves like in the previous case but \emph{the relative error may
    behave wildly}: $1000{.}2-1000{.}1$ has only a significant digit,
  so that the relative error can be up to $10\%$, but the relative
  error in the operands was at most $1\e{-4}$.
\item When multiplying, the absolute error is of the same magnitude as
  the largest factor times the absolute error in the other factor. If both
  factors are
  of the same magnitude, the absolute error can be up to double the
  maximum absolute error times the maximum factor. The relative error
  is of the same magnitude as the maximum relative error in the
  factors (and is the sum if both factors are of the same magnitude). 
\item When dividing \emph{by a number greater than or equal to $1$},
  the absolute error is approximately the absolute error in the
  numerator divided by the denominator and the relative error is the
  relative error in the numerator (more or less like in
  multiplication). When dividing by numbers near $0$, absolute
  precision is lost and later operations with numbers of the same
  magnitude will probably give rise to large cancellation
  errors. Example \ref{ex:division-truncation} is illustrative of this
  fact: the first line is assumed to be an exact
  computation, the second one an approximation using a truncation to
  $6$ significant figures in the divisor of the quotient.
  One can see that the ``approximation'' is
  totally useless.
\end{itemize}

An example of the bad behavior of division and addition is the
following:
\begin{example}\label{ex:division-truncation}
  Consider the following two computations, the first one is assumed to
  be ``correct'' while the second one is an approximation:
    \begin{equation*}
    \begin{split}
          &26493-\frac{33}{0{.}0012456}=-0{.}256 \text{ (the exact result) }.\\
          &26493-\frac{33}{0{.}001246}=8{.}2488 \text{ (approximation) }
    \end{split}
  \end{equation*}
  The relative error in the rounding of the denominator is only  $3\times
  10^{-4}$ (less than half a thousandth of a unit), but the relative error in
  the final result is 
  $33{.}2$ (which means that the obtained result is $33$ times larger
  in absolute value
  than it should, so it is worthless as an ``approximation''). Notice that
  \emph{not even the sign is correct}. This is
  (essentially) the main problem of resolution methods involving
  divisions (like Gauss' and, obviously, Cramer's). When explaining
  Gauss' method, convenient ways to choose an adequate
  denominator will be shown (these are called \emph{pivoting
    strategies}). As a general rule, \emph{the larger the denominator,
  the better}.

\end{example}


\section{Bounding the error}
As we have already said,  one does not seek an exact knowledge of the
error incurred during a measurement or the solution to a problem, as
this will likely be impossible. Rather, one aims to \emph{have an
  idea} of it and, especially, to \emph{know a good bound} of it. That
is, to be able to assess the maximum value that the absolute error can take
and this being a useful piece of information. Knowing that $2{.}71$ is
an approximation of $e$ with an error less than $400$ is
worthless. Knowing that the error is less than $0{.}01$ is useful. 

In general, the only possibility is to estimate a \emph{reasonably
  small} number larger than
the incurred error. This is \emph{bounding} the error.

\subsection{Some bounds}
If one knows that a quantity is between two values, which is usually
expressed in the form $x=a\pm \epsilon$ for some $\epsilon>0$, then
the absolute error incurred when using $a$ instead of $x$ is unknown
\emph{but is bounded by $\epsilon$}. So that the relative error is, at
most, this $\epsilon$ divided by the smallest of the possible absolute values
of $x$. \emph{Notice that this is tricky because if $a-\epsilon<0$ and
$a+\epsilon>0$ then \emph{there is no way to bound the relative error}
because $x$ can be $0$ and then there is no relative error (it is not
defined for $x=0$).}
\begin{example}
  If $\pi=3{.}14\pm 0{.}01$, then the maximum absolute error incurred
  is $0{.}01$, so that the relative error is at most
  \begin{equation*}
    \frac{0{.}01}{\abs{3{.}13}}\simeq {.}003194
  \end{equation*}
  (more or less $1/300$).
\end{example}

Notice that in order to get an upper bound of a quotient, \emph{the
denominator must be bounded from below} (the lesser the denominator,
the greater the quotient).

The rules in page \pageref{item:reglas-operaciones-errores} are
essential in order to bound the error of a sequence of arithmetic
operations. One has to be especially careful when dividing by numbers
less than one, as this may lead to useless results like ``the result
is $7$ with a relative error of $23$.''
