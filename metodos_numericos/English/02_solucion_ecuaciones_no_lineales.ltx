% -*- TeX-master: "notas.ltx" -*-
\chapter{Numerical Solutions to Non-linear Equations}
\label{cha:ecuaciones-no-lineales}

The problem of solving the non-linear equation $f(x)=0$ given $f$ and
some initial conditions is treated in this chapter.

Each of the algorithms which will be explained in this chapter has its own advantages
and disadvantages; one should not discard anyone \emph{a priori} just
for its ``slowness'' ---for example, bisection. We shall see that the
``best'' one ---namely, Newton-Raphson's--- has two drawbacks: it may
converge far from the initial condition, becoming useless for finding
a root ``near'' a specific point and it requires the computation of
the value of the derivative of $f$ at each step, which may be too
costly.

Notice that all the algorithms we present include stopping conditions
which depend \emph{on the value of $f(x)$} and are of Cauchy-type. This
is done in order to simplify the exposition. If a specific quantity of
exact figures is required, one needs to perform a much deeper study of
the problem being solved, of the derivative of $f$ and other
elements, which are out of the scope of these notes.


\section{Introduction}
Computing roots of functions, and especially of polynomials, is one of
the classical problems of Mathematics. It used to be believed that any
polynomial could be ``explicitly solved'' like the quadratic
equation, via a formula involving radicals (roots of numbers). Galois
Theory, developed in the beginning of the XIX Century, proved that
this is not the case at all and that, as a matter of fact, most
polynomials of degree greater than $4$ are not solvable using radicals.

However, the search for a \emph{closed formula} for solving equations is
just a way of putting off the real problem. In the end, and this is
what matters:
\begin{center}
\emph{  The only computations that can always be performed exactly are
  addition, subtraction and multiplication.}
\end{center}
It is not even possible to divide and get exact results\footnote{One
  could argue that using rational numbers solves this problem but,
  again, there is a point at which decimal expansions are needed.}.

From the beginning, people sought ways to quickly find approximate
solutions to nonlinear equations involving the four rules: addition,
subtraction, multiplication and division.

Two different kind of algorithms are explained in this chapter: those
with a geometric meaning and the \emph{fixed point} method, which
requires the notion of \emph{contractivity}.

Before proceeding, let us remark two essential requirements in any
root-computing algorithm:
\begin{itemize}
\item The specification of a \emph{precision}, that is an $\epsilon>0$
  such that if $\abs{f(c)}<\epsilon$, then $c$ is taken as an
  approximate root of $f$. This is needed because, as non-exact
  computations are performed, expecting a result $f(c)=0$ is
  unrealistic, so that this equality is useless as a stopping condition.
\item A second \emph{stopping condition} unrelated to the value of
  $f$. It may well happen that either the algorithm does not converge
  or that $f$ has no roots, so that the statement $\abs{f(c)}<\epsilon$
  is never true. In any case, the algorithm \emph{must} finish
  at some point. Otherwise, \emph{it is not an algorithm}. This
  implies the need for a condition unrelated to $f$. This takes usually
  the form ``perform no more than $n$ steps,'' where $n$ is a counter.
\end{itemize}

\section{The Bisection Algorithm}
Assume $f$ is a continuous function on a closed interval
$[a,b]$. Bolzano's Theorem may be useful if its conditions hold:
\begin{theorem*}[Bolzano]
  Let $f:[a,b]\rightarrow {\mathbb R}$ be a continuous function on 
  $[a,b]$ such that $f(a)f(b)<0$ (i.e. it changes sign between $a$
  and $b$). Then there is $c\in (a,b)$ with $f(c)=0$.
\end{theorem*}

This statement asserts that if the sign of $f$ changes on $[a,b]$ then
there is \emph{at least} a root of $f$ in it. One could sample the
interval using small sub-intervals (say of width $(b-a)/10^i$) and
seek, among this sub-intervals, one where the sign changes, thus
nearing a root at each step. Actually, dividing the interval into
two segments turns out to be much simpler.


Start with a function $f$, \emph{continuous} on the interval $[a,b]$ with
values $f(a)$ and $f(b)$. Specify a desired precision $\epsilon>0$ (so
that if $\abs{f(c)}<\epsilon$ then $c$ is accepted as an approximate root)
and a maximum number of iterations $N>0$ of the process. With all
these data, one proceeds taking advantage of Bolzano's Theorem, as
follows:

If $f(a)f(b)<0$, then there is a root in $(a,b)$. Take $c$ as the
midpoint\footnote{This is what gives the algorithm the alternative
  name ``midpoint rule.''} of the interval, that is
$c=\displaystyle{\frac{a+b}{2}}$. There are three possibilities now:
\begin{itemize}
\item Either $\abs{f(c)}<\epsilon $ and one takes $c$ as an
  approximate root and finishes the algorithm.
\item Or $f(a)f(c)<0$ and one substitutes $c$ for $b$ and repeats.
\item Or $f(c)f(b)<0$ and one substitutes $c$ for $a$ and repeats.
\end{itemize}
The iteration is done at most $N$ times (so that one obviously has to
keep track of their number). If after $N$ iterations no approximate
root has been found, the process ends with an error message.

The formal statement of the process just described is Algorithm
\ref{alg:biseccion}. Notice that the
expression $a \leftarrow b$ is used, meaning that $a$ takes (or
is given) the value $b$.

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} A function $f(x)$, a pair of real numbers
    $a,b$ with $f(a)f(b)<0$, a tolerance $\epsilon >0$ and
    a limit of iterations $N>0$ 
    \STATE \textbf{Output:} either an error message or a real number
    $c$ between $a$ and $b$ such that $\abs{f(c)} <
    \epsilon$ (i.e. an approximate root)
    \PART{Precondition}
    \IF{$f(a)\not\in {\mathbb R}$
      \textbf{of} $f(b)\not\in \mathbb{R}$ \textbf{or} $a+b\not\in
      {\mathbb R}$ \comm{overflow}}
    \STATE \textbf{return} ERROR
    \ENDIF
    \PART{Start}
    \STATE $i\leftarrow 0$
    \STATE $c\leftarrow \displaystyle{\frac{a+b}{2}}$ \comm{both $NaN$ and $\infty$
      can happen}
    \WHILE{$\abs{f(c)}\geq \epsilon$ \textbf{and} $i\leq N$}
    \STATE \comm{Never ever compare signs multiplying}
    \IF{$f(a)f(c) < 0$} 
    \STATE $b\leftarrow c$ \comm{interval $[a,c]$}
    \ELSE
    \STATE $a\leftarrow c$ \comm{interval $[c,b]$}
    \ENDIF
    \STATE $i\leftarrow i+1$
    \STATE $c \leftarrow \displaystyle{\frac{a+b}{2}}$
    \comm{middle point (overflow possible)}
    \ENDWHILE
    \IF{$i>N$} 
    \STATE \textbf{return} ERROR
    \ENDIF
    \STATE \textbf{return} $c$
  \end{algorithmic}
  \caption{Bisection Algorithm.}
  \label{alg:biseccion}
\end{algorithm}

\begin{example}\label{ex:bisection-cos-e-x}
  Take the function $f(x)=\cos(e^x)$, which is continuous in the
  interval $[0,1]$. Obviously, $f(0)f(1)<0$ (why is this obvious?),
  so that Bolzano's
  Theorem applies. Setting $a=0$, $b=1$, Algorithm \ref{alg:biseccion}
  gives the following sequence: each assignment to \texttt{a} or
  \texttt{b} means that the corresponding endpoint of the interval is
  substituted for that value.
  % > f = @(x) cos(exp(x));
  % > Bisec(f, 0, 1, .001, 10)
  \begin{lstlisting}
    octave > Bisec(@(x) cos(exp(x)), 0, 1, .001, 10)
    c = 0.50000
        b = 0.50000   % new interval: [0, 0.50000]
    c = 0.25000
        a = 0.25000   % new interval: [0.25000, 0.50000]
    c = 0.37500
        a = 0.37500   % new interval: [0.37500, 0.50000]
    c = 0.43750
        a = 0.43750   % new interval: [0.43750, 0.50000]
    c = 0.46875
        b = 0.46875   % new interval: [0.43750, 0.46875]
    c = 0.45312
        b = 0.45312   % new interval: [0.43750, 0.45312]
    c = 0.44531
        a = 0.44531   % new interval: [0.44531, 0.45312]
    c = 0.44922
        a = 0.44922   % new interval: [0.44922, 0.45312]
    c = 0.45117
    octave> f(c) ans = 6.4520e-04
  \end{lstlisting}
  As one can see, $0{.}45117$ is an approximate root of $\cos(e^x)$,
  in the sense  that $\abs{\cos(e^{0{.}45117})}<0{.}001$.
\end{example}

\section{Newton-Raphson's Algorithm}
A classical geometric idea (which also appears in approximation
theory) is to use the \emph{best\footnote{From the point of view of
    infinitesimal analysis and polynomial approximation.} linear approximation} to $f$ in order
to compute an approximate solution to $f(x)=0$. This best linear\margin{Examples}
approximation is, of course, the tangent line to $f$, which is
computed using the derivative $f^{\prime}(x)$. So, instead of trying
to solve $f(x)=0$ directly, one draws the tangent to the graph of $f$
at $(x,f(x))$ and finds the meeting point of this line with the $OX$
axis. Obviously, this will most likely not be a root of $f$ but
\emph{in sufficiently general conditions}, it is expected to
approach one. If the process is repeated enough times, 
it get nearer a root of $f$. This is the idea of Newton-Raphson's
method.

Recall that the equation of the line passing through 
$(x_{0},y_0)$ with slope $b$ is:
\begin{equation*}
Y = b(X-x_{0})+y_{0}
\end{equation*}
so that the equation of the tangent line to the graph of $f$ at
$(x_{0},f(x_0))$ is (assuming $f$ has a derivative at $x_0$):
\begin{equation*}
Y = f^{\prime}(x_{0})(X-x_0)+f(x_0).
\end{equation*}
The meeting point between this line and $OX$ is
\begin{equation*}
(x_1,y_1)=\left(x_0-\frac{f(x_0)}{f^{\prime}(x_0)},0 \right)
\end{equation*}
assuming it exists (i.e. $f^{\prime}(x_0)\neq 0$).

If $x_1$ is not an approximate root of $f$ with the desired
precision, one proceeds in the same way at the point $(x_1, f(x_1))$.
After having performed  $n$ steps, the next point $x_{n+1}$ takes the form:
\begin{equation*}
x_{n+1}=x_n-\frac{f(x_n)}{f^{\prime}(x_n)}.
\end{equation*}
One carries on until the desired precision or a bound in the
number of iterations is reached. This is Algorithm
\ref{alg:newton-raphson}. In its formal expression, we only specify a
possible error place in order to keep it clear but one has to take
into account that each time a computation is performed a floating-point
error might occur.
% From now on, we expect the student to be able to
% locate the places where an algorithm may give rise to this kind of
% exception (because a division takes place or a square root is computed, etc.).

\begin{algorithm}
  \begin{algorithmic}
    \STATE \textbf{Input:} A differentiable function $f(x)$, a
    \emph{seed} $x_{0}\in {\mathbb R}$, a tolerance $\epsilon >0$ and
    a limit for the number of iterations $N>0$
    \STATE \textbf{Output:} Either an error message or
    a real number $c$ such that $\abs{f(c)}< \epsilon$ (i.e. an
    approximate root)
    \PART{Start}
    \STATE $i \leftarrow 0$
%    \STATE \comm{make sure that $f(x_{i})\in {\mathbb R}$, not included}
    \WHILE{$\abs{f(x_{i})}\geq \epsilon$ \textbf{and} $i\leq N$}
%    \STATE \comm{meeting point of tangent and $OX$ axis}
    \STATE $x_{i+1} \leftarrow
    x_{i}-\displaystyle{\frac{f(x_{i})}{f^{\prime}(x_{i})}}$
    %\comm{possible NaN or $\infty$}
%    \IF{$x_{i+1}=NaN$
%    \STATE \textbf{return} ERROR
%    \ENDIF
    \STATE $i\leftarrow i+1$
%    \STATE \comm{make sure that $f(x_{i})\in {\mathbb R}$, not included}
    \ENDWHILE
    \IF{$i>N$}
    \STATE \textbf{return} ERROR
    \ENDIF
    \STATE $c\leftarrow x_i$
    \STATE \textbf{return} $c$
  \end{algorithmic}
  \caption{Newton-Raphson.}
  \label{alg:newton-raphson}
\end{algorithm}

\begin{example}\label{ex:newton-ex+x}
  Take the function $f(x)=e^x+x$, which is easily seen to have a
  unique zero on the whole real line (why is this so and why is it
  easy to see?). Its derivative is
  $f^{\prime}(x)=e^x+1$. Let us use $x_0=1$ as the seed. An
  implementation of Newton-Raphson's algorithm in Octave might show
  the following steps:
\begin{lstlisting}
octave> c=newtonF(f, fp, 1, .0001, 5)
xn = 0
xn = -0.500000000000000
xn = -0.566311003197218
xn = -0.567143165034862
xn = -0.567143290409781
xn = -0.567143290409784
c = -0.567143290409784
octave> f(c)
ans = -1.11022302462516e-16
\end{lstlisting}
  The convergence speed (which will be studied in detail in
  \ref{subs:newton-raphson-convergence-speed}) is clear in the
  sequence of \texttt{xn}, as is
  the very good approximation to $0$ of $f(c)$ for the approximate
  root after just $5$ iterations.
\end{example}

\begin{example}
  However, the students are suggested to try Newton-Raphson's
  algorithm for the function $\cos(e^{x})$ in Example
  \ref{ex:bisection-cos-e-x}. A strange phenomenon takes place: the
  algorithm is almost never convergent and no approximation to any
  root happens. Is there a simple explanation for this fact?
\end{example}

\begin{example}
  Newton-Raphson's method can behave quite strangely when
  $f^{\prime}(c)=0$, if $c$ is a root of $f$. We shall develop some
  examples in the exercises and in the practicals.
\end{example}

\section{The Secant Algorithm}
Newton-Raphson's algorithm contains the evaluation of
$\frac{f(x_n)}{f^{\prime}(x_n)}$ for which one has to compute not only
$f(x_{n})$ but also
$f^{\prime}(x_{n})$, which may be too costly. Moreover, there are
cases in which one does not have true information on $f^{\prime}(x)$,
so that assuming it can be computed may be utopic.

The simplest solution of this problem is to approximate the value of
the derivative using the geometric idea that ``the tangent line is the
limit of the secants''; instead of computing the tangent line one
approximates it by means of two points near each other. Recall that
the derivative of $f(x)$ at $c$ is (if it exists) the limit
\begin{equation*}
\lim_{h\rightarrow 0}\frac{f(c+h)-f(c)}{h}.
\end{equation*}
When applying Newton-Raphon's algorithm, if instead of using a single
point $x_n$, one makes use of two: $x_n$ and $x_{n-1}$, then the
derivative $f^{\prime}(x_{n})$ can be approximated by means of
\begin{equation}\label{eq:secant-approximation}
f^{\prime}(x_n)\simeq  \frac{f(x_n)-f(x_{n-1})}{x_n-x_{n-1}},
\end{equation}
so that the formula for computing $x_{n+1}$ becomes, using this
approximation, 
\begin{equation*}
x_{n+1}=x_n-f(x_{n})\frac{x_n-x_{n-1}}{f(x_n)-f(x_{n-1})},
\end{equation*}
and the Secant Algorithm is obtained.
Notice that, in order to start it, \emph{two seeds} are required,
instead of one. The coefficient of $f(x_n)$ in the iteration
is just the reciprocal of approximation
\eqref{eq:secant-approximation}. 

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} A function $f(x)$, a tolerance $\epsilon
>0$, a bound for the number of iterations $N>0$ and \emph{two} seeds $x_{-1},
x_0\in {\mathbb R}$
\STATE \textbf{Output:} Either a real number $c\in {\mathbb R}$ with
$\abs{f(c)} < \epsilon$ or an error message
\PART{Start}
\STATE $i\leftarrow 0$
\WHILE{$\abs{f(x_i)} \geq \epsilon$ \textbf{and} $i\leq N$}
\STATE $x_{i+1}\leftarrow {\displaystyle x_i-
  f(x_{i})\frac{x_i-x_{i-1}}{f(x_i)-f(x_{i-1})}}$ 
%\comm{possible NaN or $\infty$}
\STATE $i\leftarrow i+1$
\ENDWHILE
\IF{$i>N$}
\STATE \textbf{return} ERROR
\ENDIF
\STATE $c\leftarrow x_i$
\STATE \textbf{return} $c$
\end{algorithmic}
\caption{The Secant Algorithm.}
\label{alg:secante}
\end{algorithm}
It is worthwhile, when implementing this method, keeping in memory
not just $x_n$ and $x_{n-1}$ but also the values $f(x_n)$ and
$f(x_{n-1})$ so as not to recompute them.

\begin{example}
  Take the same function as in example \ref{ex:newton-ex+x}, that is
  $f(x)=e^x+x$. Let us use $x_0=1$ and $x_1=0{.}999$ as the two seeds. An
  implementation of the secant algorithm in Octave might show
  the following steps:
\begin{lstlisting}
octave> c=secant(f, 1, 0.999, .0001, 6)
xn = -3.65541048223395e-04
xn = -0.368146813789761
xn = -0.544505751846815
xn = -0.566300946933390
xn = -0.567139827650291
c = -0.567139827650291
octave> f(c)
ans =  5.42664370639656e-06
\end{lstlisting}
  It needs an iteration more than Newton-Raphson's method to achieve
  the same precision.
\end{example}

\begin{example}
  The secant algorithm has the same problem with the function of
  Example \ref{ex:bisection-cos-e-x} because it is essentially an
  approximation to Newton-Raphson's, so that if this fails, it is
  expected for the secant method to fail as well.
\end{example}



\section{Fixed Points}
Fixed point algorithms ---which, as we shall see later, include
the previous ones indirectly--- are based on the notion of
\emph{contractivity}, which reflects the idea that a function (a
\emph{transformation}) may map pairs of points in such a way that the
images are always nearer than the original two (i.e. the function
\emph{shrinks}, \emph{contracts} the initial space). This idea,
linked to differentiability, leads to that of \emph{fixed point} of an
iteration and, by means of a little artifact, to the approximate
solution of general equations using just iterations of a function.

\subsection{Contractivity and equations $g(x)=x$}
Let $g$ be a real-valued function of one real variable, differentiable
at $c$. That is, for any infinitesimal $\oo$, there exists another one
$\oo_1$ such that
\begin{equation*}
g(c+\oo) = g(c) + g^{\prime}(c)\oo + \oo\oo_1,
\end{equation*}
which means that \emph{near $c$, the function $g$ is very similar to its
linear approximation.}

Assume that $\oo$ is the width of a ``small'' interval centered at
$c$. Removing the \emph{supralinear} error (the term $\oo\oo_1$)
for approximating, one might think that  $(c-\oo, c+\oo)$ is mapped
into  $(g(c)-g^{\prime}(c)\oo,
g(c)+g^{\prime}(c)\oo)$: that is, an interval of radius $\oo$ is
mapped
into one of radius $g^{\prime}(c)\oo$ (it dilates or shrinks by a
factor of $g^{\prime}(c)$). This is essentially what motivates the Jacobian Theorem in
integration: the derivative measures the dilation or contraction which
the real line undergoes at a point when transformed by $g$.
If $\abs{g^{\prime}(c)}<1$, the real line \emph{contracts} near
$c$. As a matter of fact, one usually reads the following

\begin{definition}
  A map $f:[a,b]\rightarrow \mathbb{R}$ is \emph{contractive} if
  $\abs{f(x)-f(y)}<\abs{x-y}$ for any two $x,y\in[a,b]$.

  Notice that if $f$ is
  everywhere differentiable and $\abs{f^{\prime}(x)}<1$ for all
  $x\in[a,b]$ then $f$ is contractive.
\end{definition}

Assume for simplicity that  $g^{\prime}$ is continuous on
$[a,b]$ and that $\abs{g^{\prime}(x)}<1$ for all $x\in [a,b]$
---i.e., $g$ ``shrinks everywhere along $[a,b]$''. There are several facts
to notice. First of all, the conditions imply that there exists
$\lambda<1$ such that
$\abs{g^{\prime}(x)}<\lambda$, by Weierstrass' Theorem applied
to $g^{\prime}(x)$. Moreover, by the Mean
Value Theorem, for any $x_{1},x_2\in [a,b]$, the following inequality
holds: 
\begin{equation*}
\abs{g(x_1)-g(x_2)} \leq \lambda \abs{x_1-x_2},
\end{equation*}
which means: \emph{the distance between the images of two arbitrary
  points is less than  $\lambda$ times the original distance between
  the points}, and, as $\lambda<1$, what really happens is that
 \emph{the distance between the images is always less than the
   original distance}. That is, the map $g$ is \emph{shrinking} the
 interval $[a,b]$ everywhere. It is as if $[a,b]$ were a piece of
 cotton cloth and it was being washed with warm water: it shrinks
 everywhere. 

The last paragraph contains many emphasis but they are necessary for
understanding the final result: the existence of a fixed point, a
point which gets mapped to itself.

From all the explanation above, one infers that the width of
$g([a,b])$ is less than or equal to $\lambda(b-a)$. If moreover
$g([a,b])\subset [a,b]$ (that is, if $g$ mapped $[a,b]$ into itself)
then one could also compute $g(g([a,b]))$, which would be at most of
width $\lambda\lambda(b-a)=\lambda^2(b-a)$. Now one could repeat the
iteration indefinitely and $g\circ g\circ g\circ\dots\circ g=
g^{\circ n}([a,b])$ would have width less
than $\lambda^n(b-a)$. As $\lambda<1$, this width tends to $0$ and
using the Nested Intervals Principle, there must be a point $\alpha\in[a,b]$
for which $g(\alpha)=\alpha$. This is, by obvious reasons, a
\emph{fixed point}. Even more, the fact that $\lambda<1$ implies that
$\alpha$ is unique. There is one and only one fixed point for $g$ in
$[a,b]$. We have just given a plausible argument for the next result:
\begin{theorem}\label{the:punto-fijo-aplicacion-derivable}
  Let $g:[a,b]\rightarrow[a,b]$ be a map of $[a,b]$ into itself, which
  is continuous and differentiable on $[a,b]$. If there is a positive
  $\lambda <1$ such that for any
  $x\in[a,b]$, $\abs{g^{\prime}(x)}\leq \lambda$, then
  there exists one and only one $\alpha\in[a,b]$ for which
  $g(\alpha)=\alpha$. Even more, for any $x_0\in[a,b]$, if one defines
  \begin{equation*}
    x_n=g(x_{n-1})\,\,\,\mathrm{ for }\,\,\, n>0
  \end{equation*}
  then
  \begin{equation*}
    \lim_{n\rightarrow \infty}x_n = \alpha.
  \end{equation*}
\end{theorem}

This means that under suitable conditions, the equation $g(x)=x$ has a
single solution in the interval $[a,b]$. The explanation
before the statement shows how to approximate this solution: take any
$x\in [a,b]$ and compute $g(x)$, $g(g(x))$, \dots. The limit (which
exists under those conditions) is always $\alpha$, regardless of $x$.

Hence,
\begin{center}
  \emph{solving equations of the form $g(x)=x$ for contractive $g$ is
    utterly simple}: \textbf{just iterate} $g$.
\end{center}


\subsection{Application to General Equations
  $f(x)=0$}\label{subs:aplicacion-punto-fijo-raices}
In real life (if this happens to relate to real life at all, anyway),
nobody comes across an equation of the form $g(x)=x$. One always
encounters problems like $f(x)=0$ or, more generally $f(x)=c$, $c$
being a constant, which are easily turned into $f(x)=0$.

But this is not a problem because
\begin{equation*}
f(x) = 0 \Leftrightarrow  f(x) + x = x 
\end{equation*}
so that, \emph{searching for a root of $f(x)$ is the same thing as searching
for a fixed point of $g(x)=f(x) + x$}. Or, for the same reason, of
$x-f(x)$.

\begin{remark*}[Skip in a first reading]
  As a matter of fact, if $\phi(x)$ is a nowhere zero function,
    then \emph{searching for a root of $f(x)$ is the same as searching
      for a fixed point of $g(x)=x-\phi(x)f(x)$}.  This allows, for
    example, to \emph{scale} $f$ so that its derivative is
    approximately $1$ and $g^{\prime}$ is small in order to accelerate
    convergence. Or one can just take $g(x)=x-cf(x)$ for a suitable
    $c$ which makes the derivative of $g$ relatively small in absolute
    value.
\end{remark*}

\subsection{The Algorithm}
This is probably the easiest algorithm to implement, as it only
requires computing the value of $g$ each time. As any other, it
requires a tolerance $\epsilon$ and a maximum number of iterations
$N$. The drawback is that the algorithm can be useless if $[a,b]$ is not
mapped into itself. \emph{This has to be checked beforehand.}

\begin{remark}
Let $g:[a,b]\rightarrow \mathbb{R}$ be a map. If a fixed point of $g$ in
$[a,b]$ is to be found using contractivity, it is necessary:
\begin{itemize}
\item If $g$ is differentiable\footnote{If $g$ it is not, then the
    requirement ---the Lipschitz condition--- is much harder to
    test.}
  in $[a,b]$, there must exist 
  $\lambda\in {\mathbb R}$ such that $0<\lambda<1$ and for which
  $\abs{g^{\prime}(x)}\leq\lambda$ for all $x\in [a,b]$.
\end{itemize}
\end{remark}

Assumed both checks have been performed, the method for finding a
fixed point of
$g:[a,b]\rightarrow[a,b]$ is stated in Algorithm \ref{alg:punto-fijo}:

\begin{algorithm}
\begin{algorithmic}
\STATE \textbf{Input:} A function $g$ (contractive etc\dots), a seed
$x_0\in [a,b]$, a tolerance
$\epsilon>0$ and a maximum number of iterations $N>0$
\STATE \textbf{Output:} either $c\in [a,b]$ such that
$\abs{c-g(c)}<\epsilon$ or an error message
\PART{Start}
\STATE $i\leftarrow 0, c\leftarrow x_0$
\WHILE{$\abs{c-g(c)}\geq \epsilon$ \textbf{and} $i\leq N$}
\STATE $c\leftarrow g(c)$
\STATE $i\leftarrow i+1$
\ENDWHILE
\IF{$i>N$}
\STATE \textbf{return} ERROR
\ENDIF
\STATE \textbf{return} $c$
\end{algorithmic}
\caption{Fixed Point.}
\label{alg:punto-fijo}
\end{algorithm}

\begin{example}
  For the fixed point algorithm we shall use the same function as for
  the bisection method, that is $f(x)=\cos(e^x)$. In order to find a
  root of this function, we need to turn the equation
  \begin{equation*}
    \cos(e^x)=0
  \end{equation*}
  into a fixed-point problem. This is always done in the same (or
  similar) way: the above equation is obviously equivalent to
  \begin{equation*}
    \cos(e^x)+x = x,
  \end{equation*}
  which is a fixed-point problem. Let us call $g(x)=\cos(e^x)+x$, whose
  fixed point we shall try to find.

  In order to apply the fixed-point theorem, one needs an interval $[a,b]$
  which is mapped into itself. It is easily shown that $g(x)$
  decreases near $x=0{.}5$ and, as a matter of fact, that it does so
  in the interval $I=[0{.}4, 0{.}5]$. Moreover, $g(0{.}4)\simeq
  0{.}478+$ while $g(0{.}5)\simeq 0{.}4221+$, which implies that the
  interval $I$ is mapped into itself by $g$ (this is probably the most
  difficult part of a fixed-point problem: finding an appropriate
  interval which gets mapped into itself). The derivative of $g$
  is $g^{\prime}(x)=-e^x\sin(e^x)+1$, whose absolute value is,
  in that interval, less
  than $0{.}8$ (it is less than $1$ in the whole interval $[0,1]$, as
  a matter of fact). This is the second condition to be verified in
  order to apply the theorem.

  Now we can be certain that there is a fixed point for $g(x)$ in
  $[0{.}4,0{.}5]$. Matlab would give a sequence like
\begin{lstlisting}
octave> c=fixed_point(g, 0.5, .0001, 10)
xn =  0.422153896052972
xn =  0.467691234530252
xn =  0.442185884541927
xn =  0.456876713444163
xn =  0.448538950613744
xn =  0.453312782246289
xn =  0.450592834887592
xn =  0.452146949739883
xn =  0.451260386650212
xn =  0.451766601977469
c =  0.451766601977469
octave> f(c)
ans = -2.88890800240215e-04
\end{lstlisting}
  The convergence speed does not look too good, which is usual in
  fixed-point problems.
\end{example}

%\subsection{Finding Roots Using Fixed Points}
\begin{remark*}[Skip in first reading]
  The fixed point algorithm can be used, as explained in
  \ref{subs:aplicacion-punto-fijo-raices}, for finding roots of
  general equations using a suitable factor;
  to this end, if the equation is $f(x)=0$, one can
  use any function $g(x)$ of the form
  \begin{equation*}
    g(x) = x - k f(x)
  \end{equation*}
  where $k\in {\mathbb R}$ is an adequate number. This transformation
  is performed so that the derivative of $g$ is around $0$ near the
  root and so that if $c$ is the (unknown) root, then $g$ defines a
  contractive map on an interval of the form $[c-\rho,c+\rho]$ (which
  will be the $[a,b]$ used in the algorithm).

  The hard part is to check that $g$ is a contractive map of $[a,b]$
  onto itself.
\end{remark*}

\subsection{Convergence Speed of the Fixed Point Method}
If the absolute value of the derivative is bounded by $\lambda<1$, the
convergence speed of the fixed point algorithm is easily bounded
because $[a,b]$ is mapped onto a sub-interval of width at most
$\lambda(b-a)$ and this contraction is repeated at each step.
So, after $i$ iterations, the width of the image set is at most
$\lambda^i(b-a)$. If $\lambda<10^{-1}$ and $(b-a)<1$, for example,
then one can guarantee that each iteration gives an extra digit of
precision in the root. However, $\lambda$ tends to be quite large
(like $0{.}9$) and the process is usually slow.

\begin{example}
If $[a,b]=[0,1]$ and $\abs{g^{\prime}(x)}< 0{.}1$, then after each
iteration there is an exact decimal digit more in $x$ as an
approximation to the fixed point $c$, regardless of the initial value
of the seed $x$.
\end{example}


\subsection{Newton-Raphson's convergence
  speed}\label{subs:newton-raphson-convergence-speed}
As a matter of fact, Newton-Raphson's algorithm, under suitable
conditions, is just a fixed point algorithm and the analysis of the
convergence speed for these can be applied to it in this
case. Notice that the expression:
\begin{equation*}
x_{n+1}=x_n-\frac{f(x_n)}{f^{\prime}(x_n)}
\end{equation*}
corresponds to the search of a fixed point of
\begin{equation*}
g(x) = x - \frac{1}{f^{\prime}(x)}f(x)
\end{equation*}
which, as explained above, is a way of turning the equation $f(x)=0$
into a fixed-point problem. The derivative of $g$ is, in this case:
\begin{equation*}
g^{\prime}(x) = 1 -
\frac{f^{\prime}(x)^2-f(x)f^{\prime\prime}(x)}{f^{\prime}(x)^2}
\end{equation*}
which, at a root $c$ of $f$ (that is, $f(c)=0$) gives
$g^{\prime}(c)=0$. That is, the derivative of $g$ is $0$ at the
fixed point of $g$. This makes convergence \emph{very fast} when $x$
is near $c$ (and other conditions hold).

In fact, the following strong result (which is usually stated as
``Newton-Raphson has quadratic convergence'') can be proved:
\begin{theorem}
\label{the:newton-raphson-cuadratico}
Assume $f$ is a twice-differentiable function on an interval
$[r-\epsilon, r+\epsilon]$, that $r$ is a root of $f$ and that
\begin{itemize}
\item The second derivative of $f$ is bounded from above:
  $\abs{f^{\prime\prime}(x)}< K$ for $x\in[r-\epsilon, r+\epsilon]$,
\item The first derivative of $f$ is bounded from below:
  $\abs{f^{\prime}(x)}>L>0$ for $x\in[r-\epsilon, r+\epsilon]$
\end{itemize}
then, if $x_k\in[r-\epsilon, r+\epsilon]$, the following term in
Newton-Raphson's iteration is also in that interval and
\begin{equation*}
\abs{r-x_{k+1}}<\abs{\frac{K}{2L}}\abs{r-x_k}^2.
\end{equation*}
\end{theorem}
And, as a corollary:
\begin{corollary}[Duplication of exact digits in Newton-Raphson's metod]
  Under the conditions of Theorem \ref{the:newton-raphson-cuadratico},
  if $\epsilon<0{.}1$ and $K< 2L$ then, for $n\geq k$, each
  iteration $x_{n+1}$ of Newton-Raphson approximates $c$ with
  twice the number of exact digits as $x_n$.
\end{corollary}
\begin{proof}
  This happens because if $k=0$ then $x_0$ has at least one exact
  digit. By the Theorem, $\abs{x_1-r}$ is less than $0{.}1^2={.}01$.
  And from this point on, the number of zeroes in the decimal
  expansion of $\abs{x_n-r}$ gets
  doubled each time.
\end{proof}

In order to use Theorem \ref{the:newton-raphson-cuadratico} or its
corollary, one has to:
\begin{itemize}
\item Be certain that a root is near. The usual way to check this is
  using Bolzano's Theorem (i.e. checking that $f$ changes sign on a
  sufficiently small interval).
\item Bound the width of the interval (for example, by $1/10$).
\item Bound $f^{\prime\prime}(x)$ from above and $f^{\prime}(x)$
  from below on the interval (this is usually the hardest part).
\end{itemize}


\section{Annex: Matlab/Octave Code}
Some code with ``correct'' implementations of the algorithms of this
chapter are included here. They should run both on Matlab and
Octave. However, notice that the code here \emph{does not check for
  floating-point exceptions}. For example, if one uses the function
$\log(x)$ with any of this programs and it is evaluated at a negative
number, the programs will happily continue but the results will
probably be either absurd or not real. The decision not to include
those checks has been made for simplicity.

\subsection{The Bisection Algorithm}
The code in Listing \ref{code:bisection} implements the bisection algorithm for
Matlab/Octave. The input parameters are:
\begin{description}
\item[\texttt{f}] and anonymous function,
\item[\texttt{a}] the left endpoint of the interval,
\item[\texttt{b}] the right endpoint of the interval,
\item[\texttt{epsilon}] a tolerance (by default, \texttt{eps}),
\item[\texttt{n}] a bound for the number of iterations (by default, $50$).
\end{description}
The output may be null (if there is no sign change, with an
appropriate message) or an approximate root (to the tolerance) or a
warning message together with the last computed value (if the
tolerance has not been reached in the specified number of
iterations). The format of the output is a pair
\texttt{[z, N]} where \texttt{z} is the approximate root (or the last
computed value) and \texttt{N} is the number of iterations performed.

%\begin{figure}
\begin{lstlisting}[caption={Code for the  Bisection Algorithm.},
    label=code:bisection]
% Bisection Algorithm with tolerance and stopping condition
% Notice that at any evaluation f(...) an error might take place
% which is NOT checked.
function [c, N] = Bisec(f, a, b, epsilon = eps, n = 50)
  N = 0;
  if(f(a)*f(b)>0)
    warning('no sign change')
    return
  end
  % store values in memory
  fa = f(a);
  fb = f(b);
  if(fa == 0)
    c = a;
    return
  end
  if(fb == 0)
    c = b;
    returnn
  end
  c = (a+b)/2;
  fc = f(c);
  while(abs(fc) >= epsilon & N < n)
    N = N + 1;
    % multiply SIGNS, not values
    if(sign(fc)*sign(fa) < 0)
      b = c;
      fb = fc;
    else
      a = c;
      fa = fc;
    end
    % An error might happen here
    c = (a+b)/2;
    fc = f(c);
  end
  if(N >= n)
    warning("Tolerance not reached.")
  end
end
\end{lstlisting}
%\end{figure}

\subsection{Newton-Raphson's Algorithm}
Newton-Raphson's algorithm is easier to implement (always without checking
for floating point exceptions) but requires more complex input data:
the derivative of $f$, another anonymous function. The code in Listing
\ref{code:newton-raphson} does not
perform symbolic computations so that this derivative must be provided
by the user.

The input is, then:
\begin{description}
\item[\texttt{f}] an anonymous function, 
\item[\texttt{fp}] another anonymous function, the derivative of \texttt{f},
\item[\texttt{x0}] the seed,
\item[\texttt{epsilon}] the tolerance (by defect \texttt{eps}),
\item[\texttt{N}] the maximum number of iterations (by default $50$).
\end{description}

The format of the output, in order to facilitate its study, is a pair
\texttt{[xn,N]}, where \texttt{xn} is the approximate root (or the last
computed value) and \texttt{N} is the number of iterations performed.

\begin{lstlisting}[caption={Code for Newton-Raphson's Algorithm},
label=code:newton-raphson]
% Newton-Raphson implementation
function [z n] = NewtonF(f, fp, x0, epsilon = eps, N = 50)
  n = 0;
  xn = x0;
  % Both f and fp are anonymous functions  
  fn = f(xn);
  while(abs(fn) >= epsilon & n <= N)
    n = n + 1;  
    fn = f(xn); % memorize to prevent recomputing
    % next iteration
    xn = xn - fn/fp(xn); % an exception might take place here
  end
  z = xn;
  if(n == N)
    warning('Tolerance not reached.');
  end
end
\end{lstlisting}

