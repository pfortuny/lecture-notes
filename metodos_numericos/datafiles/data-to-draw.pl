#!/usr/bin/perl
open(my $f, "<", "$ARGV[0]");
$max = $ARGV[1] || 1;
while (<$f>) {
  do {
    print "%$_";
    next;
  } if /^#/;
  ($a, $b, $c, $d) = ($_ =~ /^(.*) +(.*) +(.*) +(.*)$/);
  if ($c > $max or $c<-$max) {
    $c = $max*$c/abs($c);
  }
  $arr=", ->";
  if ($d >$max or $d < -$max) {
    $d = $max*$d/abs($d);
  }
  if (abs($c)==$max or abs($d)==$max) {
    $arr = "";
  }
  print "\\addplot[blue $arr] coordinates {($1,$2) ($3, $4)};\n";
}
