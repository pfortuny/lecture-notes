% 1)
% We first define the anonymous function. 
% Notice the dotted operators ./ and .^, which ARE NEEDED
% because we shall compute the value of f on a vector when
% plotting it.

f = @(x) sin(20./(1+x.^2));

% For plotting, one needs first the X-coordinates, which the
% statement specifies. Do not use 'x' or 'y' as variables because
% one gets easily confused. It is better to use 'x1', 'y1', etc.

x1 = linspace(-20, 20, 500); 

% Why is it linspace?

% Plot. We use red color.
plot(x1, f(x1), 'r');

% 2)
% The derivative is a symbolic operation, we need a symbol
syms t

% Now we can compute df(t)/dt. Call it fp (for f-prime):
fp = diff(f(t), t);

% In order to plot fp, we MUST TRANSFORM IT INTO AN ANONYMOUS
% FUNCTION USING matlabFunction:
fp_n = matlabFunction(fp);

% And now we can plot it. As it is on the same graph, we have
% to tell Matlab to 'hold on'. We use 'green' color now.
hold on
plot(x1, fp_n(x1), 'g');

% 3)
% Recall that we have to use fp_n instead of fp everywhere.
%
% The Tangent Line at (a, f(a)) is
%      Y = fp_n(a).*(X-a)+f(a)
% 
% While the Normal Line is
%      Y = -1./fp_n(a).*(X-a)+f(a)
%
% Both will be defined as anonymous functions. The tangent
% line is at a=1, the normal line at a=-1:

tangent = @(x) fp_n(1).*(x-1)+f(1);
normal  = @(x) -1./fp_n(-1).*(x+1)+f(-1);

% These are just functions, we can plot them.
% The tangent in black and the normal in magenta
% Notice that we do not need to 'hold on' because we have
% already done.
plot(x1, tangent(x1), 'k')
plot(x1, normal(x1), 'm')
% The magenta line is perpendicular at x=-1 to the green curve.

