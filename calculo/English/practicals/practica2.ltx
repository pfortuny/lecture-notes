% -*- TeX-master: "practica2.ltx"; -*-
%\RequirePackage[spanish]{babel}
\documentclass{amsart}
\newcommand{\abs}[1]{\vert #1 \vert}
\title{Calculus - Practical II - Elementary Calculus}
\author{Pedro Fortuny Ayuso}
%\DeclareMathOperator{\sen}{sen}
%\def\datename{\emph{Fecha: }}
\usepackage{listings}
\usepackage{color}
\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\lstset{ %
  language=Matlab,                % the language of the code
  basicstyle=\tiny\ttfamily,           % the size of the fonts that are used for the code
%  numbers=left,                   % where to put the line-numbers
%  numberstyle=\tiny\color{gray},  % the style that is used for the line-numbers
%  stepnumber=10,                   % the step between two line-numbers. If it's 1, each line 
                                  % will be numbered
%  numbersep=5pt,                  % how far the line-numbers are from the code
  backgroundcolor=\color{white},      % choose the background color. You must add \usepackage{color}
  showspaces=false,               % show spaces adding particular underscores
  showstringspaces=false,         % underline spaces within strings
  showtabs=false,                 % show tabs within strings adding particular underscores
  %frame=single,                   % adds a frame around the code
  rulecolor=\color{black},        % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. commens (green here))
  tabsize=4,                      % sets default tabsize to 2 spaces
  captionpos=b,                   % sets the caption-position to bottom
  breaklines=true,                % sets automatic line breaking
  breakatwhitespace=false,        % sets if automatic breaks should only happen at whitespace
%  title=\lstname,                   % show the filename of files included with \lstinputlisting;
                                  % also try caption instead of title
  keywordstyle=\color{blue},          % keyword style
  commentstyle=\color{dkgreen},       % comment style
  stringstyle=\color{mauve},         % string literal style
  comment=[l]\%,
  morecomment=[s]{\%\{}{\%\}},
%  escapeinside={\%*}{*)},            % if you want to add a comment within your code
  morekeywords={*,...}               % if you want to add more keywords to the set
}

\input{commands}

\date{\today}
\begin{document}
\maketitle

The students will have already received the classes about limits,
continuity and derivation although these concepts should not be new
for them at all.

Each section is intended to be performed in a single file (a
\texttt{.m} file) and saved by the student for later reference.


\section{Computing limits}
Recall that in order to compute limits, one needs to declare the
``variable'' in the expression as symbolic, otherwise an error will
take place.

In order to get some help, one can use
either the \texttt{help} or the \texttt{doc} command.

Finally, the
command for computing limits in Matlab has the following syntax:\\
\matlab{limit(expr, var, point, 'left'/'right')}\\
where \texttt{expr} is a symbolic expression, \texttt{var} is the
variable with respect to which the limit is to be computed,
\texttt{point} is the point at which the limit is computed and either
\texttt{'left'} or \texttt{'right'} (which may be omitted if the limit
is in both directions) indicates the direction of the limit.

\subsection{Drill}
Compute the following limits:
\begin{equation*}
\begin{split}
  & 1) \lim_{x\rightarrow
    \infty}\frac{x^{3}-8}{\sqrt{6x^{6}-x^{3}+1}}\\
  & 2) \lim_{x\rightarrow 0}(e^{x}-1)\log(x+1)\\
  & 3) \lim_{x\rightarrow \pi}\sin(x)^{x-\pi}\\
  & 4) \lim_{x\rightarrow 0^{-}}\abs{x}^{\abs{x}}\\
  & 5) \lim_{x\rightarrow 0^{+}}x^{\sin(x)}\\
  & 6) \lim_{n\rightarrow \infty}(\frac{n}{n+1}-\frac{2}{n-1})^{n}
\end{split}
\end{equation*}

\section{Function definition and plotting}
We shall use anonymous functions for plotting graphs. For example, in
order to plot the function $f(u)=\sin(u)-\frac{1}{1+u^{2}}$ with green
lines, from $-4$ to $2$ using $500$ points, we might use the following
sequence of commands:\\
\matlab{f = @(t) sin(t) - 1./(1+t.\^{}2); \% att!: parentheses and
  dots (`.')}\\
\matlab{x = linspace(-4, 2, 500); \% prepare the points}\\
\matlab{plot(x, f(x), '-g');} 

In order to plot \emph{on the same figure} the function $\cos(t)$, one
needs to tell Matlab ``not to clear'' the graphical window: 
\\
\matlab{hold on \% do not clear the window}\\
\matlab{plot(x, cos(x), '*r'); \% use red stars}

\section{Derivatives, tangent line, etc\dots}
The equation of the tangent line to the graph of $f(x)$, derivable at
$a$ is given by:
\begin{equation*}
  y = f^{\prime}(a)(x-a) + f(a),
\end{equation*}
and the equation of the \emph{normal line} at the same place is
\begin{equation*}
  y = -\frac{1}{f^{\prime}(a)}(x-a) + f(a).
\end{equation*}

Given the anonymous function \texttt{P=@(x)
  x.\^{}2-2*x+1}, for example, one can compute the symbolic derivative
using \texttt{syms}. First of all, one needs a symbolic variable:\\
\matlab{syms u}\\
and now one can compute the derivative, evaluating \texttt{P} on the
symbolic variable and differentiating:\\
\matlab{diff(P(u), u)}\\
The value returned by this operation is a symbolic expresssion
\emph{which cannot be evaluated}. If one wishes to use the result (to
plot it, for example), one needs to transform it into an anonymous function:\\
\matlab{P = @(x) x.\^{}2 - 2*x + 1}\\
\matlab{syms u}\\
\matlab{Q = diff(P(u), u)}\\
\matlab{Q(3) \% error: symbolic expression cannot be evaluated}\\
\matlab{Q\_n = matlabFunction(Q) \% define Q\_n as anonymous}\\
\matlab{Q\_n(3) \% can be evaluated}

\subsection{Examples}
In order to determine if the following function
\begin{equation*}
  f(x) = \left\{
    \begin{array}{l}
      x \sin(1/x)\,\, \text{ if } x>0\\
      0\,\, \text{ if } x\leq 0
    \end{array}
  \right.
\end{equation*}
is continuous at $0$, one needs to compute the left and right limits
at $0$ and compare both with the value of $f$ at $0$.\\
\matlab{clear all; syms x;}\\
\matlab{l\_r = limit(0, x, 0, 'left');}\\
\matlab{l\_l = limit(x*sin(x), x, 0, 'right');}\\
\matlab{l\_r == l\_l \% do they match?}\\
The three of them match in this case, so $f$ is continuous at $0$. Is
it differentiable? Let us compute
\begin{equation*}
  \lim_{h\rightarrow 0+}\frac{f(0+h)-f(0)}{h} \,\text{ and }\, \lim_{h\rightarrow
    0+}\frac{f(0+h)-f(0)}{-h}
\end{equation*}
and compare them. Notice that both limits are \emph{on the right}
because $h>0$ and on one limit it is added, on the other, it is subtracted.\\
\matlab{d\_l = limit(0/-h,h,0,'right');}\\
\matlab{d\_r = limit((h*sin(1/h)-0)/h,h,0,'right');}\\
\matlab{d\_r == d\_l \% do they match?}\\
The second limit does not exist (\texttt{d\_r}), so that $f$ is not
differentiable at $0$. 

\subsection{Solving equations}
Solving symbolic equations (for example, to compute the zeroes of the
derivative) is performed using \texttt{solve}. As in all symbolic
operations, a symbolic variable (declared with \texttt{syms}) is
needed. The command \texttt{solve} returns a \emph{matrix} of values,
on each row a solution (either exact, if possible, or approximate) of
the equation. For example,\\
\matlab{clear all; syms x}\\
\matlab{roots   = solve(x.\^{}2 -2*x +1,x)    \% there are two 1's}\\
\matlab{more\_r = solve(x.\^{}6-2*x\^{}2-1,x) \% exact values}\\
\matlab{another = solve(log(x) - exp(x), x)   \% approx. values}

\section{Exercises}
Each exercise will be solved in a single file. If an exercise has
several sections, separate them with a comment (\texttt{\%}).

\begin{exercise}
  Compute the following limits:
  \begin{equation*}
    \begin{split}
      &  \lim_{x\rightarrow 0}\frac{\log(\tan(7x))}{\log(\tan(2x))}\\
      & \lim_{x\rightarrow
        1}\left(\frac{x}{x-1}-\frac{1}{\log(x)}\right)\\
      & \lim_{x\rightarrow 1}x^{\frac{1}{1-x}} \\
      & \lim_{x\rightarrow a}\frac{\sin(x)-\sin(a)}{x-a}
    \end{split}
  \end{equation*}
\end{exercise}
% %% ejercicios grupo 1

\begin{exercise}
  Derivatives, tangents and normals. Recall \texttt{matlabFunction}
  for the second part.
  \begin{enumerate}
  \item Define $f(x)=\sin(\frac{20}{1+x^{2}})$. Plot it from $-20$ to
    $20$ using $500$ points.
  \item Compute the first derivative $f^{\prime}(x)$ and plot it (on
    the same graph as $f(x)$).
  \item Always on the same graph, plot the tangent line to $f(x)$ at
    $x=1$ and the normal line to $f(x)$ at $x=-1$.
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Compute all the local maxima and minima and the inflection points of
  \begin{equation*}
    f(x) = \frac{x^{3}-3x^{2}+3x-1}{1+x^{2}}
  \end{equation*}
  and plot the tangent line to $f(x)$ at the inflection points in
  order to verify that $f(x)$ is convex on one side and concave on the
  other.
\end{exercise}
% % %%%%%%%%%%%%%%%%%%%%% ejercicios grupo 2

% \section{Ejercicios}
% Se realizar{\'a} cada ejercicio en un fichero denominado \texttt{ej\_2\_numero.m}
% (es decir, \texttt{ej\_2\_1.m}, \texttt{ej\_2\_2.m}, etc.). Si un
% ejercicio requiere varios c{\'a}lculos, sep{\'a}rense con un comentario,
% por favor (utilizar el \texttt{\%} para ello).

% \textbf{1.-} Calc{\'u}lense los siguientes l{\'\i}mites:
% \begin{equation*}
%   \begin{split}
%     &  \lim_{x\rightarrow
%       1}\left(\frac{2x}{x-1}-\frac{1}{3\log(x)}\right)\\
%     & \lim_{x\rightarrow a}\frac{\cos(x)-\cos(a)}{(x-a)^{2}}\\
%     & \lim_{x\rightarrow 1}e^{\log(x)\frac{1}{1-x}} \\
%     &  \lim_{x\rightarrow 1}\frac{\log(\tan(7(x-1)))}{\log(\tan(2(x-1)))}
%   \end{split}
% \end{equation*}

% \textbf{2.-} 
% Real{\'\i}cense las siguientes tareas utilizando funciones
% an{\'o}nimas. Recu{\'e}rdense los comandos \texttt{hold on}, \texttt{clf}
% y \texttt{subplot}.
% \begin{enumerate}
% \item Definir la funci{\'o}n $P(t) = \exp(\sin(t))$. Dibujarla entre
%   $-\pi$ y $\pi$ con saltos de $0.03$ en $0.03$. Definir ahora la funci{\'o}n
%   $Q(t) = \cos(\exp(t))$, dibujarla \emph{en la misma gr{\'a}fica} que
%   la anterior pero con color azul y usando los mismos puntos.
% \item Definir la funci{\'o}n $f(x)=x^{2}-3x+1$. Dibujarla entre $x=-1$ y
%   $x=10$ utilizando puntos a distancia $0.03$. Dibujarla entre $100$ y $110$
%   utilizando puntos separados $0.1$.
% \item Definir la funci{\'o}n $r(t) = \frac{t}{1+t^{2}}$. Dibujarla entre
%   $-3$ y $3$ usando 500 puntos.
% \item Dibujar las funciones $P(x), Q(x), f(x)$ y $r(x)$ en la misma
%   ventana (en cuatro gr{\'a}ficas) utilizando \texttt{subplot}. Los
%   intervalos habr{\'a}n de ser los mismos que arriba.
% \end{enumerate}

% \textbf{3.-}
% Derivadas, tangentes y normales. Recu{\'e}rdese
% \texttt{matlabFunction} para la segunda parte.
% \begin{enumerate}
% \item Def{\'\i}nase la funci{\'o}n
% $f(u)=\frac{1}{1+u^{2}}\sin(u)$. Repres{\'e}ntese gr{\'a}ficamente entre
% $-10$ y $10$ utilizando puntos separados $0.05$.
% \item Calc{\'u}lese la funci{\'o}n derivada $f^{\prime}(u)$ y
%   repres{\'e}ntese \emph{en el mismo gr{\'a}fico que} $f(u)$.
% \item Sobre la gr{\'a}fica, dib{\'u}jese la recta normal a $f(u)$ sobre
%   $u=-1$ y la recta normal sobre $u=2$.
% \end{enumerate}


% \textbf{4.-}
% Dada la funci\'on
% \begin{equation*}
%   f(x) = \frac{x^{2}-1}{\exp(x^2)}
% \end{equation*}
% se pide: calcular, entre los puntos $-10$ y $10$, los valores para los
% que la recta tangente es horizontal y los puntos de inflexi\'on. En
% los puntos de inflexi\'on, dibujar la recta tangente.


% %%%%%%%%%%%%%% ejercicios grupo 3

% \section{Ejercicios}
% Se realizar{\'a} cada ejercicio en un fichero denominado \texttt{ej\_2\_numero.m}
% (es decir, \texttt{ej\_2\_1.m}, \texttt{ej\_2\_2.m}, etc.). Si un
% ejercicio requiere varios c{\'a}lculos, sep{\'a}rense con un comentario,
% por favor (utilizar el \texttt{\%} para ello).

% \textbf{1.-} Calc{\'u}lense los siguientes l{\'\i}mites:
% \begin{equation*}
%   \begin{split}
%     &  \lim_{x\rightarrow 0}\frac{\tan(7x)}{\tan(2x)-\tan(3x)}\\
%     &  \lim_{x\rightarrow
%       1}\left(\frac{2x}{3(x-1)}-\frac{1}{\log(\vert\sin(x)\vert)}\right)\\
%     & \lim_{x\rightarrow a}\frac{\sin(x)-\sin(a)}{x-a}\\
%     & \lim_{x\rightarrow 1}e^{\frac{\log(x)}{1-x}}
%   \end{split}
% \end{equation*}

% \textbf{2.-} 
% Real{\'\i}cense las siguientes tareas utilizando funciones
% an{\'o}nimas. Recu{\'e}rdense los comandos \texttt{hold on}, \texttt{clf}
% y \texttt{subplot}.
% \begin{enumerate}
% \item Definir la funci{\'o}n $s(x) = \cos(x)+\sin(x)$. Dibujarla entre
%   $-\pi$ y $\pi$ utilizando 1000 puntos. Definir ahora la funci{\'o}n
%   $Q(t) = \cos(t)-t$, dibujarla \emph{en la misma gr{\'a}fica} que
%   la anterior pero con color negro y usando los mismos puntos.
% \item Definir la funci{\'o}n $T(u)=u^{5}-2u+1$. Dibujarla entre $x=-3$ y
%   $x=3$ utilizando puntos separados $0.025$. Dibujarla entre $100$ y $110$
%   utilizando puntos separados $0.1$.
% \item Definir la funci{\'o}n $r(x) = \frac{x-1}{1+x^{2}}$. Dibujarla entre
%   $-10$ y $10$ usando puntos separados $0.2$.
% \item Dibujar las funciones $s(x), Q(x), T(x)$ y $r(x)$ en la misma
%   ventana (en cuatro gr{\'a}ficas) utilizando \texttt{subplot}. Los
%   intervalos habr{\'a}n de ser los mismos que arriba.
% \end{enumerate}

% \textbf{3.-}
% Derivadas, tangentes y normales. Recu{\'e}rdese
% \texttt{matlabFunction} para la segunda parte.
% \begin{enumerate}
% \item Def{\'\i}nase la funci{\'o}n
% $H(t)=\exp(\frac{1}{1+t^{2}})$. Repres{\'e}ntese gr{\'a}ficamente entre
% $-20$ y $20$ utilizando  puntos separados $0.33$.
% \item Calc{\'u}lese la funci{\'o}n derivada $H^{\prime}(t)$ y
%   repres{\'e}ntese \emph{en el mismo gr{\'a}fico que} $f(t)$.
% \item Sobre la gr{\'a}fica, dib{\'u}jese la recta tangente a $H(t)$ sobre
%   $t=0$ y la recta normal sobre $t=0$.
% \end{enumerate}

% \textbf{4.-}
% Def\'{\i}nase simb\'olicamente la funci\'on
% \begin{equation*}
%   f(x) = \frac{x^{2}-1}{1+x^{6}}
% \end{equation*}
% y calc\'ulense los puntos en que la derivada es $0$.
% Calc\'ulense tambi\'en las zonas en que la gr\'afica es convexa y
% c\'oncava. Dibujar la recta normal en los puntos de inflexi\'on.



% %%%%%%%%%%%%%% ejercicios grupo 4

% \section{Ejercicios}
% Se realizar{\'a} cada ejercicio en un fichero denominado \texttt{ej\_2\_numero.m}
% (es decir, \texttt{ej\_2\_1.m}, \texttt{ej\_2\_2.m}, etc.). Si un
% ejercicio requiere varios c{\'a}lculos, sep{\'a}rense con un comentario,
% por favor (utilizar el \texttt{\%} para ello).

% \textbf{1.-} Calc{\'u}lense los siguientes l{\'\i}mites:
% \begin{equation*}
%   \begin{split}
%     &  \lim_{x\rightarrow \pi}\frac{\tan(-x)}{\tan(x/3)-\tan(x/4)}\\
%     &  \lim_{x\rightarrow
%       1}\left(\frac{2x}{3(x-1)}-\frac{1}{\log(\vert\sin(x)\vert)}\right)\\
%     & \lim_{x\rightarrow a}\frac{\sin(a)-\sin(x)}{x-a}\\
%     & \lim_{x\rightarrow 1}2^{\frac{\log(x)}{1-x^{3}}}
%   \end{split}
% \end{equation*}

% \textbf{2.-} 
% Real{\'\i}cense las siguientes tareas utilizando funciones
% an{\'o}nimas. Recu{\'e}rdense los comandos \texttt{hold on}, \texttt{clf}
% y \texttt{subplot}.
% \begin{enumerate}
% \item Definir la funci{\'o}n $s(x) = \cos(x)+\sin(x)$. Dibujarla entre
%   $-\pi$ y $\pi$ utilizando 1000 puntos. Definir ahora la funci{\'o}n
%   $Q(t) = \cos(t)-t$, dibujarla \emph{en la misma gr{\'a}fica} que
%   la anterior pero con color negro y usando los mismos puntos.
% \item Definir la funci{\'o}n $T(u)=u^{5}-2u+1$. Dibujarla entre $x=-3$ y
%   $x=3$ utilizando puntos separados $0.025$. Dibujarla entre $100$ y $110$
%   utilizando puntos separados $0.1$.
% \item Definir la funci{\'o}n $r(x) = \frac{x-1}{1+x^{2}}$. Dibujarla entre
%   $-10$ y $10$ usando puntos separados $0.2$.
% \item Dibujar las funciones $s(x), Q(x), T(x)$ y $r(x)$ en la misma
%   gr\'afica, con diferentes colores pero todas con l\'{\i}neas, en
%   el intervalo $-pi, pi$, usando saltos de $.05$.
% \end{enumerate}

% \textbf{3.-}
% Derivadas, tangentes y normales. Recu{\'e}rdese
% \texttt{matlabFunction} para la segunda parte.
% \begin{enumerate}
% \item Def{\'\i}nase la funci{\'o}n
% $H(t)=\exp(\frac{1}{1+t^{2}})$. Repres{\'e}ntese gr{\'a}ficamente entre
% $-20$ y $20$ utilizando  puntos separados $0.33$.
% \item Calc{\'u}lese la funci{\'o}n derivada $H^{\prime}(t)$ y
%   repres{\'e}ntese \emph{en el mismo gr{\'a}fico que} $H(t)$.
% \item Sobre la gr{\'a}fica, dib{\'u}jese la recta tangente a $H(t)$ sobre
%   $t=0$ y la recta normal sobre $t=0$.
% \end{enumerate}

% \textbf{4.-}
% Def\'{\i}nase simb\'olicamente la funci\'on
% \begin{equation*}
%   f(x) = \frac{\exp(x)-1}{1+x^{2}}
% \end{equation*}
% y calc\'ulense los puntos en que la derivada es $0$.
% Calc\'ulense tambi\'en las zonas en que la gr\'afica es convexa y
% c\'oncava. Dibujar la recta normal en los puntos de
% inflexi\'on. Para convertir valores ``exactos'' a n\'umeros, \'usese
% \texttt{double()}. 

% %%%%%%%%%%%%%% ejercicios grupo 5

% \section{Ejercicios}
% Se realizar{\'a} cada ejercicio en un fichero denominado \texttt{ej\_2\_numero.m}
% (es decir, \texttt{ej\_2\_1.m}, \texttt{ej\_2\_2.m}, etc.). Si un
% ejercicio requiere varios c{\'a}lculos, sep{\'a}rense con un comentario,
% por favor (utilizar el \texttt{\%} para ello).

% \textbf{1.-} Calc{\'u}lense los siguientes l{\'\i}mites:
% \begin{equation*}
%   \begin{split}
%     &  \lim_{x\rightarrow \pi}\frac{\tan(-x)}{\tan(x/3)-\tan(x/4)}\\
%     &  \lim_{x\rightarrow
%       1}\left(\frac{2x}{3(x-1)}-\frac{1}{\log(\vert\sin(x)\vert)}\right)\\
%     & \lim_{x\rightarrow a}\frac{\sin(a)-\sin(x)}{x-a}\\
%     & \lim_{x\rightarrow 1}2^{\frac{\log(x)}{1-x^{3}}}
%   \end{split}
% \end{equation*}

% \textbf{2.-} 
% Real{\'\i}cense las siguientes tareas utilizando funciones
% an{\'o}nimas. Recu{\'e}rdense los comandos \texttt{hold on}, \texttt{clf}
% y \texttt{subplot}.
% \begin{enumerate}
% \item Definir la funci{\'o}n $s(x) = \cos(x*\sin(x))+$. Dibujarla entre
%   $-10$ y $10$ utilizando 1000 puntos. Definir ahora la funci{\'o}n
%   $Q(t) = \cos(t)-t$, dibujarla \emph{en la misma gr{\'a}fica} que
%   la anterior pero con color negro y usando los mismos puntos.
% \item Definir la funci{\'o}n $T(u)=u^{5}-2u+1$. Dibujarla entre $x=-3$ y
%   $x=3$ utilizando puntos separados $0.025$. Dibujarla entre $100$ y $110$
%   utilizando puntos separados $0.1$.
% \item Definir la funci{\'o}n $r(x) = \frac{\exp(x)}{1+x^{2}}$. Dibujarla entre
%   $-10$ y $10$ usando puntos separados $0.2$.
% \item Dibujar las funciones $s(x), Q(x), T(x)$ y $r(x)$ en la misma
%   gr\'afica, con diferentes colores pero todas con l\'{\i}neas, en
%   el intervalo $-pi, pi$, usando saltos de $.05$.
% \end{enumerate}

% \textbf{3.-}
% Derivadas, tangentes y normales. Recu{\'e}rdese
% \texttt{matlabFunction} para la segunda parte.
% \begin{enumerate}
% \item Def{\'\i}nase la funci{\'o}n
% $H(t)=t^{3}\exp(-t^2)+3$. Repres{\'e}ntese gr{\'a}ficamente entre
% $-20$ y $20$ utilizando  puntos separados $0.33$.
% \item Calc{\'u}lese la funci{\'o}n derivada $H^{\prime}(t)$ y
%   repres{\'e}ntese \emph{en el mismo gr{\'a}fico que} $H(t)$.
% \item Sobre la gr{\'a}fica, dib{\'u}jese la recta tangente a $H(t)$ sobre
%   $t=0$ y la recta normal sobre $t=0$.
% \end{enumerate}

% \textbf{4.-}
% Def\'{\i}nase simb\'olicamente la funci\'on
% \begin{equation*}
%   f(x) = \frac{5x^{3}-2}{1+x^{2}}
% \end{equation*}
% y calc\'ulense los puntos en que la derivada es $0$.
% Calc\'ulense tambi\'en las zonas en que la gr\'afica es convexa y
% c\'oncava. Dibujar la recta normal en los puntos de
% inflexi\'on. Para convertir valores ``exactos'' a n\'umeros, \'usese
% \texttt{double()}. 


% ejercicios grupo 6

% \section{Ejercicios}
% Se realizar{\'a} cada ejercicio en un fichero denominado \texttt{ej\_2\_numero.m}
% (es decir, \texttt{ej\_2\_1.m}, \texttt{ej\_2\_2.m}, etc.). Si un
% ejercicio requiere varios c{\'a}lculos, sep{\'a}rense con un comentario,
% por favor (utilizar el \texttt{\%} para ello).

% \textbf{1.-} Calc{\'u}lense los siguientes l{\'\i}mites:
% \begin{equation*}
%   \begin{split}
%     & \lim_{x\rightarrow a}\frac{\sin(a)-\sin(x)}{x-a}\\
%     & \lim_{x\rightarrow 1}2^{\frac{\log(x)}{1-x^{3}}}\\
%     &  \lim_{x\rightarrow \pi}\frac{\tan(-x)}{\tan(x/3)-\tan(x/4)}\\
%     &  \lim_{x\rightarrow
%       1}\left(\frac{2x}{3(x-1)}-\frac{1}{\log(\vert\sin(x)\vert)}\right)\\
%   \end{split}
% \end{equation*}

% \textbf{2.-} 
% Real{\'\i}cense las siguientes tareas utilizando funciones
% an{\'o}nimas. Recu{\'e}rdense los comandos \texttt{hold on}, \texttt{clf}
% y \texttt{subplot}.
% \begin{enumerate}
% \item Definir la funci{\'o}n $s(x) = \cos(x^2\cos(x))+1$. Dibujarla entre
%   $-10$ y $10$ utilizando 1000 puntos. Definir ahora la funci{\'o}n
%   $Q(t) = \cos(t)-t$, dibujarla \emph{en la misma gr{\'a}fica} que
%   la anterior pero con color negro y usando los mismos puntos.
% \item Definir la funci{\'o}n $T(u)=u^{3}-2u^2-1$. Dibujarla entre $x=-3$ y
%   $x=3$ utilizando puntos separados $0.025$. Dibujarla entre $100$ y $110$
%   utilizando puntos separados $0.01$.
% \item Definir la funci{\'o}n $r(x) = \frac{1+x^{3}}{\exp(x)}$. Dibujarla entre
%   $-10$ y $10$ usando puntos separados $0.2$.
% \item Dibujar las funciones $s(x), Q(x), T(x)$ y $r(x)$ en la misma
%   gr\'afica, con diferentes colores pero todas con l\'{\i}neas, en
%   el intervalo $-pi, pi$, usando saltos de $.05$.
% \end{enumerate}

% \textbf{3.-}
% Derivadas, tangentes y normales. Recu{\'e}rdese
% \texttt{matlabFunction} para la segunda parte.
% \begin{enumerate}
% \item Def{\'\i}nase la funci{\'o}n
% $H(t)=t^{3}\sin(-t^2)+3$. Repres{\'e}ntese gr{\'a}ficamente entre
% $-20$ y $20$ utilizando  puntos separados $0.33$.
% \item Calc{\'u}lese la funci{\'o}n derivada $H^{\prime}(t)$ y
%   repres{\'e}ntese \emph{en el mismo gr{\'a}fico que} $H(t)$.
% \item Sobre la gr{\'a}fica, dib{\'u}jese la recta tangente a $H(t)$ sobre
%   $t=0$ y la recta normal sobre $t=0$.
% \end{enumerate}

% \textbf{4.-}
% Def\'{\i}nase simb\'olicamente la funci\'on
% \begin{equation*}
%   f(x) = \frac{x^{3}}{1+x^{2}}
% \end{equation*}
% y calc\'ulense los puntos en que la derivada es $0$.
% Calc\'ulense tambi\'en las zonas en que la gr\'afica es convexa y
% c\'oncava. Dibujar la recta normal en los puntos de
% inflexi\'on. Para convertir valores ``exactos'' a n\'umeros, \'usese
% \texttt{double()}. 

\newpage

\section{Solutions to the Exercises}

\setcounter{exercise}{0}
\begin{exercise}
  We shall define anonymous functions first and then compute the
  limits using the \texttt{limit} command. The code in Listing
  \ref{ex1-code} shows
  a way to do this exercise.
  \lstinputlisting[language=Matlab, caption={Code for exercise 1.},
  label=ex1-code]{sols2-1.m}
\end{exercise}

\begin{exercise}
  The only complication in this exercise is the computation of the
  tangent and normal lines. The code in Listing \ref{ex2-code} does
  everything.
  \lstinputlisting[language=Matlab, caption={A solution to exercise
    2.}, label=ex2-code]{sols2-2.m}
\end{exercise}

\begin{exercise}
  This exercise uses \texttt{solve} extensively. Local maxima and
  minima are the points at which the derivative is $0$ (and which are
  not inflection points!). Inflection points are points at which the
  second derivative is $0$ (well, not exactly but mostly). A
  detailed solution can be found in Listing \ref{ex3-code}.
  \lstinputlisting[language=Matlab, caption={A solution to exercise
    3.}, label=ex3-code]{sols2-3.m}
\end{exercise}

\end{document}

