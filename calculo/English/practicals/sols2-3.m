% Exercise 3, practical 2.

% Define the anonymous function. Notice the dots.

f = @(x) (x.^3-3*x.^2+3*x-1)./(1+x.^2);

% We have to compute the derivative and the second derivative, this
% requires a symbol
syms x

fp  = diff(f(x), x);
fpp = diff(f(x), x, 2);

% Find the critical points (derivative = 0):
criticals = solve(fp)
% There are three 'critical points' but only one is real, x=1. So
% this is the only one. Is it a maximum, a minimum? We compute the
% value of the second derivative. To this end, we MUST TRANSFORM
% fpp INTO A NUMERICAL FUNCTION:
fpp_n = matlabFunction(fpp);
fpp_n(criticals(1))
% It turns out to be 0, which is non-informative...

% There are multiple ways to overcome this problem. For example,
% since f(x) is defined on all R (because the denominator never
% vanishes), it is either a global maximum or a global minimum or
% an inflection point and we can tell which of them it is just
% evaluating f at three points.
f(criticals(1)-1) % This is -1
f(criticals(1))   % This is 0
f(criticals(1)+1) % This is 1/5

% This means that the function is INCREASING, hence criticals(1)
% CAN ONLY BE AN INFLECTION POINT!

% Let us plot f to check this
x1 = linspace(-10, 10, 1000);
plot(x1, f(x1));
hold on
% Plot the critical point with a large red circle:
plot(criticals(1), f(criticals(1)), 'ro')
% (One sees clearly that it is an inflection point)

% Now the tangent line at the inflection point should be easy. Remember
% that the tangent line has equation
%   Y = fp(a)*(X-a)+f(a)
% We need to transform fp again into a numerical function
fp_n = matlabFunction(fp);
% There is just one critical point, we need just one equation:
tangent = @(x) fp_n(criticals(1)).*(x-criticals(1))+f(criticals(1));

% No need to hold on, already done above
plot(x1, tangent(x1), 'r')
