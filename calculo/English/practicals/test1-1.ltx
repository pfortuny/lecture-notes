% -*- TeX-master: "practica1.ltx"; -*-
\RequirePackage[spanish]{babel}
\documentclass{amsart}
\usepackage[spanish]{babel}
\title{Calculus - Practical I - Introduction}
\author{Pedro Fortuny Ayuso}
\input{commands}
\date{\today}
\begin{document}
\maketitle

This practical gives a simple introduction to Matlab.

\section{Basic computations}
Matlab works mainly with \emph{vectors}, not with numbers. Another way
to looking at it is saying that it works with \emph{lists} (in a
sense, a vector is no more than a list). This is the natural and usual
way to use it.
\subsection{Making lists}
There are two main commands for building lists:
\begin{description}
\item[\texttt{linspace}] Generates a list of evenly spaced values between two
  numbers:\\ 
    \texttt{linspace(a, b, n)}
  generates a list (vector) of $n$ numbers, evenly spaced between $a$
  and $b$. Try it for several values of $a, b$ and $n$.
  
\item[\texttt{[a:s:b]}] This instruction generates a list of numbers
  starting at $a$, going up to (at most) $b$ and in steps of length
  $s$. Try it.
\end{description}
For example, to assign to a variable $x$ the vector of $200$ evenly
spaced values
between $-\pi$  and $\pi$, one writes:\\
\matlab{x = linspace(-pi, pi, 200);}\\
(For what is the semicolon?).

On the other hand, to assign to $y$ the vector of values between
$-\pi$ and $\pi$ in steps of $0{.}01$, one writes:\\
\matlab{y = -pi:.01:pi;}\\
(For what is the semicolon?).

We are not going to insist on these two instructions, they will be
used continually along the course.

\subsection{Elementary operations}
Let us assign to $x$ the vector of $300$ evenly distributed components
between $-\pi$ and $\pi$:\\
\matlab{x = linspace(-pi, pi, 300);}\\
and compute, for example, the sine of each of those values:\\
\matlab{y = sin(x);}\\
(notice that the semicolon hides the output). Write $y$ without the
semicolon to see its value:\\
\matlab{y}\\
(a list of $300$ numbers, each the sine of the corresponding value of
$x$).

Let us plot $y$ against $x$ (notice that $y$ is a vector and $x$
is another one of the same length: we are plotting a \emph{table}):\\
  \matlab{plot(x,y);}\\
A window with the graph of the sine function should appear. Notice
that simply using $x$ and $y$, which are vectors, we have been able to
plot the whole family of pairs $(x_1,y_{1})$, $(x_2, y_2)$, \dots,
$(x_{300}, y_{300})$. This is what makes Matlab so powerful.

Drill:
\begin{enumerate}
\item Plot the graph of the function $exp(x)$ for $x$ from $-5$ to
  $5$, using steps of size $0{.}03$.
\item Plot the graph of the function  $x^{2}-3x+1$, for $x$ from $-10$
  to $10$ using $300$ points. What happens if one writes
  \texttt{x\^{}2}? An error. Why?
\item Plot the graph of $tan(x)cos(x)$ for $x$ from $-2$ to $3$ using
  steps of length $0{.}025$. What happens if one writes
  \texttt{tan(x)*cos(x)}? An error. Why? 
\end{enumerate}

\subsection{The dotted (\texttt{.}) operators.}
As the student has noticed in the previous examples, if \texttt{x} is a vector
\emph{one cannot compute} \texttt{x*x} (which would be the product of
a vector by itself), nor \texttt{x\^{}2} (raising to the power $2$);
an error happens. This is because element-wise operations (which were
the desired ones) require a \texttt{.} (dot).

In order to multiply, divide and rise to a power vectors elementwise
one has to insert a \texttt{.} \emph{before} the operator.\\
   \matlab{a = linspace(-3, 3, 200); b = linspace(-4, 4, 200);}\\
   \matlab{b = a * b ; \% error}\\
   \matlab{y = a .* b; \% correct \textrm{a$\times$b}}\\
   \matlab{u = a / b; \% error}\\
   \matlab{u = a ./ b; \% correct}\\
   \matlab{v = a \^{} 2; \% error}\\
   \matlab{v = a .\^{} 2; \% correct}\\
This must be clear. We (in Calculus) shall use the dot
\emph{always}. Never forget it. This is unlike in Algebra, where most
of the multiplications are matricial, which are written without the dot.

\section{Anonymous functions}
The second most powerful property of Matlab is the possibility of
defining functions easily. Assume that, for whatever reason, we need
to use the polynomial $P(x)=321x^2-3x+1$, say for plotting it several
times for different sets of points. Having to write it time and again
would be boring and error-prone. To avoid this, one can define a
function representing it, an \emph{anonymous function}:\\
\matlab{P = @(x) 321.*x.\^{}2 - 3.*x + 1}\\
From now on, \texttt{P} represents the function $P(x)$, the polynomial
written above. It can be used quite easily:
\begin{itemize}
\item Plot $P$ between $-5$ and $5$ using $300$ points.
\item Plot $P$ between $-\pi$ and $20$ with steps of $0.025$.
\item Compute $P(3), P(0), P(2132)$. 
\end{itemize}

Anonymous functions can be of several variables. For example,
$f(x,y)=xy - x^{2}+y^{3}$ (\emph{notice the dots!}):\\
\matlab{f = @(x,y) x.*y - x.\^{}2 + y.\^{}3;}\\
Computing $f(1,2), f(2,0), f(-\pi, 7)$ is now trivial.

\section{Symbolic calculus}
In Calculus, one needs to perform symbolic computations many times
(limits, derivatives, integrals\dots). Matlab can handle these
but it is necessary to specify the symbolic variables in advance.

Before proceeding, let us clear all the values of the variables:\\
\matlab{clear all;}\\
(Now neither \texttt{x} nor \texttt{y} nor any of the variables
defined above have any value).

Let us compute the limit of 
$\left(1+\frac{1}{n}\right)^n$ for $n$ tending to infinite
(what is it?). Let us try it straightaway:\\
\matlab{limit((1+1./n).\^{}n,n,inf)}\\
(\texttt{inf} is the symbol for $\infty$ in Matlab).
This gives an error because Matlab \emph{does not know what \texttt{n}
is}. We want it to be a symbol, not a value, so we have to specify it:\\
\matlab{syms n}\\
And let us now repeat the instruction above:\\
\matlab{limit((1+1./n).\^{}n,n,inf)}\\
The answer is \texttt{exp(1)}, which is what is called $e$, obviously
(or not?).

But there are many more operations that can be performed.

Compute the following:
\begin{equation*}
  \begin{split}
    & \lim_{x\rightarrow 0}\frac{\sin(x)}{x};\;\;\; \lim_{x\rightarrow
      \pi}\frac{(x-\pi)^{2}}{\tan(x)^{2}};\;\;\; \lim_{x\rightarrow
      1}(x-1)\log(1-x)\\
    & \lim_{x\rightarrow -\pi}(\cos(x)-1)\tan(x+\pi); \;\;\;  \lim_{x\rightarrow
      0}x^{x}; \;\;\; \lim_{x\rightarrow 3}\frac{x+1}{x-3}.
  \end{split}
\end{equation*}

Derivatives of functions are also easily computed:\\
\matlab{P =@(t) cos(t).*sin(t) - t.\^{}3 + exp(t).*t}\\
defines an anonymous function. If we want to compute its derivative,
we need a symbolic variable first (say \texttt{u}):\\
\matlab{syms u}\\
and now we just need to compute the derivative:\\
  \matlab{diff(P(u),u)}\\
the second derivative:\\
  \matlab{diff(P(u),u,2)}\\
the third one:\\
  \matlab{diff(P(u),u,3)}\\
etc.

\subsection{From symbolic to anonymous}
In older (which are the ones we are using) versions of Matlab, there
is no way to evaluate a symbolic function at a point. For example,
let \texttt{Q} be the symbolic derivative of \texttt{P} above:\\
\matlab{syms u; Q = diff(P(u),u)}\\
and let us try to evaluate it at $3$:\\
  \matlab{Q(3)}\\
gives an error. In order to obtain the equivalent \emph{anonymous}
function, the method \texttt{matlabFunction} is used. Thus,\\
  \matlab{Q\_n = matlabFunction(Q)}\\
creates the function \texttt{Q\_n}, which is the \emph{anonymous}
equivalent of the symbolic one \texttt{Q}. This anonymous version can
be evaluated:\\
  \matlab{Q\_n(3)}\\
gives its value at $3$.

\section{Exercises}
\begin{exercise}
  Given
  \begin{equation*}
    f(x) = \tan(x)x^{3}-\cos(x)+\frac{x+1}{x^{2}+1},
  \end{equation*}
  do the following:
  \begin{enumerate}
  \item Define it as an anonymous function.
  \item Plot its graph between $-4$ and $4$ using $300$ points.
  \item Plot its graph between $-10$ and $10$ in steps of $0{.}01$.
  \item Compute its limits for $x$ tending to $-\infty$, $+\infty$,
    $0$ and $1$.
  \item Compute its first, second and fourth derivatives.
  \end{enumerate}
\end{exercise}

\begin{exercise}
  Same for
  \begin{equation*}
    \begin{split}
      & g(x) = \frac{\tan(x)}{\sin(x/2)}\\
      & h(u) = (e^{u}-1)\log(\vert u\vert+.01)\\
      & k(u) = u^3-u^7\\
      & l(v) = \frac{v}{v^2+1}\\
      & m(t) = \frac{1}{t^{4}+1}
    \end{split}
  \end{equation*}
\end{exercise}

\begin{exercise}
  Plot, \emph{on the same graph}, the following functions (choosing
  freely their domains, etc.):
  \begin{equation*}
    f(x) = \sin(x)\cos(x),\;\;\;g(x) = \exp(x)/10.
  \end{equation*}
  Same (on a different graph) for:
  \begin{equation*}
    f(u) = u^{2} - 2u+1,\;\;\;g(t) = \exp(-t)/10.
  \end{equation*}
  In order to perform these tasks, one uses the instruction
  \texttt{hold on}, which tells Matlab not to use a new graph for the
  next plots. The alternative \texttt{hold off} tells Matlab to use a
  new graph for each plot. In order to clear the graphical space, one
  uses \texttt{clf}.
\end{exercise}

\begin{exercise}
  Plot each of the four functions of the previous
  exercise in a single figure, all figures inside the same window. Use
  the \texttt{subplot} command for doing this. Use a different domain
  for each function.
\end{exercise}
\end{document}

\begin{exercise}
  Perform the following tasks using anonymous functions. Recall the
  commands \texttt{hold on}, \texttt{clf} and \texttt{subplot}.
  \begin{enumerate}
  \item Define the function $f(x)=x^{3}-2x+1$. Plot it from $x=-3$ to
    $x=3$ using $300$ points. Plot it from $100$ to $110$ using points
    separated by $0.1$.
  \item Define $P(t) = \cos(t)-\sin(t)$. Plot it from $-\pi$ to $\pi$
    using 1000 points. Define $Q(t) = \cos(t)+\sin(t)$, plot it on the
    same graph as the previous one but using green lines.
  \item Define $r(t) = \frac{1}{1+t^{2}}$. Plot it from $-10$ to $10$
    with 500 points.
  \item Plot the functions $f(x), P(x), Q(x)$ and $r(x)$ on the same
    window (with four graphs) using \texttt{subplot}. Use the same
    intervals as above.
  \end{enumerate}
\end{exercise}

