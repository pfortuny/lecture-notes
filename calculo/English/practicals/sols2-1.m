% Exercise 1, practical 2.

% First of all, define the appropriate anonymous functions.
% Notice the parenthesis (this is very hard for the students)
% We shall use dotted operators even though they are not
% necessary for this exercise.

f1 = @(x) log(tan(7*x))./log(tan(2*x));

% There is no need to use 'x' to define the functions, this
% is why they are called 'anonymous' because the name of the 
% variable is irrelevant.

f2 = @(t) t./(t-1) - 1./log(t);

f3 = @(t) t.^(1./(1-t));

% for the last one, we need to define 'a' as a symbol!
syms a
f4 = @(x) (sin(x) - sin(a))./(x-a);

% Computing limits is a symbolic operation, so we need a symbol
% to be used as variable.
% We might use 'x' but let us use 'z' instead.
syms z;

% Now we proceed to compute the limits.
% 1) Always assign the results
%    to variables, otherwise they are 'lost' in the run.
% 2) We do not use semicolons in the next lines because we want to
%    see the results.
l1 = limit(f1(z), z, 0)
l2 = limit(f2(z), z, 1)
l3 = limit(f3(z), z, 1)
l4 = limit(f4(z), z, a)
