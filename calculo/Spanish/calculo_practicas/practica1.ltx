% -*- TeX-master: "practica1.ltx"; -*-
%\RequirePackage[spanish]{babel}
\documentclass{amsart}
\usepackage[spanish]{babel}
\usepackage{inputenc}
\title{C\'alculo - Pr\'actica I - Introducci\'on}
\author{Pedro Fortuny Ayuso}
\def\datename{\emph{Fecha:}}
\date{\today}
\begin{document}
\maketitle

En esta pr\'actica trataremos r\'apidamente de la utilizaci\'on de
Matlab.

\section{Cuentas b\'asicas}
Matlab es un programa que trabaja con \emph{vectores}, no con
n\'umeros. Se puede pensar que trabaja con \emph{listas}, tambi\'en
(es este sentido, es lo mismo una lista que un vector). Esa es la
manera natural de utilizarlo.
\subsection{Creaci\'on de listas}
Para crear listas hay dos comandos:
\begin{description}
\item[\texttt{linspace}] Con este se genera una lista de n\'umeros
  entre dos va\-lo\-res, compuesta por una cantidad de elementos
  equidistribuidos:
  \begin{center}
    \texttt{linspace(a, b, n)}
  \end{center}
  genera una lista de $n$ n\'umeros equidistribuidos entre $a$ y $b$,
  comenzando en $a$. Pru\'ebese.
\item[\texttt{[a:s:b]}] Con esta construcci\'on (los corchetes son
opcionales) se genera una lista de n\'umeros que empieza en $a$,
termina en $b$ (o lo m\'as cerca posible antes de $b$) y va de $s$ en
$s$. Pru\'ebese.
\end{description}
Por ejemplo, para asignar a una variable $x$ el vector de 200 valores
entre $-\pi $ y $\pi$, equidistribuidos, se escribe:
\begin{center}
  \texttt{x = linspace(-pi, pi, 200);}
\end{center}
(?`qu\'e ha hecho el punto y coma?).

Mientras que para asignar a $y$ el vector de valores entre $-\pi$ y
$\pi$ yendo de $0.01$ en $0.01$, se escribe
\begin{center}
  \texttt{y = -pi:.01:pi;}
\end{center}
(?`qu\'e ha hecho el punto y coma?).

Estas dos construcciones son tan comunes que no voy a repetirlas: se
dar\'an por supuestas de ahora en adelante.

\subsection{Operaciones elementales}
Asignemos a la variable $x$ el vector de 300 componentes
equidistribuidas entre $-\pi$ y $\pi$:
\begin{center}
  \texttt{x = linspace(-pi, pi, 300);}
\end{center}
y calculemos, por ejemplo, el seno de cada uno de esos valores.
\begin{center}
  \texttt{y = sin(x);}
\end{center}
(no se ve nada, por el punto y coma). Si queremos ver el valor de $y$,
quitemos el punto y coma:
\begin{center}
  \texttt{y}
\end{center}
aparece una lista de 300 n\'umeros, cada uno de ellos es el seno del
valor de $x$ correspondiente. Para asegurarnos, lo dibujamos:
\begin{center}
  \texttt{plot(x,y);}
\end{center}
Debe aparecer una ventana con una gr\'afica de la funci\'on seno. Lo
que hemos hecho es \emph{dibujar} (plot) el conjunto de pares $(x,y)$
donde las $x$ se toman del vector \texttt{x} y las $y$ se toman del
vector \texttt{y}.
\vspace*{15pt}

\noindent\textbf{Ejemplos para realizar:}
\begin{enumerate}
\item Dibujar la gr\'afica de la funci\'on $e^x$ con $x$ entre $-5$
  y $5$, y con saltos de $0.03$ en $0.03$. Téngase en cuenta que en
  Matlab, $e^x$ se representa como \texttt{exp(x)}.
\item Dibujar la gr\'afica de la funci\'on $x^{2}-3x+1$, con $x$
  entre $-10$ y $10$ y usando 300 puntos. ?`Qu\'e pasa si se escribe
  \texttt{x\^{}2}? Que da un error.
\item Dibujar la gr\'afica de la funci\'on $tan(x)cos(x)$ con $x$
  entre $-2$ y $2$ con saltos de $0.025$. ?`Qu\'e pasa si se escribe
  \texttt{tan(x)*cos(x)}. Que da un error
\end{enumerate}

\subsection{Los operadores con punto (\texttt{.})}
Como se deber\'{\i}a haber visto en los ejemplos anteriores, \emph{no
  se puede calcular} \texttt{x*x} (el producto de un vector consigo
mismo) ni \texttt{x\^{}2} (elevar al cuadrado), se produce un error. Esto
es porque las operaciones que se hacen elemento a elemento requieren
un \texttt{.} (punto).

Para multiplicar, dividir y elevar vectores \emph{elemento a elemento}
se ha de escribir un punto \texttt{.} antes de la operaci\'on.
\begin{equation*}
\begin{split}
  & \texttt{a = linspace(-3, 3, 200); b = linspace(-4, 4, 200);}\\
  & \texttt{b = a * b ; \% esto da un error}\\
  & \texttt{y = a .* b; \% esto es lo correcto: } a\times b\\
  & \texttt{u = a / b; \% esto da un error}\\
  & \texttt{u = a ./ b; \% correcto}\\
  & \texttt{v = a \^{} 2; \% error}\\
  & \texttt{v = a .\^{} 2; \% correcto}
\end{split}
\end{equation*}
Esto ha de quedar claro. Lo usaremos siempre. \textbf{SIEMPRE}.

\section{Funciones an\'onimas}
Pero es un l\'{\i}o trabajar siempre escribiendo todas las
ecuaciones. Supongamos que tenemos que calcular muchas veces (o
representar gr\'aficamente) el polinomio $P(x)=321x^2-3x+1$, por la
raz\'on que sea. Lo que no vamos a hacer es repetirlo
continuamente. Para eso se definen las \emph{funciones an\'onimas}:

\begin{center}
  \texttt{P = @(x) 321.*x.\^{}2 - 3.*x + 1}
\end{center}
Ahora \texttt{P} representa la funci\'on $P(x)$, el polinomio del que
hemos hablado arriba y se puede hacer muy f\'acilmente:
\begin{itemize}
\item Representar $P$ entre $-5$ y $5$ usando $300$ puntos.
\item Representar $P$ entre $-\pi$ y $20$ con saltos de $0.025$.
\item Calcular los valores $P(3), P(0), P(2132)$. 
\end{itemize}

Las funciones an\'onimas pueden ser de una o varias variables. Por
ejemplo, $f(x,y)=xy - x^{2}+y^{3}$ (atenci\'on a los puntos):
\begin{center}
  \texttt{f = @(x,y) x.*y - x.\^{}2 + y.\^{}3;}
\end{center}
Calcular $f(1,2), f(2,0), f(-\pi, 7)$ es muy sencillo, ahora.

\noindent\textbf{Ejemplos para realizar:}
\begin{enumerate}
\item Definir una función anónima llamada $f$ que corresponda a
  $x^2-2x+1$. Dibujarla entre $-2$ y $2$ usando $300$ puntos.
\item Definir una función anónima llamada $g$ que corresponda a
  $x^{3}-3x^{2}+2x-1$. Dibujarla entre $-3$ y $5$ con saltos de $0{.}01$.
\item Definir una función anónima llamada $patata$ que corresponda a
  $\cos(e^x)$. Dibujarla entre $-\pi$ y $1$. Dibujarla también entre
  $0$ y $2\pi$.
\item Definir una función anónima llamada $Q$ que corresponda a
  $\sin(\tan(x))$. Dibujarla entre $-\pi/2$ y $\pi/2$.
\end{enumerate}


\section{C\'alculo simb\'olico}
Cuando se trata de hacer un c\'alculo simb\'olico (por ejemplo,
l\'{\i}mites, derivadas, integrales), es preciso especificarle a
Matlab que estamos utilizando una variable simb\'olica.

Primero de todo: limpiemos los valores de las variables:
\begin{center}
  \texttt{clear all;}
\end{center}
(Ahora ni \texttt{x} ni \texttt{y} ni ninguna de las variables que
hemos utilizado antes tienen valor alguno).

Calculemos, por ejemplo, el l\'{\i}mite de
$\left(1+\frac{1}{n}\right)^n$ cuando $n$ tiende a infinito
(?`cu\'anto es esto?). Intent\'emoslo 
sin m\'as:
\begin{center}
  \texttt{limit((1+1./n).\^{}n,n,inf)}
\end{center}
(\texttt{inf} es el s\'{\i}mbolo de infinito en Matlab). Esto da un
error porque Matlab \emph{no sabe lo que es} \texttt{n}. Hay que
indicarle que se trata de una variable simb\'olica:
\begin{center}
  \texttt{syms n}
\end{center}
Y ahora tratamos de repetir la instrucci\'on:
\begin{center}
  \texttt{limit((1+1./n).\^{}n,n,inf)}
\end{center}
Debe mostrar la respuesta \texttt{exp(1)}, que significa $e$, claro.

Pero no est\'a restringido a l\'{\i}mites en el infinito.

Calc\'ulense los siguientes:
\begin{equation*}
  \begin{split}
    & \lim_{x\rightarrow 0}\frac{\sin(x)}{x};\;\;\; \lim_{x\rightarrow
      \pi}\frac{(x-\pi)^{2}}{\tan(x)^{2}};\;\;\; \lim_{x\rightarrow
      1}(x-1)\log(1-x)\\
    & \lim_{x\rightarrow -\pi}(\cos(x)-1)\tan(x+\pi); \;\;\;  \lim_{x\rightarrow
      0}x^{x}; \;\;\; \lim_{x\rightarrow 3}\frac{x+1}{x-3}.
  \end{split}
\end{equation*}

Y, puestos a hacer c\'alculo simb\'olico, se pueden tambi\'en derivar
funciones muy sencillamente:
\begin{center}
  \texttt{P =@(t) cos(t).*sin(t) - t.\^{}3 + exp(t).*t}
\end{center} 
define una funci\'on an\'onima. La variable que se use es indiferente
(igual que da lo mismo hablar de $P(t)$ que de $P(x)$ mientras tanto
$t$ como $x$ ``no signifiquen nada'').

Si ahora queremos calcular $P^{\prime}(u)$ (la derivada de $P$ con
respecto a $u$) utilizamos una variable simb\'olica $u$:
\begin{center}
  \texttt{syms u}
\end{center}
y no tenemos m\'as que derivar:
\begin{center}
  \texttt{diff(P(u),u)}
\end{center}
para calcular la derivada segunda,
\begin{center}
  \texttt{diff(P(u),u,2)}
\end{center}
la derivada tercera
\begin{center}
  \texttt{diff(P(u),u,3)}
\end{center}
etc.

\section{Ejercicios}
\textbf{1.-\/} Dada la funci\'on
\begin{equation*}
  f(x) = \tan(x)x^{3}-\cos(x)+\frac{x+1}{x^{2}+1},
\end{equation*}
se pide:
\begin{enumerate}
\item Definirla como funci\'on an\'onima.
\item Dibujarla entre $-4$ y $4$ usando 300 puntos.
\item Dibujarla entre $-10$ y $10$ con intervalos de $0.01$.
\item Calcular el l\'{\i}mite de $f$ cuando $x$ tiende a $-\infty$,
  $+\infty$, $0$ y $1$.
\item Calcular su derivada, derivada segunda y derivada cuarta.
\end{enumerate}

\textbf{2.-\/} Lo mismo para las siguientes funciones:
\begin{equation*}
  \begin{split}
    & g(x) = \frac{\tan(x)}{\sin(x/2)}\\
    & h(u) = (e^{u}-1)\log(\vert u\vert+.01)\\
    & k(u) = u^3-u^7\\
    & l(v) = \frac{v}{v^2+1}\\
    & m(t) = \frac{1}{t^{4}+1}
  \end{split}
\end{equation*}

\textbf{3.-\/} Se pide dibujar, en la misma figura, las siguientes
funciones (eligiendo el dominio y todo lo dem\'as como se quiera):
\begin{equation*}
  f(x) = \sin(x)\cos(x),\;\;\;g(x) = \exp(x)/10.
\end{equation*}
Y lo mismo (en otro dibujo) con
\begin{equation*}
  f(u) = u^{2} - 2u+1,\;\;\;g(t) = \exp(-t)/10.
\end{equation*}
Para dibujar sobre la misma gr\'afica, se ha de usar \texttt{hold on};
Para limpiar la ventana gr\'afica, se ha de usar \texttt{clf}.

\textbf{4.-\/} Dib\'ujense las 4 funciones del ejercicio anterior en
una misma ventana gr\'afica, pero en distintas im\'agenes. Para ello,
b\'usquese la ayuda de \texttt{subplot}. Cada funci\'on deber\'a tener
dominio distinto. 


\end{document}

