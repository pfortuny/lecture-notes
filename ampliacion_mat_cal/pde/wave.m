% wave equation simulation
% trying
x = 0:.1:10;
dt = 0.25;
T = 0:dt:1000;

phi = -abs(0.5-x/10);
phi_x = [phi(2)-phi(1) diff(phi)(1:end-1) phi(end)-phi(end-1)];
phi_xx = [phi_x(2)-phi_x(1) diff(phi_x)(1:end-1) phi_x(end)-phi_x(end-1)];
phi_t = zeros(size(phi));
plot(x, phi_xx);

for t = T
  phi_t = phi_t + dt * phi_xx;
  phi_t(end)=0;
  phi = phi + dt * phi_t;
  phi(end) = -0.5;
  phi_xx = [0 diff(phi)(2:end)-diff(phi)(1:end-1) 0];
  plot(x, phi);
  axis([0 10 -1.1 0.1]);
  pause(0.01);
end


