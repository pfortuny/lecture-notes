\def\dsxyz{\sqrt{\dot{x}(t)^{2}+\dot{y}(t)^{2}+\dot{z}(t)^{2}}}
\def\dsxy{\sqrt{\dot{x}(t)^{2}+\dot{y}(t)^{2}}}
\chapter{Análisis complejo}
Por sorprendente que parezca, una de las ramas más importantes del
Análisis (y de las Matemáticas en general) es la conocida como
``Variable Compleja'', que consiste en el estudio del Cálculo
Infinitesimal extendido a los números complejos de la manera que vamos
a introducir en seguida.

\section{Funciones complejas derivables}
La noción fundamental del Análisis es la derivabilidad. Para funciones
reales de una variable real, la noción de derivabilidad consiste en
``ser aproximable por una función lineal'' cerca de un punto. Es
decir, una función $f(x)$ es derivable si existe un número
$f^{\prime}(x)$ tal que, si $dx$ es un elemento infinitesimal,
entonces:
\begin{equation*}
  f(x+dx) = f(x) + f^{\prime}(x)dx + \cdots
\end{equation*}
donde los puntos suspensivos indican un elemento infinitesimal de
orden mayor que $dx$ (i.e. \emph{mucho más pequeño}). La noción
equivalente para funciones de números complejos debería ser algo así:
\begin{definition}
  Una función compleja $f(z)$ es \emph{derivable} (en un punto $z$)
  si existe un número $f^{\prime}(z)$ tal que para cualquier
  elemento infinitesimal complejo $dz$, se tiene que
  \begin{equation*}
    f(z+dz) = f(z) + f^{\prime}(z)dz + \cdots
  \end{equation*}
  donde los puntos suspensivos indican un elemento infinitesimal de
  orden mayor que $dz$.
\end{definition}
El caso es que esta definición no da una buena idea computacional ni
elemental de ``cuándo una función compleja es derivable'', que es lo
que tratamos de explicar a continuación.

\subsection{Funciones complejas como pares de funciones reales}
Un número complejo es un par $(a,b)$ de números reales, que puede
escribirse también como $a+ib$, donde $i$ denota ``la raíz cuadrada de
$-1$.'' Las operaciones de suma, producto, etc. son bien conocidas.

Una función compleja de variable compleja, como toda función, es una
\emph{asignación} que, partiendo de un número complejo $z$ da lugar a
otro número complejo $f(z)$. Así pues, si $z=x+iy$, entonces $f(z)$
será un par que depende de $x$ e $y$, y que está constituido por dos
elementos, que llamaremos de ahora en adelante $u(x,y)$ y $v(x,y)$,
las partes \emph{real} e \emph{imaginaria} de $f(z)$:
\begin{equation*}
  f(z) = f(x+iy) = u(x+iy)+iv(x+iy) = (u(x,y),v(x,y))
\end{equation*}
cualquiera de las expresiones anteriores representa el mismo objeto
(el número complejo $f(z)$ imagen de $z=x+iy$ por la función $f$).

\begin{example}\label{ex:funciones-complejas}
  Funciones complejas pueden ser las siguientes:
  \begin{equation*}
    \begin{array}{l}
      f_1(z) = f_1(x+iy) = ix^2\\
      f_2(z) = f_{2}(x+iy) = -y\\
      f_{3}(z) = f_{3}(x+iy) = x^2-iy^2\\
      f_4(z) = f_{4}(x+iy) = \left(x^2+y^2\right)\\
      f_5(z) = f_{5}(x+iy) = e^{x}\left( \cos y + i \sen y \right)\\
      f_6(z) = f_{6}(x+iy) = x^2-y^2 + 2xy i\\
      f_7(z) = f_{7}(x+iy) = \frac{1}{x^2+y^2}\left( -y + ix \right)
    \end{array}
  \end{equation*}
  y es un ejercicio interesante calcular las partes real e imaginaria
  de cada ejemplo.
\end{example}

Sin embargo, si se toma, por ejemplo, $f_1(z)$ del ejemplo anterior,
se puede observar lo siguiente: por un lado, si se considera el
infinitésimo $dx$ (es decir, se hace $dy=0$):
\begin{equation*}
  f_1(x+iy+dx) = i(x+dx)^2=i\left(x^2+2xdx+dx^2\right) = i x^2
    + 2xidx + \cdots
\end{equation*}
mientras que si se toma $dx=0$, entonces
\begin{equation*}
  f_1(x+iy+idy) = ix^2 = ix^2 + 0idx + \cdots
\end{equation*}
por lo que $f(z+dz)$ \emph{no puede escribirse} como $f(z) +
f^{\prime}(z)dz + \cdots$, pues por un lado debería ser
$f^{\prime}(z)=2x$ y por otro $f^{\prime}(z)=0$.

Así pues, hace falta alguna propiedad especial, además de que $u(x,y)$
y $v(x,y)$ (las partes real e imaginaria de $f(z)$) sean derivables
para que $f(z)$ sea ``derivable como función compleja.''

Calculemos, para empezar, utilizando la expresión de $f(z)$ como un
par de funciones reales: si $z=x+iy$ entonces
\begin{equation*}
  f(z) = u(x,y) +iv(x,y) = (u(x,y),v(x,y)).
\end{equation*}
Tomemos un infinitésimo general $dz = dx + idy=(dx,dy)$, donde $dx$ y $dy$ son
independientes. Se tiene que
\begin{equation*}
  f(z+dz) = (u(x+dx,y+dy),v(x+dx,y+dy)).
\end{equation*}
Asumiendo que $u(x,y)$ y $v(x,y)$ son diferenciables, se tiene
\begin{equation}\label{eq:cr1}
  f(z+dz) = \left( u(x,y) + \frac{\partial u}{\partial x}dx +
    \frac{\partial u}{\partial y}dy + \cdots,
  v(x,y) + \frac{\partial v}{\partial x}dx + \frac{\partial v}{\partial
    y}dy + \cdots\right).
\end{equation}

Por otro lado, si $f(z)$ ``debe'' comportarse como una función
``compleja derivable'', lo lógico es que
\begin{equation*}
  f(z+dz) = f(z) + f^{\prime}(z)dz + \dots,
\end{equation*}
para cierto $f^{\prime}(z)$. Si se escribe esta última igualdad usando
la expresión compleja clásica, queda
\begin{equation*}
  \label{eq:cr3}
  u(x,y)+iv(x,y) + \left(\overline{u}(x,y) +i \overline{v}(x,y)\right)
  (dx + idy) + \dots,
\end{equation*}
que, multiplicando, resulta
\begin{equation}
  \label{eq:cr3}
  u(x,y)+iv(x,y)+
  \left( \overline{u}(x,y)dx-\overline{v}(x,y)dy
  +iu(x,y)dy + iv(x,y)dx\right).
\end{equation}

Puesto que se quiere que \eqref{eq:cr1} sea lo mismo que
\eqref{eq:cr3}, igualando las partes reales e imaginarias y los
coeficientes de $dx$ y de $dy$, tienen que cumplirse las condiciones
siguientes:
\begin{equation*}
  \begin{array}{ll}
    \displaystyle \frac{\partial u}{\partial x} = \overline{u}(x,y) &
    \displaystyle \frac{\partial u}{\partial y} = -\overline{v}(x,y)\\[1em]
    \displaystyle \frac{\partial v}{\partial x} = \overline{v}(x,y) &
    \displaystyle \frac{\partial v}{\partial y} = \overline{u}(x,y).
  \end{array}
\end{equation*}
Y, como los números $\overline{u}(x,y)$ y $\overline{v}(x,y)$ pueden
ser cualesquiera (pues solo se quiere que $f(z)$ tenga derivada, no se
exige ningún valor), se concluye que

\begin{theorem}
  Para que una función compleja $f(z)$ sea ``derivable'' como función
  compleja, se han de cumplir las \emph{ecuaciones de Cauchy-Riemann}:
  \begin{equation}
    \label{eq:cauchy-riemann}
    \frac{\partial u}{\partial x} = \frac{\partial v}{\partial
      y},\,\,\,
    \frac{\partial u}{\partial y} = -\frac{\partial v}{\partial x}.
  \end{equation}
\end{theorem}

\begin{exercise*}
  Del Ejemplo \ref{ex:funciones-complejas}, compruébese qué funciones
  son derivables como funciones complejas y cuáles no.
\end{exercise*}

Fácilmente se comprueba que la suma, el producto y el recíproco (cuando
la función no es $0$) de cualquier función compleja derivable, es
derivable también. Las funciones constantes lo son, evidentemente, y
su derivada es $0$. Además, la composición de funciones derivables es
derivable (y su derivada se calcula con la regla de la
cadena). Finalmente, la función inversa de una función derivable
\emph{cerca de un punto} es también derivable (pero esta propiedad es
local, no puede ``usarse en todo el plano complejo'' sin más).

\begin{example}\label{ex:funciones-holomorfas}
  Otros ejemplos de funciones derivables son (el alumno debería comprobar
  que alguna lo es):
  \begin{equation*}
    \begin{array}{l}
      f_1(z) = z\\
      f_2(z) = z^n\\
      f_3(z) = e^z = e^x(\cos y + i \sen i)\\
      f_4(z) = \sqrt[m]{z}\,\mathrm{\ cuando\ }z\neq 0\\[0.5ex]
      f_5(z) = \sen z = \displaystyle \frac{e^{iz}-e^{-iz}}{2i}\\[1ex]
      f_6(z) = \cos z = \displaystyle \frac{e^{iz} + e^{-iz}}{2}
    \end{array}
  \end{equation*}
\end{example}

De lo anterior se deduce, por ejemplo, que cualquier función del tipo
\begin{equation*}
  f(z) = \frac{P(z)}{Q(z)}
\end{equation*}
donde $P(z)$ y $Q(z)$ son polinomios en $z$ es derivable fuera de los
ceros de $Q(z)$, como en el caso real.

\section{Funciones holomorfas}
Las ecuaciones \eqref{eq:cauchy-riemann} de Cauchy-Riemann son solo
una parte de la noción de ``derivabilidad'' compleja. En realidad, una
función $f(z)$ que las cumple es, automáticamente, mucho más rica de
lo que parece. En efecto:

\begin{theorem}\label{the:funcion-holomorfa}
  Sea $f(z) = u(x,y)+iv(x,y)$ una función compleja de variable
  compleja definida en un
  dominio $U$ del plano complejo. Las siguientes propiedades son
  equivalentes:
  \begin{enumerate}
  \item La función $f(z)$ satisface las ecuaciones de Cauchy-Riemann
    en todo $U$.
  \item\label{ite:power-series}
    En cualquier punto $z_{0}\in U$, se puede expresar $f(z)$ como una
    suma infinita
    \begin{equation*}
      f(z) = \sum_{n=0}^{\infty} a_n(z-z_0)^n
    \end{equation*}
    (lo que se denomina una serie de potencias centrada en $z_0$), que
    converge (i.e. define un número) en un círculo centrado en $z_0$ y
    contenido en $U$.
  \item\label{ite:green} Si $V\subset U$ es un dominio \emph{simplemente
      conexo}\footnote{Recuérdese que esto significa que ``no tiene
      agujeros.''} con borde una curva cerrada simple $\gamma$, entonces la integral
    (que se calcula del modo evidente) siguiente es siempre nula.
    \begin{equation*}
      \int_{\gamma}f(z)\,dz = \int_{\gamma}(u(x,y) + i v(x,y))\,(dx+idy)
      = 0.
    \end{equation*}
  \end{enumerate}
  Además, fijado $z_0$, los coeficientes $a_n$ de la serie de
  \ref{ite:power-series} se calculan con la \emph{igualdad de Gauss}:
  \begin{equation*}
    a_{n} = \frac{1}{2\pi i} \int_{C_{\varepsilon}}
    \frac{f(z)}{(z-z_0)^{n+1}}\,dz 
  \end{equation*}
  donde $C_{\varepsilon}$ es una circunferencia pequeña contenida en $U$ y tal
  que el disco que contiene también está contenido en $U$.
\end{theorem}

Lo primero que aparece claramente en el teorema enunciado es que la
integración forma parte importante de la derivabilidad, lo cual es
sorprendente a primera vista. Como se observará, la integración
compleja tiene un papel fundamental en toda la teoría (mucho más que
la derivación, en realidad).

Demostremos sucintamente que la propiedad de la derivabilidad (esto
es, satisfacer las ecuaciones de Cauchy-Riemann) implica la propiedad
\ref{ite:green}: como $dz=dx+idy$, se tiene que
\begin{equation*}
  I_{\gamma}=\int_{\gamma}f(z)\,dz = \int_{\gamma} (u(x,y)+iv(x,y))\,(dx + i dy),
\end{equation*}
que, multiplicando, queda
\begin{equation*}
  I_{\gamma} = \left(\int_{\gamma} u(x,y)\,dx - v(x,y)\,dy\right) +
  i\left(\int_{\gamma} u(x,y)\,dy + v(x,y)\,dx\right),
\end{equation*}
ahora hacemos uso de la Fórmula de Green: como $\gamma$ rodea al
dominio $V$, \emph{que es simplemente conexo}, se tiene que
\begin{equation*}
  I_{\gamma} = \int_V -\frac{\partial v}{\partial x} - \frac{\partial
    u}{\partial y} \,dxdy +
  i \int_V \frac{\partial u}{\partial x} - \frac{\partial v}{\partial y}\,dxdy
\end{equation*}
pero las igualdades de Cauchy-Riemann dicen justamente que tanto la
parte real de $I_{\gamma}$ como su parte imaginaria son $0$, así que
\begin{equation*}
  I_{\gamma} = 0.
\end{equation*}
(La implicación contraria es justamente el mismo razonamiento ``hacia
atrás'').

La propiedad \ref{ite:power-series} se puede expresar diciendo que
``las funciones derivables complejas son la generalización de los
polinomios'', aunque esta frase no tenga mucho sentido. De todos
modos, a partir de ahora utilizaremos la nomenclatura tradicional:

\begin{definition}
  Una función que cumple cualquiera (y, por tanto, todas) de las
  propiedades del Teorema \ref{the:funcion-holomorfa} se llamará
  \emph{holomorfa}.
\end{definition}

\subsection{Los coeficientes}
Sin duda ninguna, la propiedad más importante de las funciones
holomorfas es la que se enuncia al final del Teorema
\ref{the:funcion-holomorfa}: los coeficientes de la serie de potencias
que representa a la función cerca de cada punto $z_0$ pueden
calcularse con una integral de línea que rodea a dicho
punto. Comprobémoslo: supongamos que
\begin{equation*}
  f(z) = \sum_{n=0}^{\infty}a_n(z-z_0)^n,
\end{equation*}
``cerca'' del punto $z_0$. Sea $\varepsilon>0$ un número real tal que
el círculo de centro $z_0$ y radio $\varepsilon$ esté incluido en el
dominio $U$ (donde $f(z)$ está definida). Consideremos la integral
\begin{equation*}
  I = \int_{C_{\varepsilon}} \frac{f(z)}{(z-z_0)^{m+1}}\,dz
\end{equation*}
Si desarrollamos en serie la función $f(z)$ dentro de la integral,
queda
\begin{equation*}
  I = \int_{C_{\varepsilon}} \sum_{n=0}^{\infty}
  a_n\frac{(z-z_0)^n}{(z-z_0)^{m+1}}\, dz.
\end{equation*}
[Por razones técnicas que no vamos a explicar] se puede intercambiar
la integral y la suma infinita, así que
\begin{equation*}
  I = \sum_{n=0}^{\infty} a_n\int_{C_{\varepsilon}} (z-z_0)^{n-m-1}\,dz 
\end{equation*}
(obsérvese que el exponente de $(z-z_0)$ es negativo para $n\geq
m$. Parametricemos la circunferencia
  \begin{equation*}
    C_{\varepsilon} \equiv z=z_0 + \varepsilon e^{i t},\,\,\,t\in[0,2\pi],
  \end{equation*}
y sustituyamos en I:
\begin{equation*}
  I = \sum_{n=0}^{\infty} a_n \int_0^{2\pi} \left( \varepsilon e^{i t}
  \right)^{n-m-1}\,\varepsilon i e^{i t}\,dt.
\end{equation*}
Finalmente, estudiemos cada integral interna, que llamaremos $I_n$:
\begin{equation*}
  I_n = i\varepsilon^{n-m} \int_0^{2\pi}e^{(n-m)i t}\,dt.
\end{equation*}
Si $n\neq m$, el valor de $I_n$ es\footnote{Aunque no se haya
  estudiado integración compleja, que se verá posteriormente, ya se ha
dicho que tanto la derivación como la integración han de funcionar
como en el caso de variables reales, así que esta igualdad es
elemental de comprobar.}
\begin{equation*}
I_n = i \varepsilon^{n-m}
\left.\frac{e^{(n-m)ti}}{i(n-m)}\right|_{t=0}^{t=2\pi} = 0
\end{equation*}
pues $e^{0i}=1=e^{2\pi i}$. El caso especial es $n=m$:
\begin{equation*}
  I_m = i\varepsilon^0 \int_0^{2\pi}\,dt = 2\pi i
\end{equation*}
que, \emph{milagrosamente}, no depende de $\varepsilon$.

De toda la explicación previa se obtiene que
\begin{equation*}
  I = a_{m}I_m = 2\pi i a_m
\end{equation*}
de donde se deduce la fórmula de Cauchy: para cualquier número natural
$n\geq 0$:
\begin{equation*}
  a_n = \frac{1}{2\pi i} \int_{C_{\varepsilon}} \frac{f(z)}{(z-z_0)^{n+1}}\,dz.
\end{equation*}
Esta fórmula es, quizás, la más importante de la variable compleja.

%%% Local Variables:
%%% TeX-master: "00father.ltx"
%%% minor-mode: cdlatex
%%% TeX-engine: xetex
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
