\def\dsxyz{\sqrt{\dot{x}(t)^{2}+\dot{y}(t)^{2}+\dot{z}(t)^{2}}}
\def\dsxy{\sqrt{\dot{x}(t)^{2}+\dot{y}(t)^{2}}}
\chapter{Campos Vectoriales, curvas, superficies\dots}

% \begin{remark*}[Curso 2008-2009] Esta parte de los apuntes es mucho
%   menos detallada y precisa de lo que deber\'{\i}a, por necesidades de
%   disponibilidad y por falta de tiempo para redactarla correctamente
%   (es un tema demasiado delicado como para hacer unas notas deprisa y
%   corriendo con pretensiones de precisi\'on).
% \end{remark*}

En este cap\'{\i}tulo se introduce muy sucintamente la teor\'{\i}a de
campos de vectores en el plano y en el espacio, con el objetivo de
enunciar el Teorema de Stokes (p\'agina ~\pageref{the:stokes}), que es
quiz\'a la 
aplicaci\'on m\'as importante, %de todo el curso, 
junto con sus
consecuencias (el Teorema de Gauss, principalmente). La integraci\'on
de campos en curvas y superficies es el contexto en el que el teorema
de Stokes se encuadra, pero tambi\'en posee inter\'es en s\'{\i}
misma, como aplicaci\'on de las integrales m\'ultiples al c\'alculo de
masas (y, por ende, de longitudes y \'areas) y momentos.

En todos los enunciados habr\'a probablemente imprecisiones te\'oricas
(aunque procuraremos evitarlas), pero ha de tenerse en cuenta que el
objetivo del curso es m\'as pr\'actico que te\'orico y que
posiblemente ninguna de esas imprecisiones ocurra en los ejemplos que
el alumno pueda encontrarse en el futuro (p.ej. todas las
consideraciones podr\'{\i}an hacerse para campos de tipo
$\mathcal{C}^{2}$, pero vamos a hacerlas para campos
$\mathcal{C}^{\infty}$ y vamos a suponer que todas las fronteras de
los conjuntos son \emph{m\'{\i}nimamente normales}, es decir,
\emph{anal\'{\i}ticas a trozos}, por no decir \emph{semialgebraicas}). 

\section{Curvas}
Comenzamos con el estudio de las curvas.
\begin{definition}\label{def:curva}
  Una \emph{curva} en ${\mathbb R}^{3}$ (\'o ${\mathbb R}^{2}$, pero
  de aqu\'{\i} en adelante utilizaremos siempre ${\mathbb R}^{3}$ e
  identificaremos ${\mathbb R}^{2}$ con el plano $z=0$) es una
  aplicaci\'on de un intervalo $[a,b]\subset {\mathbb R}$ en ${\mathbb
    R}^{3}$
  \begin{equation*}
    \begin{array}{rcl}
      \gamma:[a,b] &\rightarrow &{\mathbb R}^{3}\\
      t & \mapsto & (x(t),y(t),z(t)) 
    \end{array}
  \end{equation*}
  Se denota $\gamma$, $\gamma(t)$ o bien $(x(t),y(t),z(t))$. Se
  supondr\'a de aqu\'{\i} en adelante
  que las ``ecuaciones'' de las coordenadas son funciones con
  derivadas de todos los \'ordenes.
\end{definition}
Una curva tiene asociada una colecci\'on de objetos que pasamos a
enumerar:
\begin{definition}\label{def:objetos-de-curva}
  Si $\gamma:[a,b]\rightarrow {\mathbb R}^{3}$ es una curva cuyas
  ecuaciones vienen dadas por las coordenadas $(x(t),y(t),z(t))$, se define:
  \begin{itemize}
  \item La \emph{trayectoria} de $\gamma$ es el conjunto $\Gamma$ dado
    por el recorrido de la aplicaci\'on $\gamma$; es decir,
    $\Gamma = \gamma([a,b])$.
  \item El \emph{vector tangente} a $\gamma$ en un punto
    $(x(t),y(t),z(t))$ es
    $(\dot{x}(t),\dot{y}(t),\dot{z}(t))$ donde
    \begin{equation*}
      \dot{x}(t) = \frac{dx(t)}{dt},\,\dot{y}(t)=\frac{dy(t)}{dt},\,
      \dot{z}(t) = \frac{dz(t)}{dt}
    \end{equation*}
    En general, el vector tangente a una curva se denota siempre
    $d\bar{s}$ y se puede llamar tamb\'en \emph{elemento de longitud}
    de una curva.
  \item La \emph{velocidad} de una curva $\gamma$ en un punto
    $(x(t),y(t),z(t))$ es la norma (longitud) del vector tangente:
    \begin{equation*}
      ds = \dsxyz\,dt
    \end{equation*}
    que es \emph{la longitud recorrida en un tiempo diferencial}, si
    se supone que $t$ es ``el tiempo''.
  \end{itemize}
\end{definition}

Vamos a utilizar las siguientes nociones en el futuro:
\begin{definition}\label{def:curva-simple}
  Una curva $\gamma$ se dice \emph{simple} si es \emph{inyectiva} (es
  decir, si no pasa dos veces por el mismo punto).
\end{definition}

\begin{definition}\label{def:curva-cerrada}
  Una curva $\gamma$ es cerrada si $\gamma(a)=\gamma(b)$, es decir si
  el punto inicial y el final coinciden. Un curva cerrada tambi\'en se
  puede denominar \emph{ciclo}.
\end{definition}

\begin{definition}\label{def:curva-cerrada-simple}
  Una curva $\gamma$ se dice que es \emph{cerrada simple} si es una
  curva cerrada y $\gamma$ es inyectiva en $[a,b)$.
\end{definition}

Con estos conceptos se puede ya comenzar a hacer algunos c\'alculos
interesantes. Supongamos que $f:\Gamma\rightarrow {\mathbb R}$ es una
funci\'on que toma valores en la trayectoria de una curva
$\gamma:[a,b]\rightarrow {\mathbb R}$. La \emph{integral de
  l\'{\i}nea} corresponde a la idea de integrar a lo largo de la
trayectoria de $\gamma$, no solo de intervalos en ${\mathbb R}$:
\begin{definition}\label{def:integral-linea}
  La \emph{integral de l\'{\i}nea} de $f$ a lo largo de $\gamma$ se
  define como la integral en $[a,b]$ de la funci\'on
  ``retrotra\'{\i}da'' al intervalo ponderada por la velocidad:
  \begin{flalign*}
    \int_{\gamma}f &= \int_{a}^{b}f(x(t),y(t),z(t))\,ds =\\
    &= \int_{a}^{b}f(x(t),y(t),z(t))\dsxyz\,dt
  \end{flalign*}
  Si $\gamma$ es una curva cerrada (un ciclo), se suele denotar la
  integral de l\'{\i}nea como
  \begin{equation*}
    \oint_{\gamma}f
  \end{equation*}
\end{definition}

Como es obvio, la integral de l\'{\i}nea de una funci\'on constante
corresponde a la idea de \emph{masa} de la curva suponiendo que la
densidad lineal es dicha constante. Como la masa es la curva coincide
con su longitud si la densidad es uno, se puede definir

\begin{definition}\label{def:longitud-curva}
  La \emph{longitud} de una curva es la integral de l\'{\i}nea de la
  funci\'on constante $1$ sobre la curva:
  \begin{equation*}
    long(\gamma) = \int_{\gamma}ds = \int_{a}^{b}\dsxyz\,dt
  \end{equation*}
\end{definition}

Y, por otro lado
\begin{definition}\label{def:masa-curva}
  Dada una funci\'on $f:\Gamma\rightarrow {\mathbb R}$, se define
  la \emph{masa} de la curva $\gamma$ (o, equivalentemente de
  $\Gamma$) de densidad $f$ como la integral de $f$ a lo largo de
  $\gamma$:
  \begin{equation*}
    masa(\gamma) = \int_{\gamma}f\,ds,
  \end{equation*}
  que no es m\'as que la ``suma de todas las densidades por unidad
  de longitud''.
\end{definition}

\section{Superficies}
La noci\'on de superficie se corresponde con la de aplicaci\'on de un
\emph{abierto} de ${\mathbb R}^{2}$ en el espacio. 
% Como este es un
% curso muy b\'asico m\'as que utilizar la noci\'on general de dominio,
% vamos a trabajar solo con conjuntos \emph{bien delimitados}, como en
% la p\'agina~
% \pageref{the:integracion-conjunto-bien-delimitado-vertical}.
\begin{definition}\label{def:superficie}
  Una superficie es una aplicaci\'on $T:D\rightarrow {\mathbb R}^{3}$
  de un abierto $D\subset{\mathbb R}^{2}$ a ${\mathbb R}^{3}$
  cuyas ecuaciones
  \begin{equation*}
    T(u,v) = (x(u,v),y(u,v),z(u,v))
  \end{equation*}
  son diferenciables un n\'umero infinito de veces. El recorrido de la
  superficie (es decir, el conjunto $T(D)$ imagen del 
  abierto $D$ en ${\mathbb R}^{3}$, la \emph{superficie} ``en s\'{\i}'')
  se denotar\'a $|T|$ o bien con la letra $S$.  
\end{definition}

Igual que las curvas, las superficies tienen asociado un vector, que
en este caso es \emph{normal} (no tangente):
\begin{definition}\label{def:vector-normal-superficie}
  Dada una superficie $T:D\rightarrow {\mathbb R}^{3}$, el
  \emph{vector normal} a $T$ en cada punto $T(u,v)$ es el vector
  \begin{equation*}
    d\bar{S} = \bar{T}_{u}\times \bar{T}_{v}\,dudv
  \end{equation*}
  donde $\bar{T}_{u}$ y $\bar{T}_{v}$ son, respectivamente, el ``vector
  tangente en la direcci\'on $u$'' y el ``vector tangente en la
  direcci\'on $v$'':
  \begin{equation*}
    \bar{T}_{u}=\left(
      \frac{\partial x}{\partial u},
      \frac{\partial y}{\partial u},
      \frac{\partial z}{\partial u}
    \right)\,
    \bar{T}_{v}=\left(
      \frac{\partial x}{\partial v},
      \frac{\partial y}{\partial v},
      \frac{\partial z}{\partial v}
    \right)
  \end{equation*}
  y $\times$ representa el producto vectorial.
\end{definition}
As\'{\i} que el vector normal, en coordenadas, se escribe en cada
punto $T(u,v)$ como
\begin{equation*}
  d\bar{S} = \left|
    \begin{array}{ccc}
      i & j & k\\
      \frac{\partial x}{\partial u} & 
      \frac{\partial y}{\partial u} &
      \frac{\partial z}{\partial u} \\[3pt]
      \frac{\partial x}{\partial v} &
      \frac{\partial y}{\partial v} &
      \frac{\partial z}{\partial v}
    \end{array}
    \right|
\end{equation*}
Su norma (es decir, la ra\'{\i}z cuadrada de su m\'odulo al cuadrado)
se denota siempre $dS$.

Con este concepto se puede ya definir la integral de una funci\'on
sobre una superficie:

\begin{definition}\label{def:integral-superficie-funcion}
  Dada una superficie $T:D\rightarrow {\mathbb R}^{3}$ de recorrido
  $S$ y una funci\'on
  $f:S\rightarrow {\mathbb R}$, se define la \emph{integral de
    superficie} de $f$ sobre $T$ (o sobre $S$) como la integral de $f$
  ``retrotra\'{\i}da a $D$'' seg\'un $T$:
  \begin{flalign*}
    \iint_{S}f &= \int_{D}f(T(u,v))\,dS =\\
    &= \int_{D}f(T(u,v)) \|\bar{T}_{u}\times\bar{T}_{v}\| \,du\,dv
  \end{flalign*}
  donde la \'ultima integral es una integral doble en $D$.
\end{definition}

Si $f(x,y,z)$ se entiende como una \emph{densidad}, entonces la
definici\'on que acabamos de dar es
\begin{definition}\label{def:masa-sSuperficie}
  La \emph{masa} de la superficie $S$ con densidad $f$ es la integral
  de superficie de $f$ en $S$.
\end{definition}

Y, puesto que el \'area coincide con la masa cuando la densidad es
$1$:
\begin{definition}\label{def:area-superficie}
  El \emph{\'area} de $S$ es la integral de superficie de la
  funci\'on $1$ en $S$:
  \begin{equation*}
    area(S) = \iint_{S}dS = \int_{D}\|\bar{T}_{u}\times \bar{T}_{v}\|\,du\,dv
  \end{equation*}
\end{definition}

A veces, al vector normal $\|\bar{T}_{u}\times \bar{T}_{v}\|$ se le denota
por $\bar{n}$.

\section{Campos de vectores y su integraci\'on}
Comenzamos el estudio sistem\'atico de los \emph{campos de vectores},
para lo cual primero los definimos
\begin{definition}\label{def:campo-vectores}
  Un \emph{campo de vectores} en un abierto $D \subset {\mathbb
    R}^{n}$ es una aplicaci\'on 
  $\bar{F}:D\rightarrow {\mathbb R}^{n}$. Vamos a asumir
  en todo momento que los campos de vectores tienen coordenadas
  diferenciables un n\'umero arbitrario de veces salvo quiz\'a en un
  subconjunto \emph{peque\~no}.
\end{definition}

Ha de entenderse un campo de vectores como una \emph{flecha} que, por
lo general, indicar\'a una \emph{fuerza} \'o una \emph{velocidad} en
cada punto del espacio (la fuerza ejercidad por el campo gravitatorio
sobre una unidad de masa o la velocidad de una part\'{\i}cula que se
mueve en un lugar del espacio\dots). El que las coordenadas de la
aplicaci\'on $\bar{F}$ sean diferenciables no es m\'as que un
requerimiento t\'ecnico para facilitar todo el estudio (si no
fuera as\'{\i}, la t\'ecnica ser\'{\i}a mucho m\'as compleja).

El primer concepto \emph{integral} relativo a un campo es el de
\emph{integral} a lo largo de una trayectoria, que puede entenderse
(es quiz\'a la noci\'on m\'as natural) como el \emph{trabajo} ejercido
por un campo $\bar{F}$
al moverse una part\'{\i}cula siguiendo una curva.
Dado un campo $\bar{F}$ en ${\mathbb R}^{3}$ (lo mismo vale para
${\mathbb R}^{2}$) 
y una curva $\gamma:[a,b]\rightarrow {\mathbb R}^{3}$, si escribimos
el campo $\bar{F}=(F_{1},F_{2},F_{3})$, se define
\begin{definition}\label{def:integral-de-linea}
  La \emph{integral de l\'{\i}nea} de $\bar{F}$ a lo largo de $\gamma$
  es la integral en $[a,b]$ del producto escalar de $\bar{F}$ con el
  vector velocidad:
  \begin{equation*}
    \int_{\gamma}\bar{F} = \int_{a}^{b}\bar{F}\cdot d\bar{s}
  \end{equation*}
  (recu\'erdese que $d\bar{s}$ denota el vector velocidad de $\gamma$)
  que, en coordenadas 
  se escribe (en ${\mathbb R}^{3}$): 
  \begin{equation*}
    \int_{\gamma}\bar{F} = \int_{a}^{b}F_{1}\,dx +
    \int_{a}^{b}F_{2}\,dy + \int_{a}^{b}F_{3}\,dz
  \end{equation*}
  que, a su vez, no es m\'as que
  \begin{flalign*}
    \int_{\gamma}\bar{F}= 
    &\int_{a}^{b}F_{1}(x(t),y(t),z(t))\dot{x}(t)\,dt + \\
    +&\int_{a}^{b}F_{2}(x(t),y(t),z(t))\dot{y}(t)\,dt + 
    \int_{a}^{b}F_{3}(x(t),y(t),z(t))\dot{z}(t)\,dt
  \end{flalign*}
\end{definition}

Como se ve, si la curva $\gamma$ es perpendicular a $\bar{F}$ en todos
los puntos, el campo no realiza ning\'un trabajo, mientras que se
$\gamma$ tiene la misma direcci\'on que $\bar{F}$, el trabajo es
m\'aximo \'o m\'{\i}nimo, dependiendo del sentido. N\'otese que es el
trabajo \emph{realizado por el campo}. Finalmente, es un buen
ejercicio comprobar que \emph{el trabajo realizado no depende de la
  velocidad}: si $\tilde{\gamma}:[c,d]\rightarrow {\mathbb R}^{n} $ es
otra parametrizaci\'on simple de la misma curva $\Gamma$, y $d\tilde{s}$ es
su elemento de
velocidad, entonces
\begin{equation*}
  \int_{\tilde{\gamma}}\bar{F}\,d\tilde{s} = \int_{\gamma}\bar{F}\,d\bar{s}
\end{equation*}
suponiendo que $\gamma$ tambi\'en era simple
(este resultado no es m\'as que un cambio de variable).

\begin{example}\label{def:circulacion}
  Si $\bar{F}$ es el campo de velocidades de un fluido y $\gamma$ es
  un ciclo (una curva cerrada), se
  denomina \emph{circulaci\'on} de 
  $\bar{F}$ a la integral
  \begin{equation*}
    \oint_{\gamma}\bar{F}\,d\bar{s}
  \end{equation*}
\end{example}

\begin{example}
  La intersecci\'on de una esfera con un cilindro de radio mitad y que
  pasa por el centro de la esfera se denomina \emph{b\'oveda de
    Viviani}\margin{La b\'oveda de Viviani}.
\end{example}

La otra noci\'on clave es la que relaciona un campo de vectores con
una superficie: el \emph{flujo}. Supongamos que $\bar{F}$ es un campo
de vectores en ${\mathbb R}^{3}$ y $S$ es una superficie (es decir,
$S$ es la imagen de $T:D\rightarrow {\mathbb R}^{n}$ donde $D$ es un
abierto de ${\mathbb R}^{2}$). Entonces
\begin{definition}\label{def:flujo}
  El \emph{flujo} de $\bar{F}$ en $S$ es la integral del
  producto escalar de $\bar{F}$ por el vector normal $\bar{n}$ a $S$:
  \begin{equation*}
    \iint_{S}\bar{F}\,d\bar{S} = \int_{D}\left(\bar{F}\cdot
      \bar{n}\right)\,dS = \int_{D}\bar{F}\cdot\|T_{u}\times
    T_{v}\|\,du\,dv
  \end{equation*}
\end{definition}

Si el campo tiene la misma direcci\'on que el vector
normal, el flujo es \emph{positivo}, mientras que si tiene direcci\'on
opuesta, el flujo es \emph{negativo}. Este es uno de los lugares en
que la \emph{orientaci\'on} del vector normal es importante.

\subsection{El operador Nabla}

Por lo general, los campos de vectores que se encuentran en
F\'{\i}sica no son cualesquiera, y los problemas de campos de vectores
relacionan unos con otros. Existe un ``operador'' que se utiliza mucho
(aunque no sea m\'as que un artificio de notaci\'on) para construir
campos a partir de otros campos o de funciones escalares, el
\emph{operador Nabla} (con la notaci\'on $i,j,k$ para los vectores
coordenados):
\begin{equation*}
  \nabla = i\parc{}{x} + j\parc{}{y} + k \parc{}{z}
\end{equation*}

\begin{definition}\label{def:gradiente-rotacional-divergencia}
Supongamos que $f$ es una funci\'on en ${\mathbb R}^{3}$ y que
$\bar{F}$ es un campo de vectores en ${\mathbb R}^{3}$. Se definen:
\begin{itemize}
\item El \emph{gradiente} de $f$ es el campo de vectores dado por
  \begin{equation*}
    \nabla f = \nabla \cdot f = \left(\parc{f}{x},\parc{f}{y},\parc{f}{z}\right)    
  \end{equation*}
\item El \emph{rotacional} del campo $\bar{F}$ es es producto
  vectorial de $\nabla$ por $\bar{F}$:
  \begin{flalign*}
    \rot \bar{F} &= \nabla \times \bar{F} = \left|
      \begin{array}{ccc}
        i & j & k\\
        \parc{}{x} & \parc{}{y} & \parc{}{z} \\
        F_{1} & F_{2} & F_{3}
      \end{array}
      \right|=\\
      &= 
      \left( \parc{F_{3}}{y} - \parc{F_{2}}{z}\right) i +
      \left( \parc{F_{1}}{z} - \parc{F_{3}}{x}\right) j +
      \left( \parc{F_{2}}{x} - \parc{F_{1}}{y}\right) k
  \end{flalign*}
\item Y, finalmente, la \emph{divergencia} de un campo de vectores
  $\bar{F}$ es un campo escalar (es decir, una funci\'on), producto
  escalar del operador nabla
  con $\bar{F}$:
  \begin{equation*}
    \diver \bar{F} = \nabla \cdot \bar{F} = \parc{F_{1}}{x}
    + \parc{F_{2}}{y} + \parc{F_{3}}{z}
  \end{equation*}
\end{itemize}
\end{definition}

Cada uno de los campos definidos arriba representa, hasta cierto
punto, un concepto f\'{\i}sio:

\begin{description}
\item[El gradiente] indica la direcci\'on (e intensidad) de la
  m\'axima pendiente de las superficies de nivel de una
  funci\'on. La direcci\'on del gradiente es normal (perpendicular) a
  dichas superficies de nivel.
\item[El rotacional] indica la \emph{tendencia} al giro de un campo de
  vectores cuando este se interpreta como la velocidad en cada punto
  de un fluido. Si en un punto se ubicara una esfera ``muy peque\~na'',
  esta girar\'{\i}a (o no) seg\'un la forma del campo de vectores. El
  eje de giro viene indicado por el rotacional y la velocidad angular
  es la mitad del m\'odulo.
\item[La divergencia] indica la \emph{tendencia a expandirse o
    comprimirse} de un
  campo de vectores, cuando este se interpreta como un campo de
  velocidades de un fluido. Cuanto m\'as positiva es la divergencia,
  m\'as tiende el fluido a ``expandirse'' (salir) del punto, cuanto
  m\'as negativa, m\'as tiende el fluido a ``comprimirse'' (entrar) en
  el punto. Otra manera de visualizarlo es como el cambio (con el
  signo opuesto) de densidad
  (si un fluido se comprime, la densidad aumenta, si un fluido se
  expande, la densidad disminuye). Los fluidos incompresibles se
  mueven seg\'un trayectorias sin divergencia.
\end{description}

Un campo de vectores cuyo rotacional es cero se denomina
\emph{irrotacional} y uno cuya divergencia es cero se denomina
\emph{incompresible} \'o \emph{solenoidal}.

Hay dos igualdades fundamentales:
\begin{theorem*}
  Los campos gradientes no giran y los campos rotacionales son
  incompresibles:
  \begin{flalign*}
    \nabla \times \nabla f = 0\\
    \nabla \cdot \left( \nabla \times \bar{F}\right) = 0
  \end{flalign*}
\end{theorem*}

\input{04topologia.ltx}

\section{El teorema de Stokes}

Todas las definiciones y propiedades anteriores se orientan  al que
posiblemente sea el resultado m\'as profundo de todo este curso, el
Teorema de Stokes, que relaciona el flujo del rotacional de un campo
de vectores a trav\'es de una superficie con el trabajo realizado por
el campo en el borde de la superficie. Comenzamos por un lema algo
m\'as simple: el Teorema de Green, pero previamente necesitamos hablar
algo de \emph{bordes} de superficies, aunque lo haremos solo en el
contexto en que trabajaremos siempre.

A partir de ahora, si $D$ es un dominio de Stokes y
$\gamma:[a,b]\rightarrow {\mathbb R}^{2}$ es una de las curvas del
borde, diremos que $\gamma$ \emph{est\'a bien orientada} si $D$
\emph{queda a la izquierda} del vector tangente a $\gamma$. Si no se
especifica lo contrario, cada vez que se tome una curva del borde de
un dominio de Stokes, se supondr\'a que \emph{est\'a bien
  orientada}. En cualquier caso, si $C$ es la imagen de $\gamma$, se
denotar\'a $C^{+}$ a la curva \emph{bien orientada} y $C^{-}$ a la curva
orientada negativamente.

Enunciamos el resultado primero que se conoci\'o, aunque es el menos
informativo:

\begin{theorem}[de Green]\label{the:green}
  Sea $D$ un dominio de Stokes cuyo borde se supone que es la imagen
  de una sola
  curva cerrada simple (llamamemos $C$ al borde). Sean $P$ y
  $Q$ dos funciones de ${\mathbb
    R}^{2}$ diferenciables. Entonces
  \begin{equation*}
    \oint_{C^{+}}P\,dx + Q\,dy = \int_{D}\parc{Q}{x}-\parc{P}{y}\,dx\,dy
  \end{equation*}
\end{theorem}
(N\'otese que la primera integral es sobre una curva y la segunda
sobre un dominio entero, i.e. una superficie).

En realidad, este resultado no es m\'as que una sencilla consecuencia
del siguiente, que es el realmente importante para superficies:

\begin{theorem}[de Stokes]\label{the:stokes}
  Sea $D$ un dominio de Stokes y $C_{1},\dots,C_{n}$ las curvas
  cerradas que determinan su borde. Sea $T:D\rightarrow {\mathbb
    R}^{3}$ una superficie \emph{definida fuertemente en $D$} tal que
  su borde es 
  la imagen de $C_{1}\cup\dots\cup C_{n}$ por una extensi\'on de
  $T$. Supongamos que $\bar{F}$ es un campo de vectores en ${\mathbb
    R}^{3}$. Entonces
  \begin{equation*}
    \iint_{S}\nabla \times \bar{F} \, d\bar{S} = \int_{\partial S}\bar{F}\,d\bar{s}
  \end{equation*}
  donde $S$ es la superficie definida por $T$ y $d\bar{s}$ denota el
  elemento de longitud en cada componente del borde de $S$, $\partial
  S$.
\end{theorem}

En definitiva, para conocer el flujo de un rotacional, basta calcular
el trabajo del campo original en el borde.

Para vol\'umenes, se tiene el siguiente resultado:

\begin{theorem}[de Gauss o de la Divergencia]\label{the:gauss}
  Supongamos que $\Omega$ es un abierto de ${\mathbb R}^{3}$ y que
  $\partial \Omega$ es una superficie orientada que es uni\'on finita
  de las im\'agenes de superficies $T_{i}:D_{i}\rightarrow {\mathbb
    R}^{3}$. Sea $\bar{F} $ un campo vectorial infinitamente diferenciable
  fuertemente definido en 
  $\Omega$. Entonces
  \begin{equation*}
    \iiint_{\Omega}\nabla \cdot \bar{F} \,dx\,dy\,dz =
    \iint_{\partial \Omega}\bar{F}\,d\bar{S}
  \end{equation*}
  o dicho de otra forma,
  \begin{equation*}
    \iiint_{\Omega}(\diver \bar{F})\,dx\,dy\,dz =
    \iint_{\partial \Omega}\bar{F}\cdot \bar{n}\,dS
  \end{equation*}
  donde $\bar{n}$ es el vector normal a $\partial \Omega$.
\end{theorem}

Lo que quiere decir la igualdad anterior es que para calcular la
``divergencia total'' de un campo en un volumen, basta calcular el
flujo del campo por el borde (la cantidad total de entrada y salida de
un campo en un volumen se sabe en el borde y es precisamente el flujo).

La \emph{orientaci\'on} que se toma para la superficie respecto del
volumen es la que deja el vector normal ``hacia el exterior'' del
volumen.

\section{Consecuencias y otras propiedades}

Quiz\'as la consecuencia f\'{\i}sica m\'as clara del Teorema de Stokes
es la propiedad de los gradientes de ser conservativos.

\begin{definition}\label{def:campo-conservativo}
  Un campo de vectores $\bar{F}$, \emph{fuertemente definido} en un
  abierto $A\subset {\mathbb R}^{n}$,
  se dice 
  \emph{conservativo} si el trabajo realizado por el campo no depende
  de la trayectoria. Es decir, si $\gamma_{1}$ y $\gamma_{2}$ son dos
  curvas que unen los puntos $P$ y $Q$, entonces
  \begin{equation*}
    \int_{\gamma_{1}}\bar{F}\,d\bar{s}_{1} = \int_{\gamma_{2}}\bar{F}\,d\bar{s}_{2}
  \end{equation*}
  donde $d\bar{s}_{1}$ y $d\bar{s}_{2}$ son los elementos de longitud
  (vectores tangentes \'o velocidades)
  correspondientes a 
  $\gamma_{1}$ y $\gamma_{2}$ respectivamente.
\end{definition}

Es sencillo probar que si $A$ es un abierto tal que dos puntos
cualesquiera se pueden unir mediante una curva, entonces

\begin{theorem}
  Si $\bar{F}=\nabla f$ es el gradiente de $f$ en $A$,
  entonces $\bar{F}$ es conservativo.
\end{theorem}

Pero no solo ocurre esto, sino tambi\'en que

\begin{theorem}
  Supongamos que $\bar{F}$ est\'a fuertemente definido en un abier\-to conexo
  $A\subset {\mathbb R}^{n}$ y es conservativo. Entonces existe una
  funci\'on (que se denomina
  \emph{potencial}) $f:A\rightarrow {\mathbb R}$ tal que $\bar{F} = \nabla f$.
\end{theorem}

Ahora bien, la propiedad ``irrotacional'' de los campos de vectores
gradientes es \emph{casi} suficiente, pero no del todo\dots

\begin{example}
  Sea $S\subset {\mathbb R}^{3}$ el ``tubo'' $1< x^{2}+y^{2} <2$ y sea
  $\bar{F}$ el campo de vectores fuertemente definido en $S$ por las
  ecuaciones 
  \begin{equation*}
    \bar{F}(x,y,z)=\left(\frac{-y}{x^{2}+y^{2}}, \frac{x}{x^{2}+y^{2}},0\right).
  \end{equation*}
  Este campo tiene rotacional $0$ pero \emph{no es conservativo} (y
  por tanto no es el gradiente de una funci\'on), pues la integral en
  cualquier camino cerrado que ``rodee'' al borde interior es $2\pi$.
\end{example}

El problema del ejemplo anterior es que el tubo es un conjunto abierto
que \emph{no es simplemente conexo}: hay curvas cerradas simples que
no se pueden ``deformar'' dentro del conjunto hasta convertirlas en un punto.

El caso es que eso no ocurre en ${\mathbb R}^{3}$ cuando se le quitan
unos pocos puntos:

\begin{theorem}
  Si $\bar{F}$ es un campo de vectores definido en ${\mathbb R}^{3}$
  salvo quiz\'a en un n\'umero finito de puntos, entonces son
  equivalentes:
  \begin{itemize}
  \item $\bar{F}$ es conservativo.
  \item Para cualquier curva cerrada simple $\gamma$, se tiene
    \begin{equation*}
      \oint_{\gamma}\bar{F} = 0
    \end{equation*}
  \item Existe una funci\'on $f$ definida en ${\mathbb R}^{3}$ salvo
    quiz\'a un n\'umero finito de puntos tal que $\bar{F}=\nabla f$.
  \item $\bar{F}$ es irrotacional: $\rot\bar{F} =0$.
  \end{itemize}
\end{theorem}

Este resultado \emph{no es cierto en ${\mathbb R}^{2}$}, pues si al
plano se le quita una cantidad finita de puntos, resulta un conjunto
que \emph{no es} simplemente conexo (hay curvas cerradas simples que
no se pueden contraer a ``puntos''). El resultado para el plano es el
siguiente

\begin{theorem}
  Sea $\bar{F}$ un campo vectorial definido en ${\mathbb R}^{2}$ salvo
  en un n\'umero finito de puntos. Entonces
  \begin{enumerate}
  \item Para cualquier curva $\gamma$ cerrada simple se tiene
    \begin{equation*}
      \oint_{\gamma}\bar{F}\,d\bar{s} = 0
    \end{equation*}
    si y solo si el campo es conservativo.
  \item Si se tiene la igualdad anterior para cualquier curva cerrada
    simple, entonces $\bar{F}$ es el gradiente de alguna funci\'on
    $f$, es decir $\bar{F}=\nabla f$ definida en todo ${\mathbb
      R}^{2}$ menos quiz\'as en los puntos en que $\bar{F}$ no lo est\'a.
  \end{enumerate}
\end{theorem}

%%% Local Variables:
%%% mode: LaTeX
%%% minor-mode: cdlatex
%%% TeX-master: "00father.ltx"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-engine: xetex
%%% End:
