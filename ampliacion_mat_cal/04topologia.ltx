\section{Anexo topol\'ogico}

Antes de continuar
necesitamos algunas nociones muy b\'asicas de \emph{to\-po\-lo\-g\'{\i}a},
solo para poder enunciar los resultados con precisi\'on, sin pretender
que estos conceptos sean importantes, pues al final el alumno siempre
va a trabajar con conjuntos ``bien delimitados'', o por
conjuntos delimitados por curvas muy sencillas. En cualquier caso,
pensamos que el esfuerzo por entender estos conceptos merece la pena.

Quiz\'as la noci\'on m\'as importante de todas es la de \emph{conjunto
abierto}, que corresponde a la idea intuitiva de conjunto cuyos puntos
est\'an ``bien contenidos'':
\begin{definition}\label{def:abierto}
  Un conjunto $A\subset {\mathbb R}^{n}$ se llama \emph{abierto} si
  cualquier punto $x\in A$ tiene una ``bola'' alrededor totalmente
  contenida en $A$. Es decir, si para cualquier $x\in A$ existe un
  $\varepsilon >0$ tal que el conjunto
  \begin{equation*}
    B_{\varepsilon}(x) = \left\{y\in {\mathbb R}^{n}\big\vert\,
      |y-x|<\varepsilon\right\}
  \end{equation*}
  est\'a contenido en $A$:
  \begin{equation*}
    B_{\varepsilon}(x) \subset A.
  \end{equation*}
\end{definition}
Es f\'acil comprender un poco mejor la noci\'on de conjunto abierto si
se tiene en cuenta lo siguiente:

\begin{remark}
  Cualquier conjunto definido por \emph{desigualdades estrictas} de
  funciones continuas es un abierto.
\end{remark}

\begin{example}
  Por tanto, los siguientes conjuntos son abiertos:
  \begin{itemize}
  \item Los semiespacios $ax+by+cz+d>0$, para cualesquiera $a,b,c$ y $d$.
  \item Los ``cubos'' abiertos: $(a,b)\times (c,d) \times (e,f)$.
  \item Las \emph{esferas}: $B_{r}(x)$ definidas como arriba.
  \end{itemize}
\end{example}

Adem\'as, se tiene que
\begin{theorem*}
  Cualquier uni\'on de abiertos es un abierto y las intersecciones
  \emph{finitas} de abiertos son abiertas.
\end{theorem*}

La segunda noci\'on importante es la de \emph{frontera}: los puntos
que est\'an ``cerca'' de un conjunto y de su complementario.

\begin{definition}\label{def:frontera}
  Dado un conjunto $A\subset {\mathbb R}^{n}$, la \emph{frontera} de
  $A$, denotada $\partial A$ es el conjunto de puntos tales que
  cualquier bola centrada en ellos contiene a un punto de $A$ y uno
  que no es de $A$ (del complementario de $A$, que se denota $A^{c}$).
\end{definition}

Por lo general, si $A$ es un abierto definido por una desigualdad, la
frontera de $A$ se corresponde con el conjunto definido por ``la
igualdad'':

\begin{example}
  Se tiene:
  \begin{itemize}
  \item   La frontera de una bola es la esfera ``externa'': si $A$ es la bola
  $B_{\varepsilon}(x)$, entonces $\partial A=S_{\varepsilon}(x)$,
  donde
  \begin{equation*}
    S_{\varepsilon}(x) = \left\{y\in{\mathbb R}^{n}\big\vert\, |y-x|=\varepsilon\right\}.
  \end{equation*}
  \item La frontera de un semiespacio es un plano: si $A$ es el
  semiespacio $ax+by+cz+d>0$, entonces $\partial A$ es el plano $ax+by+cz+d=0$.
  \end{itemize}
\end{example}

Aunque la siguiente noci\'on se suele utilizar en otros contextos,
pensamos que es \'optima para enunciar con precisi\'on los teoremas
siguientes.

\begin{definition}\label{def:bien-contenido}
  Diremos que un abierto $A$ est\'a \emph{bien contenido} en otro
  abierto $B$ si $B$ incluye a $A$ y a la frontera de $A$. Es decir,
  si cualquier punto ``muy cercano'' a $A$ est\'a en $B$. Si $A$
  est\'a bien contenido en $B$, se escribir\'a $A\sqsubset B$.
\end{definition}

La noci\'on anterior es \'util para lo siguiente, que no es m\'as que
la definici\'on correcta de ``funci\'on diferenciable en un abierto y
en su frontera'':

\begin{definition}\label{def:funcion-definida-fuertemente}
  Sea $A$ un abierto de ${\mathbb R}^{n}$. Diremos que una funci\'on
  $f:A\rightarrow {\mathbb
    R}$ diferenciable un n\'umero $k$ de veces con continuidad est\'a
  \emph{definida fuertemente en $A$} si existen un abierto $B$ tal que
  $A\sqsubset B$ y una funci\'on
  $\tilde{f}:B\rightarrow {\mathbb R}$ de un abierto $B$ a ${\mathbb
    R}$ que es diferenciable un n\'umero $k$ de veces con continuidad,
  que coincide con $f$ en $A$.
\end{definition}

Un campo de vectores $\bar{F}=(F_{1},F_{2},F_{3})$ est\'a
\emph{definido fuertemente en $A$} si cada componente $F_{i}$ lo
est\'a. Lo mismo una aplicaci\'on $T:A\rightarrow {\mathbb R}^{m}$.

Finalmente,

\begin{definition}\label{def:dominio-stokes}
  Un \emph{dominio de Stokes} es un abierto $A$ de ${\mathbb R}^{2}$
  tal que su frontera 
  $\partial A$ es una \emph{uni\'on finita} de curvas cerradas simples.
\end{definition}