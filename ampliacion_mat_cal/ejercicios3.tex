% -*- TeX-master: "ejercicios.ltx" -*-
%\setcounter{chapter}{2}
\chapter{Ecuaciones Diferenciales}

\vspace*{5pt}
\begin{tcolorbox}[breakable,enhanced]
%  \begin{minipage}{0.9\linewidth}
    \begin{remark*}
      Las ecuaciones diferenciales fundamentales que se utilizan en el
      curso son:
      \begin{enumerate}
      \item\label{ite:cons-masa}
        El \emph{principio de conservación de la masa}. En un sistema en
        el que hay entrada y salida de masa con flujos respectivos
        \begin{equation*}
          \phi_{<}(t), \, \phi_{>}(t)
        \end{equation*}
        (flujo de entrada y flujo de salida, en unidades de masa por
        unidad de tiempo), si la masa en el sistema en el instante $t$
        es $m(t)$, entonces
        \begin{equation*}
          \frac{dm(t)}{dt} = \phi_{<}(t) - \phi_{>}(t)
        \end{equation*}
        pues, claramente,
        \begin{equation*}
          dm(t) = \left( \phi_{<}(t) - \phi_{>}(t) \right)\,dt
        \end{equation*}
        (esta igualdad es, precisamente, el principio de conservación
        de la masa). Como se ve, esta ley es de primer orden.
      \item\label{ite:newton-2} La \emph{segunda Ley de Newton}. Un sistema mecánico
        compuesto por una partícula, que ocupa una posición $x(t)$,
        sujeta a una familia de fuerzas 
        $F_1(t)$, \dots, $F_n(t)$ (que pueden variar en función del
        tiempo) cumple la siguiente ley:
        \begin{equation*}
          \ddot{x}(t) = m(t)\left(F_1(t)+\dots+F_n(t)\right)
        \end{equation*}
        donde $m(t)$ es la masa de la partícula en el instante $t$
        (puede variar, por ejemplo, en un cohete que gasta
        combustible).
      \item\label{ite:interes}
        La ley del \emph{interés}. Si un capital $c(t)$ está depositado a
        un interés en tiempo continuo de $r(t)$ ---podría variar con
        el tiempo--- y se le añade más capital con un flujo de
        $\phi_{<}(t)$ y se gasta con un flujo $\phi_{>}(t)$, entonces
        \begin{equation*}
          \frac{dc(t)}{dt} = \phi_{<}(t) - \phi_{>}(t) + c(t)r(t),
        \end{equation*}
        que es una ecuación de primer orden. El flujo de entrada
        $\phi_{<}(t)$ se puede entender como una inversión periodica y
        el de salida como un gasto periodico.
      \item\label{ite:enfriamiento}
        La ley de \emph{enfriamiento} de Newton. Un cuerpo que
        posee una temperatura $T(t)$ en un ambiente a temperatura
        $A(t)$ que está más frío, pierde temperatura de manera proporcional al
        gradiente:
        \begin{equation*}
          \frac{dT(t)}{dt} = k(T(t) - A(t)).
        \end{equation*}
        La constante $k$ depende de la materia de que está formado el
        cuerpo (y, también de la
        superficie expuesta al ambiente, pero esto se obvia).
      \end{enumerate}
    \end{remark*}
    Por otro lado, la ecuación diferencial
    \begin{equation}\label{eq:solucion-ode-orden-1}
      y^{\prime} + ay = b
    \end{equation}
    donde $a$ y $b$ son constantes tiene como solución general:
    \begin{equation*}
      y = k e^{-ax} + \frac{b}{a}
    \end{equation*}
    donde $k$ es una constante que depende de la condición inicial:
    \begin{equation*}
      k = y(0) - \frac{b}{a}.
    \end{equation*}
    Esta solución de esta ecuación diferencial es sencilla de derivar
    (se puede ver en los apuntes de teoría varias veces).
%\end{minipage}
\end{tcolorbox}
\vspace*{5pt}


\begin{exercise}
  Una piscina con $1000\mathrm{l}$
  de agua contiene una disolución de alcohol al $1\%$ en
  volumen. Tiene una salida por la que desagua a
  $1\mathrm{l}/\mathrm{s}$ y una entrada por la que se vierte la misma
  cantidad de agua pero con una concentración de alcohol del $3\%$ en
  volumen. Escribir la evolución de la concentración dentro de la
  piscina, calcular en qué
  momento llega al $2\%$ y calcular la concentración tras
  $1\mathrm{h}$. La densidad del alcohol es
  $0{.}79\mathrm{g}/\mathrm{cm}^3$. 

  ¿Es el volumen de la piscina relevante para el problema?
\end{exercise}
\begin{solution}
  Llamemos $m(t)$ a la \emph{masa} de alcohol (en $\mathrm{kg}$)
  en la piscina en el instante
$t$. Por el principio de conservación de la masa (ver página
\pageref{ite:cons-masa}),
$m(t)$ cumple que
\begin{equation*}
  \frac{dm(t)}{dt} = \phi_{<}(t) - \phi_{>}(t)
\end{equation*}
donde $\phi_{<}(t)$ es el flujo entrante y $\phi_{>}(t)$ el flujo
saliente  (ambos con dimensiones $MT^{-1}$, claro). Calculemos estos
flujos. El de entrada:
\begin{equation*}
  \phi_{<}(t) = 3\%\cdot 1\mathrm{l}/\mathrm{s}\cdot
  0{.}789\mathrm{kg}/\mathrm{l} = 0{.}02367 \mathrm{kg}/\mathrm{s}.
\end{equation*}
El de salida: la concentración es la que hay en la piscina en ese
momento, que será $m(t)$ kilogramos dividido entre $1000\mathrm{l}$.
Así que
\begin{equation*}
  \phi_{>}(t) = \frac{m(t)\mathrm{kg}}{1000\mathrm{l}}  \cdot
  1 \mathrm{l}/\mathrm{s} = m(t) \mathrm{kg}/\mathrm{s}.
\end{equation*}
Por tanto:
\begin{equation*}
  \frac{dm(t)}{dt} = 0{.}02367 - m(t).
\end{equation*}
Esta ecuación diferencial está resuelta en general en la página
\eqref{eq:solucion-ode-orden-1}. En este caso, tenemos
\begin{equation*}
  y^{\prime} + y = 0{.}02367,
\end{equation*}
por tanto
\begin{equation*}
  y(t) = y(0)\cdot 0{.}02367e^{t}+ 0{.}02367,
\end{equation*}
que está en unidades de masa. La masa inicial es $y(0)$, que se nos
dice corresponde a un $1\%$ en volumen, es decir $10\mathrm{l}\times
0{.}79\mathrm{kg}/\mathrm{l} = 7{.}9\mathrm{kg}$. Por tanto
\begin{equation*}
  m(t) = 0{.}186993e^t + 0{.}02367.
\end{equation*}

Se nos pregunta cuándo llega la
concentración al $2\%$, es decir (puesto que la piscina tiene
$1000\mathrm{l}$), cuándo hay $20\mathrm{l}$ de alcohol, o lo que es
lo mismo, $20\cdot 0{.}79=15{.}8\mathrm{kb}$ de alcohol.  

\end{solution}


\begin{exercise}
  El periodo de semidesintegración del radio es de $1600$ años. Se
  parte de una masa de $2\mathrm{g}$. Calcúlese en qué momento se
  tendrán $1{.}99g$. ¿Qué masa habrá dentro de $100$ años?

  Misma pregunta con el plutonio $238$, cuyo periodo de
  semidesintegración es $88$ años.
\end{exercise}
\begin{solution}
  La ecuación de la radiación (y del decaímiento radiactivo) es del
  mismo tipo que la del enfriamiento. Si la masa en un instante $t$ es
  $m(t)$, entonces
  \begin{equation*}
    \dot{m}(t) = -km(t)
  \end{equation*}
  donde $k>0$ es una constante que es intrínseca a la sustancia
  radiactiva. (Obsérvese que la ecuación es la misma que la del
  enfriamiento si el entorno está a $0^{\circ}$). Como se sabe, la
  solución de esta ecuación es
  \begin{equation}\label{eq:semidesintegracion-solucion}
    m(t) = m(0)e^{-kt},
  \end{equation}
  donde $m(0)$ es la masa inicial.

  La constante $k$
  tiene que ver con el periodo de semidesintegración de la manera que
  sigue: el periodo de semidesintegración $T$ es el tiempo en que
  una masa $m$ de una sustancia
  radiactiva se desintegra hasta quedar en masa $m/2$ (la mitad). Es
  decir,
  \begin{equation*}
    m(T) = \frac{m(0)}{2}.
  \end{equation*}
  Pero por otro lado, por \eqref{eq:semidesintegracion-solucion}, se
  tiene que
  \begin{equation*}
    m(T) = m(0)e^{-kT},
  \end{equation*}
  por lo que
  \begin{equation*}
    m(0)e^{-kT} = \frac{m(0)}{2} \Longrightarrow e^{-kT} = \frac{1}{2}
    \Longrightarrow kT = \log 2 \Longrightarrow k = \frac{\log 2}{T}.
  \end{equation*}
  Así pues, conocido el periodo de semidesintegración, se conoce la
  constante que dicta la evolución de la masa según la ecuación
  \eqref{eq:semidesintegracion-solucion}.

  Si las unidades de tiempo se establecen como años, entonces la
  evolución de una masa de radio es:
  \begin{equation*}
    \dot{m}(t) = -\frac{\log 2}{1600}m(t),
  \end{equation*}
  cuya solución es, para $m(0)=2\mathrm{g}$, 
  \begin{equation*}
    \valor{m(t) = 2 e^{-0.000433t} \Longrightarrow t \simeq 0.9954},
  \end{equation*}
  es decir, unos $363$ días (casi un año).

  Para conocer la masa al cabo de $100$ años, no hay más que sustituir
  $t$ por $100$ en la fórmula anterior:
  \begin{equation*}
    \valor{m(100) \simeq 1{.}915\mathrm{g}}.
  \end{equation*}

  Para el plutionio $238$ sirven las mismas fórmulas:
  \begin{equation*}
    k = \frac{\log 2}{88} \simeq 0.00788,
  \end{equation*}
  por lo que
  \begin{equation*}
    m(t) \simeq 2e^{-0{.}00788 t}.
  \end{equation*}
  El momento en que $m(t)=1{.}99$ se calcula como
  \begin{equation*}
    \valor{1{.}99 = 2e^{-0{.}00788 t} \Longrightarrow t \simeq 0.005}, 
  \end{equation*}
  que es $1{.}825$ días. Dentro de $100$ años habrá
  \begin{equation*}
    \valor{m(100)\simeq 2 e^{-0.788} \simeq 0{.}9095\mathrm{g}},
  \end{equation*}
  menos de la mitad de lo que había (claro).
\end{solution}


\begin{exercise}
  Una inversión (a interés compuesto en tiempo continuo) comenzó con
  $100$
  Euros y al cabo de cinco años consiste en $120$ Euros. Calcular el
  tipo interés.
\end{exercise}
\begin{solution}
  Como se explica en la página \pageref{ite:interes}, la ecuación del
  interés compuesto a tiempo continuo (sin inversión ni gastos
  añadidos) es, si $c(t)$ es el capital en tiempo $t$:
  \begin{equation*}
    \dot{c}(t) = rc(t),
  \end{equation*}
  donde $r$ es el tipo de interés (que se supone constante). La
  solución de esta ecuación es conocida
  \begin{equation*}
    c(t) = c(0) e^{rt}.
  \end{equation*}
  En este problema se pide calcular el tipo de interés sabiendo la
  inversión inicial y el capital al cabo de un tiempo. Si las unidades
  de tiempo son años, entonces se nos dice que
  \begin{equation*}
    120 = 100 e^{5r},
  \end{equation*}
  por lo que
  \begin{equation*}
    \valor{e^{5r} = 1{.}2 \Longrightarrow r = \log 1{.}2/5 \Longrightarrow r
    \simeq 0{.}036 = 3.6\%/\mathrm{\text{a\~no}}},
  \end{equation*}
  que es un interés no demasiado malo (pero sí malo) en el año 2016.
\end{solution}


\begin{exercise}
  Calcular a partir de qué tiempo compensa más invertir $1000$ Euros
  al $8\%$ anual (interés compuesto continuo) que $1500$ Euros al
  $4\%$ anual.
\end{exercise}
\begin{solution}
  La ecuación del interés sin inversión ni gasto es
  \begin{equation*}
    \dot{c}(t) = rc(t),
  \end{equation*}
  para un capital $c(t)$ en el momento $t$. La solución es conocida:
  \begin{equation*}
    c(t) = c(0)e^{rt},
  \end{equation*}
  donde $c(0)$ es el capital inicial. Por tanto, la primera inversión
  $c_1(t)$ sigue la ley
  \begin{equation*}
    c_1(t) = 1000e^{0.08t}
  \end{equation*}
  mientras que la segunda sigue la ley
  \begin{equation*}
    c_2 = 1500e^{0.05t}.
  \end{equation*}
  Está claro que $c_1(0) < c_2(0)$ y el problema nos pide calcular
  cuándo $c_1(t) = c_{2}(t)$, puesto que las curvas de sendas fórmulas
  solo se cortan una vez (¿por qué?). Así que hay que resolver
  \begin{equation*}
    \valor{1000e^{0.08t} = 1500e^{0.05t} \Longrightarrow t \simeq 13.52}.
  \end{equation*}
  Por tanto, la primera inversión compensa más que la segunda a partir
  de la mitad del año decimocuarto.
\end{solution}

\begin{exercise}\label{exer:muelle-1}
  De un muelle de $1\mathrm{m}$ (de peso irrelevante) cuelga una masa de
  $1\mathrm{kg}$. La constante de Hooke del muelle es
  $30N/m$. Calcular la posición del muelle al cabo de $2s$ si se
  parte de la posición de reposo del muelle a velocidad $0$. Obviar el
  rozamiento. La aceleración gravitatoria es $10\mathrm{m}/\mathrm{s}^2$.
\end{exercise}
\begin{solution}
    \begin{figure}[h!]
  \centering
  \begin{tikzpicture}
    \draw (0,3) -- (0,2.7);
    \draw[pattern=north west lines] (-0.3,1.7) rectangle
    (0.3,1.1);
    \draw[->,blue,line width=2pt,fill opacity=0.9] (0,1.4) -- (0,2.1);
    \draw[decorate,decoration={coil,segment length=3pt}] (0,2.7) --
    (0,1.914);
    \draw (0,1.914) -- (0,1.7);
    \draw[->,red,line width=2pt] (0,1.4) -- (0,0.5);
    \node[anchor=east] at (0,0.7) {$F_g$};
    \node[anchor=south east] at (-0.1,1.9) {$F_H$};
    \draw (-1,3) -- (1,3);
  \end{tikzpicture}
  \caption{Representación gráfica del problema \ref{exer:muelle-1}. Solo hay dos fuerzas
    actuando: la gravedad y la de Hooke. La de Hooke se ha puesto
    hacia arriba pero hay momentos en que irá en el mismo sentido que
    la gravedad.}
  \label{fig:muelle-1}
\end{figure}
Este es un problema sencillo de la Segunda Ley de Newton (ver página
\pageref{ite:newton-2}). Solo hay dos fuerzas actuando, como se ve en
la Figura \ref{fig:muelle-1}: la gravedad y la de Hooke.

Coloquemos el
origen de coordenadas en la unión del muelle con el techo y hagamos
que $y>0$ hacia abajo (cuidado con esta orientación). Sea $y(t)$
la posición del centro del cuerpo en el instante $t$.

Según la Segunda Ley de Newton,
\begin{equation*}
  1\cdot\ddot{y}(t) = F_H + F_g = -30(y(t) - 1) + 10\cdot 1.
\end{equation*}
Expliquemos los signos: a la izquierda no hay más que ``masa'' por
``aceleración.'' A la derecha: la fuerza del muelle siempre va
\emph{contra} la elongación. Por eso la constante de Hooke
multiplicada por la posición siempre lleva un signo negativo. Téngase
en cuenta que la elongación no es $y(t)$ sino la diferencia entre esta
y la longitud del muelle. La fuerza de la gravedad tiene signo
positivo porque la coordenada $y(t)$ crece en el mismo sentido en que
dicha fuerza (hacia abajo).

Sin unidades y escribiéndola como una ecuación de segundo orden lineal:
\begin{equation}\label{eq:muelle-1-1}
  \ddot{y}(t) + 30y(t) = 40.
\end{equation}
La ecuación característica es
\begin{equation*}
T^2 + 30 = 0
\end{equation*}
que tiene por raíces
\begin{equation*}
  T = \pm \sqrt{30}i.
\end{equation*}
La solución general de la ecuación \eqref{eq:muelle-1-1} es
\begin{equation*}
  y(t) = k_1\sen \sqrt{30}t + k_2 \cos \sqrt{30}t + \frac{4}{3}.
\end{equation*}
Solo queda sustituir las condiciones iniciales: posición inicial de
reposo (es decir, el objeto está en la posición $1$) y velocidad inicial cero:
\begin{equation*}
  y(0) = 1,\,\dot{y}(0)=0.
\end{equation*}
La primera condición hace que
\begin{equation*}
  1 = k_2 + \frac{4}{3},
\end{equation*}
así que $k_2 = -1/3$. La segunda condición hace que
\begin{equation*}
  0 = \sqrt{30}k_{1},
\end{equation*}
así que $k_1 = 0$. Por tanto,
\begin{equation*}
  \valor{y(t) = -\frac{1}{3}\cos \frac{t}{5} + \frac{4}{3}}.
\end{equation*}
Al cabo de dos segundos:
\begin{equation*}
  \valor{y(2) = \frac{4}{3}-\frac{\cos 2 \sqrt{30}}{3} \simeq 1{.}347}.
\end{equation*}
¿Es razonable ese valor para la constante de Hooke?
\end{solution}

\begin{exercise}\label{exer:muelle-2}
  Igual que el ejercicio \ref{exer:muelle-1} pero asumiendo que el
  rozamiento es proporcional a la velocidad (y de sentido contrario),
  con una constante de $0.1Ns/m$.
\end{exercise}
\begin{solution}
  En este caso hay tres fuerzas que influyen en el movimiento. Una
  representación gráfica puede ser la de la Figura \ref{fig:muelle-2}.
      \begin{figure}[h!]
  \centering
  \begin{tikzpicture}
    \draw (0,3) -- (0,2.7);
    \draw[pattern=north west lines] (-0.3,1.7) rectangle
    (0.3,1.1);
    \draw[->,blue,line width=2pt,fill opacity=0.9] (0,1.4) -- (0,2.1);
    \draw[decorate,decoration={coil,segment length=3pt}] (0,2.7) --
    (0,1.914);
    \draw (0,1.914) -- (0,1.7);
    \draw[->,red,line width=2pt] (0,1.4) -- (0,0.5);
    \node[anchor=east] at (0,0.7) {$F_g$};
    \node[anchor=south east] at (-0.1,1.9) {$F_H$};
    \draw (-1,3) -- (1,3);
    \draw[->,opacity=1, orange, line width=1.5pt] (0.05,1.4) --
    (0.05,1.8);
    \node[anchor=west] at (0.05, 1.8) {$F_r$};
  \end{tikzpicture}
  \caption{Representación gráfica del problema
    \ref{exer:muelle-2}. Los sentidos de la fuerza de Hooke y de
    rozamiento son arbitrarios
    en este dibujo, no así la gravedad.}
  \label{fig:muelle-2}
\end{figure}
\end{solution}

\begin{exercise}
  ¿Puede la siguiente ecuación describir el movimiento de un muelle
  con rozamiento sin fuentes de energía aparte de la gravedad?
  \begin{equation*}
    y^{\prime\prime}(x) -0{.}03y^{\prime}(x) + 0{.}7y(x) = 0.
  \end{equation*}
  Explicar por qué.
\end{exercise}
\begin{solution}
  La ecuación característica es
  \begin{equation*}
    T^2 - 0{.}03T + 0{.}7 = 0
  \end{equation*}
  cuyas soluciones son
  \begin{equation*}
    T = \frac{0{.}03 \pm \sqrt{0{.}009 - 2{.}8}}{2} \simeq 0{.}015 \pm
    0{.}8365i.
  \end{equation*}
  La parte real de estos números es positiva. Esto significa que la
  solución de la ecuación diferencial (que es homogénea)
  será más o menos de la forma
  \begin{equation*}
    y(t) \sim e^{0.015t}(A\cos 0{.}8365t+B\sen 0{.}8365t),
  \end{equation*}
  para ciertos valores de $A$ y $B$. De aquí se deduce que la amplitud
  de las oscilaciones crece con el tiempo (esta amplitud es
  $e^{0.015t}$), lo cual implica que el muelle va ganando energía con
  el paso del tiempo. Si no hay ninguna fuente de energía, esto es
  imposible.
\end{solution}


\begin{exercise}
  Se ha planteado la ecuación diferencial del movimiento de un muelle
  con rozamiento sin generadores de energía y, aplicando el método de
  variación de las
  constantes, se han obtenido las raíces $2\pm 4i$. ¿Puede esto
  ocurrir? ¿Y si las raíces son $-2\pm 4i$?
\end{exercise}
\begin{solution}
  El primer caso no puede ocurrir porque si las raíces del polinomio
  característico tienen parte real positiva, esto quiere decir que la
  solución será más o menos de la forma
  \begin{equation*}
    y(t) \sim e^{2t}(\cdots) 
  \end{equation*}
  donde lo que va entre paréntesis es la ecuación de una onda de
  amplitud $1$. Esta función tiene amplitud creciente, lo cual
  significa que la energía del sistema (el muelle) crece, algo
  imposible si no hay generadores.

  Las raíces $-2\pm 4i$ son perfectamente compatibles con el sistema
  propuesto. 
\end{solution}


\begin{exercise}
  En una caída libre con rozamiento proporcional a la velocidad (y de
  sentido contrario), ¿de qué parámetros depende la velocidad límite?
\end{exercise}
\begin{solution}
  Se supone que la respuesta no se sabe de memoria, claro. La ecuación
  que rige este movimiento es (ver página \pageref{ite:newton-2}) la
  dada por la Segunda Ley de Newton que, en este caso queda
  \begin{equation*}
    m\cdot\ddot{y}(t) = g\cdot m - k\dot{y}(t),
  \end{equation*}
  para cierta constante $k>0$ (del rozamiento). En esta escritura,
  $y(t)$ es la posición, $m$ es la masa y $g$ la gravedad. Si ponemos
  $v(t)=\dot{y}(t)$, la ecuación anterior se puede reescribir
  \begin{equation*}
    m \dot{v}(t) + k v(t)= gm,
  \end{equation*}
  que, dividiendo por $m$ se convierte en
  \begin{equation*}
    \dot{v}(t) +\frac{k}{m}v(t) = g.
  \end{equation*}
  Esta ecuación se sabe resolver (ver página
  \eqref{eq:solucion-ode-orden-1}) y su solución es
  \begin{equation*}
    v(t) = \left(v(0) - \frac{gm}{k}\right) e^{-kt/m} + \frac{gm}{k}.
  \end{equation*}
  La velocidad límite es justamente el límite de esta expresión cuando
  $t\rightarrow \infty$:
  \begin{equation*}
    \lim_{t\rightarrow \infty} \left(v(0) - \frac{gm}{k}\right) + \frac{gm}{k}
    e^{-kt/m} = \frac{gm}{k}.
  \end{equation*}
  Es decir, la velocidad límite depende de la aceleración gravitatoria
  $g$, de la masa $m$ y de la constante de rozamiento $k$. Por
  supuesto, no depende ni de la velocidad inicial ni de la posición.
\end{solution}


\begin{exercise}\label{exer:dos-muelles-1}
  Un cuerpo de masa $m$ y  longitud $2l$ (en las unidades adecuadas)
  está sujeto 
  por cada uno de sus extremos a dos paredes, que están separadas una
  distancia $L$, mediante sendos muelles
  de longitudes $A$ y $B$. Las constantes de Hooke respectivas son
  $k_1$ y $k_2$. Si la posición del muelle es $x(t)$ y no hay
  rozamiento, expresar cual es la ecuación que describe su movimiento
  cuando la posición inicial es $x(0)$ y la velocidad inicial $v(0)$.
\end{exercise}
\begin{solution}
  La Figura \ref{fig:dos-muelles-1} muestra un esquema de cómo puede
  plantearse este problema. Nótese (es importante) que se fija el
  origen de coordenadas, en este caso en el extremo izquierdo del
  sistema, y que la coordenada que se estudia, $x(t)$, es el punto
  medio del cuerpo. En el dibujo, las fuerzas se han dispuesto en
  sentidos contrarios y hacia la compresión de los muelles pero no
  tiene en absoluto por qué ser así: lo importante es que las fuerzas
  vayan en el sentido correcto siguiendo la Ley de Hooke:
  \begin{equation*}
    F = -k X(t)
  \end{equation*}
  donde $X(t)$ es la elongación del muelle (positiva si el muelle está
  estirado, negativa si el muelle está encogido) y el sentido es
  \emph{contra tal elongación} (es decir, la fuerza de la ecuación
  precedente apunta hacia la posición de reposo del muelle).
\begin{figure}[h!]
  \centering
  \begin{tikzpicture}
    \draw (-3.05,2) -- (-3.05,0) -- (3.05,0) -- (3.05,2);
    \draw (-3.05,1) -- (-3.0,1);
    \draw (3.05,1) -- (3.0,1);
    \draw (-3.05,0) -- (-3.05,-0.1);
    \draw (3.05,0) -- (3.05, -0.1);
    \draw [domain=0:6*pi, parametric, smooth, samples=300] plot function
    {-3+t/10,1+0.3*sin(5*t)};
    \draw [domain=0:6*pi, parametric,smooth, samples=300] plot
    function {3-t/5,1+0.3*sin(6*t)};
    \draw (-1.115,1) -- (-1.08,1);
    \draw (-0.7699,1) -- (-0.8,1);
    \draw[pattern = north east lines] (-1.08,0.5) rectangle
    (-0.8,1.5);
    \draw[->,blue,line width=2pt, opacity=0.8] (-0.94,1) -- (-1.8,1);
    \draw[->,red, line width=2pt, opacity=0.8] (-0.94,1) -- (0,1);
    \node[anchor=north] at (-3.05,0) {$x=0$};
    \node[anchor=north] at (3.05,0) {$x=L$};
    \draw[<->] (-1.08,1.7) -- (-0.8,1.7);
    \draw (-0.8,1.6) -- (-0.8,1.8);
    \draw (-1.08,1.6) -- (-1.08,1.8);
    \node[anchor=south] at (-0.94,1.7) {{\footnotesize $2l$}};
    \node[anchor=north, blue] at (-1.4,1) {$F_1$};
    \node[anchor=north,red] at (-0.47,01) {$F_2$};
    \draw[dashed] (-0.94,0.5) -- (-0.94,-0.3) node[anchor=north]
    {$x(t)$};
    \draw[dashed] (-2,0.7) -- (-2,-0.3) node[anchor=north] at (-2,-0.2)
    {{\footnotesize $A$}};
    \draw[dashed] (0,0.7) -- (0,-0.3) node[anchor=north] at (0,-0.2)
    {{\footnotesize $L-B$}};
     % \draw (-1,1) -- (-1.115,0.7) -- (-0.7699,0.7) --
    % (-0.7699,1.3) -- (-1.115,1.3) -- (-1.115,1);
  \end{tikzpicture}
  \caption{Representación gráfica del problema
    \ref{exer:dos-muelles-1}. Los sentidos de las fuerzas son
    arbitrarios en este dibujo..}
  \label{fig:dos-muelles-1}
\end{figure}

Siguiendo el esquema de la Figura \ref{fig:dos-muelles-1}, colocamos
el origen de coordenadas en el extremo izquierdo del sistema y
llamamos $x(t)$ a la posición del centro geométrico del cuerpo. La
Segunda Ley de Newton (ver página \pageref{ite:newton-2}) en este caso
es
\begin{equation*}
  m \cdot \ddot{x}(t) = F_1 + F_2.
\end{equation*}
Donde $F_1$ es la fuerza impresa por el muelle de la izquierda y $F_2$
la impresa por el muelle de la derecha.

Calculemos $F_1$. Como la longitud del muelle de la izquierda es $A$,
al ser $x(t)$ la posición del centro, la elongación real será
$x(t)-l-A$ (positiva si el muelle está estirado, negativa si está
comprimido, en el dibujo está estirado). Por tanto:
\begin{equation*}
  F_1 = -k_1(x(t)-l-A).
\end{equation*}
Nótese cómo, si $x(t)-l-A<0$ entonces la fuerza ``empuja hacia la
derecha'' mientras que si $x(t)-l-A>0$, la fuerza ``tira hacia la
izquierda.''

Para calcular $F_2$ seguimos un razonamiento similar. En este caso, la
elongación del muelle será $(L-x(t))+l-(L-B)$, pues el muelle está
sujeto por la derecha, no por la izquierda (así que el sentido de la
elongación es opuesto). Por tanto
\begin{equation*}
  F_2 = -k_2((L-x(t))+l-(L-B)) = -k_2(B+l-x(t)),
\end{equation*}
que ``funciona al revés que $F_1$'': si $x(t)-l-B<0$ entonces el
muelle ``empuja'' hacia la izquierda, mientras que si $x(t)-l-B>0$,
entonces el muelle ``tira'' hacia la derecha.

Una vez fijadas las coordenadas y
determinadas las fuerzas que actúan sobre el cuerpo móvil, la
Segunda Ley de Newton (página \pageref{ite:newton-2}) nos dice que,
\begin{equation}\label{eq:dos-muelles-1}
  m\cdot \ddot{x}(t) = F_1 + F_2 = -k_1(x(t)-l-A) - k_2(B+l-x(t)),
\end{equation}
que, quitando paréntesis, queda
\begin{equation*}
  m\ddot{x}(t) = (-k_1-k_2)x(t)+l(k_1-k_2)+k_1A-k_2B.
\end{equation*}
Si llevamos la incógnita al lado izquierdo obtenemos
\begin{equation*}
  m\ddot{x}(t) + (k_1+k_2)x(t) = K
\end{equation*}
donde $K=l(k_1+k_2)+k_1A-k_2B$. La solución general de esta ecuación
diferencial es
\begin{equation*}
  x(t) = \frac{K}{k_1+k_2} + c_1\sen \frac{\sqrt{k_1+k_2}t}{\sqrt{m}}
  + c_2\cos \frac{\sqrt{k_1+k_2}t}{\sqrt{m}}.
\end{equation*}
Las constantes $c_1$ y $c_2$ dependen, claro, de las condiciones
iniciales. 
\end{solution}



\begin{exercise}\label{exer:dos-muelles-2}
  El mismo problema que el \ref{exer:dos-muelles-1} pero asumiendo que
  el rozamiento es proporcional a la velocidad y de signo contrario,
  con constante $r$.
\end{exercise}
\begin{solution}
  Sin entrar en más detalles, lo único que hay que añadir a la
  ecuación \eqref{eq:dos-muelles-1} es la fuerza de rozamiento, que
  según se nos dice, tendrá la forma
  \begin{equation*}
    F_r = -k_r \dot{x}(t),
  \end{equation*}
  para una constane $k_r>0$. La ecuación que hay que resolver es, por
  tanto:
  \begin{equation*}
    m\ddot{x}(t) + k_r \dot{x}(t) + (k_1+k_2)x(t) = K.
  \end{equation*}
  Y la solución se puede calcular con el método de variación de las
  constantes, por ejemplo.
\end{solution}

\begin{exercise}
  Utilizar el método de variación de las constantes para resolver las
  ecuaciones:
  \begin{equation*}
    \begin{array}{ll}
      \mathrm{a)\ } y^{3)}-y = 1 & \mathrm{b)\ } y^{4)} - 2y^{\prime\prime} + y = 1\\
      \mathrm{c)\ } y^{\prime\prime} - 2y^{\prime} + 1 = x &
      \mathrm{d)\ } y^{\prime\prime} - 2y^{\prime} = x^2\\
    \end{array}
  \end{equation*}
\end{exercise}
\begin{solution}
  Se supone que el método es conocido y lo aplicamos sin entrar en
   demasiados detalles. Solo resolvemos la ecuación \textrm{a)} para
   no recargar el texto con los mismos comentarios.
  \begin{enumerate}
  \item[a)] El polinomio característico es
    \begin{equation*}
      P(T) = T^3 - 1,
    \end{equation*}
    cuyas raíces son
    \begin{equation*}
      r_1 = 1,\, r_2 = -\frac{1}{2}+\frac{\sqrt{3}}{2}i,\, r_3 = -
      \frac{1}{2}- \frac{\sqrt{3}}{2}i
    \end{equation*}
    las tres raíces cúbicas de la unidad. Por tanto, las soluciones
    fundamentales son
    \begin{equation*}
      y_1(x) = e^x,\, y_2(x)=e^{r_2x},\,y_3(x)=e^{r_3x},
    \end{equation*}
    y la solución general de la homogénea es
    \begin{equation*}
      y_h(x) = A_1e^x + A_2e^{r_2x} + A_3e^{r_3x},
    \end{equation*}
    para constantes $A_1, A_2, A_3\in \mathbb{C}$. Una solución
    particular vendrá dada por una expresión
    \begin{equation*}
      y_p(x) = A_1(x)e^x + A_2(x)e^{r_2x} + A_3(x)e^{r_3x},
    \end{equation*}
    (esto es la ``variación de las constantes''). Se imponen las
    condiciones correspondientes a una ecuación de orden $3$:
    \begin{equation*}
      \begin{split}
        &A_1(x)^{\prime}e^x +
        r_2A_2(x)^{\prime}e^{r_2x}+r_3A_3(x)^{\prime}e^{r_3x} = 0\\
        &A_1(x)^{\prime}e^x + r_2^{2}A_{2}^{\prime}(x)e^{r_2x}
        + r_3^{2}A_3(x)^{\prime}e^{r_3x} = 0
      \end{split}
    \end{equation*}
    y, aparte, se tiene la ecuación diferencial original que, con las
    condiciones anteriores, queda
    \begin{equation*}
        A_{1}(x)^{\prime}e^{x} + r_2^3A_2(x)^{\prime}e^{r_2x}+
        r_3^3A_3(x)^{\prime}e^{r_3x} = 1
    \end{equation*}
  Estas tres ecuaciones lineales tienen las soluciones:
  \begin{equation*}
    A_1(x) = \frac{e^{-x}}{3},\,A_2(x) = \frac{e^{-r_2x}}{3},\,
    A_{3}(x) = \frac{e^{-r_3x}}{3}.
  \end{equation*}
  Con esto ya podemos escribir una solución particular de la ecuación
  diferencial:
  \begin{equation*}
    y_p(x) = \frac{e^{-x}}{3}e^x +
    \frac{e^{-r_2x}}{3}e^{r_2x} +
    \frac{e^{-r_3x}}{3}e^{r_3x}.
  \end{equation*}
  Y, para terminar, la solución general es esta solución particular
  más una general de la homogénea:
  \begin{equation*}
    y(x) = \frac{e^{-x}}{3}e^x +
    \frac{e^{-r_2x}}{3}e^{r_2x} +
    \frac{e^{-r_3x}}{3}e^{r_3x} + c_1e^x + c_2 e^{r_2x} + c_3 e^{r_3(x)}.
  \end{equation*}
\item[b)] En este caso, el polinomio característico es
  \begin{equation*}
    P(T) = T^4 -2T^2 +1 = (T^{2}-1)^{2},
  \end{equation*}
  que tiene dos raíces $T=\pm 1$ ambas con multiplicidad $2$. Esto
  significa que las soluciones fundamentales de la ecuación homogénea son
  \begin{equation*}
    y_1(x) = e^x,\, y_2(x)=xe^x,\,y_3(x) = e^{-x},\,y_4(x)=xe^{-x}.
  \end{equation*}
  Y el resto del problema es como el \textrm{a)}, pero con un sistema
  lineal de orden $4$.
\item[c)] El polinomio característico es
  \begin{equation*}
    P(T) = T^2-2T+1 = (T-1)^2,
  \end{equation*}
  que tiene una única raíz doble, $T=1$. Así que la soluciones
  fundamentales
  de la homogénea son
  \begin{equation*}
    y_1(x) = e^x, \,y_2(x)=xe^x.
  \end{equation*}
  Y se procede igual que en los anteriores (aunque el sistema es
  $2\times 2$).
\item Esta ecuación tiene por polinomio característico
  \begin{equation*}
    P(T)=T^2-2T,
  \end{equation*}
  cuyas raíces son $T=0$ y $T=2$. La raíz $0$ da lugar a la solución
  de la homogénea $y_1(x)=1$, la raíz $2$ a $y_2(x)=e^{2x}$, así que
  la solución general de la homogénea es
  \begin{equation*}
    y_h(x) = A_1 + A_2 e^{2x},
  \end{equation*}
  y se procede como en los anteriores.
  \end{enumerate}
\end{solution}

\begin{exercise}
  ¿Pueden ser los siguientes números las raíces del polinomio
  característico de una ecuación
  diferencial asociada a un problema mecánico Newtoniano sin
  generadores de energía? ¿Por qué?
  \begin{equation*}
    \lambda_1 = 3, \lambda_2 = 2+3i, \lambda_3 = 4+i.
  \end{equation*}
  ¿Y los siguientes?
  \begin{equation*}
    \lambda_1 = 3, \lambda_2 = 2+3i, \lambda_3 = 2-3i.
  \end{equation*}
\end{exercise}
\begin{solution}
  La primera familia no puede serlo porque no correspondería a una
  ecuación diferencial con coeficientes reales (pues si fuera así, las
  raíces complejas deberían ser conjugadas).

  La segunda familia no puede serlo porque las raíces del polinomio
  característico tienen parte real positiva y esto haría que las ondas
  que describen el sistema mecánico tuvieran amplitud creciente: el
  sistema estaría ganando energía espontáneamente.
\end{solution}


\begin{exercise}
  Se sabe que la densidad del aire es inversamente proporcional a suma
  de la
  altura más 100 unidades y que
  la fuerza de rozamiento del aire es proporcional a la velocidad
  (y en sentido contrario) y a la densidad (del aire). Dar unas ecuaciones para
  la caída libre de un cuerpo. La aceleración de la gravedad se supone
  constante e igual a $9.8$ en valor absoluto.
\end{exercise}
\begin{solution}
  En este problema mecánico actúan solo $F_g$,
  la gravedad y $F_r$, el rozamiento. La fuerza de la gravedad se
  supone constante (porque la aceleración gravitatoria se supone
  constante) pero la de rozamiento no. Si se coloca el origen de
  coordenadas (para la altura) en el suelo y $x(t)$ es dicha altura,
  entonces, por la Segunda Ley de Newton (ver página
  \pageref{ite:newton-2}): 
  \begin{equation*}
    m\cdot \ddot{x}(t) = -9.8\cdot m + F_r,
  \end{equation*}
  y la fuerza de rozamiento, según se nos dice en el enunciado es
  \begin{equation*}
    F_r = -(k_{1}\cdot k_2/(x(t)+100))\dot{x}(t) = -
    \frac{k_1k_2}{x(t)+100}\dot{x}(t),
  \end{equation*}
  que es lo que significa que sea ``proporcional a la velocidad en
  sentido contrario) y proporcional a la densidad (y que la densidad
  sea inversamente proporcional a la altura más cien). Por tanto, la
  ecuación diferencial de este sistema es:
  \begin{equation*}
    m\ddot{x}(t) = -9.8m - \frac{k_1k_2}{x(t)+100}\dot{x}(t).
  \end{equation*}
  Esta ecuación es mejor no tratar de resolverla.
\end{solution}

