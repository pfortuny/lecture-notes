\documentclass[draft,a4paper,twoside]{amsart}
%\usepackage{showkeys}
\usepackage{tcolorbox}
\usepackage{mathpazo} % add possibly `sc` and `osf` options
\usepackage{eulervm}
\usepackage{cclicenses}
\usepackage{graphicx}
% footnotes below floats
\usepackage[bottom]{footmisc}
\usepackage[spanish]{babel}
\def\datename{\emph{Fecha}:\/}
\usepackage[utf8]{inputenc}
\usepackage{tikz}
\usetikzlibrary{arrows,decorations,positioning,fit}
\usepackage{pgfplots}
\usetikzlibrary{calc}
\pgfplotsset{compat=1.9}
\newcommand{\abs}[1]{\vert #1 \vert}

\let\originalparagraph\paragraph
\makeatletter
\renewcommand\paragraph{%%
  \@startsection{paragraph}%%                        name                
                {4}%%                                level               
                {\z@}%%                              indentation         
                {1ex \@plus1ex \@minus.2ex}%%        beforeskip
                {-1em}%%                             afterskip
                {\normalfont\normalsize\bfseries}}%% style
\makeatother

\theoremstyle{definition}
\newtheorem{exercise}{Problema}
\newtheorem{theorem}{Teorema}
\title{Varios elementos de Cálculo}
\author{P. Fortuny Ayuso}
\date{Enero-junio 2016}
\begin{document}
\maketitle
\vspace*{-25pt}
\centerline
  \noindent  \begin{minipage}{.90\textwidth}
    \begin{tcolorbox}
    \footnotesize{\cc\hspace*{-10pt}\ccby
Copyright {\copyright} 2011--\the\year\ Pedro Fortuny Ayuso\par
This work is licensed under the Creative Commons Attribution 3.0
License. To view a copy of this license, visit\\
\texttt{http://creativecommons.org/licenses/by/3.0/es/}\\
or send a letter to Creative Commons, 444 Castro Street, Suite 900, 
Mountain View, California, 94041, USA.
}.
\end{tcolorbox}
\end{minipage}\\

\vspace*{5pt}
\noindent\textbf{Nota:\/} Se recomienda utilizar la aplicación de
representación gráfica de funciones que se encuentra en
\texttt{http://pfortuny.net/fooplot.com}.


\section{Senos, cosenos y la exponencial compleja}
La función exponencial $f(z)=e^z$, para valores complejos de $z$ se
define de la siguiente manera:
\begin{equation*}
  e^{a+ib} = e^a(\cos b + i \sen b),
\end{equation*}
donde $a$ es la parte real de $z$ y $b$ la parte imaginaria.

Por ello, para entender bien la exponencial compleja se precisa,
primero de todo, comprender \emph{claramente} las funciones
trigonométricas elementales. De aquí esta sección.

\subsection{Algún recordatorio}
De momento nos circunscribiremos a variables reales.
La fórmula más importante de trigonometría es
\begin{equation*}
  \sen^2 x + \cos^2 x = 1, 
\end{equation*}
que no es más que una manera breve de expresar el Teorema de Pitágoras
(pues el seno es un cateto y el coseno el otro de un triángulo
rectángulo de lado $1$). Las otras fórmulas básicas que nos conciernen
en este curso son:
\begin{equation*}
  \begin{array}{l}
    \sen (x + y) = \sen x \cos y + \cos x \sen y\\
    \cos (x + y) = \cos x \cos y - \sen x \sen y
  \end{array}
\end{equation*}
(la segunda se puede deducir de la primera y viceversa). Como
consecuencia, se tiene que
\begin{equation*}
  \begin{array}{l}
    \sen 2x = 2 \sen x \cos x\\
    \cos 2x = \cos^2 x - \sen^{2} x
  \end{array}
\end{equation*}
y otras tantas.

\subsection{Periodos, frecuencias y desfases}
La función $\sen x$ es periódica (con periodo $2\pi$) y
es una onda que pasa por $(0,0)$ y es impar (esto quiere decir que
$\sen -x = - \sen x$). La función $\cos x$ es exactamente igual pero
la onda no pasa por $(0,0)$ sino que es cero para $x=-\pi/2$ y 
$x=\pi/2$. 

Sea $f(x)$ una función real de variable real, periódica. Se define el
\emph{periodo} de $f(x)$ como el menor valor $l$ tal que $f(x+l)=f(x)$
para todo $x$. Tanto $\sen x$ como $\cos x$ tienen periodo $2\pi$.

Es común en Teoría de la Señal utilizar las funciones $\sen nx$ y
$\cos nx$ para valores enteros positivos de $n$ (son las funciones de
la Serie de Fourier).

\begin{exercise}
  Calcular el periodo de las funciones $\sen nx$ y $\cos nx$ para
  $n=1,2,\dots$. ¿El periodo aumenta o disminuye con $n$?
\end{exercise}

La noción dual al periodo es la \emph{frecuencia}. Como se habla
habitualmente en Herzios, la definiremos en estas notas como \emph{el
  inverso del periodo multiplicado por $2\pi$}. Por ejemplo, $\sen x$
tiene frecuencia $(2\pi)/(2\pi) = 1$ (es una función de ``un
Herzio''). Igualmente, $\cos 3x$ tiene frecuencia $3$ y así.

\begin{exercise}
  Calcular la frecucencia de la función $f(x)=\sen 3x + \cos 2x$.
\end{exercise}

Se sabe que  $\cos x = \sen (x+\pi/2)$. Esto se enuncia
diciendo que  $\cos x$
tiene un desfase de $-\pi/2$ (lo que significa que $\cos
(x-\pi/2)=\sen x$) respecto de $\sen x$. En general, dadas dos
funciones periodicas con el mismo periodo, se dice que $f(x)$ está
desfasada $\omega$ respecto de $g(x)$ si $\omega$ es el número real
positivo más pequeño tal que $ f(x-\omega)=g(x)$.

\begin{exercise}
  Calcular el desfase de $\sen (3x + 2)$ respecto de $\sen 3x$. Y
  respecto de $\cos 3x$
\end{exercise}

\begin{exercise}
  Calcular el desfase de la función $\sen (nx + b)$ respecto de $\sen
  nx$ para $n$ un número entero positivo y $b$ un número real
  positivo. Lo mismo respecto de $\cos nx$. ¿Es importante $b$ o más
  bien $b/n$?
\end{exercise}



\subsection{Combinaciones de senos y cosenos}
El hecho de que $\cos (nx+b)$ y $\sen (nx + b)$ estén desfasados
hace posible el siguiente resultado:
\begin{exercise}
  Demostrar que cualquier combinación
  \begin{equation*}
    f(x) = A \cos nx + B \sen nx
  \end{equation*}
  puede reescribirse como
  \begin{equation*}
    C \sen (n(x + \omega))
  \end{equation*}
  para cierto $C$ y $\omega>0$. ¿Cuál es el desfase de $f(x)$ respecto
  de $\sen nx$? ¿Qué relación hay entre $A, B$ y $C$?
\end{exercise}

De manera igual, cualquier onda $g(x)=R\sen(nx + b)$ puede
reescribirse como una combinación lineal de $\sen nx$ y $\cos nx$:

\begin{exercise}
  Comprobar que cualquier función
  \begin{equation*}
    g(x) = R\sen(nx + b)
  \end{equation*}
  puede reescribirse como
  \begin{equation*}
    T \sen nx + U \cos nx.
  \end{equation*}
  Y calcular la relación entre $R, T$ y $U$.
\end{exercise}

\subsection{Algunos ejercicios}
\begin{exercise}
  Calcular el primer cero no negativo, el periodo, la frecuencia y el
  desfase, respecto del seno correspondiente, de las siguientes funciones:
  \begin{itemize}
  \item $\sen(3x+2)$
\item $\sen(5(x+1))$
\item $\cos(6x-23)$
\item $\sen(7x+2)+\cos(7x-1)$
  \end{itemize}
\end{exercise}

\newpage

\subsection{Amplitudes e interferencias...}
Una onda compuesta por senos y cosenos de la misma frecuencia pero
distinto desfase puede siempre escribirse como una combinación de un
seno y un coseno sin desfase. Esta es la manera común (que veremos más
adelante) de escribir funciones periódicas.

Si $f(x)=A\sen nx + B\cos nx$, para $n>0$, se denomina \emph{amplitud} de $f(x)$
en la frecuencia $n$ al valor $\sqrt{A^2 + B^2}$. El valor $A$ es la
\emph{amplitud senoidal} y el valor $B$ la amplitud cosenoidal. Nótese
cómo la amplitud total \emph{no es la suma} de las amplitudes sino que
es la amplitud de la onda ``cuando se mira como una onda desfasada.''

\begin{exercise}
Calcúlense, utilizando las fórmulas trigonométricas básicas, las
amplitudes en las frecuencias correspondientes de
\begin{itemize}
\item $f(x)= \cos 3x - 2\sen (3x-1)$.
\item $f(x) = 4\cos (5x+2) - \sen (5x-2)$.
\item $f(x) = \cos 7x + \sen (7(x+\pi/2))$.
\end{itemize}
El último ejemplo es \emph{muy interesante}: una función $f(x)$ que
``parece tener amplitud no nula'' pero que resulta ser $cero$: las dos
ondas tienen el mismo valor con signo distinto en cada punto, así que
se anulan. Es justamente una interferencia.

Compruébese cómo el valor máximo alcanzado por las funciones $f(x)$ de
cada ejemplo no es en ninguno de esos casos la raíz cuadrada de la
suma de los cuadrados
de los coeficientes de las funciones \emph{no elementales}. Es decir,
en el primer ejemplo, el valor máximo de $f(x)$ no es $\sqrt{1+4}$, en
el segundo tampoco es $\sqrt{16+1}$, etc. Sí lo es cuando se expresan
en funciones elementales (puede comprobarse esto pero no es necesario).
\end{exercise}

Las amplitudes $A$ y $B$ son, esencialmente, las energías de la onda $f(x)$
correspondientes a la frecuencia, en la parte cosenoidal y
senoidal. Esencialmente, $\sqrt{A^2 + B^2}$ es la energía de la onda
correspondiente a la frecuencia $n$.

\subsection{Aproximación de Fourier}
Las funciones $\sen nx$ y $\cos nx$, para $n$ entero no negativo, se
denominan \emph{funciones trigonométricas básicas}. Con ellas pueden
aproximarse todas las funciones periódicas de periodo $2\pi$. Si se
trata con una función $f(x)$ con periodo $a$, se puede transformar en
una de periodo $\pi$:

\begin{exercise}
  ¿Cómo puede transformarse una función $f(x)$ periódica de periodo $a$ en
  una función $g(x)$ que tenga periodo $2\pi$?
\end{exercise}

Dada una combinación lineal
\begin{equation*}
  f(x) = c_0 + a_1 \sen x + b_1 \cos x + a_2 \sen x + b_2 \cos x + \dots +
  a_n \sen nx + b_n \cos nx
\end{equation*}
los coeficientes $a_1, a_2, \dots, a_n$ y $b_1, b_2, \dots, b_n$
son las \emph{amplitudes} de cada función correspondiente \emph{en
$f(x)$}. Por ejemplo (represéntese):
\begin{equation*}
  f(x) = 2\cos x + 3\sen 3x - 2 \cos 3x + 7\sen 9x
\end{equation*}
es una función periódica de periodo $2\pi$ con amplitud $2$ en la
frecuencia $1$ cosenoidal, amplitud $3$ en la frecuencia $3$ senoidal
y $-2$ en la cosenoidal y amplitud $7$ en la frecuencia $9$
senoidal. ¿Cuáles son las amplitudes ``sin más''?

Sea ahora $f(x)$ una función periódica de periodo $2\pi$. Se definen
los \emph{coeficientes de Fourier} de $f(x)$ como sigue:
\begin{itemize}
\item $c_0 = (1/2\pi)\int_{-\pi}^{\pi} f(x)\,dx$.
\item $a_n = (1/\pi) \int_{-\pi}^{\pi} f(x)\sen nx\,dx$.
\item $b_n = (1/\pi) \int_{-\pi}^{\pi} f(x) \cos nx\,dx$.
\end{itemize}

Para los siguientes problemas, utilícese la aplicación Wolfram$|$Alpha,
que se encuentra en:
\texttt{http://wolframalpha.com}.

\begin{exercise}
  Calcúlense los coeficientes de Fourier hasta $n=3$ de
  \begin{itemize}
  \item $-\cos(3x) + 5\sen(2x) + 9$.
\item $\sen(3x)+2\cos(3x)-5\cos(x)+1$.
\end{itemize}
¿Qué se observa?
\end{exercise}

\begin{exercise}
  Lo mismo con:
  \begin{itemize}
  \item $\cos(3x+2)+\sen(2x+1)$.
\item $\cos(x+1)-\sen(x+2)+3\cos (2x +2)-5\sen(2x-1)$.
\end{itemize}
Dibújense las gráficas de las funciones dadas y de las combinaciones
lineales correspondientes $c_0 + a_1\sen x + b_1\cos x + \dots$. ¿Qué
ocurre?
\end{exercise}

\begin{exercise}
  Finalmente, calcúlense los coefcientes de Fourier de $f(x)=e^{\cos
    x}$ para $n=0,1,2,3,4$ y dibújense tanto $f(x)$ como la
  combinación lineal correspondiente. ¿Qué se observa?
\end{exercise}

Lo que se observa en el ejercicio anterior es que las sumas de Fourier
de orden cada vez mayor se aproximan \emph{mucho} a la función. Veamos
más ejemplos. Antes de hacerlo, enunciamos sin demostración el
siguiente resultado:

\begin{theorem}
  Si una función $f(x)$ periódica de periodo $2\pi$ es par
  (i.e. $f(-x)=f(x)$) entonces los coeficientes de Fourier
  \emph{sinoidales} son nulos. Si la función es impar
  (i.e. $f(-x)=-f(x)$), entonces los coeficientes cosenoidales son
  $0$.
\end{theorem}

\begin{exercise}\label{exer:parabola}
  Considérese la función $f(x)$ periódica de periodo $2\pi$ definida,
  entre $-\pi$ y $\pi$ como $f(x)=x^2$. Calcúlense los coeficientes de
  Fourier de orden $0,1,2,3$ y $4$ y compárense las gráficas de las
  sumas correspondientes con las de $f(x)$.
\end{exercise}

Obsérvese cómo, salvo cerca del punto en que la función no tiene
derivada, la aproximación de orden $4$ ya es extraordinaria, aunque
uno ``esperaría'' que las fluctuaciones trigonométricas se notaran más
(pero no lo hacen).

Un ejemplo de función impar:
\begin{exercise}
  Considérese la función $f(x)$ periódica de periodo $2\pi$ definida,
  entre $-\pi$ y $\pi$ como $f(x)=x(x-\pi)(x+\pi)$. Calcúlense los coeficientes de
  Fourier de orden $0,1,2,3$ y $4$ y compárense las gráficas de las
  sumas correspondientes con dos, tres y cuatro términos con las de
  $f(x)$. Compruébese si la función
  es par o impar antes de ``ponerse a echar cuentas.''
\end{exercise}
Como se observa, la aproximación es muy buena en seguida (si se
continúan añadiéndo términos, es cada vez mejor).

La propiedad que se nota en los ejemplos es general:
\begin{theorem}
  Si $f(x)$ es una función continua y periódica de periodo $2\pi$, entonces las
  sumas
  \begin{equation*}
    s_n(x) = c_{0}+\sum_{i=1}^n a_i\cos ix + b_i\sen ix
  \end{equation*}
  definen funciones que convergen \emph{muy rápidamente} a $f(x)$.
\end{theorem}

En el caso de funciones discontinuas la convergencia también se da
pero en las discontinuidades ocurre un fenómeno singular (el
\emph{fenómeno de Gibbs}) que no vamos a describir (que se percibe ya
en el ejercicio \ref{exer:parabola}, donde la convergencia es lenta en
el punto en que $f(x)$ no es derivable.

En resumen: toda función periodica continua de periodo $2\pi$ puede
``representarse'' como una suma infinita de funciones trigonométricas
elementales. Los coeficientes $a_n$ y $b_n$ de cada $\cos nx$ y $\sen nx$ se llaman
\emph{coeficientes de Fourier} de orden $i$ (o de frecuencia $i$) de
$f(x)$. Además:

\begin{theorem}[Desigualdad de Bessel]
  Si $c_0$, $a_i$ y $b_i$ son los coeficientes de Fourier de una función
  periodica de periodo $2\pi$ continua $f(x)$, entonces
  \begin{equation*}
    \frac{c_0^2}{2\pi}+\frac{1}{\pi}\sum_{n=1}^{\infty}(a_n^2+b_n^2) \leq
    \frac{1}{2\pi} \int_{-\pi}^{\pi}f(x)^2\,dx
  \end{equation*}
  que tiene como consecuencia que
  \begin{equation*}
    c_0^2 + \sum a_n^2+b_{n}^2 \leq \infty.
  \end{equation*}
\end{theorem}
En lenguaje ordinario, la desigualdad de Bessel afirma que \emph{la
  suma de las 
  energías de todas las frecuencias en una función periódica es como mucho la
energía total de la onda}. La consecuencia es que \emph{la suma de
las amplitudes de todas las frecuencia es finita} si la función es
``normal'' (por ejemplo, acotada y derivable salvo quizás en un número
finito de puntos). 


\end{document}
